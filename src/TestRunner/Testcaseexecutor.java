package TestRunner;

import java.io.IOException;
import java.util.List;
import org.testng.TestNG;
import org.testng.collections.Lists;

public class Testcaseexecutor {
	public static void main(String[] args) throws IOException {
		String Excelreport = System.getProperty("user.dir") + "\\Excelreporter.jar";
		ProcessBuilder PBExcelReport = new ProcessBuilder("java", "-jar", Excelreport);

		/*// Block to run TestDataGenerator.xml scripts
		TestNG TDGeneratorSuite = new TestNG();
		List<String> TDGeneratorList = Lists.newArrayList();
		String TDGeneratorXMLPath = System.getProperty("user.dir") + "//TestDataGenerator.xml";
		TDGeneratorList.add(TDGeneratorXMLPath);
		TDGeneratorSuite.setTestSuites(TDGeneratorList);
		TDGeneratorSuite.run();
		PBExcelReport.start(); */
		
		// Block to run TestSuite.xml scripts
		TestNG testNGSuite = new TestNG();
		List<String> TestSuiteList = Lists.newArrayList();
		String testsuiteXMLPath = System.getProperty("user.dir") + "//TestSuite.xml";
		TestSuiteList.add(testsuiteXMLPath);
		testNGSuite.setTestSuites(TestSuiteList);
		testNGSuite.run();
		PBExcelReport.start();
		
		// Block to run testng-failed.xml scripts
		TestNG FailedSuite = new TestNG();
		List<String> FailedSuitesList = Lists.newArrayList();
		String failedSuiteXMLPath = System.getProperty("user.dir") + "\\test-output\\testng-failed.xml";
		FailedSuitesList.add(failedSuiteXMLPath);
		FailedSuite.setTestSuites(FailedSuitesList);
		FailedSuite.run();
		PBExcelReport.start();
	}
}
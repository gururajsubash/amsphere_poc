package com.ladbrokes.commonclasses;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import comTR.gurock.testrail.APIClient;
import comTR.gurock.testrail.APIException;
import com.ladbrokes.commonclasses.TestRailSetup;

//import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import com.ladbrokes.Utils.ReadTestSettingConfig;

import org.openqa.selenium.chrome.ChromeOptions;

public class DriverCommon extends BaseClass {

	public static ThreadLocal<WebDriver> dr1 = new ThreadLocal<WebDriver>();
	WebDriver driver;
	Properties prop;
	public static String Mode;
	public String url = null;
	public ExtentReports extent;
	public ExtentTest logger;
	String path;
	public APIClient client;
	public int runID;
	public List<Integer> caseID;
	public ITestResult result;

	public void startReport() {
		extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/DesktopReport.html", true);
		extent.addSystemInfo("Browser Info", "Chrome").addSystemInfo("Environment", "Pre-Prod")
				.addSystemInfo("User Name", "Sneha");
		extent.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

	public static WebDriver getDriver() {
		return dr1.get();
	}

	public void launchweb() throws Exception {
		System.out.println("Inside Launch the website");
		String BrowserType = ReadTestSettingConfig.getTestsetting("BrowserType");
		System.out.println(BrowserType);
		switch (BrowserType) {
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "TestConfig/chromedriver_win32_2.36.exe");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--start-maximized");
			chromeOptions.addArguments("--disable-extensions");
			chromeOptions.addArguments("disable-infobars");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("password_manager_enabled", false);
			chromeOptions.setExperimentalOption("prefs", prefs);
			dr1.set(new ChromeDriver(chromeOptions));
			getDriver().manage().window().maximize();
			break;
		case "InternetExplorer":
			System.out.println("Inside InternetExplorer");
			System.setProperty("webdriver.ie.driver", "TestConfig/IEDriverServer_x64_3.8.0.exe");
			dr1.set(new InternetExplorerDriver());
			getDriver().manage().window().maximize();
			break;
		case "Firefox":
			System.out.println("Inside Firefox");
			System.setProperty("webdriver.gecko.driver", "TestConfig/geckodriver-v0.19.1-win64.exe");
			dr1.set(new FirefoxDriver());
			break;
		case "Edge":
			System.out.println("Inside Edge");
			System.setProperty("webdriver.edge.driver", "TestConfig/MicrosoftWebDriver_Release-16299.exe");
			dr1.set(new EdgeDriver());
			getDriver().manage().window().maximize();
		}
		url = ReadTestSettingConfig.getTestsetting("BaseURL1_xpath");
		System.out.println(url);
		getDriver().get(url);
		getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.ladbrokesLogo), "Failed to launch Web");
		if (isElementPresent(HomeGlobalElements.settings)) {

			click(HomeGlobalElements.settingsIcon);
			Actions builder = new Actions(getDriver());
			builder.moveToElement(getDriver().findElement(By.xpath("//h1[@class='logo']")));
			getDriver().findElement(By.xpath("//h1[@class='logo']")).click();

		}
	}

	public static void switchFrame(By key) {
		try {
			getDriver().switchTo().frame(getDriver().findElement(key));
		} catch (Exception ex) {
			Assert.fail("Unable to switch to the frame" + ex.getMessage());
		}
	}
	
	public static void switchDefaultContent() {
		getDriver().switchTo().defaultContent();
	}
	
	
	public static void click(By Key) throws Exception, NoSuchElementException {
		try {
			System.out.println(Key);
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getDriver().findElement(Key).click();
			Thread.sleep(2000);
		} catch (NoSuchElementException ex) {
			throw new Exception("Unable to find/click the element" + Key, ex);
		}
	}
	
	
	public static void click(WebElement element) throws Exception, NoSuchElementException {
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			element.click();
			Thread.sleep(2000);
		}
		catch (AssertionError ex) {
			throw new Exception("Unable to find/click the element", ex);
		}
	}

	public static void click(By Key, String message) throws Exception, NoSuchElementException {
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getDriver().findElement(Key).click();
		} catch (Exception e) {
			Assert.fail(message, e);
		}
	}

	public static void Entervalue(By locatorKey, String value) throws Exception {
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getDriver().findElement(locatorKey).sendKeys(value);
			Thread.sleep(2000);
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static boolean isElementDisplayed(By locatorKey) {
		boolean elementPresent = false;
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			elementPresent = getDriver().findElement(locatorKey).isDisplayed();
		} catch (Exception e) {
			Assert.fail(e.getMessage(), e);
		}
		return elementPresent;
	}

	public static boolean isElementPresent(By locatorKey) {
		boolean isPresent = false;
		try {
			isPresent = getDriver().findElements(locatorKey).size() > 0;
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return isPresent;
	}

	public static boolean isTextPresent(String value) {
		boolean isPresent = false;
		try {
			isPresent = getDriver().getPageSource().contains(value);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return isPresent;
	}

	public String getAttribute(By locatorKey, String value) {
		String getAttribute = null;
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getAttribute = getDriver().findElement(locatorKey).getAttribute(value);
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return getAttribute;
	}

	public static boolean isChecked(By locatorKey) {
		boolean isChecked = false;
		try {
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			isChecked = getDriver().findElement(locatorKey).isSelected();
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return isChecked;
	}

	public void selectValueInDropdown(By locator, String value) {
		Select select = new Select(getDriver().findElement(locator));
		select.selectByValue(value);
	}

	public static void selectVisibleTextInDropdown(By locator, String visibleText) {
		try {
			Select select = new Select(getDriver().findElement(locator));
			select.selectByVisibleText(visibleText);
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void switchwindow() {
		try {
			Set<String> set = getDriver().getWindowHandles();
			Iterator<String> it = set.iterator();
			String parentWindow = it.next();
			String childWindow = it.next();
			System.out.println(set);
			getDriver().switchTo().window(childWindow);
			Thread.sleep(2000);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void SwitchToPage(String pageTitle) {
		try {
			Set<String> set = getDriver().getWindowHandles();
			Iterator<String> it = set.iterator();
			String parentWindow = it.next();
			String childWindow = it.next();
			System.out.println(set);
			getDriver().switchTo().window(childWindow);
			Thread.sleep(2000);
			String formwindowTitle = getDriver().getTitle().toLowerCase();
			if (formwindowTitle.contains(pageTitle.toLowerCase())) {
				getDriver().close();
				getDriver().switchTo().window(parentWindow);
			} else {
				getDriver().close();
				getDriver().switchTo().window(parentWindow);
				Assert.fail("Failed to switch to " + pageTitle + " page");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public boolean isElementEnabled(By locatorKey) {
		boolean elementDisplayed = false;
		try {
			System.out.println("Inside Entervalue");
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			elementDisplayed = getDriver().findElement(locatorKey).isEnabled();
		} catch (NoSuchElementException e) {
			e.getMessage();
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return elementDisplayed;
	}

	public String GetElementText(By locatorKey) {
		String getelementText = null;
		try {
			System.out.println("Inside Entervalue");
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			getelementText = getDriver().findElement(locatorKey).getText();
		} catch (NoSuchElementException e) {
			e.getMessage();
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return getelementText;
	}

	public void scrollToView(WebElement locatorKey) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			System.out.println("Inside Scroll");
			js.executeScript("arguments[0].scrollIntoView(true);", locatorKey);
			js.executeScript("window.scrollBy(0,-250)", "");
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void scrollToView(By locatorKey) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			System.out.println("Inside Scroll");
			js.executeScript("arguments[0].scrollIntoView(true);", getDriver().findElement(locatorKey));
			js.executeScript("window.scrollBy(0,-250)", "");
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Thread.sleep(1000);
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static void scrollAndClick(By by) throws Exception {
		WebElement element = getDriver().findElement(by);
		int elementPosition = element.getLocation().getY();
		String js = String.format("window.scroll(0, %s)", elementPosition);
		((JavascriptExecutor) getDriver()).executeScript(js);
		element.click();
	}

	public static void scrollAndClick(WebElement by) throws Exception {
		WebElement element = by;
		int elementPosition = element.getLocation().getY();
		String js = String.format("window.scroll(0, %s)", elementPosition);
		((JavascriptExecutor) getDriver()).executeScript(js);
		element.click();
	}

	public void scrollPage(String direction) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			direction = direction.toLowerCase();
			getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			switch (direction) {
			case "top":
				js.executeScript("window.scrollTo(0, 0);");
				break;
			case "bottom":
				getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
				// js.executeScript( "window.scrollBy(0,250)", "");
				break;
			}
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static String toTitleCase(String input) {
		StringBuilder titleCase = new StringBuilder();
		boolean nextTitleCase = true;
		for (char c : input.toCharArray()) {
			if (Character.isSpaceChar(c)) {
				nextTitleCase = true;
			} else if (nextTitleCase) {
				c = Character.toTitleCase(c);
				nextTitleCase = false;
			}
			titleCase.append(c);
		}
		return titleCase.toString();
	}

	public static String removeChar(String fromString, Character character) {
		int indexOf = fromString.indexOf(character);
		if (indexOf == -1)
			return fromString;
		String front = fromString.substring(0, indexOf);
		String back = fromString.substring(indexOf + 1, fromString.length());
		return front + back;
	}

	public static String removeCharAt(String s, int i) {
		StringBuffer buf = new StringBuffer(s.length() - 1);
		buf.append(s.substring(0, i)).append(s.substring(i + 1));
		return buf.toString();
	}

	public void back() {
		getDriver().navigate().back();
	}

	public void tabs(int tapnumber, By element) {
		getDriver().findElement(By.xpath("//android.view.View[@content-desc='Promos Link']")).sendKeys(Keys.TAB);
	}

	public void endReport() {
		extent.flush();
		extent.close();
	}

	public static String getScreenshot(String screenshotname) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) getDriver();
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = System.getProperty("user.dir") + "\\Screenshots\\" + screenshotname + ".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}

	public static String capitalize(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return str;
		}
		return new StringBuffer(strLen).append(Character.toTitleCase(str.charAt(0))).append(str.substring(1))
				.toString();
	}

	public static void clickByJS(WebElement ele) {
		try {
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", ele);
			
		} catch (Exception ex) {
			Assert.fail("Unable to find/click the element" + ele, ex);
		}
	}

	@BeforeClass(alwaysRun = true)
	public void TestRail() throws Exception {
		client = TestRailSetup.myClient();
		runID = TestRailSetup.TestrailID();
	}
	
	@AfterMethod(alwaysRun = true)
	public void toTestrail(ITestResult result)
			throws MalformedURLException, IOException, APIException, InterruptedException {

		try {
			if (runID != 0)
				TestRailSetup.sendResultsToTestrail(client, result, runID, caseID);
		} catch (Exception e) {
			System.out.println(e);
		}
		if (driver != null)
			System.out.println("--------> Inside AfterMethod to close driver");
		getDriver().quit();
		Thread.sleep(4000);
	}
	
} // End of 'DriverCommon' class

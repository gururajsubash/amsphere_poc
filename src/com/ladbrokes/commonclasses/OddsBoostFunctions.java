package com.ladbrokes.commonclasses;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ladbrokes.EnglishOR.OBTIElements;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;

public class OddsBoostFunctions extends DriverCommon {
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	TestdataFunction testdata = new TestdataFunction();
	PotentialReturnFunctions PRT = new PotentialReturnFunctions();
	MyBetsFunctions myBets = new MyBetsFunctions();
	Common common = new Common();
	
	public String ValidateBetReceiptOddsBoost(String typeName, String eventName, String marketName, String selectionName, String[] oddsArr, String stake, String eachWay, Boolean SP, int betCount, String multiBetType , int betSlipCount) throws Exception
	{
	    int numLines , multiLines;
	    String xPath, odds, textToVerify, currDate, actualString = "", PRstake = stake, TempPRstake = "", NewtextToVerify = null, newXPath; 
	    double potentialReturn = 0.0, gTaxStake = 0.0, totalStake, individualStake;
	    boolean bStatusGTax = false;
	    String potReturn = null; String NewpotReturn = null; String NewpotReturn1 = null; 
	    String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
	    try
	    {
	    	//Condition to handle German Tax
	        if(isElementPresent(By.xpath("//div[@class='tax-disclaimer']")))
	          {
	                  bStatusGTax = true;
	                  PRstake = stake;
	                  gTaxStake = Convert.ToDouble(stake) - (Convert.ToDouble(stake) * (0.05));
	          }
	    	//Validating odds Boost on BetReceipt
	        Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostReceiptIcon), "Odd Boost icon is not displayed on Bet Receipt");
	    	Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostReceipt), "Odd Boost confirmation message is not displayed on Bet Receipt");
	        // Getting the Initial Balance and actual Number of lines
	         totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
	         numLines =   CalculateBetLines(multiBetType , betSlipCount);
	        multiLines = numLines;
	        Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip), "Tick mark on Bet Receipt was not found");
	        Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg), "Bet successfull message is not displayed in Bet Receipt");
	        WebElement element = getDriver().findElement(BetslipElements.betReceiptContainer);
	        
	        actualString = element.getText().toLowerCase();

	       //Checking for BetTime                     
	        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	        Date dateobj = new Date();
	        System.out.println(df.format(dateobj));
	        currDate = df.format(dateobj) ;
	        
	       textToVerify = TextControls.timeText.toLowerCase() + " " + currDate + " ";
	       getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
	      Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        
	        //Checking for Bet Receipts No 
	        textToVerify = TextControls.receiptNoText.toLowerCase() + " o/";
	       
	       GetElementText(BetslipElements.betReceiptNo);
	        
	        //Checking for Bet lines
	        if (!TextUtils.isEmpty(eachWay))
	            numLines = numLines * 2;
	        //*** Verifying  BetLines in Bet Receipt
	        if (numLines > 1)
	        { 
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                TempPRstake = PRstake.replace('.', ',');
	                textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " " + TempPRstake + " " + TextControls.currencySymbol + " " + TextControls.preLineText + "";

	            }
	            else
	            textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " " +TextControls.currencySymbol + PRstake + " " + TextControls.preLineText.toLowerCase() + "";
	        }
	        else
	        {
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                TempPRstake = PRstake.replace('.', ',');
	                textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " " + TempPRstake + " " + TextControls.currencySymbol + "";

	            }

	            else
	                textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " " + TextControls.currencySymbol +  Double.parseDouble(PRstake) + "";
	        }
	        Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        System.out.println("Correct Betlines are displayed in Bet receipt.");
	        //Checking for Stake for Each way  
	       
	        if (!TextUtils.isEmpty(eachWay))
	        {
	            // ** "Verifying  Stake for Each way in Bet Receipt"
	            individualStake = Double.parseDouble(stake) * 2;
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
	                textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " " + TextControls.currencySymbol + "";

	            }
	            else
	                textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TextControls.currencySymbol + individualStake;
	        }
	        else
	        {
	            individualStake = Double.parseDouble(stake) * numLines;
	            individualStake = Math.round(individualStake * 100.0) / 100.0;
	            
	           if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	                {

	                    String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
	                    textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " " + TextControls.currencySymbol + "";

	                }
	                else
	                    textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " +TextControls.currencySymbol+  individualStake ;
	            }
	          

	        Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        System.out.println("Correct total stake for multilines are displayed in Bet receipt.");
	        // ** Getting Potential Return only if Each way is false
	        System.out.println("Verifying  Potential Return for Each way in Bet Receipt, Potential Return for Each way should display in Bet receipt");
	        if (SP == true)
	            textToVerify = TextControls.PRetText.toLowerCase() + ": n/a";
	        else
	        {
	           
	            if (!TextUtils.isEmpty(eachWay))
	            {
	                if (oddsArr.length > 1)
	                {
	                    eachWay = getDriver().findElement(By.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[1]")).getText() + "&" + getDriver().findElement(By.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[2]")).getText();

	                }
	                else
	                {
	                    eachWay = getDriver().findElement(By.xpath("//div[@data-bind='text: eachWayTermsInfo']")).getText();
	                }
	                if(bStatusGTax == false)
	                potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, Double.parseDouble(PRstake), multiBetType);
	                else if (bStatusGTax == true && !TextUtils.isEmpty(eachWay))
	                potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, gTaxStake, multiBetType);
	            }
	                else
	                potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr, Double.parseDouble(PRstake), multiBetType);
	            
	          //5% is deducted in case if German customer
	            if (bStatusGTax == true && TextUtils.isEmpty(eachWay))
	            {
	                potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr, gTaxStake, multiBetType);
	               //Assert.assertFalse((TextControls.germanTaxMsgText), "German tax inline message is displayed for German User on Bet receipt");
	            }
	                
	            //============================================================
	            if (String.valueOf(potentialReturn).contains("0.99"))
	                potReturn = String.valueOf(Math.round(potentialReturn));
	                
	            else
	            {
	            double roundOff = Math.round(potentialReturn * 100.00) / 100.00;
	            potentialReturn = roundOff ;
	            potReturn = String.valueOf(potentialReturn);
	            // Below NewpotReturn1 is added to handle Point decimal difference in Potential Return (now it is verfying @ interger level)
	            NewpotReturn = potReturn.replace(".", "XZ"); 
	            NewpotReturn1 = NewpotReturn.substring(0,NewpotReturn.indexOf("XZ"));
	            }
	            
	            //============================================================
	          
	             
	            
	           if (ProjectName.contains("Desktop_Swedish"))//Added conditon for swedish 
	            {
	                textToVerify = TextControls.PRetText.toLowerCase() + ": " +potReturn.replace('.', ',') + " " + TextControls.currencySymbol;
	                NewtextToVerify = TextControls.PRetText.toLowerCase() + ": "+ NewpotReturn1;
	            }
	            
	            else
	            {
	                textToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol + potReturn;
	                NewtextToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol + NewpotReturn1;
	            }
	        }
	        if (actualString.contains(textToVerify))
	        {
	           Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        else
	        {
	        	Assert.assertTrue(actualString.contains(NewtextToVerify), "'" + NewtextToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        System.out.println("Correct total potential returns for multilines are displayed in Bet receipt.");
	        if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single"))
	        {
	            //Checking for Event Details
	            // For HR and GH, TypeName is displayed in the betSlip
	           
	            if (!TextUtils.isEmpty(typeName))
	            {
	                if (typeName.contains("'"))
	                {
	                    String[] typeArr = typeName.replace("'", "|").split("|");
	                    textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " " + typeArr[0].toLowerCase();
	                   Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	                    textToVerify = typeArr[1].toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	                    Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	                    //goto a ;
	                }
	                else
	                    textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " " + typeName.toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	            }
	            else
	            {
	                textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	                //Market doesn't appear for WDW market
	                if (TextUtils.isEmpty(marketName))
	                    textToVerify = TextControls.single.toLowerCase() + " - " + eventName.toLowerCase() + "\r\n" + selectionName.toLowerCase();
	            }
	         // Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	           
	            // Checking for Each way
	        if (!TextUtils.isEmpty(eachWay))
	        {
	            textToVerify = TextControls.ewoddsText.toLowerCase() + " " + PRT.GetEachWayOdd(eachWay) + " " + TextControls.placeText.toLowerCase() + " " +  BTbetslipObj.GetEachWayPlaces(eachWay);
	          
	           Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        System.out.println("Verify selection details in  Bet Receipt , Selection details must be displayed in Bet Receipt");
	        // Checking for SP
	         if (SP == true)
	            {
	                //textToVerify = TextControls.oddsText.ToLower() + ":\r\nsp";
	                textToVerify = TextControls.oddsText.toLowerCase() + ": sp";
	                Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	            else
	            //Checking for Odds 
	            {
	                odds =  String.valueOf((Double.parseDouble(oddsArr[0])));
	                textToVerify = TextControls.oddsText.toLowerCase() + ": " + odds;
	               
	              Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	        }
	        else
	        {
	            String[] selArr = selectionName.split("-");
	            String[] eventArr = eventName.split("-");
	           // String[] typeArr = eventName.split("-");
	            String multibetABR = null;
	            multibetABR = common.GetMultiplesBetAbbreviationOnReceipit(multiBetType);
	            //===================================================
	            //Checking for BetType
	            textToVerify = multibetABR.toLowerCase() + " (x" + multiLines + ")";
	            Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            //=========================================================

	            
	            //Verify the selections
	            for (int i = 0; i < selArr.length; i++)
	            {
	                textToVerify = (i + 1) + " . " + selArr[i].toLowerCase() + " (" + eventArr[i].toLowerCase() + "";
	                if (actualString.contains(" +1.0 "))
	                    actualString = actualString.replace(" +1.0 ", " ");
	                Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	        }
	        System.out.println("Selection Details in Bet receipt.");
	        // Looking for Total stake
	        if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	        {

	            xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + Convert.ToString(totalStake).replace('.',',') + " " + TextControls.currencySymbol + "')]";
	           
	        }
	        else
	        xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + TextControls.currencySymbol + totalStake + "')]";
	       Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total stake: " + TextControls.currencySymbol + totalStake + "' text was is not present in the Bet Receipt");

	        // Looking for Total PR 
	        if (!SP)
	        {
	            if(ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {
                    xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + potReturn.replace('.', ',') + " " + TextControls.currencySymbol + "')]";
	                newXPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + NewpotReturn1 + "')]";
	            }
	            else
	            { 
	            	xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + TextControls.currencySymbol + potReturn + "')]";
	                newXPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + TextControls.currencySymbol + NewpotReturn1 + "')]";
	            }
	            
	            if (isElementPresent(By.xpath(xPath)))
	            Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: " + TextControls.currencySymbol + potReturn + "' text was is not present in the Bet Receipt");
	            else
	            Assert.assertTrue(isElementDisplayed(By.xpath(newXPath)), "'Total potential return: " + TextControls.currencySymbol + NewpotReturn1 + "' text was is not present in the Bet Receipt");
	        }    
	        else
	        {
	            xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), 'N/A')]";
	            Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: N/A' text was is not present in the Bet Receipt");
	        }
	       
	        //Validate the Total Stake & Total Bets
	        //xPath = "//span[@class='your-bets' and contains(text(),'" + TextControls.urBetsText + "')]/following-sibling::span[@class='your-bets' and contains(text(), '(" + betCount + ")')]";
	        xPath = "//span[@class='your-bets' and contains(text(),'" + TextControls.urBetsText+ "')]";
	        Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Your bet is not present in the Bet Receipt");
            System.out.println("Betplacement was successful and Bet receipt details was validated");
            
            	
	    }
	    catch ( Exception | AssertionError  e){
	    
	      Assert.fail(e.getMessage() , e);
	    }
	    if(actualString.contains(Convert.ToString(potentialReturn)))
	    return Convert.ToString(potentialReturn);
	    else
	    return NewpotReturn1;
	}
	
	public void VerifyOddsBoostBetDroppedFromOpenSettledBets(String openSettledBets, String[] eventNames, String[] selectionNames, String stake, String pr, String multiBetType,String time) throws Exception
    {
		try
        {
        	myBets.NavigateToMyBets(openSettledBets);
            List<WebElement> mybetHistroyItems = getDriver().findElements(MyBetsControls.mybetHistroyItems);
            List<WebElement> mybetArrows = getDriver().findElements(MyBetsControls.myBetDownArrow);
           
            if (mybetHistroyItems.size() != mybetArrows.size())
             Assert.fail("Mybets and Down arrow  items do not match in " + openSettledBets);

            for (int i = 0; i < mybetHistroyItems.size(); i++)
            {
                if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single"))
                {
                    if (mybetHistroyItems.get(i).getText().toLowerCase().contains(time) && mybetHistroyItems.get(i).getText().toLowerCase().contains(stake) && mybetHistroyItems.get(i).getText().toLowerCase().contains(String.valueOf(pr)) &&
                        mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[0].toLowerCase()) && mybetHistroyItems.get(i).getText().toLowerCase().contains(selectionNames[0].toLowerCase()))
                     Assert.fail("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + "' present in " + openSettledBets );
                }
                else
                {
                    for (int j = 1; i < eventNames.length; i++)
                    {
                        if ( mybetHistroyItems.get(i).getText().toLowerCase().contains(time) && mybetHistroyItems.get(i).getText().toLowerCase().contains(stake) && mybetHistroyItems.get(i).getText().toLowerCase().contains(String.valueOf(pr)) &&
                            mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[j].toLowerCase()) && mybetHistroyItems.get(i).getText().toLowerCase().contains(selectionNames[j].toLowerCase()))
                        	Assert.fail("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + "' present in " + openSettledBets);
                    }
                }
            }
           System.out.println("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + "' was NOT present in " + openSettledBets);
        }
        catch ( Exception | AssertionError  e){
			Assert.fail(e.getMessage(), e);
		}
    }
	
	public void VerifyOddsBoostBetUnderOpenSettledBets(String openSettledBets, String[] eventNames, String[] marketNames, String[] selectionNames, String[] odds, String stake, String pr, String betStatus, String multiBetType,String BetTime) throws Exception
    {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		
		
        List<WebElement> mybetHistroyItems;
        String myBetItem = null;
        String OddsBoostinfo;
        try
        {
           myBets.NavigateToMyBets(openSettledBets);
           String time = BetTime.substring(0, BetTime.length()-3 );
         //  String time = BetTime.substring(0, BetTime.length()-5 );

          mybetHistroyItems = getDriver().findElements(MyBetsControls.mybetHistroyItems);
            if (mybetHistroyItems.size() != getDriver().findElements(MyBetsControls.myBetDownArrow).size())
              Assert.fail("Mybets and Down arrow  items do not match in " + openSettledBets);

            for (int i = 0; i < mybetHistroyItems.size();i++)
            {
            	
            	System.out.println(mybetHistroyItems.size());
                myBetItem = "(//div[@class='bet-detail'])[" + (i + 1) + "]";
                
                if(ProjectName.contains("Desktop_Swedish") || ProjectName.contains("Desktop_German"))
                	OddsBoostinfo = "(//div[@class='bet-detail']//div[@class='mb-header-bet-time' and text() = '"+ GetCurrentDate() +"']/../following::div[@class='odds-boost-mb-container-content-container'])[" + (i + 1) + "]" ; 
                else
                OddsBoostinfo = "(//div[@class='bet-detail']//div[@class='mb-header-bet-time' and text() = '"+time+"']/../following::div[@class='odds-boost-mb-container-content-container'])[" + (i + 1) + "]";
                
                System.out.println(OddsBoostinfo);
               
                Assert.assertTrue(isElementDisplayed(By.xpath(OddsBoostinfo)), "Odds Boost info 'This bet has been boosted!' is not displayed in OpenBets");
                
                By xpath = By.xpath(myBetItem +"//div[@class='mb-header-arrow-container']//div[@class='mb-arrow']");
              //  if (((mybetHistroyItems.get(i).getText().toLowerCase().contains(stake) && mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[0].toLowerCase()))))
                if (((mybetHistroyItems.get(i).getText().toLowerCase().contains(convertStake(stake)) && mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[0].toLowerCase()))))
                
                {
                    //Verify the up and down arrow functionality
                   
                  if(getAttribute(xpath , "expanded").contains("true"))
                     {
                        Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Bet details are not displayed when expanded");
                       click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"), "[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
                        Thread.sleep(3000);
                        Assert.assertTrue(isElementDisplayed(By.xpath(OddsBoostinfo)), "Odds Boost info 'This bet has been boosted!' is not displayed in OpenBets");
                        Assert.assertFalse(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Clicking on up arrow failed to hide its details");
                        click(By.xpath(myBetItem +"//div[@class='mb-header-arrow-container']"), "[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
                        Thread.sleep(3000);
                        Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Clicking on down arrow failed to display its details");
                    }

                    else
                    {
                       click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"), "[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
                       Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Clicking on down arrow failed to display its details");
                       click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"), "[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
                       Assert.assertFalse(driver.findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Clicking on up arrow failed to hide its details");
                       click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"), "[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
                       Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(), "Clicking on down arrow failed to display its details");
                    }
                    Thread.sleep(1000);
                   
                    //Verify bet details (Single)
                    if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single"))
                    {
                   	 Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//div[@class='mb-content-short-desc']//span[contains(@data-bind, 'html: selectionName')]")).equalsIgnoreCase(selectionNames[0]), selectionNames[0] + " - Selection was not found in " + openSettledBets);
                   	 Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//div[@class='mb-content-market']")).equalsIgnoreCase("(" + marketNames[0]+ ")"), marketNames[0] + " - Market was not found in " + openSettledBets);
                    Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//div[@class='mb-content-desc']")).equalsIgnoreCase(eventNames[0]), eventNames[0] + " - Event was not found in " + openSettledBets);
                    Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//span[@class='mb-content-odds']")).contains(odds[0]), odds[0] + " - Odds was not found in " + openSettledBets);
                  //  Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsStakeText + "']/../../span[@class='mb-info-stake-value']")).contains(stake), stake + " - Stake was not found in " + openSettledBets);
                    Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsStakeText + "']/../../span[@class='mb-info-stake-value']")).contains( convertStake(stake)),  convertStake(stake) + " - Stake was not found in " + openSettledBets);
                    
                   
                        if (openSettledBets.toLowerCase().contains("open"))
                        {
                            
                          // Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsReturnsText + "']/../../span[@class='mb-info-potential-value']")).getText().contains(Convert.ToString(pr)), pr + " - PR was not found in " + openSettledBets);
                        	//String prString = Convert.ToString(pr);
                        	Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsReturnsText + "']/../../span[@class='mb-info-potential-value']")).getText().contains(convertStake(pr)), pr + " - PR was not found in " + openSettledBets);
                        }
                        else
                        {
                          
                           Assert.assertFalse(getDriver().findElement(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsReturnsText + "']/../../span[@class='mb-info-potential-value']")).getText().equals(""), pr + " - PR was not found in " + openSettledBets);
                        }
                          
                       Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]")).getText().toLowerCase().contains(betStatus.toLowerCase()), betStatus + " - Bet Status was not found in " + openSettledBets);
                       Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[contains(@class, 'bet-type-info')]")).getText().toLowerCase().contains(multiBetType.toLowerCase()), multiBetType + " - Bet Type was not found in " + openSettledBets);
                       Assert.assertFalse(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-header-bet-time']")).getText().equals(""), "Today's date was not found in " + openSettledBets);
                        break;
                    }
                    //Verify bet details (Multiples)
                    else
                    {
                        
                        if (eventNames.length == marketNames.length && selectionNames.length == odds.length && eventNames.length == odds.length)
                        {
                            int k = 0;
                            for (int j = 1; j < eventNames.length; j++)
                            {
                               Assert.assertTrue(getDriver().findElement(By.xpath("("+myBetItem + "//div[@class='mb-content'])[" + j + "]//span[contains(@data-bind, 'selectionName')]")).getText().equalsIgnoreCase(selectionNames[k]), selectionNames[k] + " - Selection was not found in " + openSettledBets);
                               Assert.assertTrue(getDriver().findElement(By.xpath("("+myBetItem + "//div[@class='mb-content'])[" + j + "]//div[@class='mb-content-market']")).getText().equalsIgnoreCase( "(" + marketNames[k]+ ")"), marketNames[k] + " - Market was not found in " + openSettledBets);
                                
                               Assert.assertTrue(getDriver().findElement(By.xpath("("+myBetItem + "//div[@class='mb-content'])[" + j + "]//div[@class='mb-content-desc']")).getText().equalsIgnoreCase( eventNames[k]), eventNames[k] + " - Event was not found in " + openSettledBets);
                               Assert.assertTrue(getDriver().findElement(By.xpath("("+myBetItem + "//div[@class='mb-content'])[" + j + "]//span[@class='mb-content-odds']")).getText().contains(odds[k]), odds[k] + " - Odds was not found in " + openSettledBets);
                                k++;
                            }
                            
                           Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[text()='"+TextControls.mybetsStakeText+"']/../../span[@class='mb-info-stake-value']")).getText().contains(convertStake(stake)), convertStake(stake) + " - Stake was not found in " + openSettledBets);
                          
                           //String prString = Convert.ToString(pr);
                            if (openSettledBets.toLowerCase().contains("open"))	
                               Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[text()='"+TextControls.mybetsReturnsText+"']/../../span[@class='mb-info-potential-value']")).getText().contains(convertStake(pr)), pr + " - PR was not found in " + openSettledBets);
                            else
                               Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[text()='"+TextControls.mybetsReturnsText+"']/../../span[@class='mb-info-potential-value']")).getText() != "", pr + " - PR was not found in " + openSettledBets);
                            
                           Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]")).getText().toLowerCase().contains(betStatus.toLowerCase()), betStatus + " - Bet Status was not found in " + openSettledBets);
                           Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//span[contains(@class, 'bet-type-info')]")).getText().toLowerCase().contains(multiBetType.toLowerCase()), multiBetType + " - Bet Type was not found in " + openSettledBets);
                           Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-header-bet-time']")).getText() != "", "Today's date was not found in " + openSettledBets);
                        }
                        else
                          Assert.fail("Mismatch in event details count. Event(" + eventNames.length + "), Market(" + marketNames.length + "), Selection(" + selectionNames.length + "), Odds(" + odds.length + ")");
                        break;
                    }
                }
               
                else if (i == (mybetHistroyItems.size() - 1))
               	 Assert.fail("Event-Selction '" + eventNames[0] + "-" + selectionNames[0] + "' was not found in " + openSettledBets);

            }
           
           /* if (cashout)
            {
                if(isElementPresent(By.xpath(myBetItem + "//div[@class='disabled-text']"))) //To Handle new Story CashOut tool tip story
                {
               	 logger.log(LogStatus.PASS,"Cashout is not available in " + openSettledBets );
                }
                else
                {
                double val, oldBalance, newBalance;
                String cashoutVal, notificationText;

                oldBalance = loginLogoutFunctions.GetBalance();
              
                cashoutVal = GetElementText(By.xpath( myBetItem + "//div[@class='button']//div[text()='CASH OUT']/following-sibling::div[@class='cashout-holder-value']"));
               val = Convert.ToDouble(cashoutVal.replace(TextControls.currencySymbol, ""));
                //check cashout value is less than total stake
               
                Assert.assertTrue(val < Convert.ToDouble(stake), "Cashout value is greater than the total stake. CashoutVal: '" + val + "', Total Stake: '" + stake + "'");
                getDriver().findElement(By.xpath(myBetItem + "//cashout")).click();
                Thread.sleep(3000);
               
                notificationText = GetElementText(By.xpath(myBetItem + "//span[@class='cashout-flow-notifications black']"));
                Assert.assertTrue(notificationText.equalsIgnoreCase(TextControls.cashOutWrngNotificationText + cashoutVal + "?"), "Wrong notification text displayed '" + notificationText + "'");
             
                Assert.assertTrue(isElementPresent(MyBetsControls.cashoutAccept), "Cashout Accept option failed to appear");
               click(MyBetsControls.cashoutDecline, "Cashout Decline option failed to appear");
              // Assert.assertFalse(isElementPresent(MyBetsControls.cashoutAccept), "Cashout Accept option failed to close");
               //Assert.assertFalse(isElementPresent(MyBetsControls.cashoutDecline), "Cashout Decline option failed to close");
              click(By.xpath("(//div[@class='button']//div[@class='cashout-holder-value'])[1]"), "Cashout option is not present");
              click(MyBetsControls.cashoutAccept, "Cashout Accept option failed to appear");

                //verify Cashout
               Assert.assertTrue(isElementPresent(MyBetsControls.cashoutSpinner), "Cashout Spinner was not displayed");
                Thread.sleep(1000);
                Assert.assertTrue(isElementPresent(By.xpath(myBetItem + "//span[text()='"+TextControls.cashedOutText+"']/following-sibling::span[text()='" + cashoutVal + "']")), "Cashout Spinner was not displayed");
               Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]")).getText().toLowerCase().contains("cashed out"), "'Cashed Out' - Bet Status was not found in " + openSettledBets);
               Assert.assertTrue(isElementPresent(By.xpath(myBetItem + "//div[@class='top-message-icon']")), "Cashout Tick was not displayed");
              
                //According to PROG-800 in Mars , Refresh icon is added for main balance to be updated
               click(MyBetsControls.balRefresh, "Balance Refresh button is not found");
               Thread.sleep(6000);

                //Verify the balance
                newBalance = loginLogoutFunctions.GetBalance();
                oldBalance = oldBalance + val;
               
                Assert.assertTrue(oldBalance < newBalance || oldBalance == newBalance, "Mismatch in the cashedout balance. Expected: '" + oldBalance + "', Actual: '" + newBalance + "'");
               
                //logger.log(LogStatus.PASS,"Successfully verified " + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + " in " + openSettledBets);
            }
            //logger.log(LogStatus.PASS,"Successfully verified " + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + " in " + openSettledBets);
        }*/
        }
        catch (AssertionError e)
        {
       	 Assert.fail(e.getMessage() ,e);
        }
    }

	public int CalculateBetLines(String betType ,int betSlipCount ) throws Exception {
		double numerator;
		double denom;

	    //'Taking the betSlip count for knowing the number of selections added to the betslip so that we can calculate the total stake for different bet types
	   int betLines = 0;

	    switch (betType.toUpperCase())
	    {
	        case "SINGLE":
	            return betLines = 1;
	        case "DOUBLE":
	            numerator = BTbetslipObj.Factorial(betSlipCount);
	            denom = BTbetslipObj.Factorial(2) * BTbetslipObj.Factorial(betSlipCount - 2);
	            betLines = (int)(numerator / denom);
	            return betLines;
	        case "TREBLE":
	            numerator = BTbetslipObj.Factorial(betSlipCount);
	            denom = BTbetslipObj.Factorial(3) * BTbetslipObj.Factorial(betSlipCount - 3);
	            betLines = (int)(numerator / denom);
	            return betLines;
	        case "ACCUMULATOR (4)":
	            numerator = BTbetslipObj.Factorial(betSlipCount);
	            denom = BTbetslipObj.Factorial(4) * BTbetslipObj.Factorial(betSlipCount - 4);
	            betLines = (int)(numerator / denom);
	            return betLines;

	        case "TRIXIE":
	            return betLines = 4;
	        case "PATENT":
	            return betLines = 7;
	        case "YANKEE":
	            return betLines = 11;
	        case "LUCKY 15":
	            return betLines = 15;
	        case "CANADIAN":
	            return betLines = 26;
	        case "LUCKY 31":
	            return betLines = 31;
	        case "ACCUMULATOR (5)":
	            numerator =BTbetslipObj.Factorial(betSlipCount);
	            denom =BTbetslipObj.Factorial(5) *BTbetslipObj.Factorial(betSlipCount - 5);
	            betLines = (int)(numerator / denom);
	            return betLines;
	        case "ACCUMULATOR (6)":
	            numerator =BTbetslipObj.Factorial(betSlipCount);
	            denom =BTbetslipObj.Factorial(6) *BTbetslipObj.Factorial(betSlipCount - 6);
	            betLines = (int)(numerator / denom);
	            return betLines;
	        case "ACCUMULATOR (7)":
	            numerator =BTbetslipObj.Factorial(betSlipCount);
	            denom =BTbetslipObj.Factorial(7) *BTbetslipObj.Factorial(betSlipCount - 7);
	            betLines = (int)(numerator / denom);
	            return betLines;
	        case "HEINZ":
	            return betLines = 57;
	        case "SUPER HEINZ":
	            return betLines = 120;
	        case "LUCKY 63":
	            return betLines = 63;
	        case "FORECAST":
	        case "REVERSE FORECAST":
	        case "COMBINATION FORECAST":
	        case "TRICAST":
	        case "COMBINATION TRICAST":
	        default:
	            return betLines = 1;
	    }
	}
	
	
	public String ValidateBetReceipt_OA(String typeName, String eventName, String marketName, String selectionName, String[] oddsArr, String stake, String eachWay, Boolean SP, int betCount, String multiBetType , int betSlipCount) throws Exception
	{
	    int numLines , multiLines;
	    String xPath, betReceiptNo = null, odds, textToVerify, currDate, actualString = "", PRstake = stake, TempPRstake = "", NewtextToVerify = null; 
	    double potentialReturn = 0.0, gTaxStake = 0.0, individualStake;
		String totalStake;
	    boolean bStatusGTax = false;
	    String potReturn = null; String NewpotReturn = null; String NewpotReturn1 = null; 
	    String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
	    try
	    {
	    	// Getting the Initial Balance and actual Number of lines
	         totalStake = GetElementText(OBTIElements.OAtotalStake).replace(TextControls.currencySymbol, "");
	         numLines =   CalculateBetLines(multiBetType , betSlipCount);
	        multiLines = numLines;
	       
	        Assert.assertTrue(isElementDisplayed(OBTIElements.OAtickMarkOnBetslip), "Tick mark on Bet Receipt was not found");
	        Assert.assertTrue(isElementDisplayed(OBTIElements.betSuccessfulMsg), "Bet successfull message is not displayed in Bet Receipt");
	        WebElement element = getDriver().findElement(BetslipElements.betReceiptContainer);
	        
	        actualString = element.getText().toLowerCase();

	       //Checking for BetTime                     
	        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	        Date dateobj = new Date();
	        System.out.println(df.format(dateobj));
	        currDate = df.format(dateobj) ;
	        
	       textToVerify = TextControls.timeText.toLowerCase() + " " + currDate + " ";
	       getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
	       Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        
	        //Checking for Bet Receipts No 
	        textToVerify = TextControls.receiptNoText.toLowerCase() + " o/";
	       
	       //Verifier. Assert.IsTrue(actualString.Contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        betReceiptNo = GetElementText(BetslipElements.betReceiptNo);
	        
	        //Checking for Bet lines
	        if (!TextUtils.isEmpty(eachWay))
	            numLines = numLines * 2;
	        //*** Verifying  BetLines in Bet Receipt
	        if (numLines > 1)
	        { 
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                TempPRstake = PRstake.replace('.', ',');
	                textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " " + TempPRstake + " " + TextControls.currencySymbol + " " + TextControls.preLineText + "";

	            }
	            else
	            textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " " +TextControls.currencySymbol + PRstake + " " + TextControls.preLineText.toLowerCase() + "";
	        }
	        else
	        {
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                TempPRstake = PRstake.replace('.', ',');
	                textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " " + TempPRstake + " " + TextControls.currencySymbol + "";

	            }

	            else
	                textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " " + TextControls.currencySymbol +  Double.parseDouble(PRstake) + "";
	        }
	        Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        System.out.println("Correct Betlines are displayed in Bet receipt.");
	        //Checking for Stake for Each way  
	       
	        if (!TextUtils.isEmpty(eachWay))
	        {
	            // ** "Verifying  Stake for Each way in Bet Receipt"
	            individualStake = Double.parseDouble(stake) * 2;
	            if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {

	                String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
	                textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " " + TextControls.currencySymbol + "";

	            }
	            else
	                textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TextControls.currencySymbol + individualStake;
	        }
	        else
	        {
	            individualStake = Double.parseDouble(stake) * numLines;
	            individualStake = Math.round(individualStake * 100.0) / 100.0;
	            
	           if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	                {

	                    String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
	                    textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " " + TextControls.currencySymbol + "";

	                }
	                else
	                    textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " +TextControls.currencySymbol+  individualStake ;
	            }
	          

	        Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        System.out.println("Correct total stake for multilines are displayed in Bet receipt.");
	        // ** Getting Potential Return only if Each way is false
	        System.out.println("Verifying  Potential Return for Each way in Bet Receipt, Potential Return for Each way should display in Bet receipt");
	        if (SP == true)
	            textToVerify = TextControls.PRetText.toLowerCase() + ": n/a";
	        else
	        {
	           
	            if (!TextUtils.isEmpty(eachWay))
	            {
	                if (oddsArr.length > 1)
	                {
	                    eachWay = getDriver().findElement(By.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[1]")).getText() + "&" + getDriver().findElement(By.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[2]")).getText();

	                }
	                else
	                {
	                    eachWay = getDriver().findElement(By.xpath("//div[@data-bind='text: eachWayTermsInfo']")).getText();
	                }
	                if(bStatusGTax == false)
	                potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, Double.parseDouble(PRstake), multiBetType);
	                else if (bStatusGTax == true && !TextUtils.isEmpty(eachWay))
	                potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, gTaxStake, multiBetType);
	            }
	                else
	                potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr, Double.parseDouble(PRstake), multiBetType);
	            
	          //5% is deducted in case if German customer
	            if (bStatusGTax == true && TextUtils.isEmpty(eachWay))
	            {
	                potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr, gTaxStake, multiBetType);
	               //Assert.assertFalse((TextControls.germanTaxMsgText), "German tax inline message is displayed for German User on Bet receipt");
	            }
	                
	            //============================================================
	            if (String.valueOf(potentialReturn).contains("0.99"))
	                potReturn = String.valueOf(Math.round(potentialReturn));
	                
	            else
	            {
	            double roundOff = Math.round(potentialReturn * 100.0) / 100.0;
	            potentialReturn = roundOff ;
	            potReturn = String.valueOf(potentialReturn);
	            // Below NewpotReturn1 is added to handle Point decimal difference in Potential Return (now it is verfying @ interger level)
	            NewpotReturn = potReturn.replace(".", "XZ"); 
	            NewpotReturn1 = NewpotReturn.substring(0,NewpotReturn.indexOf("XZ"));
	            }
	            
	            //============================================================
	          
	             
	            
	           if (ProjectName.contains("Desktop_Swedish"))//Added conditon for swedish 
	            {
	                textToVerify = TextControls.PRetText.toLowerCase() + ": " +potReturn.replace('.', ',') + " " + TextControls.currencySymbol;
	                NewtextToVerify = TextControls.PRetText.toLowerCase() + ": "+ NewpotReturn1;
	            }
	            
	            else
	            {
	                textToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol + potReturn;
	                NewtextToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol + NewpotReturn1;
	            }
	        }
	        if (actualString.contains(textToVerify))
	        {
	           Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        else
	        {
	        	Assert.assertTrue(actualString.contains(NewtextToVerify), "'" + NewtextToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        System.out.println("Correct total potential returns for multilines are displayed in Bet receipt.");
	        if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single"))
	        {
	            //Checking for Event Details
	            // For HR and GH, TypeName is displayed in the betSlip
	           
	            if (!TextUtils.isEmpty(typeName))
	            {
	                if (typeName.contains("'"))
	                {
	                    String[] typeArr = typeName.replace("'", "|").split("|");
	                    textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " " + typeArr[0].toLowerCase();
	                   Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	                    textToVerify = typeArr[1].toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	                    Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	                    //goto a ;
	                }
	                else
	                    textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " " + typeName.toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	            }
	            else
	            {
	                textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
	                //Market doesn't appear for WDW market
	                if (TextUtils.isEmpty(marketName))
	                    textToVerify = TextControls.single.toLowerCase() + " - " + eventName.toLowerCase() + "\r\n" + selectionName.toLowerCase();
	            }
	         // Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	           
	            // Checking for Each way
	        if (!TextUtils.isEmpty(eachWay))
	        {
	            textToVerify = TextControls.ewoddsText.toLowerCase() + " " + PRT.GetEachWayOdd(eachWay) + " " + TextControls.placeText.toLowerCase() + " " +  BTbetslipObj.GetEachWayPlaces(eachWay);
	          
	           Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	        }
	        System.out.println("Verify selection details in  Bet Receipt , Selection details must be displayed in Bet Receipt");
	        // Checking for SP
	         if (SP == true)
	            {
	                //textToVerify = TextControls.oddsText.ToLower() + ":\r\nsp";
	                textToVerify = TextControls.oddsText.toLowerCase() + ": sp";
	                Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	            else
	            //Checking for Odds 
	            {
	                odds =  String.valueOf((Double.parseDouble(oddsArr[0])));
	                textToVerify = TextControls.oddsText.toLowerCase() + ": " + odds;
	               
	              Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	        }
	        else
	        {
	            String[] selArr = selectionName.split("-");
	            String[] eventArr = eventName.split("-");
	           // String[] typeArr = eventName.split("-");
	            String multibetABR = null;
	            multibetABR = common.GetMultiplesBetAbbreviationOnReceipit(multiBetType);
	            //===================================================
	            //Checking for BetType
	            textToVerify = multibetABR.toLowerCase() + " (x" + multiLines + ")";
	            Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            //=========================================================

	            
	            //Verify the selections
	            for (int i = 0; i < selArr.length; i++)
	            {
	                textToVerify = (i + 1) + " . " + selArr[i].toLowerCase() + " (" + eventArr[i].toLowerCase() + "";
	                if (actualString.contains(" +1.0 "))
	                    actualString = actualString.replace(" +1.0 ", " ");
	                Assert.assertTrue(actualString.contains(textToVerify), "'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
	            }
	        }
	        System.out.println("Selection Details in Bet receipt.");
	        // Looking for Total stake
	        if (ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	        {

	            xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + totalStake.replace('.',',') + " " + TextControls.currencySymbol + "')]";
	           
	        }
	        else
	        xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + TextControls.currencySymbol + totalStake + "')]";
	       Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total stake: " + TextControls.currencySymbol + totalStake + "' text was is not present in the Bet Receipt");

	        // Looking for Total PR 
	        if (!SP)
	        {
	            if(ProjectName.contains("Desktop_Swedish"))//Added condition for Swedish as potential return has currency located after digits whether in other langauge it is before digits
	            {


	                xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + potReturn.replace('.', ',') + " " + TextControls.currencySymbol + "')]";
	            }
	            else
	            
	                // xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + FrameGlobals.currencySymbol + BFcommonObj.ReplaceDecimals(String.Format("{0:0.00}", potReturn)) + "')]";
	                xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), '" + TextControls.currencySymbol + potReturn + "')]";
	                if (isElementDisplayed(By.xpath(xPath)))
	            Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: " + TextControls.currencySymbol + potReturn + "' text was is not present in the Bet Receipt");
	            else
	            Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: " + TextControls.currencySymbol + NewpotReturn1 + "' text was is not present in the Bet Receipt");
	        }    
	        else
	        {
	            xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText + "')]/following-sibling::div[@class='amount' and contains(text(), 'N/A')]";
	            Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: N/A' text was is not present in the Bet Receipt");
	        }
	       
	        //Validate the Total Stake & Total Bets
	        //xPath = "//span[@class='your-bets' and contains(text(),'" + TextControls.urBetsText + "')]/following-sibling::span[@class='your-bets' and contains(text(), '(" + betCount + ")')]";
	        xPath = "//span[@class='your-bets' and contains(text(),'" + TextControls.urBetsText+ "')]";
	        Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Your bet is not present in the Bet Receipt");
            System.out.println("Betplacement was successful and Bet receipt details was validated");
	    
	    }
	    catch ( Exception | AssertionError  e){
	    
	      Assert.fail(e.getMessage() , e);
	    }
	    return betReceiptNo;
	}

	public static String GetCurrentDate() {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
	    Calendar cal = Calendar.getInstance();
	    String str= dateFormat.format(cal.getTime());
	  // String date=str.toLowerCase();
	   return str;
	}
	
	
	public static String convertStake(String Stake) throws InvalidPropertiesFormatException, IOException{
		String str = Stake;
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		
		if(ProjectName.contains("Desktop_Swedish"))
				{
			 str =Stake.replace(".", ",");
			
		}
		return str;		
	}

}

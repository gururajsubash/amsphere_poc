package com.ladbrokes.commonclasses;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;

import org.apache.http.util.TextUtils;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import com.ladbrokes.Utils.ReadTestSettingConfig;

public class Common extends DriverCommon {
	public static String username = null;
	public static String password = null;

	public static void Login() throws NoSuchElementException, Exception {
		String url;
		String ProjectName;
		try {
			username = ReadTestSettingConfig.getTestsetting("UserName1");
			password = ReadTestSettingConfig.getTestsetting("Password1");
			ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");	
			
			Entervalue(LoginElements.Username, username);
			Entervalue(LoginElements.Password, password);
			click(LoginElements.Login);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp)) {
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			}
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp)) {
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			}
			Thread.sleep(2000);
			switch (ProjectName) {
			case "Desktop_English":
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				break;
			case "Desktop_Swedish":
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
				click(HomeGlobalElements.languageBtn);
				Thread.sleep(2000);
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.swedish),
						" Swedish Language is not found in languages content");
				click(HomeGlobalElements.swedish);
				url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.toLowerCase().contains("sv-se"),
						"Choosed  Swedish language is not displayed on the website");
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				Thread.sleep(2000);
				break;
			case "Desktop_Irish":
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
				click(HomeGlobalElements.languageBtn);
				Thread.sleep(2000);
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.engIrelend),
						" English (Ireland) Language is not found in languages content");
				click(HomeGlobalElements.engIrelend);
				Thread.sleep(2000);
				url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.toLowerCase().contains("en-ie"),
						"Choosed  English(Ireland) language is not displayed on the website");
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				Thread.sleep(2000);
				break;
			case "Desktop_German":
				url = getDriver().getCurrentUrl();
				if (!url.toLowerCase().contains("de-de")) {
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn),
							"Language Button is not found");
					click(HomeGlobalElements.languageBtn);
					Thread.sleep(2000);
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.german),
							" German Language is not found in languages content");
					click(HomeGlobalElements.german);
					url = getDriver().getCurrentUrl();
					Assert.assertTrue(url.toLowerCase().contains("de-de"),
							"Choosed  German language is not displayed on the website");
					Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				}
				Thread.sleep(2000);
				break;
			}
		} catch (Exception e) {
			Assert.fail("Login Function Failed", e);
		}
	}

	/**
	 * This method selects the different of odds format
	 * 
	 * @return none
	 * @throws Exception
	 */
	public static void OddSwitcher(String oddtype) throws NoSuchElementException, Exception {
		try {
			Thread.sleep(2000);
			Assert.assertTrue(isElementDisplayed(LoginElements.settingsBtn),
					"Settings Tooltip is not present on the header");
			click(LoginElements.settingsBtn);
			Thread.sleep(2000);
			Assert.assertTrue(isElementDisplayed(LoginElements.settingsContent),
					"Settings Tooltip content is not shown to user");
			Assert.assertTrue(isElementDisplayed(LoginElements.settingsStaticText),
					"Setting static text is not present in the Settings tooltip");
			Assert.assertTrue(isElementDisplayed(LoginElements.decimalOdd),
					"Decimal Odd type is not present in Settings tooltip");
			Assert.assertTrue(isElementDisplayed(LoginElements.fractionalOdd),
					"Fractional Odd type is not present in Settings tooltip");
			if (oddtype.toLowerCase().equals("decimal"))
				click(LoginElements.decimalOdd);
			else
				click(LoginElements.fractionalOdd);
			click(LoginElements.settingsBtn);
			Thread.sleep(2000);
		} catch (Exception e) {
			Assert.fail("OddSwitcher Method Failed", e);
		}
	}

	/**
	 * Navigates to specified sport from A-Z menu header
	 * 
	 * @return none
	 * @throws Exception
	 */
	public void SelectLinksFromAZ(String className) throws NoSuchElementException, Exception {
		try {
			Thread.sleep(5000);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.AZMenu), "A-Z Sports header is not present in LHN");
			click(HomeGlobalElements.AZMenu);
			Thread.sleep(5000);
			WebElement sportsXpath = getDriver().findElement(By.xpath(
					"//ul[contains(@data-bind,'allSports')]//span[@class='name' and text()='" + className + "']"));
			clickByJS(sportsXpath);
			if (className.equals(TextControls.HorseRacing))
				Assert.assertTrue(
						isElementDisplayed(
								By.xpath("//span[contains(text(),'" + TextControls.HorseRacingToday + "')]")),
						"" + className + " page is not loaded");
			else if (className.equals(TextControls.Greyhounds))
				Assert.assertTrue(
						isElementDisplayed(By.xpath("//span[contains(text(),'" + TextControls.GreyhoundsToday + "')]")),
						"" + className + " page is not loaded");
			else
				Assert.assertTrue(isElementDisplayed(By.xpath("//h1[contains(text(),'" + className + "')]")),
						"" + className + " page is not loaded");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * This method gets Bet Slip count
	 * 
	 * @return betslipCount
	 * @throws Exception
	 */
	public int GetBetSlipCount() throws NoSuchElementException, Exception {
		String betslipCount = null;
		try {
			betslipCount = GetElementText(BetslipElements.betSlipCounter);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return Integer.parseInt(betslipCount);
	}

	/**
	 * This Function navigates to the specified sports page
	 * 
	 * @return none
	 * @throws Exception
	 */
	public void NavigateToSportsPage(String className) throws NoSuchElementException, Exception {
		SelectLinksFromAZ(className);
		/*
		 * if (iselementDisplayed(Sports.upcomingTab)) {
		 * 
		 * String upcomingTab =
		 * "//span[contains(text(), 'Inplay & Upcoming')]/..";
		 * iselementDisplayed(By.xpath(upcomingTab));
		 * 
		 * if (!iselementDisplayed(Sports.upcomingTab))
		 * click(By.xpath(upcomingTab)); } else {
		 * 
		 * Assert.assertTrue(iselementDisplayed(Sports.upcomingTab),
		 * "Upcoming Tab is not present");
		 * 
		 * if (!iselementDisplayed(Sports.upcomingTab))
		 * click(Sports.upcomingTab);
		 */
	}

	/**
	 * This Function to go to type and sub type without test data
	 * 
	 * @return none
	 * @throws Exception
	 */
	public void NavigateToEventSubType(String className, Boolean TypePage, Boolean subTypePage)
			throws NoSuchElementException, Exception {
		try {
			NavigateToSportsPage(className);
			// Select the Type
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			WebElement TypeNameList = getDriver().findElement(By.xpath("//*[@id='content']//li[@class='tab']"));
			List<WebElement> TypeName = TypeNameList.findElements(By.xpath("//span[@class='title-container']"));
			// System.out.println("------------>" + TypeName.size());
			if (TypePage == true) {
				if(subTypePage ==false)
				{
					TypeName = TypeNameList.findElements(By.xpath("//span[@class='title-container']"));
					TypeName.get(0).click();
				}
				else
				{
					TypeName = TypeNameList.findElements(By.xpath("//span[@class='title-container']"));
				TypeName.get(1).click();
				}
				Thread.sleep(1000);
			}
			if (subTypePage == true) {
				// Select SubType
				TypeName.get(0).click();
				List<WebElement> SubTypeNameList = getDriver().findElements(By.xpath("//div[@class='links']//a"));
				SubTypeNameList.get(0).click();
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * This Function navigates to the specified sports page
	 * 
	 * @return none
	 * @throws Exception
	 */
	public void BetPlacement_WithAcceptNContinue(String multibetType, String stakeValue, int betCount)
			throws NoSuchElementException, Exception {
		String xPath, stakeXPath, betReceiptNo = null;
		int tapCounter;
		try {
			String multibetABR = GetMultiplesBetAbbreviation(multibetType);
			if (multibetType.toLowerCase().equals("single"))
				stakeXPath = "//*[@class='stake-box-container']//input";
			else
				stakeXPath = "//*[@class='slip-item multiple']//span[contains(text(), '" + multibetABR
						+ "')]/../..//*[@class='stake-box-container']//input";
			if (isElementPresent(BetslipElements.placeBet)) {
				// #region PlaceBet_NoPriceChange
				Assert.assertTrue(isElementDisplayed(By.xpath(stakeXPath)), "Stake field was not found");
				Entervalue(By.xpath(stakeXPath), TextControls.stakeValue);
				Thread.sleep(3000);
				click(BetslipElements.placeBet);
			} else {
				// #region PlaceBet_PriceChanged
				// Initialise the count before counting number of taps on accept
				// and place bet button
				tapCounter = 0;
				while (isElementPresent(BetslipElements.acceptPlaceBet)) {
					click(BetslipElements.acceptPlaceBet);
					// ..........Exit after 5 clicks on Accept and Place bet
					// assuming it as invalid odd.........//
					if (tapCounter > 5) {
						Assert.fail("Invalid odd value for selected event");
						break; // .........exit while loop...........//
					}
					tapCounter += 1; // ....continue of click for odds change
										// value
					if (isElementPresent(BetslipElements.placeBet)) {
						if (isElementPresent(By.xpath(stakeXPath))) {
							Assert.assertTrue(isElementDisplayed(By.xpath(stakeXPath)), "Stake field was not found");
							Entervalue(By.xpath(stakeXPath), TextControls.stakeValue);
						}
						if (!isElementPresent(BetslipElements.acceptPlaceBet)) {
							click(BetslipElements.placeBet);
							break;
							
						}
					}
				}
				// #end region
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * Gets the Multiple bet type text
	 * 
	 * @return multibetABR
	 * @throws Exception
	 */
	public String GetMultiplesBetAbbreviation(String multiBetType) throws NoSuchElementException, Exception {
		String multibetABR = null;
		try {
			switch (multiBetType.toLowerCase()) {
			case "single":
				multibetABR = TextControls.single;
				break;
			case "double":
				multibetABR = TextControls.doublesText;
				break;
			case "treble":
				multibetABR = TextControls.treblesText;
				break;
			case "trixie":
				multibetABR = "Trixie";// TextControls.trixieText;
				break;
			case "patent":
				multibetABR = "Patent";// TextControls.patentText;
				break;
			case "accumulator (4)":
				multibetABR = "Fourfold";// TextControls.accumulator4Text;
				break;
			case "yankee":
				multibetABR = "Yankee";// TextControls.yankeeText;
				break;
			case "lucky 15":
				multibetABR = "Lucky 15";// TextControls.Lucky15Text;
				break;
			case "accumulator (5)":
				multibetABR = "Fivefold";// TextControls.accumulator5Text;
				break;
			case "accumulator (6)":
				multibetABR = "Sixfold";// TextControls.accumulator6Text;
				break;
			case "canadian":
				multibetABR = "Canadian";// TextControls.canadianText;
				break;
			case "lucky 31":
				multibetABR = "Lucky 31";// TextControls.lucky31Text;
				break;
			case "heinz":
				multibetABR = "Heinz";// TextControls.heinzText;
				break;
			case "lucky 63":
				multibetABR = "Lucky 63";// TextControls.lucky63Text;
				break;
			case "accumulator (7)":
				multibetABR = "Sevenfold Accumulator";// TextControls.accumulator7Text;
				break;
			case "super heinz":
				multibetABR = "Super Heinz";// TextControls.superheinzText;
				break;
			default:
				Assert.fail("Invalid BetType Passed " + multiBetType);
				break;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return multibetABR;
	}

	/**
	 * Gets the Multiple bet type text
	 * 
	 * @return multibetABR
	 * @throws Exception
	 */
	public String GetMultiplesBetAbbreviationOnReceipit(String multiBetType) throws NoSuchElementException, Exception {
		String multibetABR = null;
		try {
			switch (multiBetType.toLowerCase()) {
			case "single":
				multibetABR = TextControls.single;
				break;
			case "double":
				multibetABR = TextControls.doublesText;
				break;
			case "treble":
				multibetABR = TextControls.treblesText;
				break;
			case "trixie":
				multibetABR = "Trixie";
				break;
			case "patent":
				multibetABR = "Patent";
				break;
			case "accumulator (4)":
				multibetABR = "Fourfold Accumulator";
				break;
			case "yankee":
				multibetABR = "Yankee";
				break;
			case "lucky 15":
				multibetABR = "Lucky 15";
				break;
			case "accumulator (5)":
				multibetABR = "Fivefold Accumulator";
				break;
			case "accumulator (6)":
				multibetABR = "Sixfold Accumulator";
				break;
			case "canadian":
				multibetABR = "Canadian";
				break;
			case "lucky 31":
				multibetABR = "Lucky 31";
				break;
			case "heinz":
				multibetABR = "Heinz";
				break;
			case "lucky 63":
				multibetABR = "Lucky 63";
				break;
			case "accumulator (7)":
				multibetABR = "Sevenfold Accumulator";
				break;
			case "super heinz":
				multibetABR = "Super Heinz";
				break;
			default:
				Assert.fail("Invalid BetType Passed " + multiBetType);
				break;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return multibetABR;
	}

	public void SportLandingPagePlaceBet(String Sport) throws NoSuchElementException, Exception {
		SelectLinksFromAZ(Sport);
		Login();
		OddSwitcher("decimal");
		click(HomeGlobalElements.SportLandingPageOdd);
		Enterstake(TextControls.stakeValue);
		placebet();
	}

	public static void placebet() throws NoSuchElementException, Exception {
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		click(BetslipElements.placeBet);
		Assert.assertTrue(isElementDisplayed(BetslipElements.betReceiptBanner),
				"Bet receipt banner is not displayed in Bet Receipt");
		Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip),
				"Tick mark on Bet Receipt was not found");
		Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg),
				"Bet successfull message is not displayed in Bet Receipt");
	}

	public static void Enterstake(String stake) throws NoSuchElementException, Exception {
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
			click(BetslipElements.oddsBoostTooltipIcon);
		Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipTitle), "BetSlip Title is not Found");
		click(BetslipElements.stakeBox);
		// Thread.sleep(2000);
		Entervalue(BetslipElements.stakeBox, stake);
		Thread.sleep(2000);
	}

	public static void logOut() throws Exception {
		click(LoginElements.userNameAfterLogin);
		Assert.assertTrue(isElementDisplayed(LoginElements.logOutBtn), "Logout button is found");
		click(LoginElements.logOutBtn);
	}

	public static void scrollAndClick(WebDriver driver, By by) throws Exception {
		// JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement element = driver.findElement(by);
		int elementPosition = element.getLocation().getY();
		String js = String.format("window.scroll(0, %s)", elementPosition);
		((JavascriptExecutor) driver).executeScript(js);
		// js.executeScript("window.scrollBy(0,-200)", "");
		Thread.sleep(3000);
		element.click();
	}

	public static String ExtractNumberFromString(String value, String a, String b) {
		String requiredString = value.substring(value.indexOf(a) + 1, value.indexOf(b));
		return requiredString;
	}

	public void VerifyEventTypesInTitleBar() throws Exception {
		scrollToView(By.xpath(
				"//li[@class='tab active']//span[@class='title-container']"));
		String typeName = GetElementText(By.xpath(
				"//li[@class='tab active']//span[@class='title-container']"));
		String title = GetElementText(By.xpath("//span[@data-bind='html: title']"));
		Assert.assertTrue(typeName.equalsIgnoreCase(title), "" + typeName + " is selected");
	}

	public boolean StringIsSortedAsc(String[] arr) {
		for (int i = 1; i < arr.length; i++) {
			if (arr[i - 1].compareTo(arr[i]) > 0) // If previous is bigger,
													// return false
			{
				return false;
			}
		}
		return true;
	}

	public boolean StringIsSortedDsc(String[] arr) {
		for (int i = arr.length - 2; i >= 0; i--) {
			if (arr[i].compareTo(arr[i + 1]) < 0) // If previous is smaller,
													// return false
			{
				return false;
			}
		}
		return true;
	}

	public final boolean containsDigit(String s) {
		boolean containsDigit = false;
		if (s != null && !s.isEmpty()) {
			for (char c : s.toCharArray()) {
				if (containsDigit = Character.isDigit(c)) {
					break;
				}
			}
		}
		return containsDigit;
	}

	public boolean onlyContainsNumbers(String text) {
		try {
			Long.parseLong(text);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	public boolean isAlpha(String name) {
		char[] chars = name.toCharArray();
		for (char c : chars) {
			if (!Character.isLetter(c)) {
				return false;
			}
		}
		return true;
	}

	public static boolean AscendingOrder(List<Double> list) {
		if (list == null || list.isEmpty())
			return false;
		if (list.size() == 1)
			return true;
		for (int i = 1; i < list.size(); i++) {
			if (list.get(i).compareTo(list.get(i - 1)) < 0)
				return false;
		}
		return true;
	}

	public static boolean descendingOrder(List<Double> list) {
		boolean sorted = true;
		for (int i = 1; i < list.size(); i++) {
			if (list.get(i - 1) >= (list.get(i))) {
				sorted = true;
			} else {
				return false;
			} // if else ends
		} // for "i" ends
		return sorted;
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public void fractToDec(By Locator) {
		try {
			String fractionalValue, decimalValue, betslipText, actualFractionalValue;
			double highlightnum1, highlightnum2, decimalResult = 0.00;
			fractionalValue = GetElementText(Locator);
			String[] eventNumber = fractionalValue.split("/");
			highlightnum1 = Double.parseDouble(eventNumber[0]);
			highlightnum2 = Double.parseDouble(eventNumber[1]);
			decimalResult = (highlightnum1 / highlightnum2) + 1;
			decimalResult = Math.floor(decimalResult * 100.00) / 100.00;
			// change fractional to decimal
			scrollPage("Top");
			OddSwitcher("Decimal");
			decimalValue = GetElementText(Locator);
			double value = Convert.ToDouble(decimalValue);
			// compare
			Assert.assertTrue(decimalResult <= value, "Fraction value is not changed to Decimal value");
			// Add to BetSlip
			click(Locator);
			// Verify decimal odd value displayed in betslip
			String oddxPath = "//div[@class='betslip-text odds-convert' and text()='" + decimalValue + "']";
			Thread.sleep(5000);
			if (!isElementPresent(By.xpath(oddxPath)))
				oddxPath = "//div[@class='odds-container']//select/option[text()='" + decimalValue + "']";
			betslipText = GetElementText(By.xpath(oddxPath));
			double betslipvalue = Convert.ToDouble(betslipText);
			Assert.assertTrue(betslipvalue >=value, "Same decimal value is not reflected in Betslip");
			click(BetslipElements.removeAllBtn, "Remove All button not found in betslip");
			scrollPage("Top");
			OddSwitcher("fractional");
			Thread.sleep(2000);
			actualFractionalValue = GetElementText(Locator);
			Assert.assertTrue(actualFractionalValue.equals(fractionalValue), "Odds are changing back to fractional ");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void SwitchWindow_ValidateNewWindow(String locatorTab, String titleXpath, String expUrl) {
		try {
			click(By.xpath(locatorTab));
			Thread.sleep(2000);
			switchwindow();
			if (!TextUtils.isEmpty(titleXpath)) {
				Assert.assertTrue(isElementPresent(By.xpath(titleXpath)), "Failed to switch to correct window");
			} else if (!TextUtils.isEmpty(expUrl)) {
				String url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.equalsIgnoreCase(expUrl), "Failed to switch to correct window");
			}
			getDriver().close();
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public boolean descendingInteger(List<Integer> data) {
		for (int i = 0; i < data.size() - 1; i++) {
			if (data.get(i) < data.get(i) + 1) {
				return true;
			}
		}
		return false;
	}

	public boolean AscendingOrderInteger(List<Integer> data) {
		for (int i = 0; i < data.size() - 1; i++) {
			if (data.get(i) > data.get(i) + 1) {
				return false;
			}
		}
		return true;
	}

	public void Removeall_Function() throws InvalidPropertiesFormatException, IOException {
		String betslipCount = null;
		String BrowserType = ReadTestSettingConfig.getTestsetting("BrowserType");
		try {
			if (BrowserType == "InternetExplorer") {
				betslipCount = GetElementText(BetslipElements.betSlipCounter);
				if (Convert.ToDouble(betslipCount) != 0) {
					click(BetslipElements.removeAllBtn);
				} else
					System.out.println("\r\n betslip is clear");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public void NavigateToTypes(String className) throws NoSuchElementException, Exception {
		String actBreadcrumbs, pageTitle;
		NavigateToSportsPage(className);
		List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
		for (int i = 0; i < TypeNameList.size(); i++) {
			TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			if (TypeNameList.get(i).getText().equalsIgnoreCase(TextControls.moreText)) {
				break;
			} else {
				if (TypeNameList.get(i).getText().toLowerCase().contains("grand slam specials")) {
					i++;
				}
				String typename = TypeNameList.get(i).getText();
				TypeNameList.get(i).click();
				TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
				pageTitle = getDriver().findElement(By.xpath("//h1[@data-bind='text: headerViewModel.title()']"))
						.getText();
				Assert.assertTrue(typename.toLowerCase().contains(pageTitle.toLowerCase()), "Page title is different");
				actBreadcrumbs = (getDriver().findElement(HomeGlobalElements.breadcrumbs)).getText();
				Assert.assertTrue(actBreadcrumbs.toLowerCase().contains(pageTitle.toLowerCase()),
						"Navigation to '" + pageTitle + "' type page of class '" + className + "' was unsuccessful");
				if (isElementPresent(By.xpath("//div[@id='content']/div/div/div[1]/div/div[1]/header/div")))
					back();
				if (className.equalsIgnoreCase(TextControls.Football)) {
					if (pageTitle.equalsIgnoreCase(TextControls.englishText)) {
						if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
							click(HomeGlobalElements.inPlayTab);
						String inplayList = "//div[@data-bind='with: featuredInPlay']//event-group-simple/div/event-list";
						if (!isElementPresent(By.xpath(inplayList)))
							System.out.println("Inplay events are not present for type " + pageTitle + "");
						else {
							String eventNameXpath = "(//div[@data-bind='with: featuredInPlay']//event-group-simple/div/event-list[1]//*[@class='teams']//div[@class='name'])[1]";
							String eventName = getDriver().findElement(By.xpath(eventNameXpath)).getText();
							click(By.xpath(
									"//div[@data-bind='with: featuredInPlay']//event-group-simple/div/event-list[1]"));
							Assert.assertTrue((getDriver().findElement(HomeGlobalElements.breadcrumbs).isDisplayed()),
									"Hierarchy (breadcrumbs) was not found");
							actBreadcrumbs = (getDriver().findElement(HomeGlobalElements.breadcrumbs)).getText();
							Assert.assertTrue(actBreadcrumbs.contains(eventName),
									"Navigated to event details page from inplay module in English type page");
							NavigateToSportsPage(className);
						}
					}
				}
			}
		}
		System.out.println(
				"Navigation through the different 'types' from a sports class " + className + " is successful");
	}

	public void TickCheckBox(String locator, String value) {
		try {
			WebElement checkBox1 = getDriver().findElement(By.xpath(locator));
			if (value.toLowerCase().trim().equals("check")) {
				if (!checkBox1.isSelected()) {
					checkBox1.click();
				} else
					Assert.fail("Checkbox already checked");
			} else {
				if (checkBox1.isSelected()) {
					checkBox1.click();
				} else
					Assert.fail("Checkbox already unchecked");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static <T extends Comparable<? super T>> boolean isSorted(Iterable<T> iterable) {
		Iterator<T> iter = iterable.iterator();
		if (!iter.hasNext()) {
			return true;
		}
		T t = iter.next();
		while (iter.hasNext()) {
			T t2 = iter.next();
			if (t.compareTo(t2) > 0) {
				return false;
			}
			t = t2;
		}
		return true;
	}
}

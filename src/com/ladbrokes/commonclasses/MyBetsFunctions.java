package com.ladbrokes.commonclasses;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ladbrokes.Utils.ReadTestSettingConfig;

public class MyBetsFunctions extends DriverCommon {
	Common common = new Common();
	LoginLogoutFunctions loginLogoutFunctions = new LoginLogoutFunctions();

	public void NavigateToMyBets(String tabName) throws Exception {
		try {
			System.out.println("Navigate to My Bets  -  " + tabName + " , User should be able to navigate to My Bets - "
					+ tabName);
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipTitle), "Betslip is not displayed");
			click(MyBetsControls.myBets, "My Bets was not found in Betslip");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(4000);
			switch (tabName.toLowerCase()) {
			case "my accas":
				click(MyBetsControls.myAccas, "My Accas tab was not found in MyBets");
				Thread.sleep(4000);
				break;
			case "settled bets":
				click(MyBetsControls.settledBets, "Settled Bets tab was not found in MyBets");
				Thread.sleep(4000);
				break;
			case "open bets":
				click(MyBetsControls.openBets, "Open Bets tab was not found in MyBets");
				Thread.sleep(4000);
				break;
			default:
				Assert.fail(tabName + " - Invalid tab in My Bets passed");
				break;
			}
			System.out.println("User was able to navigate to My Bets - " + tabName);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyBetDroppedFromOpenSettledBets(String openSettledBets, String[] eventNames,
			String[] selectionNames, String stake, double pr, String multiBetType, String time) throws Exception {
		String myBetItem = null;
		try {
			NavigateToMyBets(openSettledBets);
			System.out.println(
					"Verify Bet ' + multiBetType + : + eventNames[0] + - + selectionNames[0] + ' is NOT present in  + openSettledBets, User should NOT be able to view Bet '"
							+ multiBetType + ":" + eventNames[0] + "-" + selectionNames[0] + "' in " + openSettledBets);
			List<WebElement> mybetHistroyItems = getDriver().findElements(MyBetsControls.mybetHistroyItems);
			List<WebElement> mybetArrows = getDriver().findElements(MyBetsControls.myBetDownArrow);
			if (mybetHistroyItems.size() != mybetArrows.size())
				Assert.fail("Mybets and Down arrow  items do not match in " + openSettledBets);
			for (int i = 0; i < mybetHistroyItems.size(); i++) {
				myBetItem = "(" + MyBetsControls.mybetHistroyItems + ")[" + (i + 1) + "]";
				if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single")) {
					if (mybetHistroyItems.get(i).getText().toLowerCase().contains(time)
							&& mybetHistroyItems.get(i).getText().toLowerCase().contains(stake)
							&& mybetHistroyItems.get(i).getText().toLowerCase().contains(String.valueOf(pr))
							&& mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[0].toLowerCase())
							&& mybetHistroyItems.get(i).getText().toLowerCase()
									.contains(selectionNames[0].toLowerCase()))
						Assert.fail("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0]
								+ "' present in " + openSettledBets);
				} else {
					for (int j = 1; i < eventNames.length; i++) {
						if (mybetHistroyItems.get(i).getText().toLowerCase().contains(time)
								&& mybetHistroyItems.get(i).getText().toLowerCase().contains(stake)
								&& mybetHistroyItems.get(i).getText().toLowerCase().contains(String.valueOf(pr))
								&& mybetHistroyItems.get(i).getText().toLowerCase()
										.contains(eventNames[j].toLowerCase())
								&& mybetHistroyItems.get(i).getText().toLowerCase()
										.contains(selectionNames[j].toLowerCase()))
							Assert.fail("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0]
									+ "' present in " + openSettledBets);
					}
				}
			}
			System.out.println("Bet '" + multiBetType + ":" + eventNames[0] + "-" + selectionNames[0]
					+ "' was NOT present in " + openSettledBets);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyBetUnderOpenSettledBets(String openSettledBets, String[] eventNames, String[] marketNames,
			String[] selectionNames, String[] odds, String stake, double pr, String betStatus, String multiBetType,
			boolean cashout) throws Exception {
		List<WebElement> mybetHistroyItems;
		String myBetItem = null, stringToVerify;
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String SwedishPR = null;
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			NavigateToMyBets(openSettledBets);
			if (ProjectName.contains("Desktop_Swedish")) {
				stake = stake.replace(".", ",");
				SwedishPR = Convert.ToString(pr).replace(".", ",");
			}
			if (ProjectName.contains("Desktop_German")) {
				double gTaxStake = Convert.ToDouble(stake) - (Convert.ToDouble(stake) * (0.05));
				double gPR = gTaxStake * Convert.ToDouble(odds[0]);
				if (String.valueOf(gPR).contains("0.99")) {
					pr = Math.round(gPR);
				} else {
					double roundOff = Math.round(gPR * 100.0) / 100.0;
					pr = roundOff;
				}
			}
			mybetHistroyItems = getDriver().findElements(MyBetsControls.mybetHistroyItems);
			if (mybetHistroyItems.size() != getDriver().findElements(MyBetsControls.myBetDownArrow).size())
				Assert.fail("Mybets and Down arrow  items do not match in " + openSettledBets);
			for (int i = 0; i < mybetHistroyItems.size(); i++) {
				myBetItem = "(//div[@class='bet-detail'])[" + (i + 1) + "]";
				By xpath = By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']//div[@class='mb-arrow']");
				if (((mybetHistroyItems.get(i).getText().toLowerCase().contains(stake)
						&& mybetHistroyItems.get(i).getText().toLowerCase().contains(eventNames[0].toLowerCase())))) {
					// Verify the up and down arrow functionality
					if (getAttribute(xpath, "expanded").contains("true")) {
						Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']"))
								.isDisplayed(), "Bet details are not displayed when expanded");
						click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"),
								"[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
						Thread.sleep(3000);
						Assert.assertFalse(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']"))
								.isDisplayed(), "Clicking on up arrow failed to hide its details");
						click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"),
								"[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
						Thread.sleep(3000);
						Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']"))
								.isDisplayed(), "Clicking on down arrow failed to display its details");
					} else {
						click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"),
								"[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
						Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']"))
								.isDisplayed(), "Clicking on down arrow failed to display its details");
						click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"),
								"[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
						Assert.assertFalse(
								driver.findElement(By.xpath(myBetItem + "//div[@class='mb-content']")).isDisplayed(),
								"Clicking on up arrow failed to hide its details");
						click(By.xpath(myBetItem + "//div[@class='mb-header-arrow-container']"),
								"[" + (i + 1) + "] Down arrow items in " + openSettledBets + " was not found");
						Assert.assertTrue(getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-content']"))
								.isDisplayed(), "Clicking on down arrow failed to display its details");
					}
					Thread.sleep(1000);
					// Verify bet details (Single)
					if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single")) {
						Assert.assertTrue(
								GetElementText(By.xpath(myBetItem
										+ "//div[@class='mb-content-short-desc']//span[contains(@data-bind, 'html: selectionName')]"))
												.equalsIgnoreCase(selectionNames[0]),
								selectionNames[0] + " - Selection was not found in " + openSettledBets);
						Assert.assertTrue(
								GetElementText(By.xpath(myBetItem + "//div[@class='mb-content-market']"))
										.equalsIgnoreCase("(" + marketNames[0] + ")"),
								marketNames[0] + " - Market was not found in " + openSettledBets);
						Assert.assertTrue(
								GetElementText(By.xpath(myBetItem + "//div[@class='mb-content-desc']"))
										.equalsIgnoreCase(eventNames[0]),
								eventNames[0] + " - Event was not found in " + openSettledBets);
						Assert.assertTrue(GetElementText(By.xpath(myBetItem + "//span[@class='mb-content-odds']"))
								.contains(odds[0]), odds[0] + " - Odds was not found in " + openSettledBets);
						Assert.assertTrue(
								GetElementText(By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsStakeText
										+ "']/../../span[@class='mb-info-stake-value']")).contains(stake),
								stake + " - Stake was not found in " + openSettledBets);
						if (openSettledBets.toLowerCase().contains("open")) {
							if (ProjectName.contains("Desktop_Swedish"))
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath(
														myBetItem + "//span[text()='" + TextControls.mybetsReturnsText
																+ "']/../../span[@class='mb-info-potential-value']"))
												.getText().contains(SwedishPR),
										pr + " - PR was not found in " + openSettledBets);
							else
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath(
														myBetItem + "//span[text()='" + TextControls.mybetsReturnsText
																+ "']/../../span[@class='mb-info-potential-value']"))
												.getText().contains(Convert.ToString(pr)),
										pr + " - PR was not found in " + openSettledBets);
						} else {
							Assert.assertFalse(
									getDriver()
											.findElement(By.xpath(
													myBetItem + "//span[text()='" + TextControls.mybetsReturnsText
															+ "']/../../span[@class='mb-info-potential-value']"))
											.getText().equals(""),
									pr + " - PR was not found in " + openSettledBets);
						}
						Assert.assertTrue(
								getDriver().findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]"))
										.getText().toLowerCase().contains(betStatus.toLowerCase()),
								betStatus + " - Bet Status was not found in " + openSettledBets);
						Assert.assertTrue(
								getDriver()
										.findElement(By.xpath(myBetItem + "//span[contains(@class, 'bet-type-info')]"))
										.getText().toLowerCase().contains(multiBetType.toLowerCase()),
								multiBetType + " - Bet Type was not found in " + openSettledBets);
						Assert.assertFalse(
								getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-header-bet-time']"))
										.getText().equals(""),
								"Today's date was not found in " + openSettledBets);
						break;
					}
					// Verify bet details (Multiples)
					else {
						if (eventNames.length == marketNames.length && selectionNames.length == odds.length
								&& eventNames.length == odds.length) {
							int k = 0;
							for (int j = 1; j < eventNames.length; j++) {
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath(
														"(" + myBetItem + " //div[@class='mb-content-short-desc'])[" + j
																+ "]//span[contains(@data-bind, 'html: selectionName')]"))
												.getText().equals(selectionNames[k]),
										selectionNames[k] + " - Selection was not found in " + openSettledBets);
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath("(" + myBetItem + "//div[@class='mb-content'])["
														+ j + "]//div[@class='mb-content-market']"))
												.getText().equals("(" + marketNames[k] + ")"),
										marketNames[k] + " - Market was not found in " + openSettledBets);
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath("(" + myBetItem + "//div[@class='mb-content'])["
														+ j + "]//div[@class='mb-content-desc']"))
												.getText().equalsIgnoreCase(eventNames[k]),
										eventNames[k] + " - Event was not found in " + openSettledBets);
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath("(" + myBetItem + "//div[@class='mb-content'])["
														+ j + "]//span[@class='mb-content-odds']"))
												.getText().contains(odds[k]),
										odds[k] + " - Odds was not found in " + openSettledBets);
								k++;
							}
							Assert.assertTrue(
									getDriver()
											.findElement(By
													.xpath(myBetItem + "//span[text()='" + TextControls.mybetsStakeText
															+ "']/../../span[@class='mb-info-stake-value']"))
											.getText().contains(stake),
									stake + " - Stake was not found in " + openSettledBets);
							if (openSettledBets.toLowerCase().contains("open"))
								Assert.assertTrue(
										getDriver()
												.findElement(By.xpath(
														myBetItem + "//span[text()='" + TextControls.mybetsReturnsText
																+ "']/../../span[@class='mb-info-potential-value']"))
												.getText().contains(Convert.ToString(pr)),
										pr + " - PR was not found in " + openSettledBets);
							else
								Assert.assertTrue(getDriver()
										.findElement(
												By.xpath(myBetItem + "//span[text()='" + TextControls.mybetsReturnsText
														+ "']/../../span[@class='mb-info-potential-value']"))
										.getText() != "", pr + " - PR was not found in " + openSettledBets);
							Assert.assertTrue(
									getDriver()
											.findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]"))
											.getText().toLowerCase().contains(betStatus.toLowerCase()),
									betStatus + " - Bet Status was not found in " + openSettledBets);
							Assert.assertTrue(
									getDriver()
											.findElement(
													By.xpath(myBetItem + "//span[contains(@class, 'bet-type-info')]"))
											.getText().toLowerCase().contains(multiBetType.toLowerCase()),
									multiBetType + " - Bet Type was not found in " + openSettledBets);
							Assert.assertFalse(
									getDriver().findElement(By.xpath(myBetItem + "//div[@class='mb-header-bet-time']"))
											.getText().equals(""),
									"Today's date was not found in " + openSettledBets);
						} else
							Assert.fail("Mismatch in event details count. Event(" + eventNames.length + "), Market("
									+ marketNames.length + "), Selection(" + selectionNames.length + "), Odds("
									+ odds.length + ")");
						break;
					}
				} else if (i == (mybetHistroyItems.size() - 1))
					Assert.fail("Event-Selction '" + eventNames[0] + "-" + selectionNames[0] + "' was not found in "
							+ openSettledBets);
			}
			if (cashout) {
				if (isElementPresent(By.xpath(myBetItem + "//div[@class='disabled-text']"))) // To
																								// Handle
																								// new
																								// Story
																								// CashOut
																								// tool
																								// tip
																								// story
				{
					System.out.println("Cashout is not available in " + openSettledBets);
				} else {
					double val, oldBalance, newBalance;
					String cashoutVal, notificationText;
					oldBalance = loginLogoutFunctions.GetBalance();
					cashoutVal = GetElementText(By.xpath(myBetItem + "//div[@class='button']//div[text()='"
							+ TextControls.cashOut + "']/following-sibling::div[@class='cashout-holder-value']"));
					// check cashout value is less than total stake
					if (ProjectName.contains("Desktop_Swedish")) {
						stake = stake.replace(",", ".");
						cashoutVal = cashoutVal.replace(",", ".").toLowerCase();
						val = Convert.ToDouble(cashoutVal.replace(TextControls.currencySymbol, ""));
						Assert.assertTrue(val < Convert.ToDouble(stake),
								"Cashout value is greater than the total stake. CashoutVal: '" + val
										+ "', Total Stake: '" + stake + "'");
					} else {
						val = Convert.ToDouble(cashoutVal.replace(TextControls.currencySymbol, ""));
						Assert.assertTrue(val < Convert.ToDouble(stake),
								"Cashout value is greater than the total stake. CashoutVal: '" + val
										+ "', Total Stake: '" + stake + "'");
					}
					getDriver().findElement(By.xpath(myBetItem + "//cashout")).click();
					Thread.sleep(3000);
					notificationText = GetElementText(
							By.xpath(myBetItem + "//span[@class='cashout-flow-notifications black']"));
					if (ProjectName.contains("Desktop_Swedish"))
						Assert.assertTrue(
								notificationText.equalsIgnoreCase(
										TextControls.cashOutWrngNotificationText + cashoutVal.replace(".", ",") + "?"),
								"Wrong notification text displayed '" + notificationText + "'");
					else
						Assert.assertTrue(
								notificationText
										.equalsIgnoreCase(TextControls.cashOutWrngNotificationText + cashoutVal + "?"),
								"Wrong notification text displayed '" + notificationText + "'");
					Assert.assertTrue(isElementPresent(MyBetsControls.cashoutAccept),
							"Cashout Accept option failed to appear");
					click(MyBetsControls.cashoutDecline, "Cashout Decline option failed to appear");
					Assert.assertFalse(isElementPresent(MyBetsControls.cashoutAccept),
							"Cashout Accept option failed to close");
					Assert.assertFalse(isElementPresent(MyBetsControls.cashoutDecline),
							"Cashout Decline option failed to close");
					click(By.xpath("(//div[@class='button']//div[@class='cashout-holder-value'])[1]"),
							"Cashout option is not present");
					scrollPage("bottom");
					js.executeScript("window.scrollBy(0,-300)", "");
					click(MyBetsControls.cashoutAccept, "Cashout Accept option failed to appear");
					Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutSpinner),
							"cash out spinner is not displayed");
					Thread.sleep(1000);
					if (ProjectName.contains("Desktop_Swedish"))
					
						Assert.assertTrue(
								isElementPresent(By.xpath(myBetItem + "//span[text()='" + TextControls.cashoutText
										+ "']/following-sibling::span[text()='" + cashoutVal.replace(".", ",") + "']")),
								"Cashed out value text is not displayed");					
					
					else
					
						Assert.assertTrue(
								isElementPresent(By.xpath(myBetItem + "//span[text()='" + TextControls.cashedOutText
										+ "']/following-sibling::span[text()='" + cashoutVal + "']")),
								"Cashed out value text is not displayed");
					Assert.assertTrue(
							getDriver().findElement(By.xpath(myBetItem + "//div[contains(@class, 'bet-status')]"))
									.getText().toLowerCase().contains((TextControls.cashedOutText).toLowerCase()),
							"'Cashed Out' - Bet Status was not found in " + openSettledBets);
					
					Assert.assertTrue(isElementPresent(By.xpath(myBetItem + "//div[@class='top-message-icon']")),
							"Cashout Tick was not displayed");
					// According to PROG-800 in Mars , Refresh icon is added for
					// main balance to be updated
					click(MyBetsControls.balRefresh, "Balance Refresh button is not found");
					Thread.sleep(6000);
					// Verify the balance
					newBalance = loginLogoutFunctions.GetBalance();
					System.out.println("==" + oldBalance + "+" + val);
					oldBalance = oldBalance + val;
					Assert.assertTrue(oldBalance <= newBalance, "Mismatch in the cashedout balance. Expected: '"
							+ oldBalance + "', Actual: '" + newBalance + "'");
				}
			}
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyHistoryCashoutLinksInMyBets_function(String openSettledBets) throws Exception {
		String mainWindow;
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			System.out.println("Verify History, cashout links in  " + openSettledBets
					+ " , User should be able to navigate to History, cashout links in " + openSettledBets + "");
			mainWindow = getDriver().getWindowHandle();
			NavigateToMyBets(openSettledBets);
			if (openSettledBets.contains("settled") || openSettledBets.contains("my accas")) {
				scrollPage("bottom");
				js.executeScript("window.scrollBy(0,-300)", "");
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				Thread.sleep(4000);
			}
			// Verify history link
			Assert.assertTrue(isElementDisplayed(MyBetsControls.historyLinkMyBets),
					"History link is not present in " + openSettledBets + "");
			js.executeScript("arguments[0].scrollIntoView(true);",
					getDriver().findElement(MyBetsControls.historyLinkMyBets));
			// js.executeScript("window.scrollBy(0,-200)", "");
			Thread.sleep(1000);
			click(MyBetsControls.historyLinkMyBets, "History link is not clickable in " + openSettledBets + "");
			switchwindow();
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.myAccounttitle),
					"Account history title is not present in popup");
			getDriver().close();
			getDriver().switchTo().window(mainWindow);
			if (!openSettledBets.contains("settled") && isElementPresent(MyBetsControls.cashoutLinkAcca)) {
				Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutLinkAcca),
						"Cashout link is not present in " + openSettledBets + "");
				click(MyBetsControls.cashoutLinkAcca, "Cashout link is not clickable");
				switchwindow();
				String url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.contains("cashout"), "Failed to Switch to Cashout window");
				getDriver().close();
				getDriver().switchTo().window(mainWindow);
			}
			System.out.println("User is able to navigate with History, Cashout links in " + openSettledBets + "");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public String[] NavigateAddSelectionFromCoupons(int noOfSelections) throws Exception {
		String[] eventNames = new String[noOfSelections];
		int arrayCount = 0;
		List<WebElement> oddsCollection, couponsColl;
		try {
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			// Navigate to coupon page
			click(FootballElements.couponLink, "Coupon link is not present in home page");
			Thread.sleep(2000);
			oddsCollection = getDriver().findElements(MyBetsControls.oddsColl);
			// Check for sufficient odds
			if (oddsCollection.size() < noOfSelections * 5) {
				couponsColl = getDriver().findElements(By.xpath("//div[@class='container coupon-sidebar']//li"));
				for (int i = 1; i < couponsColl.size(); i++) {
					couponsColl.get(i).click();
					getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					oddsCollection = getDriver().findElements(MyBetsControls.oddsColl);
					if (oddsCollection.size() > noOfSelections * 5)
						break;
				}
			}
			for (int i = 1; i < oddsCollection.size(); i++) {
				eventNames[arrayCount] = getDriver()
						.findElement(
								By.xpath("(//div[@class='odds-button'])[" + i + "]//ancestor::a//span[@class='name']"))
						.getText();
				click(By.xpath("(//div[@class='odds-button'])[" + i + "]"), "Odds are not present");
				if (common.GetBetSlipCount() == noOfSelections)
					break;
				else {
					if (i == oddsCollection.size() || i == oddsCollection.size() - 1)
						Assert.fail("There are no enough coupons to add");
				}
				i = i + 2;
				arrayCount++;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return eventNames;
	}

	public void VerifyAccasPage(String[] eventNames, Boolean cashOut, String pr, String stake) throws Exception {
		List<WebElement> mybetHistroyItems;
		String[] teams = new String[2];
		try {
			System.out.println("Verify Accas page for " + eventNames[0] + " " + eventNames[1]
					+ ", Acca page should be loaded with all the details of bet");
			NavigateToMyBets("My Accas");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(2000);
			// Verify Acca page
			Assert.assertTrue(isElementDisplayed(MyBetsControls.accaTitle), "Acca title is not present");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.hidePricesAcca), "Hide prices in acca is not present");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.gamesValueAcca), "Games value is not present in table");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.stakeValueAcca), "Stake value is not present in table");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.prValueAcca), "PR is not present in table");
			// there is no acca active text in New
			// EMAAssert.assertTrue(isElementDisplayed(MyBetsControls.accaActive)),
			// "Acca active text is not present");
			String xpath = "//div[@type='potential' and contains(text(),'" + pr + "')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(xpath)), "Potential returns is not present in acca page");
			getDriver().findElement(By.xpath("//div[@class='close-popup']")).click();
			Thread.sleep(2000);
			// Verify event names
			for (int i = 0; i < eventNames.length; i++) {
				int count;
				if (eventNames[i].contains(" v ") || eventNames[i].contains(" V ")) {
					count = eventNames[i].indexOf(" v ");
					teams[0] = eventNames[i].substring(0, count);
					teams[1] = eventNames[i].substring(count + 3);
				} else if (eventNames[i].contains(" vs ") || eventNames[i].contains(" Vs ")) {
					count = eventNames[i].indexOf(" vs ");
					teams[0] = eventNames[i].substring(0, count);
					teams[1] = eventNames[i].substring(count + 4);
				} else if (eventNames[i].toLowerCase().contains("-")) {
					count = eventNames[i].indexOf(" - ");
					teams[0] = eventNames[i].substring(0, count);
					teams[1] = eventNames[i].substring(count + 3);
				} else if (eventNames[i].toLowerCase().contains("\n")) {
					String evnt = eventNames[i].replaceAll("\n", "X");
					String[] Split = evnt.split("X");
					teams[0] = Split[0];
					teams[1] = Split[1];
				}
				String teamAxpath = "(//*[@class='acca-event-container']//*[@class='acca-event-market-container']//*[@class='acca-event-market-team'])["
						+ (i + 1) + "]//div[@class='acca-event-market-team-info-name' and contains(text(), '" + teams[0]
						+ "')]";
				String teamBxpath = "(//*[@class='acca-event-container']//*[@class='acca-event-market-container']//*[@class='acca-event-market-team'])["
						+ (i + 1) + "]//div[@class='acca-event-market-team-info-name' and contains(text(), '" + teams[1]
						+ "')]";
				Assert.assertTrue(isElementDisplayed(By.xpath(teamAxpath)),
						"" + (i + 1) + " event name is not present in acca page");
				Assert.assertTrue(isElementDisplayed(By.xpath(teamBxpath)),
						"" + (i + 1) + " event name is not present in acca page");
			}
			Assert.assertTrue(isElementDisplayed(MyBetsControls.historyLinkMyBets),
					"History link is not present in acca page");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.historyInfoTextAcca),
					"History information text is not present");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutInfoTextAcca),
					"Cash out information text is not present");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutLinkAcca), "Cashout link is not present");
			// Cash out the bets
			if (cashOut) {
				VerifyCashOutInAccas(stake);
				NavigateToMyBets("Settled bets");
				Thread.sleep(2000);
				mybetHistroyItems = getDriver().findElements(MyBetsControls.mybetHistroyItems);
				Assert.assertTrue(mybetHistroyItems.get(0).getText().toLowerCase().contains(teams[0].toLowerCase()),
						"Cashed out bet is not present in settled bets");
			}
			System.out.println("Accas tab is verified successfully");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyCashOutInAccas(String stake) throws Exception {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		double val, oldBalance, newBalance;
		String cashoutVal, actNotificationText, expNotificationText;// myBetItem
		try {
			System.out.println("Verify cashout in accas page, User should be able to cashout in accas page");
			oldBalance = loginLogoutFunctions.GetBalance();
			cashoutVal = GetElementText(MyBetsControls.cashoutInOpenBets).toLowerCase();
			if (ProjectName.contains("Desktop_Swedish")) {
				String val1 = cashoutVal.replaceAll(",", ".");
				val1 = val1.replace(TextControls.currencySymbol, "");
				val = Convert.ToDouble(val1);
			} else
				val = Convert.ToDouble(cashoutVal.replace(TextControls.currencySymbol, ""));
			// check cash out value is less than total stake
			Assert.assertTrue(val < Convert.ToDouble(stake),
					"Cashout value is greater than the total stake. CashoutVal: '" + val + "', Total Stake: '" + stake
							+ "'");
			click(MyBetsControls.cashoutInOpenBets, "Cashout option is not present in OpenBets");
			actNotificationText = GetElementText(MyBetsControls.cashoutNotification);
			expNotificationText = TextControls.cashOutWrngNotificationText + cashoutVal + "?";
			Assert.assertTrue(actNotificationText.equals(expNotificationText),
					"Wrong notification text displayed '" + actNotificationText + "'");
			Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutAccept),
					"Cashout Accept option failed to appear");
			click(MyBetsControls.cashoutDecline, "Cashout Decline option failed to appear");
			Assert.assertFalse(isElementPresent(MyBetsControls.cashoutAccept), "Cashout Accept option failed to close");
			Assert.assertFalse(isElementPresent(MyBetsControls.cashoutDecline),
					"Cashout Decline option failed to close");
			click(MyBetsControls.cashoutInOpenBets, "Cashout option is not present");
			click(MyBetsControls.cashoutAccept, "Cashout Accept option failed to appear");
			// verify Cashout
			Assert.assertTrue(isElementDisplayed(MyBetsControls.cashoutSpinner), "Cashout Spinner was not displayed");
			Thread.sleep(3000);
			Assert.assertTrue(isElementDisplayed(MyBetsControls.cashedOutAcca),
					"You have cashed out accas text is not present");
			// Acoording to PROG-800 in Mars , Refresh icon is added for main
			// balance to be updated
			click(MyBetsControls.balRefresh, "Balance Refresh button is not found");
			Thread.sleep(4000);
			// Verify the balance
			newBalance = loginLogoutFunctions.GetBalance();
			oldBalance = oldBalance + val;
			Assert.assertTrue(oldBalance <= newBalance,
					"Mismatch in the cashedout balance. Expected: '" + oldBalance + "', Actual: '" + newBalance + "'");
			System.out.println("Successfully verified cashout in accas page");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public String[] NavigateAddSelectionFromInplayModule(int noOfSelections, String sportsName) {
		int counter = 0, sel = 0;
		String[] eventName = new String[noOfSelections];
		String sportsTabName;
		try {
			// Navigate to In-play tab
			if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
				click(HomeGlobalElements.inPlayTab, "Inplay Module is not present on home page");
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++) {
				inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
				if (counter == noOfSelections)
					break;
				sportsTabName = inPlayFiltersList.get(i).getText();
				if (sportsTabName.contains("MORE")) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j < sportsNameInMore.size(); j++) {
						sportsNameInMore = getDriver().findElements(By.xpath(
								"//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
						if (sel == noOfSelections)
							break;
						sportsTabName = sportsNameInMore.get(j).getText().toUpperCase();
						sportsNameInMore.get(j).click();
						// sportsTabName = toTitleCase(sportsTabName);
						if (sportsTabName.contains(sportsName.toUpperCase())) {
							sportsTabName = sportsTabName.substring(0, sportsTabName.length() - 4);
							// sportsTabName =
							// MBFBetslipTestObj.UpperFirst(sportsTabName);
							inPlayFiltersList.get(i).click();
							String showMore = "//span[contains(text(),'In-Play')]/following::div[@class='show-more']";
							if (isElementPresent(By.xpath(showMore)))
								scrollAndClick(By.xpath(showMore));
							eventName = addInplaySels(sportsTabName, noOfSelections);
							if (eventName.length == noOfSelections)
								break;
							else
								counter = counter + sel;
						} else if (j == sportsNameInMore.size() - 1 && sel != noOfSelections) {
							Assert.fail("Inplay Football events not found");
						}
					}
				} else if (sportsTabName.contains(sportsName.toUpperCase())) {
					sportsTabName = sportsTabName.substring(0, sportsTabName.length() - 5).toLowerCase();
					sportsTabName = toTitleCase(sportsTabName);
					inPlayFiltersList.get(i).click();
					// String showMore =
					// "//span[contains(text(),'In-Play')]/following::div[@class='show-more']";
					/*
					 * if (isElementPresent(By.xpath(showMore)))
					 * scrollAndClick(By.xpath(showMore));
					 */
					eventName = addInplaySels(sportsTabName, noOfSelections);
					if (eventName.length == noOfSelections)
						break;
					else
						counter = counter + sel;
				} else if (i == inPlayFiltersList.size() - 1) {
					if (sel != noOfSelections)
						Assert.fail("Inplay Football events not found");
				}
			}
			if (eventName.length != noOfSelections)
				Assert.fail("Number of inplay football event is less than 2");
			return eventName;
		} catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return eventName;
	}

	public String[] addInplaySels(String tabName, int noOfSelections) throws Exception {
		int x = 1, selCount = 0;
		String price, eventXpath, teamA, teamB;
		String[] eventNames = new String[noOfSelections];
		int j = 0;
		String events = "//h1//span[contains(text(), 'In-Play')]/../..//h2//div[@class='title' and contains(text(), '"
				+ tabName + "')]/../..//event-list";
		List<WebElement> eventList = getDriver().findElements(By.xpath(events));
		for (j = 0; j < eventList.size(); j++) {
			if (selCount == noOfSelections)
				break;
			eventList = getDriver().findElements(By.xpath(events));
			String selPath = "((//h1//span[contains(text(), 'In-Play')]/../..//h2//div[@class='title' and contains(text(), '"
					+ tabName + "')]/../..//event-list)[" + x + "]//selection-button)[1]";
			if (isElementPresent(By.xpath(selPath)))
			// if (driver.FindElement(By.XPath(selPath)).Displayed == true)
			{
				if (isElementPresent(By.xpath(selPath + "//span[@class='price']//span"))) {
					price = GetElementText(By.xpath(selPath + "//span[@class='price']//span"));
					if (!(price.equals("SUSP"))) {
						click(By.xpath(selPath));
						selCount = selCount + 1;
						eventXpath = "(//h1//span[contains(text(), 'In-Play')]/../..//h2//div[@class='title' and contains(text(), '"
								+ tabName + "')]/../..//event-list)[" + x + "]//div[@class='teams']";
						if (isElementPresent(By.xpath(eventXpath + "//div[@class='team'][1]"))) {
							teamA = GetElementText(By.xpath(eventXpath + "//div[@class='team'][1]"));
							teamB = GetElementText(By.xpath(eventXpath + "//div[@class='team'][2]"));
							eventNames[j] = teamA + " v " + teamB;
							eventNames[j] = GetElementText(By.xpath(eventXpath));
						} else {
							eventNames[j] = GetElementText(By.xpath(eventXpath));
						}
					}
				}
			} else
				System.out.println("Selection is either suspended or not found");
			x++;
		}
		return eventNames;
	}

	public void VerifyShowMoreInOpenSettledBets_function(String openSettledBets) {
		int prevCount;
		int afterCount;
		try {
			// Verify show more in " + openSettledBets + " , Show more button is
			// loading more bets
			NavigateToMyBets(openSettledBets);
			Assert.assertTrue(isElementPresent(MyBetsControls.showMoreInHistory),
					"Show more button is not present in open bets");
			prevCount = getDriver().findElements(MyBetsControls.mybetHistroyItems).size();
			Assert.assertTrue(prevCount <= 10, "No of bets exceeds 10 in " + openSettledBets + "");
			if (prevCount >= 10) {
				click(By.xpath(
						"(//div[@id='my-bets-tabs']//*[@class='bet-detail']//*[@class='mb-header-bet-type'])[1]"));
				Thread.sleep(1000);
				scrollPage("bottom");
				scrollPage("Top");
				scrollPage("bottom");
				WebElement elem = getDriver()
						.findElement(By.xpath("//div[@class='mb-show-more']//span[@class='mb-show-more-main-text']"));
				((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", elem);
				Thread.sleep(1000);
				click(By.xpath("//div[@class='mb-show-more']//span[@class='mb-show-more-main-text']"),
						"Show more button is not clickable in " + openSettledBets + "");
				Thread.sleep(5000);
				afterCount = getDriver().findElements(MyBetsControls.mybetHistroyItems).size();
				Assert.assertFalse(afterCount <= prevCount, "Show more button did not load more bets in open bets");
			} else {
				System.out.println("Show More button not shown as " + openSettledBets + " has less than 10 bets");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}
}

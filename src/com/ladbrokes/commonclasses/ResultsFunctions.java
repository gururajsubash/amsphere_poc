package com.ladbrokes.commonclasses;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ladbrokes.Utils.ReadTestSettingConfig;

public class ResultsFunctions extends DriverCommon {
	Common common = new Common();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();

	public void VerifyResultsTabOnSportsLandingPage_Function(String sportName) {
		try {
			common.NavigateToSportsPage(sportName);
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTab),
					"Results tab is not found on '" + sportName + "' landing page");
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTabIcon),
					"Results tab icon is not found on '" + sportName + "' landing page");
			click(ResultsElements.resultsTab);
			HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.resultsText, sportName, "");
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTitle),
					"Results Title is not Present and expandible by default");
			String tabname = "//li[@class='tab active']//span[@class='title-container' and contains(text(),'"
					+ sportName + "')]";
			String tabnameInMore = "//li[@class='tab tabs-dropdown dropdown active']//div[@class='title-container' and contains(text(),'"
					+ TextControls.more + " (" + sportName + ")')]";
			if (sportName.equals(TextControls.Football))
				Assert.assertTrue(isElementDisplayed(By.xpath(tabname)),
						"'" + sportName + "' tab is not active in Results page");
			else
				Assert.assertTrue(isElementDisplayed(By.xpath(tabnameInMore)),
						"'" + sportName + "' tab is not active in Results page");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyGoToSportsLinkOnResultspage_Function(String sportName) throws Exception {
		try {
			common.NavigateToSportsPage(sportName);
			click(ResultsElements.resultsTab);
			String goToSports = "//a[@class='results-container__link' and contains(text(),'" + TextControls.goTo + " "
					+ sportName + " >')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(goToSports)),
					"' Go To " + sportName + "' Link is not found on Results page");
			click(By.xpath(goToSports));
			HGfunctionObj.VerifyBreadCrums(TextControls.Home, sportName, "", "");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyTodaysResultsFormatOnSport_Function(String sportName) {
		try {
			List<WebElement> time = null;
			List<WebElement> teams = null;
			List<WebElement> settled = null;
			common.NavigateToSportsPage(sportName);
			click(ResultsElements.resultsTab);
			Thread.sleep(2000);
			// Verifying Todays Tab
			Assert.assertTrue(isElementDisplayed(ResultsElements.todaysTab),
					"Todays tab is not found in '" + sportName + "' results page");
			if (isElementPresent(ResultsElements.emptyText)) {
				System.out.println("No events present in Todays Results Tab for '" + sportName + "' sports");
			} else {
				List<WebElement> types = getDriver().findElements(ResultsElements.type);
				for (int i = 0; i < types.size(); i++) {
					if (sportName.equals(TextControls.golf)) {
						Assert.assertTrue(isElementDisplayed(ResultsElements.golftime), "Time stamp is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.golfteamNames), "Team name is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.golfscores), "Score is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.golfsettled),
								"Settled status is not found");
					} else {
						Assert.assertTrue(isElementDisplayed(ResultsElements.subType), "SubType is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.time), "Time stamp is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.teamNames), "Team name is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.scores), "Score is not found");
						Assert.assertTrue(isElementDisplayed(ResultsElements.settled), "Settled status is not found");
					}
				}
				if (sportName.equals(TextControls.golf)) {
					time = getDriver().findElements(ResultsElements.golftime);
					teams = getDriver().findElements(ResultsElements.golfteamNames);
					settled = getDriver().findElements(ResultsElements.golfsettled);
					Assert.assertTrue(isElementDisplayed(ResultsElements.golfscores),
							"Score is not displayed on '" + sportName + "' Results page");
					List<WebElement> settledevents = getDriver().findElements(ResultsElements.golfsettled);
					List<WebElement> badge = getDriver().findElements(ResultsElements.golfbadge);
					Assert.assertTrue(badge.size() == settledevents.size(),
							"All events in Todays results tab are not showing settled");
				} else {
					time = getDriver().findElements(ResultsElements.time);
					teams = getDriver().findElements(ResultsElements.teamNames);
					settled = getDriver().findElements(ResultsElements.settled);
					Assert.assertTrue(isElementDisplayed(ResultsElements.scores),
							"Score is not displayed on '" + sportName + "' Results page");
					Assert.assertTrue(time.size() == settled.size(),
							"All events in Todays results tab are not showing settled");

				}
				Assert.assertTrue(time.size() == teams.size(),
						"Time and teams count is not matching on '" + sportName + "' results page");

			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyYesterdaysResultsFormatOnSport_Function(String sportName) {
		try {
			List<WebElement> resultsDate=null;
			Assert.assertTrue(isElementDisplayed(ResultsElements.YesterdaysTab),
					"Yesterdays result tab is not found in '" + sportName + "' results page");
			click(ResultsElements.YesterdaysTab);
			if (isElementPresent(ResultsElements.emptyText)) {
				System.out.println("No events present in Todays Results Tab for '" + sportName + "' sports");
			} else {
				if(sportName.equals(TextControls.golf))
				{
					Assert.assertTrue(isElementDisplayed(ResultsElements.golfdate),
							"Date stamp is not found in Yesterdays Results Tab");
					resultsDate = getDriver().findElements(ResultsElements.golfdate);
				}
				else{
					Assert.assertTrue(isElementDisplayed(ResultsElements.date),
							"Date stamp is not found in Yesterdays Results Tab");
					resultsDate = getDriver().findElements(ResultsElements.date);
				}

				for (int i = 0; i < resultsDate.size(); i++) {
					String date = resultsDate.get(i).getText();
					String yesterdaysDate = GetYesterdayDate();
					Assert.assertTrue(date.equals(yesterdaysDate),
							"Yesterdays results tab is not showing yesterdays date " + yesterdaysDate + "");
					if(sportName.equals(TextControls.golf))

						Assert.assertTrue(isElementPresent(By
								.xpath("(//div['results-outright-events__winner'])["
										+ i + " + 1]")),
								"Team names are not displayed for yesterdays date");
					else

						Assert.assertTrue(isElementPresent(By
								.xpath("(//div[@class='results-sport-events']//div[@class='results-sport-events__teams'])["
										+ i + " + 1]")),
								"Team names are not displayed for yesterdays date");
				}
				if(sportName.equals(TextControls.golf))
				{
					List<WebElement> settledevents = getDriver().findElements(ResultsElements.golfsettled);
					List<WebElement> badge = getDriver().findElements(ResultsElements.golfbadge);
					Assert.assertTrue(badge.size() == settledevents.size(),
							"All events in yesterdays results tab are not showing settled");
				}
				else
				{
					List<WebElement> settledevents = getDriver().findElements(ResultsElements.settled);
					Assert.assertTrue(resultsDate.size() == settledevents.size(),
							"All events in yesterdays results tab are not showing settled");
				}
			}

		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static String GetYesterdayDate() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	public void VerifyResultsFormatHR_GH(String Racing) throws Exception {
		try {
			common.NavigateToSportsPage(TextControls.Football);
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTab),
					"Results tab is not found on 'Football' landing page");
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTabIcon),
					"Results tab icon is not found on 'Football' landing page");
			click(ResultsElements.resultsTab);
			// Navigate to HR/GH tab
			Assert.assertTrue(isElementDisplayed(By.xpath("//span[@title='" + Racing + "']")),
					"'" + Racing + "' tab is not found in Results page");
			click(By.xpath("//span[@title='" + Racing + "']"));
			HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.resultsText, Racing, "");
			Assert.assertTrue(isElementDisplayed(ResultsElements.resultsTitle),
					"Results Title is not Present and expandible by default");
			String tabname = "//li[@class='tab active']//span[@class='title-container' and contains(text(),'" + Racing
					+ "')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(tabname)),
					"'" + Racing + "' tab is not active in Results page");
			if (Racing.equals(TextControls.Greyhounds))
				Assert.assertTrue(
						isElementDisplayed(By.xpath("//div[@class='title' and contains(text(),'"
								+ TextControls.GreyhoundText + " - Live')]")),
						"" + Racing + " -live is not found on todays results Page");
			else
				Assert.assertTrue(
						isElementDisplayed(By.xpath("//div[@class='title' and contains(text(),'" + Racing + " - "
								+ TextControls.Live + "')]")),
						"" + Racing + " -live is not found on todays results Page");
			Assert.assertTrue(isElementDisplayed(ResultsElements.raceTime),
					"Time is not found for todays '" + Racing + "' results");
			Assert.assertTrue(isElementDisplayed(ResultsElements.win),
					"Win is not found for todays '" + Racing + "' results");
			Assert.assertTrue(isElementDisplayed(ResultsElements.vOid),
					"Void is not found for todays '" + Racing + "' results");
			Assert.assertTrue(isElementDisplayed(ResultsElements.unnamedFav),
					"Unnamed Favourite is not found for todays '" + Racing + "' results");
			Assert.assertTrue(isElementDisplayed(ResultsElements.place),
					"Place is not found for todays '" + Racing + "' results");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyResultsTab_Today_Yesterday_Last3Days_function() throws Exception {
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		try {
			// Verifying Football Results Tabs
			common.NavigateToSportsPage(TextControls.Football);
			// Verifying Todays results
			click(ResultsElements.resultsTab);
			String todaysTime = GetElementText(By.xpath("(//div[@class='results-sport-events__time'])[1]"));
			String todaysTeam = GetElementText(By.xpath(
					"(//div[@class='results-sport-events__teams']//div[@class='results-sport-events__team-name home'])[1]"));
			String todaysEvent = "//div[@class='results-sport-events']//div[@class='results-sport-events__time' and contains(text(),'"
					+ todaysTime
					+ "')]// following::div[@class='results-sport-events__team-name home' and contains(text(),'"
					+ todaysTeam + "')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(todaysEvent)),
					"Todays event is not found in Todays Football result tab");
			// Verifying Yesterdays results
			click(ResultsElements.YesterdaysTab);
			Assert.assertFalse(isElementPresent(By.xpath(todaysEvent)),
					"Todays event is found in Yesterdays Football result tab");
			String yesterdayTime = GetElementText(By.xpath("(//div[@class='results-sport-events__time'])[1]"));
			String yesterdaysTeam = GetElementText(By.xpath(
					"(//div[@class='results-sport-events__teams']//div[@class='results-sport-events__team-name home'])[1]"));
			String yesterdaysEvent = "//div[@class='results-sport-events']//div[@class='results-sport-events__time' and contains(text(),'"
					+ yesterdayTime + "')]// following::div[@class='results-sport-events__team-name home' and text() ='"
					+ yesterdaysTeam + "']";
			Assert.assertTrue(isElementDisplayed(By.xpath(yesterdaysEvent)),
					"Yesterdays Event is not found in Yesterdays Football result tab");
			// Verifying Last3Days results
			click(ResultsElements.Last3DaysTab);
			Assert.assertTrue(isElementDisplayed(By.xpath(yesterdaysEvent)),
					"Yesterdays Event is not found in Last3Days Football result tab");
			// Verifying HR Results Tabs
			if (germanLanguage.equals("Desktop_German")) {
				System.out.println("GreyHounds/HorseRacing is not available for German");
			} else {
				click(ResultsElements.HRtab);
				String racetime = GetElementText(By.xpath("(//div[@class='results-outright-events__time'])[1]"));
				String raceTeam = GetElementText(
						By.xpath("(//div[@class='results-outright-events__winner-name']//span)[1]"));
				String raceEvent = "//div[@class='results-outright-events__time' and text()='" + racetime
						+ "']//following::div[@class='results-outright-events__winner-name']//span[text()='" + raceTeam
						+ "']";
				Assert.assertTrue(isElementDisplayed(By.xpath(raceEvent)),
						"Todays event is not found in  HR Todays result tab");
				// Verifying Yesterdays HR results
				click(ResultsElements.YesterdaysTab);
				Assert.assertFalse(isElementPresent(By.xpath(raceEvent)),
						"Todays event is found in Yesterdays HR result tab");
				String yesterdayRaceTime = GetElementText(
						By.xpath("(//div[@class='results-outright-events__time'])[1]"));
				String yesterdayraceTeam = GetElementText(
						By.xpath("(//div[@class='results-outright-events__winner-name']//span)[1]"));
				String yesterdayraceEvent = "//div[@class='results-outright-events__time' and text()='"
						+ yesterdayRaceTime
						+ "']//following::div[@class='results-outright-events__winner-name']//span[text()='"
						+ yesterdayraceTeam + "']";
				Assert.assertTrue(isElementDisplayed(By.xpath(yesterdayraceEvent)),
						"Yesterdays Event is not found in Yesterdays HR result tab");
				// Verifying Last3Days results
				click(ResultsElements.Last3DaysTab);
				Assert.assertTrue(isElementDisplayed(By.xpath(yesterdayraceEvent)),
						"Yesterdays Event is not found in Last3Days HR result tab");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static String GetLastMonth() throws Exception {
		String Language = ReadTestSettingConfig.getTestsetting("ProjectName");
		SimpleDateFormat dateFormat = null;
		Calendar cal = null;
		try {
			if (Language.equals("Desktop_German")) {
				dateFormat = new SimpleDateFormat("MMMMM yyyy", Locale.GERMANY);
				cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, -1);
			} else if (Language.equals("Desktop_Swedish")) {
				Locale locale = new Locale("SV", "SV");
				dateFormat = new SimpleDateFormat("MMMMM yyyy", locale);
				cal = Calendar.getInstance(locale);
				cal.add(Calendar.MONTH, -1);
			} else {
				dateFormat = new SimpleDateFormat("MMMMM yyyy");
				cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, -1);
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return dateFormat.format(cal.getTime());
	}

	public static String GetCurrentMonth() throws Exception {
		String Language = ReadTestSettingConfig.getTestsetting("ProjectName");
		SimpleDateFormat dateFormat = null;
		Calendar cal = null;
		try {
			if (Language.equals("Desktop_German")) {
				dateFormat = new SimpleDateFormat("MMMMM yyyy", Locale.GERMANY);
				cal = Calendar.getInstance();
			} else if (Language.equals("Desktop_Swedish")) {
				Locale locale = new Locale("SV", "SV");
				dateFormat = new SimpleDateFormat("MMMMM yyyy", locale);
				cal = Calendar.getInstance(locale);
			} else {
				dateFormat = new SimpleDateFormat("MMMMM yyyy");
				cal = Calendar.getInstance();
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return dateFormat.format(cal.getTime());
	}
}

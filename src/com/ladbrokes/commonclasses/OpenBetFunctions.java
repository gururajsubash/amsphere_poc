package com.ladbrokes.commonclasses;

import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;
import com.ladbrokes.EnglishOR.OBTIElements;

public class OpenBetFunctions extends DriverCommon {
	
	/**
	 * Function to change the Result status of a selection for an Event in OpenBet
	 * @param: EventID, MarketName, SelectionName, EventStatus
	 * @author: Sreekanth
	 * @since : 16-Jan-2018
	 */
	@Test
	public static void UpdateEventSelectionStatusInOB(String EventID, String MarketName, String SelectionName, String EventStatus) throws Exception {
		String testCase = new Object() {}.getClass().getEnclosingMethod().getName();
		try {
			
			/*Stub Values
			String EventID, MarketName, SelectionName, EventStatus;
			EventID = "371292"; //218591
			MarketName = "Match Result";
			SelectionName = "Red Bull Brasil SP";
			EventStatus = "Lose";*/
			
			LoginLogoutFunctions.LoginOB_TI();
			
			((JavascriptExecutor) getDriver()).executeScript("window.open()");
			ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs.get(1));
			
			switchFrame(OBTIElements.office_TopBarFrame);
			click(OBTIElements.Admin_Menu);
			Thread.sleep(2000);
			switchDefaultContent();
			switchFrame(OBTIElements.content_Pane);
			switchFrame(OBTIElements.LHS_frame);
			click(OBTIElements.BettingSetUp_expand_option);
			click(OBTIElements.events);
			switchDefaultContent();
			switchFrame(OBTIElements.content_Pane);
			switchFrame(OBTIElements.Admin_RHS_frame);
			Entervalue(OBTIElements.searchByID_txtbox, EventID);
			click(OBTIElements.Admin_Search_btn);
			click(getDriver().findElement(By.xpath("//a[contains(text(),'" + MarketName +"')]")));
			click(getDriver().findElement(By.xpath("//a[contains(text(),'" + SelectionName +"')]")));
			selectVisibleTextInDropdown(OBTIElements.Result_drpdwn, EventStatus);
			click(OBTIElements.setSelectionResult_btn);
			click(OBTIElements.modifyMarket_btn);
			click(OBTIElements.updateEvent_btn);
			
			getDriver().switchTo().window(tabs.get(0)); //Switches to main tab before exiting this function

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(" ** 'UpdateEventSelectionStatusInOB' function failed ** " + e.getMessage());
		}
	}
	
	
	/**
	 * Function to change the Status of an Event in OpenBet to Suspended
	 * @param: EventID, MarketName
	 * @author: Sreekanth
	 * @since : 18-Jan-2018
	 */
	@Test
	public static void UpdateEventStatusToSuspendedInOB(String EventID, String MarketName) throws Exception {
		String testCase = new Object() {}.getClass().getEnclosingMethod().getName();
		try {
			
			/*Stub Values
			String EventID, MarketName, SelectionName, EventStatus;
			EventID = "371292"; //218591
			MarketName = "Match Result";
			EventStatus = "Suspended";*/
			
			LoginLogoutFunctions.LoginOB_TI();
			
			((JavascriptExecutor) getDriver()).executeScript("window.open()");
			ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tabs.get(1));
			
			switchFrame(OBTIElements.office_TopBarFrame);
			click(OBTIElements.Admin_Menu);
			Thread.sleep(2000);
			switchDefaultContent();
			switchFrame(OBTIElements.content_Pane);
			switchFrame(OBTIElements.LHS_frame);
			click(OBTIElements.BettingSetUp_expand_option);
			click(OBTIElements.events);
			switchDefaultContent();
			switchFrame(OBTIElements.content_Pane);
			switchFrame(OBTIElements.Admin_RHS_frame);
			Entervalue(OBTIElements.searchByID_txtbox, EventID);
			click(OBTIElements.Admin_Search_btn);
			click(getDriver().findElement(By.xpath("//a[contains(text(),'" + MarketName +"')]")));
			selectVisibleTextInDropdown(OBTIElements.Status_drpdwn, "Suspended");
			click(OBTIElements.modifyMarket_btn);
			click(OBTIElements.updateEvent_btn);
			
			getDriver().switchTo().window(tabs.get(0)); //Switches to main tab before exiting this function

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(" ** 'UpdateEventStatusToSuspendedInOB' function failed ** " + e.getMessage());
		}
	}
	
}

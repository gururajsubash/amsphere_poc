package com.ladbrokes.commonclasses;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Map;

import org.testng.ITestResult;

import com.ladbrokes.Utils.ReadTestSettingConfig;

import comTR.gurock.testrail.APIClient;
import comTR.gurock.testrail.APIException;

public class TestRailSetup {
	
	
	
	public static comTR.gurock.testrail.APIClient myClient() throws Exception{
		String TestRailUsername = ReadTestSettingConfig.getTestsetting("TestRailUserName");
		String TestRailPassword = ReadTestSettingConfig.getTestsetting("TestRailUserPassword");
		
	    APIClient client;
	    client = new APIClient("https://ladbrokescoral.testrail.com/");    
	    client.setUser(TestRailUsername);
	    client.setPassword(TestRailPassword);
	    return client;

	}
	
	
	public static int TestrailID() throws Exception{
		String runID = ReadTestSettingConfig.getTestsetting("runID");
	    return Integer.parseInt(runID);
	}

	public static void sendResultsToTestrail(APIClient client, ITestResult result, int runID, List<Integer> caseID)
			   throws MalformedURLException, IOException, APIException {

			    Map<String, Integer> data = new HashMap<String, Integer>();
			   int Status = new Integer(result.getStatus());
			   if (Status == 2) {
			    Status = Status + 3;
			   }
			   data.put("status_id", Status);
			   for (int id : caseID) {
			    client.sendPost("add_result_for_case/" + runID + "/" + id, data);
			   }
	}
}

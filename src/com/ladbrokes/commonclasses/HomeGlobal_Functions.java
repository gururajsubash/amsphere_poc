package com.ladbrokes.commonclasses;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;

public class HomeGlobal_Functions extends DriverCommon {
	Common common = new Common();

	public void VerifyContactUsUI() throws Exception {
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		try {
			click(LoginElements.ContactUsBtn);
			Assert.assertTrue(!getAttribute(LoginElements.contactUsContent, "Class").contains(" dn"),
					"Contact Us Tooltip content failed to open");
			Assert.assertTrue(isElementDisplayed(LoginElements.contactUsContent),
					"Contact Us Tooltip content is not shown to user");
			Assert.assertTrue(isElementDisplayed(LoginElements.contactUsStaticText),
					"Contact Us static text is not present in the Settings tooltip");
			click(LoginElements.ContactUsBtn);
			Assert.assertTrue(getAttribute(LoginElements.contactUsContent, "Class").contains(" dn"),
					"Contact Us Tooltip content failed to close");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyHelpUI() throws Exception {
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		click(HomeGlobalElements.HelpIcon);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.HelpHeader),
				"Help text is not present in Help tootlip");
		Assert.assertTrue(!getAttribute(HomeGlobalElements.HelpContent, "Class").contains(" dn"),
				"Help Tooltip content failed to open");
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.HelpContent),
				"Help Tooltip content is not shown to user");
		click(HomeGlobalElements.HelpIcon);
		Thread.sleep(2000);
		Assert.assertTrue(getAttribute(HomeGlobalElements.HelpContent, "Class").contains(" dn"),
				"Help Tooltip content failed to close");
	}

	public void VerifySettingsUI() throws Exception {
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		click(LoginElements.settingsBtn);
		Assert.assertTrue(!getAttribute(LoginElements.settingsContent, "Class").contains(" dn"),
				"Settings Tooltip content failed to open");
		Assert.assertTrue(isElementDisplayed(LoginElements.settingsContent),
				"Settings Tooltip content is not shown to user");
		click(LoginElements.settingsBtn);
		Thread.sleep(2000);
		Assert.assertTrue(getAttribute(LoginElements.settingsContent, "Class").contains(" dn"),
				"Settings Tooltip content failed to Close");
	}

	public void VerifyLanguageUI() throws Exception {
		String url;
		// ***Verify Language English(Ireland)
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
		click(HomeGlobalElements.languageBtn);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.engIrelend),
				" English (Ireland) Language is not found");
		click(HomeGlobalElements.engIrelend);
		url = getDriver().getCurrentUrl();
		Assert.assertTrue(url.toLowerCase().contains("en-ie"),
				"Choosed  English(Ireland) language is not displayed on the website");
		Thread.sleep(2000);
		click(HomeGlobalElements.ladbrokesLogo);
		// ***Verify Language German
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
		click(HomeGlobalElements.languageBtn);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.german), " German Language is not found");
		click(HomeGlobalElements.german);
		url = getDriver().getCurrentUrl();
		Assert.assertTrue(url.toLowerCase().contains("de-de"),
				"Choosed  German language is not displayed on the website");
		Thread.sleep(2000);
		click(HomeGlobalElements.ladbrokesLogo);
		// ***Verify Language Swedish
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
		click(HomeGlobalElements.languageBtn);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.swedish), " German Language is not found");
		click(HomeGlobalElements.swedish);
		url = getDriver().getCurrentUrl();
		Assert.assertTrue(url.toLowerCase().contains("sv-se"),
				"Choosed  German language is not displayed on the website");
		Thread.sleep(2000);
		click(HomeGlobalElements.ladbrokesLogo);
		// ***Verify Language English(Africa)
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
		click(HomeGlobalElements.languageBtn);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.african), " German Language is not found");
		click(HomeGlobalElements.african);
		url = getDriver().getCurrentUrl();
		Assert.assertTrue(url.toLowerCase().contains("en-af"),
				"Choosed  German language is not displayed on the website");
		Thread.sleep(2000);
		click(HomeGlobalElements.ladbrokesLogo);
	}

	public void VerifyLiveChat_Function(String headerFooter) throws Exception {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		try {
			Entervalue(LoginElements.Username, "itestedthis1");
			Entervalue(LoginElements.Password, "Password1");
			click(LoginElements.Login);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Thread.sleep(4000);
			switch (ProjectName) {
			case "Desktop_English":
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				if (headerFooter.toLowerCase().contains("header")) {
					click(LoginElements.ContactUsBtn);
					Assert.assertTrue(isElementDisplayed(LoginElements.liveChatLinkHeader),
							"Live chat is not present in header");
					Thread.sleep(2000);
					common.SwitchWindow_ValidateNewWindow(
							"//div[@class='tooltip-section-link-content']//div[text()='Go to Live Chat']",
							"//div[text()='CHAT WITH US']", "");
				} else {
					common.scrollPage("bottom");
					// click(LoginElements.liveChatFooterLink);
					Assert.assertTrue(isElementDisplayed(LoginElements.liveChatFooterLink),
							"Live chat is not present in header");
				}
				System.out.println(" User is able to use Live chat from " + headerFooter + " ");
				break;
			case "Desktop_Swedish":
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
				click(HomeGlobalElements.languageBtn);
				Thread.sleep(2000);
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.swedish),
						" Swedish Language is not found in languages content");
				click(HomeGlobalElements.swedish);
				url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.toLowerCase().contains("sv-se"),
						"Choosed  Swedish language is not displayed on the website");
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				Thread.sleep(2000);
				System.out.println(" Live chat is not available for Swedish ");
				break;
			case "Desktop_Irish":
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn), "Language Button is not found");
				click(HomeGlobalElements.languageBtn);
				Thread.sleep(2000);
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.engIrelend),
						" English (Ireland) Language is not found in languages content");
				click(HomeGlobalElements.engIrelend);
				Thread.sleep(2000);
				url = getDriver().getCurrentUrl();
				Assert.assertTrue(url.toLowerCase().contains("en-ie"),
						"Choosed  English(Ireland) language is not displayed on the website");
				Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				Thread.sleep(2000);
				break;
			case "Desktop_German":
				url = getDriver().getCurrentUrl();
				if (!url.toLowerCase().contains("de-de")) {
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.languageBtn),
							"Language Button is not found");
					click(HomeGlobalElements.languageBtn);
					Thread.sleep(2000);
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.german),
							" German Language is not found in languages content");
					click(HomeGlobalElements.german);
					url = getDriver().getCurrentUrl();
					Assert.assertTrue(url.toLowerCase().contains("de-de"),
							"Choosed  German language is not displayed on the website");
					Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Login Unsuccessful");
				}
				Thread.sleep(2000);
				break;
			}
			
			
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * This Method is to check the Error Messages
	 * 
	 * @return ErrorMessage
	 * @throws Exception
	 *             element not found
	 */
	public String CaptureLoginErrorMessage(String userName, String passWord) throws Exception {
		try {
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
			getDriver().findElement((LoginElements.Username)).clear();
			getDriver().findElement((LoginElements.Password)).clear();
			Entervalue(LoginElements.Username, userName);
			Entervalue(LoginElements.Password, passWord);
			click(LoginElements.Login);
			Thread.sleep(2000);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return GetElementText(LoginElements.loginErrorpanel);
	}

	public void VerifyCustomerEditAndViewDisplaySettings_functions() throws Exception {
		try {
			launchweb();
			VerifyContactUsUI();
			VerifyHelpUI();
			VerifySettingsUI();
			VerifyLanguageUI();
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void ChooseMyStartPage_function(String className) throws Exception {
		String url = ReadTestSettingConfig.getTestsetting("BaseURL1_xpath");
		String actualUrl, actBreadcrumbs, expBreadcrumbs;
		try {
			click(LoginElements.settingsBtn);
			if (className == TextControls.Football) {
				click(HomeGlobalElements.footballRadioBtn);
				getDriver().get(url);
				getDriver().manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
				actualUrl = getDriver().getCurrentUrl();
				System.out.println("actualUrl = " + actualUrl);
				String Football = ReadTestSettingConfig.getTestsetting("ProjectName");
				if (Football.equals("Desktop_German"))
					Assert.assertTrue(actualUrl.contains("ball"), "choose my start page as football is failed ");
				else
					Assert.assertTrue(actualUrl.contains(TextControls.Football.toLowerCase()),
							"choose my start page as football is failed ");
				VerifyBreadCrums(TextControls.Home, TextControls.Football, "", "");
				System.out.println("Successfully verified football as Start Page");
			} else {
				click(HomeGlobalElements.HRRadioBtn);
				getDriver().get(url);
				getDriver().manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
				actualUrl = getDriver().getCurrentUrl();
				System.out.println("actualUrl = " + actualUrl);
				Assert.assertTrue(actualUrl.contains(TextControls.hRText),
						"choose my start page as Horse Racing is failed");
				VerifyBreadCrums(TextControls.Home, TextControls.HorseRacing, "", "");
				System.out.println("Successfully verified HR as Start Page");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyBreadCrums(String homeText, String className, String typeName, String subTypeName)
			throws Exception {
		String actBreadcrumbs, expBreadcrumbs;
		String BrowserType = ReadTestSettingConfig.getTestsetting("BrowserType");
		try {
			Thread.sleep(3000);
			getDriver().manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs),
					"Hierarchy (breadcrumbs) was not found");
			actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs);
			actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
			if (typeName.isEmpty() && subTypeName.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className;
				else
					expBreadcrumbs = homeText + ">" + className;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			} else if (subTypeName.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className + ">" + " " + typeName;
				else
					expBreadcrumbs = homeText + ">" + className + ">" + typeName;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			} else {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className + ">" + " " + typeName + ">" + " " + subTypeName;
				else
					expBreadcrumbs = homeText + ">" + className + ">" + typeName + ">" + subTypeName;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyBreadCrums(String homeText, String className, String typeName, String subTypeName,
			String eventname) throws Exception {
		String actBreadcrumbs, expBreadcrumbs;
		String BrowserType = ReadTestSettingConfig.getTestsetting("BrowserType");
		try {
			Thread.sleep(3000);
			getDriver().manage().timeouts().implicitlyWait(70, TimeUnit.SECONDS);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs),
					"Hierarchy (breadcrumbs) was not found");
			actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs);
			actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
			if (typeName.isEmpty() && subTypeName.isEmpty() && eventname.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className;
				else
					expBreadcrumbs = homeText + ">" + className;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			} else if (subTypeName.isEmpty() && eventname.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className + ">" + " " + typeName;
				else
					expBreadcrumbs = homeText + ">" + className + ">" + typeName;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			} else if (eventname.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className + ">" + " " + typeName + ">" + " " + subTypeName;
				else
					expBreadcrumbs = homeText + ">" + className + ">" + typeName + ">" + subTypeName;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			} else if (subTypeName.isEmpty()) {
				if (BrowserType.equals("InternetExplorer"))
					expBreadcrumbs = homeText + ">" + " " + className + ">" + eventname + " " + typeName;
				else
					expBreadcrumbs = homeText + ">" + className + ">" + eventname + " " + typeName;
				Assert.assertTrue(actBreadcrumbs.equalsIgnoreCase(expBreadcrumbs));
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void NavigationAndLogin_NonSportsSite(String Site) throws Exception {
		String actualUrl, expectedUrl;
		String username = ReadTestSettingConfig.getTestsetting("UserName1");
		String password = ReadTestSettingConfig.getTestsetting("Password1");
		String Language = ReadTestSettingConfig.getTestsetting("ProjectName");
		WebElement ele ;
		try {
			if (Site.equalsIgnoreCase("casino")) {
				click(HomeGlobalElements.casino);
				Thread.sleep(2000);
				actualUrl = getDriver().getCurrentUrl();
				Assert.assertTrue(actualUrl.contains("casino"), "Failed to navigate Casio site ");
				Entervalue(LoginElements.NonSportsSiteUsername, username);
				Entervalue(LoginElements.NonSportsSitePassword, password);
				 ele = getDriver().findElement(LoginElements.casinoLogin);
				clickByJS(ele);
			} else if (Site.equalsIgnoreCase("poker")) {
				click(HomeGlobalElements.poker);
				Thread.sleep(2000);
				actualUrl = getDriver().getCurrentUrl();
				//commented this line as it varies with languages
				//expectedUrl = "http://poker.ladbrokes.com/en";
				Assert.assertTrue(actualUrl.contains("poker"), "Failed to navigate Poker site ");
				Entervalue(LoginElements.NonSportsSiteUsername, username);
				Entervalue(LoginElements.NonSportsSitePassword, password);
				getDriver().findElement(By.xpath("//button[@title='Login']")).click();
			} else {
				click(HomeGlobalElements.bingo);
				Thread.sleep(2000);
				actualUrl = getDriver().getCurrentUrl();
				//commented this line as it varies with languages
				//expectedUrl = "https://bingo.ladbrokes.com/en";
				Assert.assertTrue(actualUrl.contains("bingo"), "Failed to navigate Bingo site ");
				Entervalue(LoginElements.NonSportsSiteUsername, username);
				Entervalue(LoginElements.NonSportsSitePassword, password);
				//getDriver().findElement(By.xpath("//button[@title='Login']")).click();
				getDriver().findElement(LoginElements.BingoLogin).click();
			}
			Thread.sleep(4000);
			Assert.assertTrue(isElementDisplayed(By.xpath("//a[@class='deposit-link']")),
					"Login unsuccessfull in Casino site");
			if(isElementPresent(By.xpath("//button[@id='COOKIE_CONTINUE']")))
				click(By.xpath("//button[@id='COOKIE_CONTINUE']"));
			// then Navigate to Sports User should be logged in
			
				if(Language.equals("Desktop_Swedish")&& Site.equalsIgnoreCase("bingo"))
				{
					Thread.sleep(2000);
					click(HomeGlobalElements.Odds);
				}
				else
				click(HomeGlobalElements.sports);
			Thread.sleep(4000);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Assert.assertTrue(isElementDisplayed(LoginElements.depositIcon), "User is not logged in sports site");
			Common.logOut();
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void NavigateAddInplaySelection(int Count) throws Exception {
		int Counter = 0, sel = 0;
		String tabName;
		try {
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(3000);
			List<WebElement> inPlayFilterList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFilterList.size(); i++) {
				inPlayFilterList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
				if (Counter == Count)
					break;
				tabName = inPlayFilterList.get(i).getText().toLowerCase();
				if (tabName.toUpperCase().contains(TextControls.moreText.toUpperCase())) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j < sportsNameInMore.size(); j++) {
						sportsNameInMore = getDriver().findElements(By.xpath(
								"//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
						if (Counter == Count)
							break;
						tabName = sportsNameInMore.get(j).getText().toLowerCase();
						sportsNameInMore.get(j).click();
						sel = addInplaySel();
						if (sel == 1)
							Counter = Counter + 1;
						click(HomeGlobalElements.moreTab);
					}
				} else {
					// tabName = tabName.replace(oldChar,
					// newChar)((tabName.indexOf("(")) - 2);
					tabName = tabName.toLowerCase();
					inPlayFilterList.get(i).click();
					sel = addInplaySel();
					if (sel == 1)
						Counter = Counter + 1;
				}
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public int addInplaySel() {
		int x = 1, selCount = 0;
		int j = 0;
		List<WebElement> eventList = getDriver().findElements(HomeGlobalElements.inPlayEventList);
		for (j = 0; j < eventList.size(); j++) {
			if (selCount == 1)
				break;
			eventList = getDriver().findElements(HomeGlobalElements.inPlayEventList);
			if (isElementDisplayed(HomeGlobalElements.inPlayEventList)) {
				boolean selection = getDriver().findElements(
						By.xpath("(//div[@class='event-group']//div[contains(@class, 'event-list live')])[" + x
								+ "]//selection-button//span"))
						.size() > 0;
				if (selection == true) {
					String selectionprice = getDriver().findElement(
							By.xpath("(//div[@class='event-group']//div[contains(@class, 'event-list live')])[" + x
									+ "]//selection-button//span"))
							.getText();
					if (!selectionprice.equals("SUSP")) {
						getDriver().findElement(
								By.xpath("((//div[@class='event-group']//div[contains(@class, 'event-list live')])[" + x
										+ "]//selection-button//span)[1]"))
								.click();
						selCount = selCount + 1;
					}
				}
			} else
				System.out.println("Selection is either suspended or not found");
			x++;
		}
		return selCount;
	}

	public void VerifyClassLandingPage() throws Exception {
		int count;
		String ToggleList = "";
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		System.out.println("Verify up coming matches on Class landing page");
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.timeFilter), "Time Filter is not found");
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.competitionFilter), "Time Filter is not found");
		click(HomeGlobalElements.timeFilter);
		String tabName = GetElementText(
				By.xpath("//div[@class='module']//nav[@class='tabs']//li[contains(@class, 'tab')]"));
		if (tabName.contains(TextControls.todayText.toUpperCase())) {
			// Verifying event list under today's filter
			List<WebElement> todayEventList = getDriver().findElements(FootballElements.eventList);
			Assert.assertTrue(getAttribute(FootballElements.todayFilter, "class").contains("active"), "");
			Assert.assertTrue(todayEventList.size() > 0, "Event list is not found under today's filter");
		}
		String tabName1 = GetElementText(FootballElements.tomorrowFilter);
		if (tabName1.contains(TextControls.tomorrowText.toUpperCase())) {
			// Verifying event list under tomorrow's filter
			click(FootballElements.tomorrowFilter);
			List<WebElement> TomorroweventList = getDriver().findElements(FootballElements.eventList);
			Assert.assertTrue(TomorroweventList.size() > 0, "Event list is not found under tomorrow's filter");
		}
		// Verifying event list under Future's filter
		click(FootballElements.futureFootball, "Future tab is not found");
		Thread.sleep(2000);
		List<WebElement> futureEventList = getDriver().findElements(FootballElements.eventList);
		Assert.assertTrue(futureEventList.size() > 0, "Event list is not found under future's filter");
		// Verifying event list under league filter
		click(HomeGlobalElements.competitionFilter);
		if (isElementDisplayed(
				By.xpath("//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list expanded']"))) {
			System.out.println("List is expanded");
		} else if (isElementDisplayed(
				By.xpath("//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list']"))) {
			click(By.xpath("(//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list'])[1]"));
			List<WebElement> eventList1 = getDriver().findElements(FootballElements.eventList);
			Assert.assertTrue(eventList1.size() > 0, "Event list is not found under league filter");
			click(By.xpath("(//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list'])[1]"), "");
			click(By.xpath("(//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list'])[1]"), "");
			// Verifying expanding and collapsing event list under league filter
			List<WebElement> toggleList = getDriver().findElements(FootballElements.toggleList);
			List<WebElement> compEventList = getDriver().findElements(FootballElements.compEventList);
			Assert.assertTrue(toggleList.size() == compEventList.size(),
					"Toggle list count is not matched with future event list count");
			if (toggleList.size() > 3)
				count = 3;
			else
				count = toggleList.size();
			for (int i = 0; i < count; i++) {
				// Collapsing
				String toggleListName = toggleList.get(i).getText();
				if (toggleListName.contains("'")) {
					String listname = toggleListName.replace("'", "XZ");
					ToggleList = listname.substring(0, listname.indexOf("XZ"));
				}
				js.executeScript("arguments[0].scrollIntoView(false);", ToggleList);
				Thread.sleep(3000);
				js.executeScript("window.scrollBy(0,-200)", "");
				toggleList.get(i).click();
				String CompeventList = "//div[@style='position: relative']//h2[@class='expand-list expanded']//div[contains(text(), '"
						+ ToggleList + "')]/following::div[@class='event-group'][1]";
				Assert.assertFalse(isElementDisplayed(By.xpath(CompeventList)), "Compititation events are not found");
				// Expanding
				js.executeScript("arguments[0].scrollIntoView(false);", ToggleList);
				Thread.sleep(3000);
				js.executeScript("window.scrollBy(0,-200)", "");
				toggleList.get(i).click();
				String CompeventList1 = "//div[@style='position: relative']//h2[@class='expand-list expanded']//div[contains(text(), '"
						+ ToggleList + "')]/following::div[@class='event-group'][1]";
				Assert.assertTrue(isElementDisplayed(By.xpath(CompeventList1)), "Compititation events are not found");
				break;
			}
		}
	}

	public void VerifyInplayEventExpandingAndEventCount() throws Exception {
		String testCase = "Function - VerifyInplayEventExpandingAndEventCount";
		try {
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			click(HomeGlobalElements.inPlayFBEvent);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(3000);
			// **"Verifying events display under the class headers.", "Events
			// should be display under the class headers."
			String eventType = getDriver()
					.findElement(By
							.xpath("//*[@id='inplay-sidebar']/div[1]/h2[contains(@class, 'sport expanded')]//span[@class='title']"))
					.getText();
			// int inplayEventCnt =
			// Integer.parseInt(Common.ExtractNumberFromString(eventType, "(",
			// ")"));
			String num = Common.ExtractNumberFromString(eventType, "(", ")");
			int inplayEventCnt = Integer.parseInt(num);
			if (isElementDisplayed(By
					.xpath("//div[contains(@class, 'inplay-wrapper')]//h2[contains(@class, 'sport')]//span[contains(text(), '"
							+ eventType + "')]"))) {
				List<WebElement> inplayEvents = getDriver().findElements(HomeGlobalElements.inPlayEventsOnDetailPage);
				Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
			}
			// Select the option to expand upcoming events on contextual menu
			getDriver().findElement(By.xpath("//header[@class='inplay expanded toggle-arrow-ie']")).click();
			click(HomeGlobalElements.upcomingMatches, "Failed to click to Expand on Upcoming matches");
			Assert.assertTrue(getAttribute(HomeGlobalElements.upcomingMatchExpanded, "class").contains("expanded"),
					"Upcoming tab is not expanded");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.upcomingSports));
			System.out.println("\nUpcoming sports are not found and Upcoming matches not expanded");
			Thread.sleep(3000);
			click(HomeGlobalElements.upcomingMatches, "Failed to click to Collapse on Upcoming matches");
			// Assert.assertFalse(isElementDisplayed(HomeGlobalElements.upcomingSports),
			// "Upcoming sports are found and Upcoming matches expanded");
			// Verify events under live streaming button
			scrollPage("Top");
			click(By.xpath("//*[@id='inplay-sidebar']/header[1]/div[1]/div/div[2]/span[1]"),
					"Stream button is not found");
			// Collapsing and expanding Upcoming matches in Live streaming on
			// Contextual menu
			Thread.sleep(2000);
			click(HomeGlobalElements.upcomingMatches, "upcoming matches is not found");
			Assert.assertTrue(getAttribute(HomeGlobalElements.upcomingMatchExpanded, "class").contains("expanded"),
					"Upcoming tab is not expanded");
			Thread.sleep(2000);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.upcomingSports));
			System.out.println("\nUpcoming sports are not found and Upcoming matches in Live streaming not expanded");
			click(HomeGlobalElements.upcomingMatches);
			// Assert.assertFalse(isElementDisplayed(HomeGlobalElements.upcomingSports),"Upcoming
			// sports in Live streaming are found and Upcoming matches
			// expanded");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyEventDetailPageNavigation(String className) throws Exception {
		String eventName, name;
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		try {
			name = GetElementText(FootballElements.upcomingEventName);
			js.executeScript("window.scrollTo(0, 200)");
			Thread.sleep(3000);
			click(FootballElements.upcomingEventName);
			eventName = GetElementText(FootballElements.EdpTitle);
			Assert.assertTrue(name.equalsIgnoreCase(eventName), "Not navigating to event detail page");
			common.NavigateToSportsPage(className);
			click(HomeGlobalElements.competitionFilter);
			Thread.sleep(3000);
			name = GetElementText(FootballElements.compEventName);
			click(FootballElements.compEventName);
			eventName = GetElementText(FootballElements.EdpTitle);
			Assert.assertTrue(name.equalsIgnoreCase(eventName), "Not navigating to event detail page");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot("VerifyEventDetailPageNavigation - Function");
			Assert.fail(e.getMessage(), e);
		}
	}
}

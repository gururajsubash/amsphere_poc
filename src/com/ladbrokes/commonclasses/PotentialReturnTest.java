package com.ladbrokes.commonclasses;

import org.testng.Assert;

import com.ladbrokes.Utils.ReadTestSettingConfig;

public class PotentialReturnTest extends DriverCommon {
	
	public static double GetPotentialReturnForMultiples(String[] oddsArray,double stake, String betType)
	{
	    int cnt, oddsCount;
	    double potentialRet = 0.00, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10, dbl11, dbl12, dbl13, dbl14, dbl15;
	    double trbl1, trbl2, trbl3, trbl4, trbl5, trbl6, trbl7, trbl8, trbl9, trbl10, trbl11, trbl12, trbl13, trbl14, trbl15, trbl16, trbl17, trbl18, trbl19, trbl20;
	    double acc4 = 0.00, acc4_1, acc4_2, acc4_3, acc4_4, acc4_5, acc4_6, acc4_7, acc4_8, acc4_9, acc4_10, acc4_11, acc4_12, acc4_13, acc4_14, acc4_15;
	    double acc5 = 0.00, acc6 = 0.00, acc5_1, acc5_2, acc5_3, acc5_4, acc5_5, acc5_6 = 0.00;
	    double singles = 0.00;
	    double trebles = 0.00;
	    double dbls = 0.00;

	   if (oddsArray.length==0)
	    {
	        System.out.println("GetPotentialReturnForMultiples : Error: No Odds provided. Please provide Odd(s) in array format for calculating potential return.");
	    }

	    // Converting the fractional odds strings to its actual value(+1)
	   // FGenerateOdds(ref oddsArray);
	    oddsCount = (oddsArray.length-1) + 1;

	    //Calculating the total singles of all the odds
	    for (cnt = 0; cnt <= oddsArray.length-1; cnt++)
	    {
	        singles = singles + stake * (Double.parseDouble(oddsArray[cnt]));
	    }

	    // Gathering the required multi (s) based on number of odds provided
	    if (oddsCount == 2)
	    {
	        dbls = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	    }
	    else if (oddsCount == 3)
	    {
	        dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[0]);
	        dbls = dbl1 + dbl2 + dbl3;

	        trebles = ((stake* Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	    }
	    else if (oddsCount == 4)
	    {
	        dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0]);
	        dbl5 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[1]);
	        dbl6 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[0]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6;

	        trbl1 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0]);
	        trbl4 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4;

	        acc4 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	    }
	    else if (oddsCount == 5)
	    {
	        dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
	        dbl5 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl6 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
	        dbl7 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
	        dbl8 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl9 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        dbl10 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10;

	        trbl1 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl4 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl5 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl6 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trbl7 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl8 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trbl9 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[1]);
	        trbl10 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10;

	        acc4_1 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc4_2 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_3 = (((stake* Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_4 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        acc4_5 = (((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5;

	        acc5 = ((((stake* Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	    }
	    else if (oddsCount == 6)
	    {
	        dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
	        dbl5 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[5]);
	        dbl6 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl7 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
	        dbl8 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
	        dbl9 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5]);
	        dbl10 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl11 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        dbl12 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
	        dbl13 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        dbl14 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        dbl15 = (stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10 + dbl11 + dbl12 + dbl13 + dbl14 + dbl15;

	        trbl1 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl4 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl5 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl6 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl7 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl8 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl9 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl10 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl11 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl12 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[1]);
	        trbl13 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl14 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl15 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trbl16 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trbl17 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl18 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[1]);
	        trbl19 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[2]);
	        trbl20 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10 + trbl11 + trbl12 + trbl13 + trbl14 + trbl15 + trbl16 + trbl17 + trbl18 + trbl19 + trbl20;

	        acc4_1 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc4_2 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        acc4_3 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
	        acc4_4 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_5 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_6 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_7 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_8 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_9 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_10 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_11 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_12 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_13 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_14 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_15 = (((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5 + acc4_6 + acc4_7 + acc4_8 + acc4_9 + acc4_10 + acc4_11 + acc4_12 + acc4_13 + acc4_14 + acc4_15;

	        acc5_1 = ((((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc5_2 = ((((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc5_3 = ((((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        acc5_4 = ((((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        acc5_5 = ((((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        acc5_6 = ((((stake * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc5 = acc5_1 + acc5_2 + acc5_3 + acc5_4 + acc5_5 + acc5_6;

	        acc6 = (((((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	    }

	    String multiBetType = betType.toUpperCase().trim();
	    switch (multiBetType.toUpperCase())
	    {
	        case "SINGLE":
	            potentialRet = singles;
	            break;
	        case "DOUBLE":
	            potentialRet = dbls;
	            break;
	        case "TREBLE":
	            potentialRet = trebles;
	            break;
	        case "ACCUMULATOR (4)":
	            potentialRet = acc4;
	            break;
	        case "TRIXIE":
	            potentialRet = dbls + trebles;
	            break;
	        case "PATENT":
	            potentialRet = singles + dbls + trebles;
	            break;
	        case "YANKEE":
	            potentialRet = dbls + trebles + acc4;
	            break;
	        case "LUCKY 15":
	            potentialRet = singles + dbls + trebles + acc4;
	            break;
	        case "CANADIAN":
	            potentialRet = dbls + trebles + acc4 + acc5;
	            break;
	        case "LUCKY 31":
	            potentialRet = singles + dbls + trebles + acc4 + acc5;
	            break;
	        case "ACCUMULATOR (5)":
	        	  potentialRet = Math.round(acc5*100.0)/100.0;
	            break;
	        case "ACCUMULATOR (6)":
                potentialRet = Math.round(acc6*100.0)/100.0;
                break;
	        case "HEINZ":
	            potentialRet = dbls + trebles + acc4 + acc5 + acc6;
	            break;
	        case "LUCKY 63":
	            potentialRet = singles + dbls + trebles + acc4 + acc5 + acc6;
	            break;
	        case "FORECAST":
	            break;
	        case "REVERSE FORECAST":
	            break;
	        case "COMBINATION FORECAST":
	            break;
	        case "TRICAST":
	            break;
	        case "COMBINATION TRICAST":
	            break;
	        default:
	            break;
	    } //end of switch
	       return potentialRet;
	}
	
	public static double GetMultiplesOdds(String[] oddsArray, String betType)
	{
	    int cnt, oddsCount;
	    double potentialRet = 0.00, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10, dbl11, dbl12, dbl13, dbl14, dbl15;
	    double trbl1, trbl2, trbl3, trbl4, trbl5, trbl6, trbl7, trbl8, trbl9, trbl10, trbl11, trbl12, trbl13, trbl14, trbl15, trbl16, trbl17, trbl18, trbl19, trbl20;
	    double acc4 = 0.00, acc4_1, acc4_2, acc4_3, acc4_4, acc4_5, acc4_6, acc4_7, acc4_8, acc4_9, acc4_10, acc4_11, acc4_12, acc4_13, acc4_14, acc4_15;
	    double acc5 = 0.00, acc6 = 0.00, acc5_1, acc5_2, acc5_3, acc5_4, acc5_5, acc5_6 = 0.00;
	    double singles = 0.00;
	    double trebles = 0.00;
	    double dbls = 0.00;

	    if (oddsArray.length==0)
	    {
	        System.out.println("GetPotentialReturnForMultiples : Error: No Odds provided. Please provide Odd(s) in array format for calculating potential return.");
	    }

	    // Converting the fractional odds strings to its actual value(+1)
	   
	    oddsCount = (oddsArray.length-1) + 1;

	    //Calculating the total singles of all the odds
	    for (cnt = 0; cnt <= oddsArray.length-1; cnt++)
	    {
	        singles = singles + (Double.parseDouble(oddsArray[cnt]));
	    }

	    // Gathering the required multi (s) based on number of odds provided
	    if (oddsCount == 2)
	    {
	        dbls = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	    }
	    else if (oddsCount == 3)
	    {
	        dbl1 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[0]);
	        dbls = dbl1 + dbl2 + dbl3;

	        trebles = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	    }
	    else if (oddsCount == 4)
	    {
	        dbl1 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0]);
	        dbl5 = (Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[1]);
	        dbl6 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[0]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6;

	        trbl1 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0]);
	        trbl4 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4;

	        acc4 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	    }
	    else if (oddsCount == 5)
	    {
	        dbl1 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
	        dbl5 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl6 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
	        dbl7 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
	        dbl8 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl9 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        dbl10 = (Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10;

	        trbl1 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl4 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl5 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl6 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trbl7 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl8 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trbl9 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[1]);
	        trbl10 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10;

	        acc4_1 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc4_2 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_3 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_4 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
	        acc4_5 = (((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5;

	        acc5 = ((((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	    }
	    else if (oddsCount == 6)
	    {
	        dbl1 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        dbl2 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        dbl3 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
	        dbl4 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
	        dbl5 = (Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[5]);
	        dbl6 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        dbl7 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
	        dbl8 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
	        dbl9 = (Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5]);
	        dbl10 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        dbl11 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        dbl12 = (Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
	        dbl13 = (Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        dbl14 = (Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        dbl15 = (Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10 + dbl11 + dbl12 + dbl13 + dbl14 + dbl15;

	        trbl1 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl2 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl3 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl4 = ((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl5 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        trbl6 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl7 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl8 = ((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl9 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        trbl10 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl11 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl12 = ((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[1]);
	        trbl13 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        trbl14 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        trbl15 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trbl16 = ((Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        trbl17 = ((Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        trbl18 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[1]);
	        trbl19 = ((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[2]);
	        trbl20 = ((Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
	        trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10 + trbl11 + trbl12 + trbl13 + trbl14 + trbl15 + trbl16 + trbl17 + trbl18 + trbl19 + trbl20;

	        acc4_1 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc4_2 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
	        acc4_3 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
	        acc4_4 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_5 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_6 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_7 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_8 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_9 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_10 = (((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_11 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc4_12 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
	        acc4_13 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_14 = (((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4_15 = (((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5 + acc4_6 + acc4_7 + acc4_8 + acc4_9 + acc4_10 + acc4_11 + acc4_12 + acc4_13 + acc4_14 + acc4_15;

	        acc5_1 = ((((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
	        acc5_2 = ((((Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	        acc5_3 = ((((Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]);
	        acc5_4 = ((((Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
	        acc5_5 = ((((Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
	        acc5_6 = ((((Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
	        acc5 = acc5_1 + acc5_2 + acc5_3 + acc5_4 + acc5_5 + acc5_6;

	        acc6 = (((((Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
	    }

	    String multiBetType = betType.toUpperCase().trim();
	    switch (multiBetType.toUpperCase())
	    {
	        case "SINGLE":
	            potentialRet = singles;
	            break;
	        case "DOUBLE":
	            potentialRet = dbls;
	            break;
	        case "TREBLE":
	            potentialRet = trebles;
	            break;
	        case "ACCUMULATOR (4)":
	            potentialRet = acc4;
	            break;
	        case "TRIXIE":
	            potentialRet = dbls + trebles;
	            break;
	        case "PATENT":
	            potentialRet = singles + dbls + trebles;
	            break;
	        case "YANKEE":
	            potentialRet = dbls + trebles + acc4;
	            break;
	        case "LUCKY 15":
	            potentialRet = singles + dbls + trebles + acc4;
	            break;
	        case "CANADIAN":
	            potentialRet = dbls + trebles + acc4 + acc5;
	            break;
	        case "LUCKY 31":
	            potentialRet = singles + dbls + trebles + acc4 + acc5;
	            break;
	        case "ACCUMULATOR (5)":
	        	  potentialRet = Math.round(acc5*100.0)/100.0;
	            break;
	        case "HEINZ":
	            potentialRet = dbls + trebles + acc4 + acc5 + acc6;
	            break;
	        case "LUCKY 63":
	            potentialRet = singles + dbls + trebles + acc4 + acc5 + acc6;
	            break;
	        case "FORECAST":
	            break;
	        case "REVERSE FORECAST":
	            break;
	        case "COMBINATION FORECAST":
	            break;
	        case "TRICAST":
	            break;
	        case "COMBINATION TRICAST":
	            break;
	        default:
	            break;
	    } //end of switch
	       return potentialRet;
	}
	
public double GetPotentialReturnsForEW(String[] oddsArray, String EWterm, double stake, String multiBetType) throws Exception
    {
	String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
	String[] EWterms = EWterm.split("&");
    double returnValue = 0.0, ewReturn = 0.0, multiPotentialRet = 0.0;
    double odds, odds2, odds3;
    int count = EWterms.length, i = 0;
    double[] EWodds = new double[count];
        try
        {
        	for (i = 0; i < count; i++)
            {
                String[] ewSplit = EWterms[i].split("&");
                if (ProjectName.contains("Desktop_Swedish"))
                {
                    EWterms[i] = EWterms[i].substring(19,23);
                }
                else if (EWterm.contains("Odds"))
                {
                    EWterms[i] = EWterms[i].substring(14,18);

                }
                else
                {
                    EWterms[i] = EWterms[i].substring(16, 19);
                }
                    
                String[] ewSplit1 = EWterms[i].split("/");
                EWodds[i] = Double.parseDouble(ewSplit1[0]) / Double.parseDouble(ewSplit1[1]);
                double roundOff = Math.round(EWodds[i] * 100.0) / 100.0;
                EWodds[i] = roundOff;
            }


            if (multiBetType.toLowerCase() == "single" || multiBetType.toLowerCase() == "")
            {
                odds = Convert.ToDouble(oddsArray[0]);
                //Potential Returns (Singles with each way) = 2*Stake+ (Stake*latest available odds)+(Fraction*Stake* latest available odds)
                ewReturn = (2 * stake + (stake * (odds - 1)) + (EWodds[0] * stake * (odds - 1)));
                //System.Console.Write("\r\nEW returns" + ewReturn + "\r\nOdds" + odds);
            }

            else
            {
                multiPotentialRet = GetPotentialReturnForMultiples(oddsArray, stake, multiBetType);
                
                switch (multiBetType.toLowerCase())
                {
                    case "double":
                        odds = Convert.ToDouble(oddsArray[0]);
                        odds2 = Convert.ToDouble(oddsArray[1]);
                        ewReturn = ((((odds - 1) * EWodds[0]) + 1) * (((odds2 - 1) * EWodds[1]) + 1) * stake);
                        break;

                    case "treble":
                        odds = Convert.ToDouble(oddsArray[0]);
                        odds2 = Convert.ToDouble(oddsArray[1]);
                        odds3 = Convert.ToDouble(oddsArray[2]);
                        ewReturn = ((((odds - 1) * EWodds[0]) + 1) * (((odds2 - 1) * EWodds[1]) + 1) * (((odds3 - 1) * EWodds[2]) + 1) * stake);
                        break;
                }
            }
            returnValue = multiPotentialRet + ewReturn;
           return returnValue;
        }
        catch (Exception ex)
        {
          Assert.fail(ex.getMessage(), ex);
        }
        return returnValue;
    }

public double ConvertEWOdds(String EachwayTerms)
{
    String EWoddstr1 = GetEachWayOdd(EachwayTerms);
    String[] arrEWodds = EWoddstr1.split("/");
    double EWodds = Double.parseDouble(arrEWodds[0]) / Double.parseDouble(arrEWodds[1]);
    return Math.round(EWodds*100)/100;
}

public String GetEachWayOdd(String EWTerm)
{
    try
    {
        String[] EWTermArr;
        String[] stringSeparators = new String[] { TextControls.placeText };
        EWTermArr = EWTerm.split(TextControls.placeText);
        stringSeparators[0] = TextControls.ewoddsText;
        EWTermArr = EWTermArr[0].split(stringSeparators[0]);
        return EWTermArr[1].trim();
   }
    catch (Exception ex)
    {
        System.out.println("Function 'GetEachWayOdd' - Failed");
        return null;
    }
}

}

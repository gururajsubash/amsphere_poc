package com.ladbrokes.commonclasses;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;

public class BetslipFunctions extends DriverCommon {
	Common common = new Common();
	LoginLogoutFunctions LoginfunctionObj = new LoginLogoutFunctions();
	PotentialReturnFunctions PRT = new PotentialReturnFunctions();
	TestdataFunction testdata = new TestdataFunction();

	@Test
	public void FootballLandingPagePlaceBet_function() throws Exception {
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			// ** Get the Bet slip Count
			int initBetslipCount = common.GetBetSlipCount();
			click(HomeGlobalElements.SportLandingPageOdd);
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.BetPlacement_WithAcceptNContinue("Single", TextControls.stakeValue, 1);
			System.out.println("BetPlacement from Class page(Football) successfull");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void TennisTypePagePlaceBet_function() throws Exception {
		try {
			launchweb();
			common.NavigateToEventSubType(TextControls.Tennis, true, false);
			int initBetslipCount = common.GetBetSlipCount();
			if (!isElementDisplayed(By.xpath("//div[@class='basic-scoreboard']"))) {
				click(HomeGlobalElements.SportLandingPageOdd);
			} else {
				System.out.println("Type have only one event so there is no type page");
				click(BetslipElements.edpOddClick);
			}
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.BetPlacement_WithAcceptNContinue("Single", TextControls.stakeValue, 1);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void BasketballSubTypePagePlaceBet_function() throws Exception {
		try {
			launchweb();
			common.NavigateToEventSubType(TextControls.Basketball, false, true);
			int initBetslipCount = common.GetBetSlipCount();
			if (!isElementDisplayed(By.xpath("//div[@class='basic-scoreboard']"))) {
				click(HomeGlobalElements.SportLandingPageOdd);
			} else {
				System.out.println("Type have only one event so there is no type page");
				click(BetslipElements.edpOddClick);
			}
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			Common.Login();
			common.OddSwitcher(TextControls.decimal);
			common.BetPlacement_WithAcceptNContinue("Single", TextControls.stakeValue, 1);
			System.out.println("BetPlacement from SubType page(Basketball) successfull");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyAnySportsEDPPlaceBet_function(String className) throws Exception {
		try {
			// Navigate to EDP
			common.NavigateToEventSubType(className, false, true);
			if(isElementPresent(BetslipElements.eventClick))
			{
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				int initBetslipCount = common.GetBetSlipCount();
				// Add selection to BetSlip
				if (isElementPresent(BetslipElements.EDPHeader)) {
					click(BetslipElements.edpOddClick);
				} 
				else {

					//scrollToView(BetslipElements.eventClick);
					clickByJS(getDriver().findElement(BetslipElements.eventClick));
					//click(BetslipElements.eventClick);
					click(BetslipElements.edpOddClick);

				}
				// Validate the BetSlip counter on adding selection to BetSlip
				int laterBetslipCnt = common.GetBetSlipCount();
				Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
						"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
								+ laterBetslipCnt + ".");
				// Enter stake and place bet
				common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
				System.out.println("BetPlacement from EDP page(Footballball) successfull");
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyBetplacement_HomePage_SingleBet_Function() throws Exception {
		try {
			Common.Login();
			common.OddSwitcher(TextControls.decimal);
			int initBetslipCount = common.GetBetSlipCount();
			// Add selection to BetSlip
			if(isElementPresent(BetslipElements.HomePage_RightColumnRaces))
				click(BetslipElements.HomePage_RightColumnRaces);
			else
				click(BetslipElements.HomePage_RightColumnEvents);
			// Validate the BetSlip counter on adding selection to BetSlip
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			// Enter stake and place bet
			common.BetPlacement_WithAcceptNContinue("Single", TextControls.stakeValue, 1);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public double GetTotalsFromBetslip(String stakeORreturnORfreebet) throws Exception {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		double returnVal = 0.00;
		String val = null;
		try {
			// Return Total Stake
			if (stakeORreturnORfreebet.toLowerCase().contains(TextControls.stakeText.toLowerCase()))
				val = GetElementText(BetslipElements.totalStake);
			// Return Total free bets
			else if (stakeORreturnORfreebet.toLowerCase().contains("free"))
				val = GetElementText(BetslipElements.totalFreeBets);
			// Return Total potential returns
			else
				val = GetElementText(BetslipElements.totalPotentialReturns);
			if (val.equals("N/A"))
				returnVal = 0.0;
			else if (ProjectName.contains("Desktop_Swedish")) {
				val = val.replace(",", ".");
			}
			returnVal = Double.parseDouble(val.replace(TextControls.currencySymbol, ""));
			System.out.println("User was able get Total " + stakeORreturnORfreebet + " from betslip");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return returnVal;
	}

	public void ChecktheBalance_topHeaderChanges_function() throws Exception {
		double initbalance, totalStake, actualBal, expectedBal;
		try {
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			int initBetslipCount = common.GetBetSlipCount();
			initbalance = LoginfunctionObj.GetBalance();
			// Add selection to BetSlip
			if(isElementPresent(BetslipElements.HomePage_RightColumnRaces))
				click(BetslipElements.HomePage_RightColumnRaces);
			else
				click(BetslipElements.HomePage_RightColumnEvents);
			// Validate the BetSlip counter on adding selection to BetSlip
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			Common.Enterstake(TextControls.stakeValue);
			totalStake = GetTotalsFromBetslip(TextControls.stakeText);
			Common.placebet();
			actualBal = LoginfunctionObj.GetBalance();
			expectedBal = initbalance - totalStake;
			Assert.assertTrue(actualBal == expectedBal, "Header Balance is not updated accordingly after Placing Bet");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public void VerifyMinMax_StakeField_Function() throws Exception {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String actErrorMsg, expErrorMsg;
		double maxStake1;
		double maxStake;
		double minStake;
		String minValue;
		String maxValue;
		try {
			int initBetslipCount = common.GetBetSlipCount();
			testdata.NavigateUsingTestData(1);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			/** Validate the Minimum Stake functionality */
			minValue = GetMinMaxStakeValue("min");
			System.out.println("Min Value : " + minValue);
			minStake = Double.parseDouble(minValue);
			double minStake1 = minStake - 0.01;
			String stake = String.valueOf(minStake1);
			getDriver().findElement(BetslipElements.stakeBox).clear();
			Entervalue(BetslipElements.stakeBox, stake);
			actErrorMsg = CaptureBetslipErrorMessage(BetslipElements.minStakeErrorMessage);
			// To handle Swedish expErrorMsg its different from other languages<Sneha>
			if (ProjectName.contains("Desktop_Swedish")) 
				expErrorMsg = TextControls.minStakeErrText + minValue.replace(".", ",") + " "
						+ TextControls.currencySymbol;
			else
				expErrorMsg = TextControls.minStakeErrText + TextControls.currencySymbol + minValue;
			Assert.assertTrue(actErrorMsg.toLowerCase().trim().equals(expErrorMsg.toLowerCase().trim()),
					"Mismatch in Actual and Expected error messages (Min Stake)");
			System.out.println("Error message for Minimum stake was validated successfully");
			/** Validate the Maximum Stake functionality */
			maxValue = GetMinMaxStakeValue("max");
			maxStake = Double.parseDouble(maxValue);
			maxStake1 = maxStake + 1.00;
			System.out.println("maxstake:" + maxStake);
			getDriver().findElement(BetslipElements.stakeBox).clear();
			Entervalue(BetslipElements.stakeBox, String.valueOf(maxStake1));
			if(isElementDisplayed(BetslipElements.maxStakeErrorMessage)){
				actErrorMsg = CaptureBetslipErrorMessage(BetslipElements.maxStakeErrorMessage);
				if (ProjectName.contains("Desktop_Swedish")) 
					expErrorMsg = TextControls.maxStakeErrText + maxValue.replace(".", ",") + " "
							+ TextControls.currencySymbol;
				else
					expErrorMsg = TextControls.maxStakeErrText + TextControls.currencySymbol + maxValue;

				Assert.assertTrue(actErrorMsg.toLowerCase().trim().equals(expErrorMsg.toLowerCase().trim()),
						"Mismatch in Actual and Expected error messages (Max Stake)");
				System.out.println("Error message for Maximum stake was validated successfully");}
			else
				System.out.println("OA is turned on , Hence Error message for Maximum stake was not validated");

		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public String GetMinMaxStakeValue(String MinMaxReturn) throws Exception {
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String minValue, maxValue, returnVal = null;
		// double returnVal = 0.0;
		String[] valArr;
		int arrLen = 0;
		try {
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			click(BetslipElements.SelInBetSlip);
			Thread.sleep(3000);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertTrue(isElementDisplayed(BetslipElements.betInfoContainer),
					"Bet Info container is not present");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			// List <WebElement> element =
			// getDriver().findElements(BetslipElements.betInfoContainer);
			if (MinMaxReturn.toLowerCase() == "min") {
				minValue = GetElementText(BetslipElements.minValueInBetslip);
				if (ProjectName.contains("Desktop_Swedish")) // To handle min
					// returnval in
					// Swedish<Sneha/02/03/2017>
				{
					valArr = minValue.split(" ");
					arrLen = valArr.length;
					minValue = valArr[arrLen - 2];
					returnVal = minValue.replace(",", ".").trim();
				} else {
					valArr = minValue.split(" ");
					arrLen = valArr.length;
					minValue = valArr[arrLen - 1];
					System.out.println("Min value : " + minValue);
					returnVal = minValue;
				}
				returnVal = returnVal.replace(TextControls.currencySymbol, " ").trim();
			} else {
				maxValue = GetElementText(BetslipElements.maxValueInBetslip);
				if (ProjectName.contains("Desktop_Swedish")) // To handle min
					// returnval in
					// Swedish<Sneha/02/03/2017>
				{
					valArr = maxValue.split(" ");
					arrLen = valArr.length;
					maxValue = valArr[arrLen - 2];
					returnVal = maxValue.replace(",", ".").trim();
				} else {
					valArr = maxValue.split(" ");
					arrLen = valArr.length;
					maxValue = valArr[arrLen - 1];
					returnVal = maxValue;
				}
				returnVal = returnVal.replace(TextControls.currencySymbol, "");
				System.out.println("Max Value:" + returnVal);
			}
			click(BetslipElements.SelInBetSlip);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return returnVal;
	}

	/**
	 * This Method fetches error text from BetSlip Returns Error Message
	 **/
	public String CaptureBetslipErrorMessage(By locator) {
		Assert.assertTrue(isElementDisplayed(locator), "Error message is not Present");
		return GetElementText(locator);
	}

	public void VerifyInplaySingleBet_Function() throws Exception {
		Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		click(By.xpath("//div[@class='odds-button live'])[2]"));
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		common.BetPlacement_WithAcceptNContinue(TextControls.single, TextControls.stakeValue, 1);
	}

	public int GetMultiplesBetCount() throws Exception {
		int multiBetCnt = 100;
		try {
			int betSlipCount = common.GetBetSlipCount();
			switch (betSlipCount) {
			case 1:
				return multiBetCnt = 0;
			case 2:
				return multiBetCnt = 1;
			case 3:
				return multiBetCnt = 4;
			case 4:
				return multiBetCnt = 5;
			case 5:
				return multiBetCnt = 6;
			case 6:
				return multiBetCnt = 7;
			case 7:
				return multiBetCnt = 7;
				// =================================
			case 8:
				return multiBetCnt = 8;
			case 9:
				return multiBetCnt = 8;
			case 10:
				return multiBetCnt = 9;
			case 11:
				return multiBetCnt = 10;
			case 12:
				return multiBetCnt = 11;
			case 13:
				return multiBetCnt = 12;
			case 14:
				return multiBetCnt = 13;
			case 15:
				return multiBetCnt = 14;
				// ==================================
			default:
				return multiBetCnt;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
			return multiBetCnt;
		}
	}

	public String GetSelectionID(String eventName, String marketName, String selection) {
		String xPath, selIDstring, selID = null;
		String[] selIDArr;
		try {
			if (eventName.toLowerCase().contains(" vs ") || eventName.toLowerCase().contains(" v ")
					|| eventName.contains("'") || eventName.toLowerCase().contains("-"))
				xPath = "//div[@class='market-information-selection']/span[contains(text(), '" + selection
				+ "')]/../following-sibling::div[contains(text(), '" + marketName
				+ "')]/../following-sibling::div//input";
			else
				xPath = "//div[@class='market-information-selection']/span[contains(text(), '" + selection
				+ "')]/../following-sibling::div[contains(text(), '" + marketName
				+ "')]/following-sibling::div[normalize-space(text()= '" + eventName
				+ "')]/../following-sibling::div//input";
			System.out.println(xPath);
			selIDstring = getAttribute(By.xpath(xPath), "id");
			selIDArr = selIDstring.split("-");
			selID = selIDArr[selIDArr.length - 1];
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return selID;
	}

	public double Factorial(int anyNum) {
		try {
			// Non-recursive factorial for
			// -170< anyNum >170
			int i;
			double factorial = 0;
			if (anyNum < -170 || anyNum > 170) {
				return factorial = 0;
			}
			/*
			 * if ( anyNum != anyNum) { factorial = -1; for (i = -2; i >=
			 * anyNum; i--) { factorial = factorial * i; } return factorial; }
			 */
			else {
				factorial = 1;
				for (i = 2; i <= anyNum; i++) {
					factorial = factorial * i;
				}
				return factorial;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
			return 0;
		}
	}

	public void VerifyBetSlip(String eventName, String selection, String marketName, String Odds, String EWterms,
			String multiBetType, int MultipleBetCount, String[] arayOdds) throws Exception {
		double actTotalPotentialReturns, expTotalPotentialReturns = 0.0, actTotalStake, actPotentialReturn,
				expTotalStake = 0.00, expPotentialReturn = 0.00;
		int betLines;
		String spPR, stakeXPath, prStaticText, ewStaticText, xPath, multibetABR = "", multipleBetCount, ewXpath = null,
				selID, multiBetDesc;
		double stake = Convert.ToDouble(TextControls.stakeValue);
		try {
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipTitle), "Betslip is not displayed");
			if (multiBetType.isEmpty() || multiBetType.toLowerCase().contains("single")) {
				selID = GetSelectionID(eventName, marketName, selection);
				stakeXPath = "//input[@id='slip-odds-stake-" + selID + "']";
				ewXpath = stakeXPath + "/../../..//div[@class='each-way-container']/input";
				prStaticText = stakeXPath + "/../../..//div//span[text()='" + TextControls.PRText + "']";
				ewStaticText = stakeXPath + "/../../..//label[text()='" + TextControls.ewTermsText + "']";
				// ========================================
				int betCount = common.GetBetSlipCount();
				String includeMultiples = stakeXPath
						+ "/../../..//div[@class='stake-information']//label[contains(text(), '"
						+ TextControls.includeText + "')]";
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				if (betCount > 1) {
					Assert.assertTrue(isElementDisplayed(By.xpath(includeMultiples)),
							"Include Multiples is not present for " + selection);
					Assert.assertTrue(isChecked(By.xpath(includeMultiples + "//preceding-sibling::input")),
							"Include Multiples is NOT checked by default for " + selection);
				} else
					Assert.assertFalse(isElementPresent(By.xpath(includeMultiples)),
							"Include Multiples is not present for " + selection + " when it is the only selection");
			} else {
				multibetABR = common.GetMultiplesBetAbbreviation(multiBetType);
				xPath = "//div[@class='multiples-container']//span[contains(text(), '" + multibetABR
						+ "')]/../..//following-sibling::div";
				stakeXPath = xPath + "[@class='stake-box-container']//input";
				ewXpath = xPath + "//input[@type='checkbox']";
				prStaticText = xPath + "//div//div[text()='" + TextControls.PRText + "']";
				ewStaticText = xPath + "//label[text()='" + TextControls.ewTermsText + "']";
				// Getting expected Number of lines
				betLines = CalculateBetLines(multiBetType);
				multiBetDesc = "//div[@class='multiples-container']//span[contains(text(), '" + multibetABR
						+ "') and contains(text(), 'x" + betLines + "')]";
				Assert.assertTrue(isElementDisplayed(By.xpath(multiBetDesc)),
						multibetABR + "(x" + betLines + ") was not displayed in the Betslip");
				multipleBetCount = "//span[contains(text(), '" + TextControls.multiplesText
						+ "') and contains(text(), '(" + MultipleBetCount + ")')]";
				Assert.assertTrue(isElementDisplayed(By.xpath(multipleBetCount)),
						TextControls.multiplesText + "-" + MultipleBetCount + " was not displayed in the Betslip");
				// Verify Multiples Odds
				if (arayOdds != null) {
					String multiodds = "//div[@class='multiples-container']//div//span[@class='market-information-selection' and contains(text(), '"
							+ multibetABR + "')]/../..//div[@class='betslip-text odds-convert']";
					String actOdds = common.GetElementText(By.xpath(multiodds));
					double expOdds = PotentialReturnFunctions.GetMultiplesOdds(arayOdds, multiBetType);
					double actualOdds = Convert.ToDouble(actOdds);
					Assert.assertTrue(actualOdds <= expOdds, "Mismatch in Multiple Odds for " + multiBetType
							+ " Actual: " + actOdds + " , Expected: " + expOdds);
				}
				// ====================================
				// Verify the Multiple Info
				String multibetInfo = multiBetDesc + "/following-sibling::span[@class='market-information-event']";
				System.out.println("multibetInfo : " + multibetInfo);
				String expInfo = GetMultiplesInfo(multiBetType);
				if (TextUtils.isEmpty(expInfo)) {
					String actMultibetInfo = GetElementText(By.xpath(multibetInfo));
					Assert.assertTrue((actMultibetInfo.isEmpty() || actMultibetInfo.equals(null)),
							"Multiple info is present for " + multibetInfo);
				} else {
					String actInfo = GetElementText(By.xpath(multibetInfo));
					Assert.assertTrue(actInfo.equalsIgnoreCase(expInfo), "Mismatch in Multiple Info for " + multiBetType
							+ "Actual: " + actInfo + " , Expected: " + expInfo);
				}
			}
			Assert.assertTrue(isElementDisplayed(By.xpath(prStaticText)),
					"'Potentail' static text was not present in the betslip for " + multibetABR + "-" + selection);
			Assert.assertTrue(isElementDisplayed(By.xpath(stakeXPath)),
					"'Stake' field was not present in the betslip for " + multibetABR + "-" + selection);
			if (!EWterms.isEmpty() || EWterms.equals(null)) {
				if (EWterms.toUpperCase() == "NO") {
					getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					Assert.assertFalse(isElementDisplayed(By.xpath(ewXpath)),
							"'EW' checkbox is present in the betslip for " + multibetABR + "-" + selection);
				} else
					Assert.assertTrue(isElementDisplayed(By.xpath(ewXpath)),
							"'EW' checkbox was not present in the betslip for " + multibetABR + "-" + selection);
			}
			// Verify the Total Stake in betSlip
			actTotalStake = GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(actTotalStake == expTotalStake,
					"Mismatch in Actual and Expected Total stake in Betslip. Expected-'" + expTotalStake + "', Actual-'"
							+ actTotalStake + "'");
			if (Odds.toUpperCase().contains("SP")) {
				spPR = GetPotentialReturnFromBetSlip_SP(eventName, selection, marketName, Odds, multiBetType);
				// Assert.assertTrue(spPR.toUpperCase().contains("N/A"),
				// "Mismatch in Actual and Expected Potential Return in Betslip.
				// Expected-'" + expPotentialReturn + "', Actual-'" + spPR +
				// "'");
				// According to issue MOB-9052 , PO is happy with it
				Assert.assertTrue(spPR.contains("0.00"),
						"Mismatch in Actual and Expected Potential Return in Betslip. Expected-'" + expPotentialReturn
						+ "', Actual-'" + spPR + "'");
				spPR = GetElementText(BetslipElements.totalPotentialReturns);
				if (spPR.contains("0.0"))
					spPR = "N/A";
			} else {
				// actPotentialReturn = GetPotentialReturnFromBetSlip(eventName,
				// selection, marketName, Odds, multiBetType);
				actPotentialReturn = GetTotalsFromBetslip(TextControls.PRText);
				Assert.assertTrue(actPotentialReturn <= expPotentialReturn,
						"Mismatch in Actual and Expected Potential Return in Betslip. Expected-'N/A', Actual-'"
								+ actPotentialReturn + "'");
				actTotalPotentialReturns = GetTotalsFromBetslip("Returns");
				Assert.assertTrue(actTotalPotentialReturns == expTotalPotentialReturns,
						"Mismatch in Actual and Expected Total Potential Return in Betslip . Expected-'"
								+ expTotalPotentialReturns + "', Actual-'" + actTotalPotentialReturns + "'");
			}
			System.out.println(
					"User was able to see the betslip details for Event - selection '" + eventName + "-" + selection);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public String GetPotentialReturnFromBetSlip_SP(String eventName, String selection, String marketName, String odds,
			String multiBetType) throws Exception {
		String val = null, selId, multibetABR, xPath;
		if (!TextUtils.isEmpty(odds))
			try {
				// "Get PR for Event - selection '" + eventName + "-" +
				// selection ,"User should be able to get the PR for Event -
				// selection '" + eventName + "-" + selection
				if (multiBetType.isEmpty() || multiBetType.toLowerCase().contains("single")) {
					selId = GetSelectionID(eventName, marketName, selection);
					// xPath = "//input[@id='slip-odds-stake-" + selId +
					// "']/../../..//div[@data-bind='currencyFormat:
					// potentialReturns']";
					// xPath = "//input[@id='slip-odds-stake-" + selId +
					// "']/../../..//div[@data-bind='currencyFormat:
					// boostedPotentialReturns']";
					xPath = "//input[@id='slip-odds-stake-" + selId
							+ "']/../../..//div/span[@data-bind='currencyFormat: boostedPotentialReturns']";
					val = GetElementText(By.xpath(xPath));
					val = val.replace(TextControls.currencySymbol, "");
				} else {
					multibetABR = common.GetMultiplesBetAbbreviation(multiBetType);
					xPath = "//div[@class='multiples-container']//span[contains(text(), '" + multibetABR
							+ "')]/../..//following-sibling::div//div[@class='amount']";
					val = GetElementText(By.xpath(xPath));
				}
				System.out.println("User was able to get the PR for Event - selection '" + eventName + "-" + selection);
			} catch (Exception | AssertionError e) {
				Assert.fail(e.getMessage(), e);
			}
		return val;
	}

	public double GetPotentialReturnFromBetSlip(String eventName, String selection, String marketName, String odds,
			String multiBetType) throws Exception {
		String val = null;
		double returnVal = 0.0;
		odds = String.valueOf(odds);
		if (!TextUtils.isEmpty(odds))
			;
		try {
			val = GetPotentialReturnFromBetSlip_SP(eventName, selection, marketName, odds, multiBetType);
			// ========================
			if (val == "N/A")
				returnVal = 0.0;
			// =========================
			else {
				if (getDriver().findElement(LoginElements.userBalance).isDisplayed()) {
					String semireturnVal = val.replace(TextControls.currencySymbol, "").trim();
					semireturnVal = semireturnVal.replace(',', '.');
					returnVal = Double.parseDouble(semireturnVal);
				} else
					returnVal = Double.parseDouble(val);
			}
			System.out.println("User was able to get the PR for Event - selection '" + eventName + "-" + selection);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return returnVal;
	}

	public int CalculateBetLines(String betType) throws Exception {
		double numerator;
		double denom;
		// 'Taking the betSlip count for knowing the number of selections added
		// to the betslip so that we can calculate the total stake for different
		// bet types
		int betSlipCount = common.GetBetSlipCount();
		int betLines = 0;
		switch (betType.toUpperCase()) {
		case "SINGLE":
			return betLines = 1;
		case "DOUBLE":
			numerator = Factorial(betSlipCount);
			denom = Factorial(2) * Factorial(betSlipCount - 2);
			betLines = (int) (numerator / denom);
			return betLines;
		case "TREBLE":
			numerator = Factorial(betSlipCount);
			denom = Factorial(3) * Factorial(betSlipCount - 3);
			betLines = (int) (numerator / denom);
			return betLines;
		case "ACCUMULATOR (4)":
			numerator = Factorial(betSlipCount);
			denom = Factorial(4) * Factorial(betSlipCount - 4);
			betLines = (int) (numerator / denom);
			return betLines;
		case "TRIXIE":
			return betLines = 4;
		case "PATENT":
			return betLines = 7;
		case "YANKEE":
			return betLines = 11;
		case "LUCKY 15":
			return betLines = 15;
		case "CANADIAN":
			return betLines = 26;
		case "LUCKY 31":
			return betLines = 31;
		case "ACCUMULATOR (5)":
			numerator = Factorial(betSlipCount);
			denom = Factorial(5) * Factorial(betSlipCount - 5);
			betLines = (int) (numerator / denom);
			return betLines;
		case "ACCUMULATOR (6)":
			numerator = Factorial(betSlipCount);
			denom = Factorial(6) * Factorial(betSlipCount - 6);
			betLines = (int) (numerator / denom);
			return betLines;
		case "ACCUMULATOR (7)":
			numerator = Factorial(betSlipCount);
			denom = Factorial(7) * Factorial(betSlipCount - 7);
			betLines = (int) (numerator / denom);
			return betLines;
		case "HEINZ":
			return betLines = 57;
		case "SUPER HEINZ":
			return betLines = 120;
		case "LUCKY 63":
			return betLines = 63;
		case "FORECAST":
		case "REVERSE FORECAST":
		case "COMBINATION FORECAST":
		case "TRICAST":
		case "COMBINATION TRICAST":
		default:
			return betLines = 1;
		}
	}

	public String GetMultiplesInfo(String multiBetType) throws Exception {
		String returnVal = null;
		try {
			int betcount = common.GetBetSlipCount();
			switch (multiBetType.toLowerCase()) {
			case "double":
			case "treble":
			case "accumulator (4)":
			case "accumulator (5)":
			case "accumulator (6)":
			case "accumulator (7)":
				break;
			case "trixie":
				returnVal = TextControls.trixieInfoText;
				break;
			case "patent":
				returnVal = TextControls.patentInfoText;
				break;
			case "yankee":
				returnVal = TextControls.yankeeInfoText;
				break;
			case "lucky 15":
				returnVal = TextControls.lucky15InfoText;
				break;
			case "canadian":
				returnVal = TextControls.canadianInfoText;
				break;
			case "lucky 31":
				returnVal = TextControls.lucky31InfoText;
				break;
			case "heinz":
				returnVal = TextControls.heinzInfoText;
				break;
			case "lucky 63":
				returnVal = TextControls.lucky63InfoText;
				break;
			case "super heinz":
				returnVal = TextControls.superHeinzInfoText;
				break;
			default:
				Assert.fail(
						multiBetType + ": Invalid Multiple BetType passed. Condition handled for only 7 selections");
				break;
			}
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return returnVal;
	}

	public <T> Class<?> GetType(T[] array) {
		return array.getClass().getComponentType();
	}

	public double GetMultiplesOdds(String oddsArray[], double stake, String betType) {
		int cnt, oddsCount;
		double potentialRet = 0.00, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10, dbl11, dbl12, dbl13,
				dbl14, dbl15;
		double trbl1, trbl2, trbl3, trbl4, trbl5, trbl6, trbl7, trbl8, trbl9, trbl10, trbl11, trbl12, trbl13, trbl14,
		trbl15, trbl16, trbl17, trbl18, trbl19, trbl20;
		double acc4 = 0.00, acc4_1, acc4_2, acc4_3, acc4_4, acc4_5, acc4_6, acc4_7, acc4_8, acc4_9, acc4_10, acc4_11,
				acc4_12, acc4_13, acc4_14, acc4_15;
		double acc5 = 0.00, acc6 = 0.00, acc5_1, acc5_2, acc5_3, acc5_4, acc5_5, acc5_6 = 0.00;
		double singles = 0.00;
		double trebles = 0.00;
		double dbls = 0.00;
		// Getting array type
		GetType(oddsArray);
		// Boolean isArrayFlag = type.IsArray;
		if (oddsArray.length == 0) {
			System.out.println(
					"GetPotentialReturnForMultiples : Error: No Odds provided. Please provide Odd(s) in array format for calculating potential return.");
		}
		// Converting the fractional odds strings to its actual value(+1)
		oddsCount = (oddsArray.length - 1) + 1;
		// Calculating the total singles of all the odds
		for (cnt = 0; cnt <= oddsArray.length - 1; cnt++) {
			singles = singles + stake * (Double.parseDouble(oddsArray[cnt]));
		}
		// Gathering the required multi (s) based on number of odds provided
		if (oddsCount == 2) {
			dbls = (Double.parseDouble(oddsArray[0])) * (Double.parseDouble(oddsArray[1]));
		} else if (oddsCount == 3) {
			dbl1 = (Double.parseDouble(oddsArray[0]) * Double.parseDouble(oddsArray[1]));
			dbl2 = (Double.parseDouble(oddsArray[1]) * Double.parseDouble(oddsArray[2]));
			dbl3 = (Double.parseDouble(oddsArray[2]) * Double.parseDouble(oddsArray[0]));
			dbls = dbl1 + dbl2 + dbl3;
			trebles = (Double.parseDouble(oddsArray[0]) * Double.parseDouble(oddsArray[1])
					* Double.parseDouble(oddsArray[2]));
		} else if (oddsCount == 4) {
			dbl1 = (Double.parseDouble(oddsArray[0]) * Double.parseDouble(oddsArray[1]));
			dbl2 = (Double.parseDouble(oddsArray[1]) * Double.parseDouble(oddsArray[2]));
			dbl3 = (Double.parseDouble(oddsArray[2]) * Double.parseDouble(oddsArray[3]));
			dbl4 = (Double.parseDouble(oddsArray[3]) * Double.parseDouble(oddsArray[0]));
			dbl5 = (Double.parseDouble(oddsArray[3]) * Double.parseDouble(oddsArray[1]));
			dbl6 = (Double.parseDouble(oddsArray[2]) * Double.parseDouble(oddsArray[0]));
			dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6;
			trbl1 = (Double.parseDouble(oddsArray[0]) * Double.parseDouble(oddsArray[1])
					* Double.parseDouble(oddsArray[2]));
			trbl2 = (Double.parseDouble(oddsArray[1]) * Double.parseDouble(oddsArray[2])
					* Double.parseDouble(oddsArray[3]));
			trbl3 = (Double.parseDouble(oddsArray[2]) * Double.parseDouble(oddsArray[3])
					* Double.parseDouble(oddsArray[0]));
			trbl4 = (Double.parseDouble(oddsArray[3]) * Double.parseDouble(oddsArray[0])
					* Double.parseDouble(oddsArray[1]));
			trebles = trbl1 + trbl2 + trbl3 + trbl4;
			acc4 = (Double.parseDouble(oddsArray[0]) * Double.parseDouble(oddsArray[1])
					* Double.parseDouble(oddsArray[2]) * Double.parseDouble(oddsArray[3]));
		} else if (oddsCount == 5) {
			dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
			dbl2 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
			dbl3 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
			dbl4 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
			dbl5 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
			dbl6 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
			dbl7 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
			dbl8 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
			dbl9 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
			dbl10 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10;
			trbl1 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2]);
			trbl2 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3]);
			trbl3 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl4 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3]);
			trbl5 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl6 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[0]);
			trbl7 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl8 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[0]);
			trbl9 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[1]);
			trbl10 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[0]);
			trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10;
			acc4_1 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
			acc4_2 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			acc4_3 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			acc4_4 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]);
			acc4_5 = (((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
			acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5;
			acc5 = ((((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
		} else if (oddsCount == 6) {
			dbl1 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]);
			dbl2 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]);
			dbl3 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]);
			dbl4 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]);
			dbl5 = (stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[5]);
			dbl6 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]);
			dbl7 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]);
			dbl8 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]);
			dbl9 = (stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5]);
			dbl10 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
			dbl11 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
			dbl12 = (stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
			dbl13 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			dbl14 = (stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
			dbl15 = (stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			dbls = dbl1 + dbl2 + dbl3 + dbl4 + dbl5 + dbl6 + dbl7 + dbl8 + dbl9 + dbl10 + dbl11 + dbl12 + dbl13 + dbl14
					+ dbl15;
			trbl1 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2]);
			trbl2 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3]);
			trbl3 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl4 = ((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5]);
			trbl5 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3]);
			trbl6 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl7 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5]);
			trbl8 = ((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[0]);
			trbl9 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			trbl10 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5]);
			trbl11 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[0]);
			trbl12 = ((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[1]);
			trbl13 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5]);
			trbl14 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[0]);
			trbl15 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[0]))
					* Double.parseDouble(oddsArray[1]);
			trbl16 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]))
					* Double.parseDouble(oddsArray[1]);
			trbl17 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2]);
			trbl18 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[1]);
			trbl19 = ((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[2]);
			trbl20 = ((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[0]))
					* Double.parseDouble(oddsArray[2]);
			trebles = trbl1 + trbl2 + trbl3 + trbl4 + trbl5 + trbl6 + trbl7 + trbl8 + trbl9 + trbl10 + trbl11 + trbl12
					+ trbl13 + trbl14 + trbl15 + trbl16 + trbl17 + trbl18 + trbl19 + trbl20;
			acc4_1 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]);
			acc4_2 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[4]);
			acc4_3 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[5]);
			acc4_4 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			acc4_5 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
			acc4_6 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4_7 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			acc4_8 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
			acc4_9 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4_10 = (((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4_11 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]);
			acc4_12 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[5]);
			acc4_13 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4_14 = (((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4_15 = (((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
			acc4 = acc4_1 + acc4_2 + acc4_3 + acc4_4 + acc4_5 + acc4_6 + acc4_7 + acc4_8 + acc4_9 + acc4_10 + acc4_11
					+ acc4_12 + acc4_13 + acc4_14 + acc4_15;
			acc5_1 = ((((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4]);
			acc5_2 = ((((stake * Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5]);
			acc5_3 = ((((stake * Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[0]);
			acc5_4 = ((((stake * Double.parseDouble(oddsArray[3])) * Double.parseDouble(oddsArray[4]))
					* Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]))
					* Double.parseDouble(oddsArray[1]);
			acc5_5 = ((((stake * Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]))
					* Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2]);
			acc5_6 = ((((stake * Double.parseDouble(oddsArray[5])) * Double.parseDouble(oddsArray[0]))
					* Double.parseDouble(oddsArray[1])) * Double.parseDouble(oddsArray[2]))
					* Double.parseDouble(oddsArray[3]);
			acc5 = acc5_1 + acc5_2 + acc5_3 + acc5_4 + acc5_5 + acc5_6;
			acc6 = (((((stake * Double.parseDouble(oddsArray[0])) * Double.parseDouble(oddsArray[1]))
					* Double.parseDouble(oddsArray[2])) * Double.parseDouble(oddsArray[3]))
					* Double.parseDouble(oddsArray[4])) * Double.parseDouble(oddsArray[5]);
		}
		String multiBetType = (betType.toUpperCase()).trim();
		switch (multiBetType.toUpperCase()) {
		case "SINGLE":
			potentialRet = singles;
			break;
		case "DOUBLE":
			potentialRet = dbls;
			break;
		case "TREBLE":
			potentialRet = trebles;
			break;
		case "ACCUMULATOR (4)":
			potentialRet = acc4;
			break;
		case "TRIXIE":
			potentialRet = dbls + trebles;
			break;
		case "PATENT":
			potentialRet = singles + dbls + trebles;
			break;
		case "YANKEE":
			potentialRet = dbls + trebles + acc4;
			break;
		case "LUCKY 15":
			potentialRet = singles + dbls + trebles + acc4;
			break;
		case "CANADIAN":
			potentialRet = dbls + trebles + acc4 + acc5;
			break;
		case "LUCKY 31":
			potentialRet = singles + dbls + trebles + acc4 + acc5;
			break;
		case "ACCUMULATOR (5)":
			potentialRet = Math.round(acc5 * 100.0) / 100.0;
			break;
		case "ACCUMULATOR (6)":
			potentialRet = Math.round(acc6 * 100.0) / 100.0;
			;
			break;
		case "HEINZ":
			potentialRet = dbls + trebles + acc4 + acc5 + acc6;
			break;
		case "LUCKY 63":
			potentialRet = singles + dbls + trebles + acc4 + acc5 + acc6;
			break;
		case "FORECAST":
			break;
		case "REVERSE FORECAST":
			break;
		case "COMBINATION FORECAST":
			break;
		case "TRICAST":
			break;
		case "COMBINATION TRICAST":
			break;
		default:
			break;
		} // end of switch
		return potentialRet;
	}

	public void EnterStake(String eventName, String selection, String marketName, String stake, String multiBetType,
			Boolean EW) throws Exception {
		// if (!odds.isEmpty());
		try {
			WebElement inputStake;
			String stakeXPath, ewXpath, selID, multibetABR;
			// Check if stake is to be entered for Single bet
			if (multiBetType.isEmpty() || multiBetType.toLowerCase().contains("single")) {
				selID = GetSelectionID(eventName, marketName, selection);
				stakeXPath = "//input[@id='slip-odds-stake-" + selID + "']";
				ewXpath = "//input[@id='slip-odds-stake-" + selID
						+ "']/../../..//div[@class='each-way-container']/input";
			} else {
				multibetABR = common.GetMultiplesBetAbbreviation(multiBetType);
				stakeXPath = "//div[@class='multiples-container']//span[contains(text(), '" + multibetABR
						+ "')]/../..//*[@class='stake-box-container']//input";
				ewXpath = "//div[@class='multiples-container']//span[contains(text(), '" + multibetABR
						+ "')]/../..//following-sibling::div//input[@type='checkbox']";
			}
			// Enter Stake and EW
			inputStake = getDriver().findElement(By.xpath(stakeXPath));
			inputStake.clear();
			inputStake.sendKeys(stake);
			// check the Each Way checkBox
			if (EW == true) {
				if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
					click(BetslipElements.oddsBoostTooltipIcon);
				WebElement checkBox1 = getDriver().findElement(By.xpath(ewXpath));
				if (!checkBox1.isSelected()) {
					checkBox1.click();
				} else
					Assert.fail("EW checkbox for " + multiBetType + "-" + selection + "' was not found");
			}
			System.out.println("User was able to enter stake and EachWay is " + EW + " for selection '" + selection
					+ "'-'" + multiBetType + "' in Betslip");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	public String BetPlacement(Boolean SP) throws Exception {
		String betReceiptNo = null;
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		try {
			Thread.sleep(3000);
			click(BetslipElements.placeBet);
			Thread.sleep(2000);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertTrue(isElementDisplayed(BetslipElements.betReceiptBanner),
					"Bet receipt banner is not displayed in Bet Receipt");
			Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip),
					"Tick mark on Bet Receipt was not found");
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg),
					"Bet successfull message is not displayed in Bet Receipt");
			getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			betReceiptNo = GetElementText(BetslipElements.betReceiptNo);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return betReceiptNo;
	}

	public String GetSelectionName(String eventName) {
		String selectionName;
		int index;
		if (eventName.toLowerCase().contains("vs")) {
			index = eventName.toLowerCase().indexOf("vs");
			selectionName = eventName.substring(0, index - 1);
			if (selectionName.contains("'")) {
				String evtName = selectionName.replace("'", "XZ");
				String eventName1 = evtName.substring(0, evtName.indexOf("XZ"));
				String eventName2 = evtName.substring(evtName.indexOf("XZ") + 4);
				selectionName = eventName2;
			}
			return selectionName;
		} else if (eventName.toLowerCase().contains(" v ")) {
			index = eventName.toLowerCase().indexOf(" v ");
			selectionName = eventName.substring(0, index);
			if (selectionName.contains("'")) {
				String evtName = selectionName.replace("'", "XZ");
				String eventName1 = evtName.substring(evtName.indexOf("XZ"));
				String eventName2 = evtName.substring(evtName.indexOf("XZ") + 4);
				selectionName = eventName2;
			}
			return selectionName;
		}
		// To Handle Events in German Language
		else if (eventName.toLowerCase().contains("-")) {
			index = eventName.toLowerCase().indexOf("-");
			String eventname1 = eventName.substring(0, index);
			selectionName = eventname1.trim();
			if (selectionName.contains("'")) {
				String evtName = selectionName.replace("'", "XZ");
				String eventName1 = evtName.substring(0, evtName.indexOf("XZ"));
				String eventName2 = evtName.substring(evtName.indexOf("XZ") + 4);
				selectionName = eventName2.trim();
			}
			return selectionName;
		} else
			return selectionName = null;
	}

	public void VerifyBetDetails(String eventName, String selection, String marketName, String[] arrOdds, String stake,
			String EWterms, String multiBetType, String prevTotalStake, String prevTotalPR) throws Exception {
		double expFreebetsUsed = 0.0, actFreebetsUsed = 0.0, expPotentialReturn = 0.0, expTotalPotentialReturn,
				actTotalStake = 0.0, actPotentialReturn = 0.0, actTotalPotentialReturns = 0.0, expTotalStake;
		int betLines;
		String actTotalSPpot = "", expTotalSPpot = "", actSPpot = "", expSPpot = "", PRstake = stake;
		boolean bStatusFreebets = false;
		try {
			// Get the total stake from betSlip
			actTotalStake = GetTotalsFromBetslip(TextControls.stakeText);
			// Get the total potential returns from betslip, SP condition
			if (arrOdds[0].contains("SP")) {
				// Get the potential returns displayed along the selection side
				// in betslip
				actSPpot = GetPotentialReturnFromBetSlip_SP(eventName, selection, marketName, arrOdds[0], multiBetType);
				actTotalSPpot = GetElementText(BetslipElements.totalPotentialReturns);
			} else {
				actPotentialReturn = GetPotentialReturnFromBetSlip(eventName, selection, marketName, arrOdds[0],
						multiBetType);
				// Get the potential returns displayed along the selection side
				// in betslip
				actTotalPotentialReturns = GetTotalsFromBetslip(TextControls.PRetText);
			}
			// Get the free bets used
			if (bStatusFreebets == true) {
				actFreebetsUsed = GetTotalsFromBetslip("free");
			}
			// Getting expected Number of lines
			betLines = CalculateBetLines(multiBetType);
			// Get the expected potentials returns(based on EW)
			if (!EWterms.isEmpty()) {
				expPotentialReturn = PRT.GetPotentialReturnsForEW(arrOdds, EWterms, Double.parseDouble(PRstake),
						multiBetType);
				betLines = betLines * 2;
			} else if (arrOdds[0].contains("SP")) {
				expSPpot = "N/A";
				expTotalSPpot = "N/A";
			} else {
				expPotentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(arrOdds,
						Double.parseDouble(PRstake), multiBetType);
				double roundOff = Math.round(expPotentialReturn * 100.0) / 100.0;
				expPotentialReturn = roundOff;
			}
			// Get the expected total stake
			expTotalStake = Double.parseDouble(stake) * betLines;
			double roundOff = Math.round(expTotalStake * 100.0) / 100.0;
			expTotalStake = roundOff;
			// Add previous PR and Stake if passsed
			if (!prevTotalStake.isEmpty())
				expTotalStake = Double.parseDouble(prevTotalStake) + expTotalStake;
			expTotalPotentialReturn = expPotentialReturn;
			if (!prevTotalPR.isEmpty())
				expTotalPotentialReturn = Double.parseDouble(prevTotalPR) + expPotentialReturn;
			// Verify the Total Stake in betSlip
			Assert.assertTrue(actTotalStake == expTotalStake,
					"Mismatch in Actual and Expected Total stake in Betslip. Expected-'" + expTotalStake + "', Actual-'"
							+ actTotalStake + "'");
			// Verify the PR and total PR in betslip
			if (arrOdds[0].contains("SP")) {
				Assert.assertTrue(actSPpot.trim().equals(expSPpot.trim()),
						"Mismatch in Actual and Expected Potential Return in Quickslip. Expected-'" + expSPpot
						+ "', Actual-'" + actSPpot + "'");
				Assert.assertTrue(actTotalSPpot.trim().equals(expTotalSPpot.trim()),
						"Mismatch in Actual and Expected Potential Return in Quickslip. Expected-'" + expTotalSPpot
						+ "', Actual-'" + actTotalSPpot + "'");
			} else {
				if (String.valueOf(expPotentialReturn).contains("0.99")) {
					Assert.assertTrue(String.valueOf(actPotentialReturn) == String.valueOf(expPotentialReturn),
							"Mismatch in Actual and Expected Potential Return in Betslip. Expected-'"
									+ expPotentialReturn + "', Actual-'" + actPotentialReturn + "'");
					Assert.assertTrue(
							String.valueOf(actTotalPotentialReturns) == String.valueOf(expTotalPotentialReturn),
							"Mismatch in Actual and Expected Total Potential Return in Betslip . Expected-'"
									+ expTotalPotentialReturn + "', Actual-'" + actTotalPotentialReturns + "'");
				} else {
					Assert.assertTrue(actPotentialReturn <= expPotentialReturn,
							"Mismatch in Actual and Expected Potential Return in Betslip. Expected-'"
									+ expPotentialReturn + "', Actual-'" + actPotentialReturn + "'");
					Assert.assertTrue(actTotalPotentialReturns <= expTotalPotentialReturn,
							"Mismatch in Actual and Expected Total Potential Return in Betslip . Expected-'"
									+ expTotalPotentialReturn + "', Actual-'" + actTotalPotentialReturns + "'");
				}
			}
			// Verify freeBets used
			if (bStatusFreebets == true) {
				Assert.assertTrue(actFreebetsUsed == Math.round(expFreebetsUsed),
						"Mismatch in Actual and Expected FreeBets used in Betslip . Expected-'" + expFreebetsUsed
						+ "', Actual-'" + actFreebetsUsed + "'");
			}
			System.out.println("Bet details verified successfully for " + eventName + "-" + selection + "-" + stake);
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage());
		}
	}

	public String ValidateBetReceipt(String typeName, String eventName, String marketName, String selectionName,
			String[] oddsArr, String stake, String eachWay, Boolean SP, int betCount, String multiBetType)
					throws Exception {
		int numLines, multiLines;
		String xPath, betReceiptNo = null, odds, textToVerify, currDate, places, actualString = "", eachWayTerm,
				PRstake = stake, TempPRstake = "", NewtextToVerify = null, newXPath;
		double potentialReturn = 0.0, initialFreeBetBalance = 0.0, freebetStake = 0.0, gTaxStake = 0.0,
				freeBetGTaxStake = 0.0, intialBalance, laterBalance, totalStake, individualStake;
		boolean bStatusFreebets = false, bStatusGTax = false, bStatusFreebetGTax = false;
		String potReturn = null;
		String NewpotReturn = null;
		String NewpotReturn1 = null;
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		try {
			// Condition to handle German Tax
			if (isElementPresent(By.xpath("//div[@class='tax-disclaimer']"))) {
				bStatusGTax = true;
				PRstake = stake;
				gTaxStake = Convert.ToDouble(stake) - (Convert.ToDouble(stake) * (0.05));
			}
			// Getting the Initial Balance and actual Number of lines
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipTitle), "Betslip is not displayed");
			intialBalance = LoginfunctionObj.GetBalance();
			totalStake = GetTotalsFromBetslip(TextControls.stakeText);
			numLines = CalculateBetLines(multiBetType);
			multiLines = numLines;
			scrollPage("bottom");
			Thread.sleep(2000);
			click(BetslipElements.placeBet, "Place Bet is not found");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(2000);
			// Assert.assertTrue(isElementDisplayed(BetslipElements.betReceiptBanner),
			// "Bet receipt banner is not displayed in Bet Receipt");
			Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip),
					"Tick mark on Bet Receipt was not found");
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg),
					"Bet successfull message is not displayed in Bet Receipt");
			WebElement element = getDriver().findElement(BetslipElements.betReceiptContainer);
			actualString = element.getText().toLowerCase();
			// Checking for BetTime
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date dateobj = new Date();
			System.out.println(df.format(dateobj));
			currDate = df.format(dateobj);
			textToVerify = TextControls.timeText.toLowerCase() + " " + currDate + " ";
			getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			Assert.assertTrue(actualString.contains(textToVerify),
					"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
			// Checking for Bet Receipts No
			textToVerify = TextControls.receiptNoText.toLowerCase() + " o/";
			// Verifier. Assert.IsTrue(actualString.Contains(textToVerify), "'"
			// + textToVerify + "' text was NOT displyed in the Bet Receipt");
			betReceiptNo = GetElementText(BetslipElements.betReceiptNo);
			// Checking for Bet lines
			if (!TextUtils.isEmpty(eachWay))
				numLines = numLines * 2;
			// *** Verifying BetLines in Bet Receipt
			if (numLines > 1) {
				if (ProjectName.contains("Desktop_Swedish"))// Added condition
					// for Swedish as
					// potential return
					// has currency
					// located after
					// digits whether in
					// other langauge it
					// is before digits
				{
					TempPRstake = PRstake.replace('.', ',');
					textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " " + TempPRstake + " "
							+ TextControls.currencySymbol + " " + TextControls.preLineText + "";
				} else
					textToVerify = numLines + " " + TextControls.linesAtText.toLowerCase() + " "
							+ TextControls.currencySymbol + PRstake + " " + TextControls.preLineText.toLowerCase() + "";
			} else {
				if (ProjectName.contains("Desktop_Swedish"))// Added condition
					// for Swedish as
					// potential return
					// has currency
					// located after
					// digits whether in
					// other langauge it
					// is before digits
				{
					TempPRstake = PRstake.replace('.', ',');
					textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " " + TempPRstake + " "
							+ TextControls.currencySymbol + "";
				} else
					textToVerify = numLines + " " + TextControls.lineAtText.toLowerCase() + " "
							+ TextControls.currencySymbol + Double.parseDouble(PRstake) + "";
			}
			Assert.assertTrue(actualString.contains(textToVerify),
					"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
			System.out.println("Correct Betlines are displayed in Bet receipt.");
			// Checking for Stake for Each way
			if (!TextUtils.isEmpty(eachWay)) {
				// ** "Verifying Stake for Each way in Bet Receipt"
				individualStake = Double.parseDouble(stake) * 2;
				if (ProjectName.contains("Desktop_Swedish"))// Added condition
					// for Swedish as
					// potential return
					// has currency
					// located after
					// digits whether in
					// other langauge it
					// is before digits
				{
					String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
					textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " "
							+ TextControls.currencySymbol + "";
				} else
					textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TextControls.currencySymbol
					+ individualStake;
			} else {
				individualStake = Double.parseDouble(stake) * numLines;
				individualStake = Math.round(individualStake * 100.0) / 100.0;
				if (ProjectName.contains("Desktop_Swedish"))// Added condition
					// for Swedish as
					// potential return
					// has currency
					// located after
					// digits whether in
					// other langauge it
					// is before digits
				{
					String TempindividualStake = String.valueOf(individualStake).replace('.', ',');
					textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TempindividualStake + " "
							+ TextControls.currencySymbol + "";
				} else
					textToVerify = TextControls.ttlStakeForBetText.toLowerCase() + " " + TextControls.currencySymbol
					+ individualStake;
			}
			Assert.assertTrue(actualString.contains(textToVerify),
					"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
			System.out.println("Correct total stake for multilines are displayed in Bet receipt.");
			// ** Getting Potential Return only if Each way is false
			System.out.println(
					"Verifying  Potential Return for Each way in Bet Receipt, Potential Return for Each way should display in Bet receipt");
			if (SP == true)
				textToVerify = TextControls.PRetText.toLowerCase() + ": n/a";
			else {
				if (!TextUtils.isEmpty(eachWay)) {
					if (oddsArr.length > 1) {
						eachWay = getDriver()
								.findElement(By.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[1]"))
								.getText()
								+ "&"
								+ getDriver()
								.findElement(By
										.xpath("(//div[@data-bind='text: $parent.getEachWayTerms($data)'])[2]"))
								.getText();
					} else {
						eachWay = getDriver().findElement(By.xpath("//div[@data-bind='text: eachWayTermsInfo']"))
								.getText();
					}
					if (bStatusGTax == false)
						potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, Double.parseDouble(PRstake),
								multiBetType);
					else if (bStatusGTax == true && !TextUtils.isEmpty(eachWay))
						potentialReturn = PRT.GetPotentialReturnsForEW(oddsArr, eachWay, gTaxStake, multiBetType);
				} else
					potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr,
							Double.parseDouble(PRstake), multiBetType);
				// 5% is deducted in case if German customer
				if (bStatusGTax == true && TextUtils.isEmpty(eachWay)) {
					potentialReturn = PotentialReturnFunctions.GetPotentialReturnForMultiples(oddsArr, gTaxStake,
							multiBetType);
					// Assert.assertFalse((TextControls.germanTaxMsgText),
					// "German tax inline message is displayed for German User
					// on Bet receipt");
				}
				// ============================================================
				if (String.valueOf(potentialReturn).contains("0.99"))
					potReturn = String.valueOf(Math.round(potentialReturn));
				else {
					double roundOff = Math.round(potentialReturn * 100.0) / 100.0;
					potentialReturn = roundOff;
					potReturn = String.valueOf(potentialReturn);
					// Below NewpotReturn1 is added to handle Point decimal
					// difference in Potential Return (now it is verfying @
					// interger level)
					NewpotReturn = potReturn.replace(".", "XZ");
					NewpotReturn1 = NewpotReturn.substring(0, NewpotReturn.indexOf("XZ"));
				}
				// ============================================================
				if (ProjectName.contains("Desktop_Swedish"))// Added conditon
					// for swedish
				{
					textToVerify = TextControls.PRetText.toLowerCase() + ": " + potReturn.replace('.', ',') + " "
							+ TextControls.currencySymbol;
					NewtextToVerify = TextControls.PRetText.toLowerCase() + ": " + NewpotReturn1;
				} else {
					textToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol + potReturn;
					NewtextToVerify = TextControls.PRetText.toLowerCase() + ": " + TextControls.currencySymbol
							+ NewpotReturn1;
				}
			}
			if (actualString.contains(textToVerify)) {
				Assert.assertTrue(actualString.contains(textToVerify),
						"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
			} else {
				Assert.assertTrue(actualString.contains(NewtextToVerify),
						"'" + NewtextToVerify + "' text was NOT displyed in the Bet Receipt");
			}
			System.out.println("Correct total potential returns for multilines are displayed in Bet receipt.");
			if (TextUtils.isEmpty(multiBetType) || multiBetType.toLowerCase().contains("single")) {
				// Checking for Event Details
				// For HR and GH, TypeName is displayed in the betSlip
				if (!TextUtils.isEmpty(typeName)) {
					if (typeName.contains("'")) {
						String[] typeArr = typeName.replace("'", "|").split("|");
						textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " "
								+ typeArr[0].toLowerCase();
						Assert.assertTrue(actualString.contains(textToVerify),
								"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
						textToVerify = typeArr[1].toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n"
								+ marketName.toLowerCase();
						Assert.assertTrue(actualString.contains(textToVerify),
								"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
						// goto a ;
					} else
						textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + " "
								+ typeName.toLowerCase() + "\r\n" + selectionName.toLowerCase() + "\r\n"
								+ marketName.toLowerCase();
				} else {
					textToVerify = TextControls.single.toLowerCase() + "\r\n" + eventName.toLowerCase() + "\r\n"
							+ selectionName.toLowerCase() + "\r\n" + marketName.toLowerCase();
					// Market doesn't appear for WDW market
					if (TextUtils.isEmpty(marketName))
						textToVerify = TextControls.single.toLowerCase() + " - " + eventName.toLowerCase() + "\r\n"
								+ selectionName.toLowerCase();
				}
				// Assert.assertTrue(actualString.contains(textToVerify), "'" +
				// textToVerify + "' text was NOT displyed in the Bet Receipt");
				// Checking for Each way
				if (!TextUtils.isEmpty(eachWay)) {
					textToVerify = TextControls.ewoddsText.toLowerCase() + " " + PRT.GetEachWayOdd(eachWay) + " "
							+ TextControls.placeText.toLowerCase() + " " + GetEachWayPlaces(eachWay);
					Assert.assertTrue(actualString.contains(textToVerify),
							"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
				}
				System.out.println(
						"Verify selection details in  Bet Receipt , Selection details must be displayed in Bet Receipt");
				// Checking for SP
				if (SP == true) {
					// textToVerify = TextControls.oddsText.ToLower() +
					// ":\r\nsp";
					textToVerify = TextControls.oddsText.toLowerCase() + ": sp";
					Assert.assertTrue(actualString.contains(textToVerify),
							"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
				} else
					// Checking for Odds
				{
					odds = String.valueOf((Double.parseDouble(oddsArr[0])));
					textToVerify = TextControls.oddsText.toLowerCase() + ": " + odds;
					Assert.assertTrue(actualString.contains(textToVerify),
							"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
				}
			} else {
				String[] selArr = selectionName.split("-");
				String[] eventArr = eventName.split("-");
				// String[] typeArr = eventName.split("-");
				String multibetABR = null;
				multibetABR = common.GetMultiplesBetAbbreviationOnReceipit(multiBetType);
				// ===================================================
				// Checking for BetType
				textToVerify = multibetABR.toLowerCase() + " (x" + multiLines + ")";
				Assert.assertTrue(actualString.contains(textToVerify),
						"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
				// =========================================================
				/*
				 * // Checking for Each way string[] ewText = new
				 * string[selArr.Length]; if (!string.IsNullOrEmpty(eachWay)) {
				 * string[] arrEW = eachWay.Split('|');
				 * 
				 * for (int i = 0; i < arrEW.Length; i++) { eachWayTerm =
				 * GetEachWayOdd(arrEW[i]); places = GetEachWayPlaces(arrEW[i]);
				 * places = places.Substring(places.Length - 1).Trim();
				 * ewText[i] = " , " + TextControls.ewTermsText.ToLower() + ": "
				 * + eachWayTerm + " " + TextControls.oddsText.ToLower() + " - "
				 * + places + " " +
				 * TextControls.multiplesBetReceiptPlaceText.ToLower();
				 * 
				 * } }
				 */
				// Verify the selections
				for (int i = 0; i < selArr.length; i++) {
					textToVerify = (i + 1) + " . " + selArr[i].toLowerCase() + " (" + eventArr[i].toLowerCase() + "";
					if (actualString.contains(" +1.0 "))
						actualString = actualString.replace(" +1.0 ", " ");
					Assert.assertTrue(actualString.contains(textToVerify),
							"'" + textToVerify + "' text was NOT displyed in the Bet Receipt");
				}
			}
			System.out.println("Selection Details in Bet receipt.");
			// Looking for Total stake
			if (ProjectName.contains("Desktop_Swedish"))// Added condition for
				// Swedish as potential
				// return has currency
				// located after digits
				// whether in other
				// langauge it is before
				// digits
			{
				xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText
						+ "')]/following-sibling::div[@class='amount' and contains(text(), '"
						+ Convert.ToString(totalStake).replace('.', ',') + " " + TextControls.currencySymbol + "')]";
			} else
				xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlStakeText
				+ "')]/following-sibling::div[@class='amount' and contains(text(), '"
				+ TextControls.currencySymbol + totalStake + "')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total stake: " + TextControls.currencySymbol
					+ totalStake + "' text was is not present in the Bet Receipt");
			// Looking for Total PR
			if (!SP) {
				if (ProjectName.contains("Desktop_Swedish"))// Added condition
					// for Swedish as
					// potential return
					// has currency
					// located after
					// digits whether in
					// other langauge it
					// is before digits
				{
					xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText
							+ "')]/following-sibling::div[@class='amount' and contains(text(), '"
							+ potReturn.replace('.', ',') + " " + TextControls.currencySymbol + "')]";
					newXPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText
							+ "')]/following-sibling::div[@class='amount' and contains(text(), '" + NewpotReturn1
							+ "')]";
				} else {
					xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText
							+ "')]/following-sibling::div[@class='amount' and contains(text(), '"
							+ TextControls.currencySymbol + potReturn + "')]";
					newXPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText
							+ "')]/following-sibling::div[@class='amount' and contains(text(), '"
							+ TextControls.currencySymbol + NewpotReturn1 + "')]";
				}
				if (isElementPresent(By.xpath(xPath)))
					Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Total potential return: "
							+ TextControls.currencySymbol + potReturn + "' text was is not present in the Bet Receipt");
				else
					Assert.assertTrue(isElementDisplayed(By.xpath(newXPath)),
							"'Total potential return: " + TextControls.currencySymbol + NewpotReturn1
							+ "' text was is not present in the Bet Receipt");
			} else {
				xPath = "//div[@class='name' and contains(text(),'" + TextControls.ttlPRText
						+ "')]/following-sibling::div[@class='amount' and contains(text(), 'N/A')]";
				Assert.assertTrue(isElementDisplayed(By.xpath(xPath)),
						"'Total potential return: N/A' text was is not present in the Bet Receipt");
			}
			// Validate the Total Stake & Total Bets
			xPath = "//span[@class='your-bets' and contains(text(),'" + TextControls.urBetsText + "')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'Your bet is not present in the Bet Receipt");
			// Check Balance after bet placement
			laterBalance = LoginfunctionObj.GetBalance();
			intialBalance = intialBalance - totalStake;
			double roundOff = Math.round(intialBalance * 100.0) / 100.0;
			intialBalance = roundOff;
			Thread.sleep(1000);
			/*
			 * if (bStatusFreebets == true || bStatusFreebetGTax == true) {
			 * double laterFreeBetBalance =
			 * LoginfunctionObj.GetFreeBetBalance(); initialFreeBetBalance =
			 * initialFreeBetBalance - freebetStake; initialFreeBetBalance =
			 * FormatNumber(initialFreeBetBalance, 2); Verifier.
			 * Assert.IsTrue((laterFreeBetBalance == initialFreeBetBalance),
			 * "FreeBet Balance Validation failed after Bet Placement. Expected:"
			 * + initialFreeBetBalance + ", Actual:" + laterFreeBetBalance +
			 * "."); if (laterFreeBetBalance > 0) Verifier.
			 * Assert.IsTrue(wAction.IsElementPresent(driver,
			 * By.XPath(LoginLogoutControls.freeBetBalance)),
			 * "Remaining Freebet balance is displayed after placing a freebet"
			 * ); else Verifier. Assert.IsFalse(wAction.IsElementPresent(driver,
			 * By.XPath(LoginLogoutControls.freeBetBalance)),
			 * "Freebet balance is displayed after redeeming all freebets"); }
			 * else
			 */
			Assert.assertTrue(laterBalance <= intialBalance,
					"Mismatch in balance. Actual: " + laterBalance + ", Expected: " + intialBalance);
			System.out.println("Betplacement was successful and Bet receipt details was validated");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return betReceiptNo;
	}

	public String GetEachWayPlaces(String EWTerm) {
		try {
			// String[] stringSeparators = new String[] { TextControls.placeText
			// };
			String[] EWplaces = EWTerm.split(TextControls.placeText);
			String places = EWplaces[1];
			return places.trim();
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
			return null;
		}
	}

	public void VerifyMultipleBetContainer(List<String> betTypes, String XpathValue) throws Exception {
		Thread.sleep(2000);
		List<WebElement> multiBetTypes = getDriver().findElements(By.xpath(XpathValue));
		if (multiBetTypes.size() > 1) {
			for (int i = 0; i < multiBetTypes.size(); i++) {
				System.out.println("--->>" + multiBetTypes.get(i).getText() + "==" + betTypes.get(i));
				Assert.assertTrue((multiBetTypes.get(i).getText().equals(betTypes.get(i))),
						"On adding appropriate selections from different events to betslip required multi bet types are not displayed on BetSlip");
			}
			// Logger.Pass("On adding expected number of selections to betslip
			// appropriate multiple bet types are displayed on BetSlip");
		} else
			Assert.fail(
					"On adding expected number of selections to betslip appropriate multiple bet types are not displayed on BetSlip");
	}

	public void VerifyOddsBoostOnBetslip() throws Exception {
		try {
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostTittle),
					"Odds Boost Titte is not displayed on Betslip");
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostTooltip),
					"Odds Boost Tooltip is not displayed on Betslip");
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostTooltipIcon),
					"Odds Boost Tooltip icon 'i' is not displayed on Betslip");
			//click(BetslipElements.oddsBoostTooltipIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostTooltipInfo),
					"on clicking tooltip , odds boost information is not displayed");
			click(BetslipElements.oddsBoostTooltipIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostButton),
					"Odds Boost button is not displayed on Betslip");
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostButtonIcon),
					"Odds Boost button  icon  is not displayed on Betslip");
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}
}

package com.ladbrokes.commonclasses;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ladbrokes.EnglishOR.OBTIElements;
import com.ladbrokes.Utils.ReadTestSettingConfig;

public class LoginLogoutFunctions extends DriverCommon {
	Common common = new Common();
	
	/** 
	 * @return Balance
	 * @throws Exception element not found
	 */
	
	
	public double GetBalance() throws Exception{
		
		String bal = null;
		double returnVal = 0.0 ;
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
        try
        {
        	getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        	Thread.sleep(4000);
        	bal = GetElementText(LoginElements.userBalance);
        	Assert.assertTrue(bal.contains(TextControls.currencySymbol), "Currency is not displyed in User Balance: '" + bal + "'");
        	if(ProjectName.contains("Desktop_Swedish")){
        		bal = bal.replace(",", ".");
        	}
       	returnVal = Double.parseDouble(bal.replace(TextControls.currencySymbol, ""));
        }
        catch ( Exception | AssertionError  e){
			Assert.fail(e.getMessage(), e);
		}
		return (returnVal);
	}
	
	/** VerifyInsufficientBalance test function
	 * @return none
	 * @throws Exception element not found
	 */
	public void VerifyBetPlacementWithInsufficientBalance_function()throws Exception{
		 double intiBalance, addStake;
		try
		{
			Common.Login();
			common.OddSwitcher("decimal");
			intiBalance = GetBalance();
			addStake = intiBalance + 1.00 ;
			String stake1 = String.valueOf(addStake);
			
			if (intiBalance >= 0.1)
             Assert.fail("Balance is having sufficient fund, so we can not check error message for 'insufficient fund' ");
            
			common.NavigateToEventSubType(TextControls.Football, false, true);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			 //WebElement Event = getDriver().findElement(bet);
			 //scrollToView(BetslipElements.eventClick);
			click(BetslipElements.eventClick);
			click(BetslipElements.edpOddClick);
			common.Enterstake(stake1);
			common.placebet();
            Assert.assertTrue(isElementDisplayed(LoginElements.insuffFundsText), "Insufficient fund error message is not displayed");
            
			
		}
		catch ( Exception | AssertionError  e){
			Assert.fail(e.getMessage(), e);
		}
	}
	
	public void VerifyHelpLinks() throws Exception{
		String mainWindow = getDriver().getWindowHandle();
		click(HomeGlobalElements.HelpIcon, "Help link is not displyed on login");
        List<WebElement> helpTopMenuLinks = getDriver().findElements(By.xpath("//ul[@class='topmenu-links' and contains(@data-bind,'foreach: links.help')]//li"));
        for (int i = 0; i < helpTopMenuLinks.size(); i++)
        {
            helpTopMenuLinks = getDriver().findElements(By.xpath("//ul[@class='topmenu-links' and contains(@data-bind,'foreach: links.help')]//li"));
            String helpLinkName = helpTopMenuLinks.get(i).getText();
            helpTopMenuLinks.get(i).click();
            Thread.sleep(2000);
            switch (helpLinkName){
            	case "Help Centre" :
            		SwitchToPage("Home");
            		break;
            	case "Responsible Gambling": 
              //	if(isElementPresent(By.xpath("//div[@class='page__title fn-page-title ' and contains(text(), 'Responsible Gambling')]")))
            		SwitchToPage("Responsible Gambling");
            		break;
            	case "Rules":
            		SwitchToPage("All Sports Rules");
            		break;
            	case "Results":
            		String actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs);
            		actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
            		String expBreadCrums = TextControls.Home + ">" + "Results" ;
            		Assert.assertTrue(actBreadcrumbs.contains(expBreadCrums), "Failed to verify '" + helpLinkName + "' link");
            		if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
            			click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
            			if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
            				click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
            		click(HomeGlobalElements.HelpIcon);
            		break;
            	case "Bet Calculator":
            		SwitchToPage("Free betting calculator");
            		break;
            	case "Terms And Conditions":
            		SwitchToPage("Website Terms And Condition");
            		break;
            	case "Banking":
            		switchwindow();
            		Assert.assertTrue(isElementDisplayed(By.xpath("//h2[contains(text(),'Payments')]")), "Failed to switch to Banking Window");
            		getDriver().close();
            		getDriver().switchTo().window(mainWindow);
            		break;
            	case "Shop Locator":
            		SwitchToPage("Ladbrokes");
            		break;
            	case "Restricted Territories":
            		SwitchToPage("Restricted Territories");
            		break;
            	case "Contact Us":
            		SwitchToPage("Contact Us");
            		break;
         }
            System.out.println("Verifying for Help link '" + helpLinkName + "' on Login");
        }
	}
	
	public static void LoginOB_TI() throws Exception {

		try {
			String userName = ReadTestSettingConfig.getTestsetting("UserName2");
			String password = ReadTestSettingConfig.getTestsetting("Password2");
			String url_OBTI = ReadTestSettingConfig.getTestsetting("BaseURL2");
			getDriver().get(url_OBTI);
            Entervalue(OBTIElements.username_TI , userName);
			Entervalue(OBTIElements.password_TI , password);
			click(OBTIElements.login_TI);
			
		} catch ( Exception | AssertionError  e) {
			Assert.fail("LoginOB_TI failed", e);
		}
	}
	
	public static void login(String username , String password){
		try {
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
			Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			
			if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
				if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
					click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
				Thread.sleep(4000);
		} catch ( Exception | AssertionError  e) {
			Assert.fail("LoginOB_TI failed", e);
		}
		}
	}


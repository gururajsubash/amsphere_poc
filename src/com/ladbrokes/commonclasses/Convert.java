package com.ladbrokes.commonclasses;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Convert {
	public static BigDecimal truncateDecimal(double x, int numberofDecimals) {
		if (x > 0) {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
		} else {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
		}
	}

	public static String ToString(double d) {
		String val = null;
		val = String.valueOf(d);
		return val;
	}

	public static double ToDouble(String replace) {
		double val = 0.00;
		val = Double.parseDouble(replace);
		return val;
	}

	public static double roundOff(double d) {
		return  Math.round(d * 100.0) / 100.0 ;
	}

	public static Date toDate(String value) throws Exception {
		DateFormat format = new SimpleDateFormat("HH:mm E dd MMM");
		Date date = format.parse(value);
		String val = format.format(date);
		return format.parse(val);
	}

	public static Date toDateMonth(String value) throws Exception {
		DateFormat format = new SimpleDateFormat("dd/MM");
		Date date = format.parse(value);
		String val = format.format(date);
		return format.parse(val);
	}
}

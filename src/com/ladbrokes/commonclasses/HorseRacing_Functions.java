package com.ladbrokes.commonclasses;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class HorseRacing_Functions extends DriverCommon {
	
	Common common= new Common();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	
	public String AddAndVerifyFutureRacingSelectionInBetslip(String className, String typeName, String eventName, String marketName, String selectionName) throws  Exception
	{
		String odds ;
		JavascriptExecutor js = (JavascriptExecutor)getDriver();
		 common.NavigateToSportsPage(className);
		 /*if (eventName.contains("'"))
         {
             String eventNme = eventName.replace("'", "XZ");
             String eventName1 = eventNme.substring(0, eventNme.indexOf("XZ") - 1);
             eventName = eventName1;
         }*/
       String eventXpath = "//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming-meeting']//a//span[@class='racing-upcoming-meeting-name' and text() = "+"\"" + eventName+ "\"" + "]";
       
       WebElement element = getDriver().findElement(By.xpath(eventXpath));
       js.executeScript("arguments[0].scrollIntoView(true);", element);
       Thread.sleep(3000);
       js.executeScript("window.scrollBy(0,-200)", "");
       click(By.xpath(eventXpath));
       
          int intiBetCount = common.GetBetSlipCount();
          String marketXpath = "//header[@class='expand-list market-header expanded']/span[@class='title' and contains(text() ,'" + marketName + "')]";

          
       /** Add selection to BetSlip */
      
          String selectionXpath = marketXpath + "//../..//div[text()='" + selectionName + "']/..//span[@class='odds-convert']";
           odds =  getDriver().findElement(By.xpath(selectionXpath)).getText();
           
           click(By.xpath(selectionXpath));
           Thread.sleep(2000);
           int laterBetslipCnt = common.GetBetSlipCount();
           Assert.assertTrue((intiBetCount + 1 == laterBetslipCnt),"Mismatch in Betslip count on adding a selction, Expected:" + (intiBetCount + 1) + ", Actual:" + laterBetslipCnt + ".");
           
           //** Verify selection added to betSlip
           String selXpath = "//div[@class='market-information-selection']/span[contains(text(), '" + selectionName + "')]/../following-sibling::div[contains(text(), '" + marketName + "')]/following-sibling::div[contains(text(), '" + eventName + " " + typeName + "')]";
           if (!isElementDisplayed(By.xpath(selXpath)))
               selXpath = "//div[@class='market-information-selection']/span[contains(text(), '" + selectionName + "')]/../following-sibling::div[contains(text(), '" + marketName + "')]/following-sibling::div[contains(text(), '" + eventName + "')]";
         Assert.assertTrue(isElementDisplayed(By.xpath(selXpath)), "Selection-Event-Market '" + selectionName + "-" + eventName + "-" + marketName + "' was not found in the Betslip");
           
      //** odds are displayed in a different format for HR events
        // String oddSPxPath = selXpath + "/../following-sibling::div//div[@class='betslip-text odds-convert' and text()='" + odds + "']";
         String oddSPxPath = selXpath + "/../following-sibling::div//div[@class='betslip-text odds-convert']//div/div[text()='" + odds + "']";
         getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
         if (!isElementPresent(By.xpath(oddSPxPath)))
             oddSPxPath = selXpath + "/../following-sibling::div//select/option[text()='" + odds + "']";
        Assert.assertTrue(isElementDisplayed(By.xpath(oddSPxPath)), "Selection-Event-Market-Odd '" + selectionName + "-" + eventName + "-" + marketName + "-" + odds + "' was not found in the Betslip");
		return odds;
     
	}

	
	
	 public void NavigateToRacingEventWith_FcTcMarket(String className, String typeName, String eventName , boolean foreTriCast) throws Exception
     {
		 JavascriptExecutor js = (JavascriptExecutor)getDriver();
         try
         {
        	 common.NavigateToSportsPage(className);
             String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
            WebElement Element = getDriver().findElement(By.xpath(hrEvent));
            
//            js.executeScript("arguments[0].scrollIntoView(true);", Element);
//            Thread.sleep(3000);
//            js.executeScript("window.scrollBy(0,-200)", "");
//            click(By.xpath(hrEvent) , " "+ className +" event not found");
            clickByJS(Element);
            if(foreTriCast == true)
            {
            Assert.assertTrue(isElementDisplayed(HorseRacingElements.FcTc_Tab), "Forecast/Tricast market not found");
            click(HorseRacingElements.FcTc_Tab);
            }
            else 
            	click(HorseRacingElements.otherMarketTab);
	}
         catch ( Exception | AssertionError  e){
				Assert.fail(e.getMessage(), e);
			}
        	 }
	 
public void VerifyRacingEventBuildRaceCard_function(String className) throws Exception{
			String breadCrumsClass , raceNum ;
			int j = 1, k = 3, raceCount, counter = 0;
			JavascriptExecutor js = (JavascriptExecutor)getDriver();
			try
			{
	// ** Verify Select HR Events and build Race card , User should be able to select and build the race card **
				js.executeScript("window.scrollBy(0,500)", "");
	   List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
	   for (int x = 0; x < venueList.size(); x++)
     {
         venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
        if (counter == 1)
             break;
        List<WebElement>raceList =  getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
        if (raceList.size() >= 3)
        {
            raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
            for (int i = 0; i < raceList.size(); i++)
            {
                if (counter == 1)
                    break;
                
                if (!raceList.get(i).isSelected())
                {            	
                	Thread.sleep(1000);
                    raceList.get(i).click();
                    Thread.sleep(1000);                   
                   Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")));
                   Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
                    raceNum = getDriver().findElement(By.xpath("//a[@class='button action']/span")).getText().toLowerCase();
                    raceCount = i + 1;
                   Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
                    j = j + 1;
                }
                Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']")).isDisplayed(), "Build race card button is not enabled");
                if (i == raceList.size() - 1)
                {
                    WebElement allChkBox = getDriver().findElement(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times-all']//input"));
                    Assert.assertTrue(allChkBox.isSelected() == true, "AllCheck box is not selected");
                    raceList.get(i).click();
                   raceNum = getDriver().findElement(By.xpath("//a[@class='button action']/span")).getText().toLowerCase();
                   Assert.assertTrue(raceNum.contains("" + i + " "), "Mismatch in number of races displayed on racecard button");
                    counter = 1;
                }
                if (i == raceList.size() - 2)
                {
                    WebElement allChkBox = getDriver().findElement(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times-all']//input"));
                    Assert.assertTrue(allChkBox.isSelected() == false, "AllCheck box is not selected");
                }
            }
        }
        k = k + 1;
    }
	   Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='racing-card-footer-content']//a[@class='racing-card-footer-clear']")).isDisplayed(), "Clear all button is not enabled");
     click(By.xpath("//div[@class='racing-card-footer-content']//a[@class='racing-card-footer-clear']") , "Failed to Click on Clear All");
     WebElement allChkBox1 = getDriver().findElement(By.xpath("(//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times-all'])[1]//input"));
     getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
     Assert.assertFalse(allChkBox1.isSelected() == true , "AllCheck box is selected");
     Assert.assertTrue(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard not displayed by unchecking the races");
    System.out.println("Select HR Events and build Race card successfully verified");
	
 // ** Verify build next 3 races button on Next races module **
 		Assert.assertTrue(isElementPresent(HorseRacingElements.buildNxt3RacesBtn), "Build Next 3 Races Button is not found on HR landing Page");
 	    scrollPage("top");
 		click(HorseRacingElements.buildNxt3RacesBtn);
 	    HGfunctionObj.VerifyBreadCrums(TextControls.Home, className, TextControls.buildRaceCard, "");
 	    System.out.println("build next 3 races button on Next races module verified successfully");
 	   breadCrumsClass = "(//a[@class='breadcrumbs-link'])[2]";
 	    click(By.xpath(breadCrumsClass));
 	   Assert.assertTrue(isElementDisplayed(HorseRacingElements.allCheckBox1), "All Check Box is not found on HR Landing page" );
	
	
	}
		catch ( Exception | AssertionError  e){
				Assert.fail(e.getMessage(), e);
			}
	 }
public void selectDeslectRaces(String check_uncheck) throws Exception
{
	try{
   
    //Today
    if (isTextPresent(TextControls.HorseRacingToday))
        if (!getAttribute(By.xpath("//span[starts-with(@data-bind, 'html') and contains(text(), '" + TextControls.todayText + "')]//preceding-sibling::div[@class='icon-arrow expand']" + "/.."), "class").contains("expanded"))
            click(By.xpath("//span[starts-with(@data-bind, 'html') and contains(text(), '" + TextControls.todayText + "')]//preceding-sibling::div[@class='icon-arrow expand']"), "Today Module is not present on Horse Racing page");
     scrollPage("Top");
   
    String todayEvntXpath = "(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[3]//span[@class='racing-scheduled-meeting-race-time'][1]//input";
   WebElement checkBox = getDriver().findElement(By.xpath(todayEvntXpath));
    scrollToView(checkBox);
    click(By.xpath(todayEvntXpath), "Today's Race event is not found");
   
    if (check_uncheck.equals("check"))
      Assert.assertTrue(checkBox.isSelected(), "Today's Race is not selected");
    else
      Assert.assertTrue(!checkBox.isSelected(), "Today's Race is selected");
   
    //Tomorrow
    if (isTextPresent(TextControls.tomorrowText))
        if (!(getAttribute(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.tomorrowText + "')]" + "/.."), "class").contains("expanded")))
            click(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.tomorrowText + "')]"), "Tomorrow Module is not present on Horse Racing page");
     String tomoEvent = "(//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[2]//span[@class='racing-scheduled-meeting-race-time'][1]//input";
    WebElement tomeventelement = getDriver().findElement(By.xpath(tomoEvent));
    scrollToView(tomeventelement);
    click(By.xpath(tomoEvent), "Tommorow's Race event is not found");
    Thread.sleep(3000);         
    checkBox = getDriver().findElement(By.xpath(tomoEvent));
    if (check_uncheck.equals("check"))
      Assert.assertTrue(checkBox.isSelected(), "Tomorrow's Race is not selected");
    else
      Assert.assertTrue(!checkBox.isSelected(), "Tomorrow's Race is selected");          
    
    //Future
    if (isTextPresent(TextControls.futureText))
        if (!(getAttribute(By.xpath("//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.futureText + "')]//preceding-sibling::div[@class='icon-arrow expand']" + "/.."), "class").contains("expanded")))
            click(By.xpath("//span[starts-with(@data-bind, '') and contains(text(), 'Future')]//preceding-sibling::div[@class='icon-arrow expand']"), "Today Module is not present on Horse Racing page");
   
    String eventXPath2 = "//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']//div[2]//input";
     WebElement element2 = getDriver().findElement(By.xpath(eventXPath2));
    scrollToView(element2);
    click(By.xpath(eventXPath2), "Future Racing Event not found");            
    checkBox = getDriver().findElement(By.xpath(eventXPath2));
    if (check_uncheck.equals("check"))
      Assert.assertTrue(checkBox.isSelected(), "Future Race is not selected");
    else
      Assert.assertTrue(!checkBox.isSelected(), "Future Race is selected");

}
	catch ( Exception | AssertionError  e){
			Assert.fail(e.getMessage(), e);
		}
}


public String[] add_list_items_to_Array(List<WebElement> collName)
{
    int p = 0;
    int collItemCount = collName.size();
    String[] arrRaces = new String[collItemCount];
   
     for (WebElement ele : collName) 
     {
	   arrRaces[p++] = ele.getText();
     }

    return arrRaces;

}


public void verifyRaceCardSorting(String collectionLocator, String sortName, String sortLocator)
{
    try
    {
        click(By.xpath(sortLocator), "" + sortName + " not displayed in raceCard page");
        //Verify list is displayed in Descending order
        List<WebElement> descSlist =  getDriver().findElements(By.xpath(collectionLocator));
        String[] sortDescArr =add_list_items_to_Array(descSlist);
        if (sortName.equals("Number label"))
        {
            for (int p = 0; p < sortDescArr.length; p++)
            {
                sortDescArr[p] = sortDescArr[p].replace((sortDescArr[p].substring(sortDescArr[p].indexOf("\r"), sortDescArr[p].length() - 1)), "");
            }
        }
        boolean desc = common.StringIsSortedDsc(sortDescArr);
        if (desc == true)
        	System.out.println("" + sortName + " are displayed in descending order as expected");
        else
        	Assert.fail("" + sortName + " are not displayed in descending order as expected");

        click(By.xpath(sortLocator), "" + sortName + " not displayed in raceCard page");
        //Verify list is displayed in Ascending order
        List<WebElement> slist =  getDriver().findElements(By.xpath(collectionLocator));
        String[] sortAscArr = add_list_items_to_Array(slist);
        if (sortName.equals("Number label"))
        {
            for (int p = 0; p < sortAscArr.length; p++)
            {
                sortAscArr[p] = sortAscArr[p].replace((sortAscArr[p].substring(sortAscArr[p].indexOf("\r"), sortAscArr[p].length() - 1)), "");
            }
        }
        boolean asc = common.StringIsSortedAsc(sortAscArr);
        if (asc == true)
           System.out.println("" + sortName + " are displayed in ascending order as expected");
        else
          Assert.fail("" + sortName + " are not displayed in ascending order as expected");
    }
    catch ( Exception | AssertionError  e){
		Assert.fail(e.getMessage(), e);
	}
}

}




package com.ladbrokes.igameMedia;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IgamemediaTest {
	@Test

	public void GetHorceRacing() throws IOException {
		String currentWindow="";
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "//TestConfig//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("https://stg-backoffice-lcm.ladbrokes.com/office");
		driver.manage().window().maximize();
		driver.findElement(By.xpath(".//*[@id='loginUsername']")).sendKeys("priyanka_d");
		driver.findElement(By.xpath(".//*[@id='loginTable']/tbody/tr[2]/td[2]/input")).sendKeys("Password1");
		driver.findElement(By.xpath(".//*[@id='loginTable']/tbody/tr[3]/td/input")).click();




		driver.switchTo().frame(driver.findElement(By.name("officeMenu")));  
		driver.findElement(By.xpath(".//*[@id='officeMenu']/ul/li[2]/span")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.name("officePane")));   
		driver.switchTo().frame(driver.findElement(By.name("TopBar"))); 
		//Thread.sleep(3000);

		driver.findElement(By.xpath(".//*[@id='vclickMenu']/li[10]/span")).click();
		driver.findElement(By.xpath(".//*[@id='vclickMenu']/li[10]/ul/li[4]/a/b")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.name("officePane")));
		driver.switchTo().frame(driver.findElement(By.name("MainArea")));
		driver.findElement(By.xpath("html/body/form/table/tbody/tr[4]/th/input")).click(); 
		currentWindow = driver.getWindowHandle();
		
		
		// Readtestdata("HR",1,1);
		String excelFilePath =System.getProperty("user.dir")+"\\TestConfig\\IgameTestData.xlsx";
		Readtestdata(driver,currentWindow);
		driver.switchTo().window(currentWindow);
		System.out.println("Default");
		driver.switchTo().frame(driver.findElement(By.name("officePane")));
		System.out.println("Inside main");
		driver.switchTo().frame(driver.findElement(By.name("MainArea")));



		driver.findElement(By.xpath("//input[@type='button'][3]")).click();
	}
	public void Readtestdata(WebDriver driver,String currentWindow) throws IOException{
		String excelFilePath =System.getProperty("user.dir")+"\\TestConfig\\IgameTestData.xlsx";

		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);

		DataFormatter df = new DataFormatter();
	
		//Sheet Sheetname =workbook.getSheet(SheetName);
		for(int i=0;i<workbook.getNumberOfSheets();i++)
		{
			Iterator<Row> iterator = workbook.getSheetAt(i).iterator();

			while (iterator.hasNext()) {
				int columnnum=0;
				Row nextRow = iterator.next();
				if(nextRow.getRowNum()!=0)
				{
					Iterator<Cell> cellIterator = nextRow.cellIterator();
					driver.switchTo().window(currentWindow);
					driver.switchTo().frame(driver.findElement(By.name("officePane")));
					driver.switchTo().frame(driver.findElement(By.name("MainArea")));
					driver.findElement(By.xpath("//span[@id='LINK_TEXT_12']")).click();     
					switchwindowhandle(driver);
					while (cellIterator.hasNext()) {

						Cell nextCell = cellIterator.next();						
						System.out.println(df.formatCellValue(nextCell));
						
						if(columnnum==0)
						{
						if(workbook.getSheetName(i).equals("HR"))
						{
							
							driver.findElement(By.xpath("/html/body/form/table/tbody/tr[157]/td[2]/a")).click();
						}
						else
						
							driver.findElement(By.xpath("/html/body/form/table/tbody/tr[148]/td[1]/a")).click();
						switchwindowhandle(driver);
						
							driver.findElement(By.xpath("//a/text()[normalize-space(.)=\"" +df.formatCellValue(nextCell)+ "\"]/parent::*")).click();
							switchwindowhandle(driver);
						}
						else
						{ 
							if(isElementPresent(By.xpath("//a[contains(text(),\""+df.formatCellValue(nextCell)+"\")]"),driver))
							{
								driver.findElement(By.xpath("//a[contains(text(),\""+df.formatCellValue(nextCell)+"\")]//preceding-sibling::input[@type='radio']")).click();
								driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/th/input[2]")).click();	
							}
						}
						columnnum++;
					}
				}

			}
		}
	}

	public void switchwindowhandle(WebDriver driver){
		Set handles = driver.getWindowHandles();

		System.out.println(handles);

		// Pass a window handle to the other window

		for (String handle1 : driver.getWindowHandles()) {

			System.out.println(handle1);

			driver.switchTo().window(handle1);

		}

	}
	public static boolean isElementPresent(By locatorKey,WebDriver driver) {
		boolean isPresent = false;
		try {
			isPresent =driver.findElements(locatorKey).size() > 0;
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return isPresent;
	}
}



package com.ladbrokes.igameMedia;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.commonclasses.DriverCommon;

public class IgameMedia {


	@Test

	public void GetHorceRacing() throws InterruptedException{
		String currentWindow="";		
		
		String horseracing="https://stg-sports-lcm.ladbrokes.com/en-gb/racing/horse-racing/";
		String horseracingxpath="/html/body/form/table/tbody/tr[157]/td[2]/a";
		String greyhound="https://sports.ladbrokes.com/en-gb/racing/greyhound-racing/";
		String greyhoundxpath="/html/body/form/table/tbody/tr[148]/td[1]/a";
		ArrayList<String> horserrasingevent =new ArrayList<String>();	

		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "//TestConfig//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get(greyhound);
		driver.manage().window().maximize();
		
		List<WebElement> time=driver.findElements(By.xpath("//div[@class='module'][1]//div[@class='racing-scheduled'][1]/div[@class='racing-scheduled-meeting']"));
		java.util.Iterator<WebElement> i = time.iterator();
		while(i.hasNext()) {
			WebElement ivalue = i.next();	

			horserrasingevent.add(ivalue.getText());

		}

		System.out.println(horserrasingevent);


		

		driver.navigate().to("https://stg-backoffice-lcm.ladbrokes.com/office");
		driver.findElement(By.xpath(".//*[@id='loginUsername']")).sendKeys("priyanka_d");
		driver.findElement(By.xpath(".//*[@id='loginTable']/tbody/tr[2]/td[2]/input")).sendKeys("Password1");
		driver.findElement(By.xpath(".//*[@id='loginTable']/tbody/tr[3]/td/input")).click();

		


		driver.switchTo().frame(driver.findElement(By.name("officeMenu")));		
		driver.findElement(By.xpath(".//*[@id='officeMenu']/ul/li[2]/span")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.name("officePane")));		 
		driver.switchTo().frame(driver.findElement(By.name("TopBar")));	
		//Thread.sleep(3000);

		driver.findElement(By.xpath(".//*[@id='vclickMenu']/li[10]/span")).click();
		driver.findElement(By.xpath(".//*[@id='vclickMenu']/li[10]/ul/li[4]/a/b")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(By.name("officePane")));
		driver.switchTo().frame(driver.findElement(By.name("MainArea")));
		driver.findElement(By.xpath("html/body/form/table/tbody/tr[4]/th/input")).click();	
		currentWindow = driver.getWindowHandle();
		int Eventflag=0;
		
		for(int m=0;m<horserrasingevent.size();m++)
		{

			String[] part =horserrasingevent.get(m).split("\n");
			//String[] part={"Royal Ascot","958_TestBetAlert_192018"};       
			               
			               
			for(int z=0;z<part.length;z++)
			{


				if(part[z].equals("All"))
				{
					Eventflag=0;
					driver.switchTo().window(currentWindow);
					driver.switchTo().frame(driver.findElement(By.name("officePane")));
					driver.switchTo().frame(driver.findElement(By.name("MainArea")));
					break;

				}


				if(Eventflag==0)
				{
					driver.findElement(By.xpath("//span[@id='LINK_TEXT_10']")).click();					
					switchwindowhandle(driver);
					if(isElementPresent(By.xpath(greyhoundxpath),driver))
					{
					driver.findElement(By.xpath(greyhoundxpath)).click();
					}
					else
					{
						driver.switchTo().window(currentWindow);
						driver.switchTo().frame(driver.findElement(By.name("officePane")));
						driver.switchTo().frame(driver.findElement(By.name("MainArea")));
						break;
					}
					
					//Thread.sleep(20000);
					switchwindowhandle(driver);
					if(isElementPresent(By.xpath("//a/text()[normalize-space(.)=\"" +part[0].trim()+ "\"]/parent::*"),driver))
					{
						
						driver.findElement(By.xpath("//a/text()[normalize-space(.)=\"" +part[0].trim()+ "\"]/parent::*")).click();
						
					}
					else
					{
						driver.switchTo().window(currentWindow);
						driver.switchTo().frame(driver.findElement(By.name("officePane")));
						driver.switchTo().frame(driver.findElement(By.name("MainArea")));
						break;
					}
					
					
					System.out.println("inside Event");				
					switchwindowhandle(driver);
					Eventflag=2;		
					
					z++;
				}
				if(Eventflag==2)
				{
					
					 if(isElementPresent(By.xpath("//a[contains(text(),\""+part[z]+"\")]//preceding-sibling::input[@type='radio']"),driver))
					{
						driver.findElement(By.xpath("//a[contains(text(),\""+part[z]+"\")]//preceding-sibling::input[@type='radio']")).click();
						System.out.println("inside timed Event");
						if(isElementPresent(By.xpath("/html/body/form/table/tbody/tr[3]/th/input[2]"),driver))
							driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/th/input[2]")).click();	
					}					
					
				}

			}
		}

		driver.switchTo().window(currentWindow);
		System.out.println("Default");
		driver.switchTo().frame(driver.findElement(By.name("officePane")));
		System.out.println("Inside main");
		driver.switchTo().frame(driver.findElement(By.name("MainArea")));



		driver.findElement(By.xpath("//input[@type='button'][3]")).click();
						


		//driver.quit();


	}

	public static boolean isElementPresent(By locatorKey,WebDriver driver) {
		boolean isPresent = false;
		try {
			isPresent =driver.findElements(locatorKey).size() > 0;
		} catch (Exception | AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
		return isPresent;
	}


	
	public void switchwindowhandle(WebDriver driver){
		Set handles = driver.getWindowHandles();

		System.out.println(handles);

		// Pass a window handle to the other window

		for (String handle1 : driver.getWindowHandles()) {

			System.out.println(handle1);

			driver.switchTo().window(handle1);

		}

	}
	
}






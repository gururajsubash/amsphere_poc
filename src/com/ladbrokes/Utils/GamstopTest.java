package com.ladbrokes.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;

public class GamstopTest extends DriverCommon {
	Common common = new Common();
	static Calendar cal = Calendar.getInstance();
	static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Test
	public void GampstopuserTest() throws Exception {
		String testCase = "GampstopuserTest";
		String bal = null;
		try {
			launchweb();
			Thread.sleep(5000);
			common.Login();
			cal = Calendar.getInstance();
			System.out.println(" ---> Script Started at >>>>>> " + sdf.format(cal.getTime()));
			for (int i = 0; i < 200000; i++) {
				cal = Calendar.getInstance();
				System.out.println("--> click Iteration: " + i + " started at >>> " + sdf.format(cal.getTime()));
				click(FootballElements.couponLink);
				Thread.sleep(2000);
				click(HomeGlobalElements.AZMenu);
				Thread.sleep(2000);
				if (!isElementPresent(LoginElements.userBalance)) {
					getscreenshotgamstop();
					break;
				} else 
					System.out.println("User Session is Active");
			}
		} catch (Exception e) {
			String screenShotPath = getScreenshot(testCase);
			cal = Calendar.getInstance();
			System.out.println(" ---> Catch Block Entered due to Exception >>>>>> " + sdf.format(cal.getTime()));
		}
	}

	public void getscreenshotgamstop() throws Exception {
		getDriver().quit();
		String testCase = "GampstopuserLoginfailed";
		cal = Calendar.getInstance();
		System.out.println(" ---> User is NOT logged in Anymore. Assert Failed at >>>>>> " + sdf.format(cal.getTime()));
		launchweb();
		common.Login();
		String screenShotPath = getScreenshot(testCase);
	}
}

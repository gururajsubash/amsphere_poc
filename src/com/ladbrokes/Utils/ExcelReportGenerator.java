package com.ladbrokes.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public  class ExcelReportGenerator {

public void generateExcelReport(String destiFile) throws ParserConfigurationException, SAXException, IOException{
 
 
 String path = System.getProperty("user.dir");
 
 //path=path.replaceAll("bin", "src");
 String otherpath = "//Results//";
 File xmlFile1 = new File(otherpath);
 File xmlFile = new File (path+"//test-output//testng-results.xml");
 System.out.println(xmlFile.isAbsolute());
 
 System.out.println(xmlFile1.isAbsolute());
 
 DocumentBuilderFactory Fact = DocumentBuilderFactory.newInstance();
 DocumentBuilder Build = Fact.newDocumentBuilder();
 Document Doc = Build.parse(xmlFile);
 Doc.getDocumentElement().normalize();
 
 XSSFWorkbook book = new XSSFWorkbook();
 NodeList test_list = Doc.getElementsByTagName("test");
 
 for (int i=0; i<test_list.getLength(); i++){
  int r = 0;
  Node test_node = test_list.item(i);
  String test_name = ((Element)test_node).getAttribute("name");
  XSSFSheet Sheet = book.createSheet(test_name);
  NodeList class_list = ((Element)test_node).getElementsByTagName("class");
  
  for (int j=0; j<class_list.getLength();j++){
   
   Node class_node = class_list.item(j);
   String class_name = ((Element)class_node).getAttribute("name");
   NodeList test_method_list = ((Element)class_node).getElementsByTagName("test-method");
   
   for (int k =0; k<test_method_list.getLength();k++){
    
    Node test_method_node = test_method_list.item(k);
    String test_method_name = ((Element)test_method_node).getAttribute("name");
    String test_method_status = ((Element)test_method_node).getAttribute("status");
    XSSFRow row = Sheet.createRow(r++);
    XSSFCell cel_name = row.createCell(0);
    cel_name.setCellValue(class_name +"."+test_method_name);
    
    XSSFCell cel_status = row.createCell(1);
    cel_status.setCellValue(test_method_status);
    
    
   }
   
  } 
  
 }
 
 FileOutputStream fout = new FileOutputStream( destiFile);
 book.write(fout);
 fout.close();
 System.out.println("Results Generated");
 
} 
 
 
 //@Test
 public void testingreports() throws ParserConfigurationException, SAXException, IOException, InterruptedException{
  
  String path1 = System.getProperty("user.dir");
  
  
  String fileName = new SimpleDateFormat("yyyyMMddHHmm'.xlsx'").format(new Date());
  
    String otherpath = path1+"//Results//Results"+ fileName;
  generateExcelReport(otherpath);
  
 }
 
 
}

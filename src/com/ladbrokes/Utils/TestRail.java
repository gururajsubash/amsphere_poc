package com.ladbrokes.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestRail {
	
	/**
	 * Populates RunID in TestRail for Desktop P1 Test Run
	 * 
	 * @author: Sreekanth
	 * @since : 16-Jan-2018
	 */
	@Test
	public void CreateRunID() {
		try {
			String TestRailUserName = ReadTestSettingConfig.getTestsetting("TestRailUserName");
			String TestRailUserPassword = ReadTestSettingConfig.getTestsetting("TestRailUserPassword");
			String browsertype=ReadTestSettingConfig.getTestsetting("BrowserType");
			String projectName=ReadTestSettingConfig.getTestsetting("ProjectName");
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//TestConfig//chromedriver_win32_2.33.exe");

			WebDriver driver = new ChromeDriver();
			driver.get("https://ladbrokescoral.testrail.com/index.php?/runs/overview/106");
			driver.manage().window().maximize();
			driver.findElement(By.xpath("//*[@id='name']")).sendKeys(TestRailUserName);
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(TestRailUserPassword);
			clickByJS(driver, By.xpath("//button[@type='submit']"));
			
			clickByJS(driver, By.xpath("//a[text()='Automation Runs']"));
			clickByJS(driver, By.xpath("//span[text()='Edit']"));
			clickByJS(driver, By.xpath("//span[text()='Add Test Suite']"));
			
			Select select = new Select(driver.findElement(By.xpath("//select[@name='choose_suite_id']")));
			select.selectByVisibleText("Desktop Regression-Masterpack");
			clickByJS(driver, By.xpath("//button[@id='chooseSuiteDialogSubmit']"));
			
			Thread.sleep(3000);
			List<WebElement> editRunNameList = driver.findElements(By.xpath("//img[@src='https://static.testrail.io/5.5.0.3731/images/icons/smallEdit.png']"));
			clickByJS(driver, editRunNameList.get(editRunNameList.size()-1));
			
			String runName = "Automation: " + projectName + " " + browsertype +" - " + getCurrentDate();
			driver.findElement(By.xpath("//input[@id='editName']")).sendKeys(runName);
			driver.findElement(By.xpath("//input[@id='editName']")).sendKeys(Keys.ENTER);

			Thread.sleep(3000);
			List<WebElement> selectCasesList = driver.findElements(By.xpath("//a[text()='select cases']"));
			clickByJS(driver, selectCasesList.get(selectCasesList.size()-1));
			
			clickByJS(driver, By.xpath("//a[text()='Type']"));
			Select typeSelect = new Select(driver.findElement(By.xpath("//a[text()='Type']/../following-sibling::div/select")));
			typeSelect.selectByVisibleText("Automated");
			clickByJS(driver, By.xpath("//a[@id='selectCasesFilterApply']"));
			clickByJS(driver, By.xpath("//button[@id='selectCasesSubmit']"));
			
			//'Save Test Plan' button click and click OK on 'Review Changes' popup
			clickByJS(driver, By.xpath("//button[@id='accept']"));
			clickByJS(driver, By.xpath("//button[@id='confirmDiffSubmit']"));
			
			// Look for the newly created RunName and click that link to extract RunID
			clickByJS(driver, By.xpath("//a[text()='" + runName +"']"));
			String RunID = driver.findElement(By.xpath("//div[@class='content-header-id']")).getText();
			RunID = RunID.replace("R", "");
			
			// Inject the RunID extracted in above step into below function call to be passed to TestSettings File
			ReadTestSettingConfig.setTestsetting(RunID);

			if (driver != null)
				driver.quit();

		} catch (Exception ex) {
			System.out.println("*** Creating TestRail RunID failed" + "\n " + ex.getMessage());
		}
	}
	
	@Test
	public void getTestRailReport() throws IOException{
		String browsertype=ReadTestSettingConfig.getTestsetting("BrowserType");
		String projectName=ReadTestSettingConfig.getTestsetting("ProjectName");
		String runid=ReadTestSettingConfig.getTestsetting("runID");
		String testrailusername=ReadTestSettingConfig.getTestsetting("TestRailUserName");
		String testrailuserpassword=ReadTestSettingConfig.getTestsetting("TestRailUserPassword");
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "//TestConfig//chromedriver_win32_2.33.exe");
		
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("https://ladbrokescoral.testrail.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//*[@id='name']")).sendKeys(testrailusername);
		driver.findElement(By.xpath("//*[@id='password']")).sendKeys(testrailuserpassword);
		
		driver.findElement(By.xpath("//*[@id='content']/form/div[4]/button")).click();
		String TestrailURL ="https://ladbrokescoral.testrail.com/index.php?/runs/view/"+runid+"&group_by=cases:section_id&group_order=asc";
		driver.navigate().to(TestrailURL);

		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "//Reports//"+ projectName + "_" + browsertype + ".png"));
		driver.quit();
	}
	
	public static void clickByJS(WebDriver driver, By locator) throws Exception{
		try {		
			WebElement element = driver.findElement(locator);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click()", element);
			Thread.sleep(1000);
			
		} catch (RuntimeException ex) {
			Assert.fail("Unable to find/click the element" + locator, ex);
		}
	}
	
	public static void clickByJS(WebDriver driver, WebElement element) throws Exception{
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click()", element);
			Thread.sleep(1000);
			
		} catch (RuntimeException ex) {
			Assert.fail("Unable to find/click the element" + element, ex);
		}
	}
	
	public String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
		Calendar cal = Calendar.getInstance();
		String getCurrentDate = sdf.format(cal.getTime());
		return getCurrentDate.toUpperCase();
	}
	
} // End of TestRail Class
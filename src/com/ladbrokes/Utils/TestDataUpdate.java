package com.ladbrokes.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class TestDataUpdate extends DriverCommon {
	Common commonFunctions = new Common();
	HomeGlobal_Functions HGObjFunctions = new HomeGlobal_Functions();
	
	
	@Test(priority = 1)
	public void FirstRow() throws Exception{
		String testCase = "FirstRow";
		try {
			Sports_OutrightEWCashoutEvents(TextControls.Football, 1);
		}
		catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail("FirstRow - failed to update", e);
		}
	}
		
		@Test(priority = 2)
		public void SecondRow() throws Exception{
			String testCase = "SecondRow";
			try {
				SelectingFootBallEvent_diffSubType(TextControls.Football, 2);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("SecondRow - failed to update", e);
			}
			
	}
	
		@Test(priority = 3)
		public void ThirdRow() throws Exception{
			String testCase = "ThirdRow";
			try {
				HR_GH_FutureEvents(TextControls.HorseRacing, 3);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("ThirdRow - failed to update", e);
			}
			
	}
		@Test(priority = 4)
		public void FourthRow() throws Exception{
			String testCase = "FourthRow";
			try {
				HR_FutureSecondEvent(TextControls.HorseRacing, 4);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("FourthRow - failed to update", e);
			}
			
	}
		@Test(priority = 5)
		public void FifthRow() throws Exception{
			String testCase = "FifthRow";
			try {
				Sport_FutureEvents(TextControls.Basketball, 5);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("FifthRow - failed to update", e);
			}
		}
		
		@Test(priority = 6)
		public void seventhRow() throws Exception{
			String testCase = "seventhRow";
			try {
				AnySportEvent_EW("Snooker", 7);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("seventhRow - failed to update", e);
			}
		}

		@Test(priority = 7)
		public void eightRow() throws Exception{
			String testCase = "eightRow";
			try {
				AnySportEvent_EW("Golf", 8);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("eightRow - failed to update", e);
			}
		}
		@Test(priority = 13)
		public void ninethRow() throws Exception{
			String testCase = "ninethRow";
			try {
				HR_GH_TodayEvents(TextControls.HorseRacing, 9);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("ninethRow - failed to update", e);
			}
		}
		@Test(priority = 14 )
		public void TenthtRow() throws Exception{
			String testCase = "TenthtRow";
			try {
				HR_GH_TodaySecondEvents(TextControls.HorseRacing, 10);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("TenthtRow - failed to update", e);
			}
		}
		@Test(priority = 8)
		public void ThirteenthRow() throws Exception{
			String testCase = "ThirteenthRow";
			try {
				Sport_FutureEvents("Rugby Union", 13);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("ThirteenthRow - failed to update", e);
			}
		}
		@Test(priority = 9)
		public void FourteenthRow() throws Exception{
			String testCase = "FourteenthRow";
			try {
				AnySportEvent_EW(TextControls.rugbyLeague, 14);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("FourteenthRow - failed to update", e);
			}
		}
		@Test(priority = 12)
		public void SixteenthRow() throws Exception{
			String testCase = "SixteenthRow";
			try {
				HR_GH_TodayEvents(TextControls.Greyhounds, 16);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("SixteenthRow - failed to update", e);
			}
			
	}
		@Test(priority = 10)
		public void SeventeenthRow() throws Exception{
			String testCase = "SeventeenthRow";
			try {
				HR_GH_FutureEvents(TextControls.Greyhounds, 17);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("SeventeenthRow - failed to update", e);
			}
			
	}
		@Test(priority = 11)
		public void eightteenthRow() throws Exception{
			String testCase = "eightteenthRow";
			try {
				GH_FutureSecondEvent(TextControls.Greyhounds, 18);
			}
			catch (Exception | AssertionError e) {
				getScreenshot(testCase);
				Assert.fail("eightteenthRow - failed to update", e);
			}
			
	}
		
	public  void Sports_OutrightEWCashoutEvents(String ClassName , int rowNum) throws Exception {
		String testCase = "Sports_OutrightEWCashoutEvents";
		String typeName = null, subTypeName = null, eventName = null, marketName = null, selNme = null;
		int counter = 0;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(ClassName);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, ClassName, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
			     TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
				System.out.println(TypeNameList.get(i).getText());
				typeName = TypeNameList.get(i).getText().toLowerCase();
				System.out.println(typeName);
				if (typeName.equalsIgnoreCase(TextControls.moreText))
					break;
				if (typeName.contains("("))
				typeName = typeName.substring(0, typeName.length() - 4);
				typeName = typeName.replaceAll("\n", "");
				typeName = toTitleCase(typeName);
				if(typeName.contains("Uefa"))
					typeName = typeName.replace("Uefa", "UEFA");
				TypeNameList.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				List<WebElement> subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
				for (int j = 0; j < subTypeNameList.size(); j++) {
					subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
					subTypeName = subTypeNameList.get(j).getText();
					System.out.println(subTypeName);
					subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
					//subTypeName = toTitleCase(subTypeName);
					subTypeNameList.get(j).click();
					List<WebElement> EventList = getDriver()
							.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
					for (int k = 0; k < EventList.size(); k++) {
						EventList = getDriver()
								.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
						eventName = EventList.get(k).getText();
						System.out.println(eventName);
						//eventName = toTitleCase(eventName);
						clickByJS(EventList.get(k));
						Thread.sleep(2000);
						if (eventName.contains(" V ")) {
							getDriver().navigate().back();
							Thread.sleep(2000);
						} else {
						//	boolean eachWay = getDriver().findElement(By.xpath("//div//span[@class='title' and contains(text(),'Outright')]/..//div[@class='cash-out-container']/../..//div[@class='eachway']"))
						//			.isDisplayed();
							if (isElementPresent(By.xpath("//span[@class='title' and contains(text(),'Outright')]/..//div[@class='cash-out-container']/../..//div[@class='eachway']"))) {
								eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
								marketName = GetElementText(By
										.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
								selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
								counter = 1;
								break;
							} else
								getDriver().navigate().back();
							Thread.sleep(2000);
						}
					}
					if (counter == 1)
						break;
					else
						
						getDriver().navigate().back();
				}
				if (counter == 1)
					break;
				else
					getDriver().navigate().back();
			}
			writeTestData("PreProdEvents", rowNum, 1, ClassName);
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 3, subTypeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
	
	public  void SelectingFootBallEvent_diffSubType(String ClassName , int rowNum) throws Exception {
		String testCase = "SelectingFootBallEvent_diffSubType";
		String typeName = null, subTypeName = null, eventName = null, marketName = null, selNme = null;
		int counter = 0;
		String subTypeNmeInFirstRow;
		int m;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(ClassName);
			subTypeNmeInFirstRow =  Readtestdata("PreProdEvents", 1, 3);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, ClassName, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
				TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
				typeName = TypeNameList.get(i).getText().toLowerCase();
				if (typeName.equalsIgnoreCase(TextControls.moreText))
					break;
				if (typeName.contains("("))
				typeName = typeName.substring(0, typeName.length() - 4);
				typeName = typeName.replaceAll("\n", "");
				typeName = toTitleCase(typeName);
				if(typeName.contains("Uefa"))
					typeName = typeName.replace("Uefa", "UEFA");
				TypeNameList.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				List<WebElement> subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
				for (int j = 0; j < subTypeNameList.size(); j++) {
					subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
					subTypeName = subTypeNameList.get(j).getText();
					System.out.println(subTypeName);
					subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
					subTypeName = toTitleCase(subTypeName);
					if(subTypeNmeInFirstRow.equals(subTypeName)){
						subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
						m = j + 1;
						subTypeName = subTypeNameList.get(m).getText();
						subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
						subTypeName = toTitleCase(subTypeName);
				         subTypeNameList.get(m).click();}
					else
						subTypeNameList.get(j).click();
					List<WebElement> EventList = getDriver()
							.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
					for (int k = 0; k < EventList.size(); k++) {
						EventList = getDriver()
								.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
						eventName = EventList.get(k).getText().toLowerCase();
						eventName = toTitleCase(eventName);
						//EventList.get(k).click();
						clickByJS(EventList.get(k));
						Thread.sleep(2000);
						if (eventName.contains(" V ")) {
							getDriver().navigate().back();
							Thread.sleep(2000);
						} else {
							//	boolean eachWay = getDriver().findElement(By.xpath("//div//span[@class='title' and contains(text(),'Outright')]/..//div[@class='cash-out-container']/../..//div[@class='eachway']"))
							//			.isDisplayed();
								if (isElementPresent(By.xpath("//span[@class='title' and contains(text(),'Outright')]/..//div[@class='cash-out-container']/../..//div[@class='eachway']"))) {
									eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
									marketName = GetElementText(By
											.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
									selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
									counter = 1;
									break;
								} else
									getDriver().navigate().back();
								Thread.sleep(2000);
							}
						}
					if (counter == 1)
						break;
					else
						getDriver().navigate().back();
				}
				if (counter == 1)
					break;
				else
					getDriver().navigate().back();
			}
			writeTestData("PreProdEvents", rowNum, 1, ClassName);
			System.out.println("data entered in to Excel file");
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 3, subTypeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	public void HR_GH_FutureEvents(String className , int rowNum ) throws Exception {
		String testCase = "HR_GH_FutureEvents";
		String typeName = null, eventName = null, marketName = null, selNme = null;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			List<WebElement> FutureEvents = getDriver().findElements(By.xpath(
					"//span[contains(text(), 'Future')]/../..//div[@class='racing-upcoming-meeting']//a//span[@class='racing-upcoming-meeting-name']"));
			for (int i = 0; i < FutureEvents.size(); i++) {
				eventName = FutureEvents.get(i).getText().toLowerCase();
				eventName = toTitleCase(eventName);
				clickByJS(FutureEvents.get(i));
				if (isElementDisplayed(By.xpath("//div[@class='eachway']"))) {
					typeName = GetElementText(By.xpath("//span[@class='league']"));
					marketName = GetElementText(
							By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
					selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
					break;
				} else
					getDriver().navigate().back();
			}
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	public void HR_FutureSecondEvent(String className , int rowNum ) throws Exception {
		String testCase = "HR_FutureEvents";
		String typeName = null, eventName = null, marketName = null, selNme = null;
		String firstEvntNme;
		int m;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			firstEvntNme = Readtestdata("PreProdEvents", 3, 4);
			List<WebElement> FutureEvents = getDriver().findElements(By.xpath(
					"//span[contains(text(), 'Future')]/../..//div[@class='racing-upcoming-meeting']//a//span[@class='racing-upcoming-meeting-name']"));
			for (int i = 0; i < FutureEvents.size(); i++) {
				eventName = FutureEvents.get(i).getText().toLowerCase();
				eventName = toTitleCase(eventName);
				if(firstEvntNme.equals(eventName)){
					m = i + 1;
					eventName = FutureEvents.get(m).getText().toLowerCase();
					eventName = toTitleCase(eventName);
					clickByJS(FutureEvents.get(m));
				}
				else{
				clickByJS(FutureEvents.get(i));}
				if (isElementDisplayed(By.xpath("//div[@class='eachway']"))) {
					typeName = GetElementText(By.xpath("//span[@class='league']"));
					marketName = GetElementText(
							By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
					selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
					break;
				} else
					getDriver().navigate().back();
			}
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
	public void GH_FutureSecondEvent(String className , int rowNum ) throws Exception {
		String testCase = "GH_FutureEvents";
		String typeName = null, eventName = null, marketName = null, selNme = null;
		String firstEvntNme;
		int m;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			firstEvntNme = Readtestdata("PreProdEvents", 17, 4);
			List<WebElement> FutureEvents = getDriver().findElements(By.xpath(
					"//span[contains(text(), 'Future')]/../..//div[@class='racing-upcoming-meeting']//a//span[@class='racing-upcoming-meeting-name']"));
			for (int i = 0; i < FutureEvents.size(); i++) {
				eventName = FutureEvents.get(i).getText().toLowerCase();
				eventName = toTitleCase(eventName);
				if(firstEvntNme.equals(eventName)){
					m = i + 1;
					eventName = FutureEvents.get(m).getText().toLowerCase();
					eventName = toTitleCase(eventName);
					clickByJS(FutureEvents.get(m));
				}
				else{
				clickByJS(FutureEvents.get(i));}
				if (isElementDisplayed(By.xpath("//div[@class='eachway']"))) {
					typeName = GetElementText(By.xpath("//span[@class='league']"));
					marketName = GetElementText(
							By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
					selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
					break;
				} else
					getDriver().navigate().back();
			}
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	public void HR_GH_TodayEvents(String className , int rowNum) throws Exception {
		String testCase = "HR_GH_TodayEvents";
		String raceTime = null, eventName = null, selection= null, marketName = "Win or Each Way";
		int counter = 0;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			List<WebElement> TodayRaces = getDriver().findElements(
					By.xpath("//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']"));
			for (int i = 1; i < TodayRaces.size(); i++) {
				List<WebElement> TodayMeetings = getDriver().findElements(By
						.xpath("(//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right'])["
								+ i + "]//span[@class='racing-scheduled-meeting-race-time']"));
				for (int j = 1; j < TodayMeetings.size(); j++) {
					TodayMeetings = getDriver().findElements(By
							.xpath("(//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right'])["
									+ i + "]//span[@class='racing-scheduled-meeting-race-time']"));
					scrollToView(TodayMeetings.get(j));
					int numofevents = TodayMeetings.size();
					TodayMeetings.get(numofevents-1).click();
					Thread.sleep(2000);
					if(className.contains(TextControls.HorseRacing)){
					if (isElementPresent(By.xpath("//div[@class='text' and contains(text(),'Forecast / Tricast')]")) && isElementPresent(By.xpath("(//div[@class='horse']//span[@class='name'])[2]"))) {
						raceTime = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='time']"));
						eventName = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='name']"));
						selection = GetElementText(By.xpath("(//div[@class='horse']//span[@class='name'])[2]"));
						counter = 1;
						break;
							//selection = GetElementText(By.xpath("(//span[@class='horse']//span[@class='name'])[2]"));	
					} else {
						getDriver().navigate().back();
					}
				}
					else
					{
						if (isElementPresent(By.xpath("//div[@class='text' and contains(text(),'Forecast / Tricast')]")) && isElementPresent(By.xpath("(//span[@class='horse']//span[@class='name'])[2]"))) {
							raceTime = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='time']"));
							eventName = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='name']"));
							selection = GetElementText(By.xpath("(//span[@class='horse']//span[@class='name'])[2]"));
							counter = 1;
							break;
								
						} else {
							getDriver().navigate().back();
						}
					}
				}
				if (counter == 1)
					break;
			}
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, eventName);
			writeTestData("PreProdEvents", rowNum, 4, raceTime);
			writeTestData("PreProdEvents", rowNum, 5, selection);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	public void HR_GH_TodaySecondEvents(String className , int rowNum) throws Exception {
		String testCase = "HR_GH_TodaySecondEvents";
		String raceTime = null, eventName = null, selection= null, marketName = "Win or Each Way";
		int counter = 0;
		String firstEvntNme;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			firstEvntNme = Readtestdata("PreProdEvents", 9, 2);
			List<WebElement> TodayRaces = getDriver().findElements(
					By.xpath("//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']"));
			for (int i = 1; i < TodayRaces.size(); i++) {
				List<WebElement> TodayMeetings = getDriver().findElements(By
						.xpath("(//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right'])["
								+ i + "]//span[@class='racing-scheduled-meeting-race-time']"));
				for (int j = 1; j < TodayMeetings.size(); j++) {
					TodayMeetings = getDriver().findElements(By
							.xpath("(//span[contains(text(),'Today')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right'])["
									+ i + "]//span[@class='racing-scheduled-meeting-race-time']"));
					scrollToView(TodayMeetings.get(j));
					TodayMeetings.get(j).click();
					eventName = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='name']"));
					if(firstEvntNme.equals(eventName)){
						getDriver().navigate().back();
					}
					else{
					Thread.sleep(2000);
					if (isElementPresent(By.xpath("//div[@class='text' and contains(text(),'Forecast / Tricast')]")) && isElementPresent(By.xpath("(//div[@class='horse']//span[@class='name'])[2]"))) {
						raceTime = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='time']"));
						eventName = GetElementText(By.xpath("//div[@class='wrap-time-name']//span[@class='name']"));
						System.out.println(eventName);
						selection = GetElementText(By.xpath("(//div[@class='horse']//span[@class='name'])[2]"));
						counter = 1;
						break;
					} else {
						getDriver().navigate().back();
					}
				}
				}
				if (counter == 1)
					break;
			}
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, eventName);
			writeTestData("PreProdEvents", rowNum, 4, raceTime);
			writeTestData("PreProdEvents", rowNum, 5, selection);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	public void Sport_FutureEvents(String className , int rowNum) throws Exception {
		String testCase = "Sport_FutureEvents";
		String typeName = null, subTypeName = null, eventName = null, marketName = null, selNme = null;
		int counter = 0;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(className);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, className, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
				TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
				typeName = TypeNameList.get(i).getText().toLowerCase();
				if (typeName.equalsIgnoreCase(TextControls.moreText))
					break;
				if (typeName.contains("("))
				typeName = typeName.substring(0, typeName.length() - 4);
				typeName = typeName.replaceAll("\n", "");
				typeName = toTitleCase(typeName);
				TypeNameList.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				if (!(isElementPresent(BetslipElements.EDPHeader))) 
				{
				List<WebElement> subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
				for (int j = 0; j < subTypeNameList.size(); j++) {
					subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
					subTypeName = subTypeNameList.get(j).getText();
					subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
					subTypeName = toTitleCase(subTypeName);
					subTypeNameList.get(j).click();
					Thread.sleep(2000);
					List<WebElement> EventList = getDriver().findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
					for (int k = 0; k < EventList.size(); k++)
					{
						EventList = getDriver().findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
						eventName = EventList.get(k).getText().toLowerCase();
						eventName = toTitleCase(eventName);
						clickByJS(EventList.get(k));
						Thread.sleep(2000);
						if (eventName.contains(" V "))
						{
							getDriver().navigate().back();
						} 
						else 
						{
							eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
							marketName = GetElementText(
									By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
							selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
							//typeName = GetElementText(By.xpath("//div[@class='basic-scoreboard']//span[@class='league']"));
							counter = 1;
							break;
						}
					}
					if (counter == 1)
						break;
					else
						getDriver().navigate().back();
				}
				if (counter == 1)
					break;
				else
					getDriver().navigate().back();
			}
			
			else
				{
				    eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
					marketName = GetElementText(By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
					selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
					typeName = GetElementText(By.xpath("//div[@class='basic-scoreboard']//span[@class='league']"));
					break;
					} 
				}
			
			writeTestData("PreProdEvents", rowNum, 1, className);
			writeTestData("PreProdEvents", rowNum, 2, typeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			if(subTypeName == null)
				writeTestData("PreProdEvents", rowNum, 3, "");
			else
				writeTestData("PreProdEvents", rowNum, 3, subTypeName);
			
			
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	public void AnySportEvent_EW(String classNme , int rowNum) throws Exception {
		String testCase = "AnySportEvent_EW";
		String eventTypeName = null, subTypeName = null,eventName = null, marketName = null, selNme = null;
		int counter = 0;
		boolean eachWay = false;
		try {
			launchweb();
			commonFunctions.SelectLinksFromAZ(classNme);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, classNme, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
				TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
				eventTypeName = TypeNameList.get(i).getText().toLowerCase();
				if (eventTypeName.contains("("))
					eventTypeName = eventTypeName.substring(0, eventTypeName.length() - 4);
				if (eventTypeName.contains(TextControls.moreText.toLowerCase()))
					break;
				
				eventTypeName = eventTypeName.replaceAll("\n", "");
				eventTypeName = toTitleCase(eventTypeName);
				if(eventTypeName.contains("Us"))
					eventTypeName = eventTypeName.replace("Us", "US");
				TypeNameList.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				boolean subtype = getDriver().findElements(HomeGlobalElements.subTypeList).size() == 1;
				if (!isElementPresent(BetslipElements.EDPHeader) && subtype == false) {
					List<WebElement> subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
					for (int j = 0; j < subTypeNameList.size(); j++) {
						subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
						subTypeName = subTypeNameList.get(j).getText();
						subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
						subTypeName = toTitleCase(subTypeName);
						subTypeNameList.get(j).click();
						List<WebElement> EventList = getDriver()
								.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
						for (int k = 0; k < EventList.size(); k++) {
							EventList = getDriver()
									.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
							eventName = EventList.get(k).getText().toLowerCase();
							eventName = toTitleCase(eventName);
							clickByJS(EventList.get(k));
							Thread.sleep(2000);
							if (eventName.contains(" V ")) {
								getDriver().navigate().back();
							} else {
								 eachWay = getDriver()
										.findElement(By
												.xpath("//div[@class='eachway']"))
										.isDisplayed();
								if (eachWay == true && !isElementPresent(By.xpath("//div[@class='basic-scoreboard']//span[@class='live-badge__no-stream']"))) {
									eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
									marketName = GetElementText(By
											.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
									selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
									
									counter = 1;
									break;
								} else{
									getDriver().navigate().back();
									Thread.sleep(2000);}
							}
						}
						if (counter == 1)
							break;
						else
							getDriver().navigate().back();
						Thread.sleep(2000);
					}
					if (counter == 1)
						break;
					else
						getDriver().navigate().back();
					Thread.sleep(2000);
				}
				else if(!isElementPresent(BetslipElements.EDPHeader) && subtype == true){
					List<WebElement> EventList = getDriver()
							.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
					for (int k = 0; k < EventList.size(); k++) {
						EventList = getDriver()
								.findElements(By.xpath("//div[@class='event']//div[@class='name']//span"));
						eventName = EventList.get(k).getText().toLowerCase();
						eventName = toTitleCase(eventName);
						clickByJS(EventList.get(k));
						Thread.sleep(2000);
						if (eventName.contains(" V ")) {
							getDriver().navigate().back();
						} else {
							 eachWay = getDriver()
									.findElement(By
											.xpath("//div[@class='eachway']"))
									.isDisplayed();
							if (eachWay == true && !isElementPresent(By.xpath("//div[@class='basic-scoreboard']//span[@class='live-badge__no-stream']"))) {
								eventName = GetElementText(By.xpath("//li//span[@data-bind='html: title']"));
								marketName = GetElementText(By
										.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
								selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
								counter = 1;
								break;
							} else{
								getDriver().navigate().back();
								Thread.sleep(2000);}
						}
					}
					if (counter == 1)
						break;
					else
						getDriver().navigate().back();
					Thread.sleep(2000);
				}
				else
				{
					eachWay = getDriver()
							.findElement(By
									.xpath("//div[@class='eachway']"))
							.isDisplayed();
					if (eachWay == true) {
						eventName = GetElementText(By.xpath("//div[@class='basic-scoreboard']//span[@class='name']")).toLowerCase();
						eventName = toTitleCase(eventName);
						if(eventName.contains("Us"))
							eventName = eventName.replace("Us", "US");
						marketName = GetElementText(By.xpath("(//div[@class='icon-arrow small-expand']/../span[@class='title'])[1]"));
						selNme = GetElementText(By.xpath("(//div[@class='selection-name'])[2]"));
						counter = 1;
						break;
					} else
						getDriver().navigate().back();
					Thread.sleep(2000);
				}
				}

			writeTestData("PreProdEvents", rowNum, 1, classNme);
			writeTestData("PreProdEvents", rowNum, 2, eventTypeName);
			writeTestData("PreProdEvents", rowNum, 4, eventName);
			writeTestData("PreProdEvents", rowNum, 5, selNme);
			writeTestData("PreProdEvents", rowNum, 6, marketName);
			if(subTypeName == null)
				writeTestData("PreProdEvents", rowNum, 3, "");
			else
				writeTestData("PreProdEvents", rowNum, 3, subTypeName);
			
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	
	
	public String writeTestData(String SheetName, int Rownum, int Columnnum, String value) throws IOException {
		String excelFilePath = "TestConfig/TestData.xlsx";
		FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		Workbook workbook = new XSSFWorkbook(inputStream);
		DataFormatter df = new DataFormatter();
		Cell FetchingData = workbook.getSheet(SheetName).getRow(Rownum).getCell(Columnnum);
		FetchingData.setCellValue(value);
		String TestData = df.formatCellValue(FetchingData);
		inputStream.close();
		FileOutputStream outFile = new FileOutputStream(new File(excelFilePath));
		workbook.write(outFile);
		outFile.close();
		return TestData;
	}
	
	public String Readtestdata(String SheetName, int Rownum, int Columnnum) throws IOException{
		 String excelFilePath = "TestConfig/TestData.xlsx";
		  
		        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		        Workbook workbook = new XSSFWorkbook(inputStream);
		        DataFormatter df = new DataFormatter();
		        
		        Cell FetchingData =workbook.getSheet(SheetName).getRow(Rownum ).getCell(Columnnum );
		        String TestData = df.formatCellValue(FetchingData);
		        //System.out.println("------>>" + TestData);
		        return TestData; 

}
	
}

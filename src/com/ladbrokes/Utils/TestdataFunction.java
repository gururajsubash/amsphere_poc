package com.ladbrokes.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.http.util.TextUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;

import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.testng.Assert;

public class TestdataFunction extends DriverCommon {
	HomeGlobal_Functions HGfunctions = new HomeGlobal_Functions();
	//WebDriver driver ;
	JavascriptExecutor js;

	

	
	public String Readtestdata(String SheetName, int Rownum, int Columnnum) throws IOException{
		 String excelFilePath = "TestConfig/TestData.xlsx";
		  
		        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
		        Workbook workbook = new XSSFWorkbook(inputStream);
		        DataFormatter df = new DataFormatter();
		        
		        Cell FetchingData =workbook.getSheet(SheetName).getRow(Rownum ).getCell(Columnnum );
		        String TestData = df.formatCellValue(FetchingData);
		        //System.out.println("------>>" + TestData);
		        return TestData; 

}
	

	/** 
	 * Navigates to specified type,sub type,Market and Event detail page
	 * @return none
	 * @throws Exception 
	 */
	
	
public String NavigateUsingTestData (int Rownumber) throws Exception{
	
	String ClassName , TypeName , Market , eventName ,SubType , verse ,Selection , ClassNameXpath , odds = null;
	String replaceItem;
	//double value = 0.0 ;
	
	try{
		 getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
		     ClassName = Readtestdata("PreProdEvents", Rownumber, 1);
			 TypeName = Readtestdata("PreProdEvents", Rownumber, 2);
			 Market = Readtestdata("PreProdEvents", Rownumber, 6);
			 SubType = Readtestdata("PreProdEvents", Rownumber, 3);
			 eventName = Readtestdata("PreProdEvents", Rownumber, 4);
			 System.out.println(eventName);
			 Selection = Readtestdata("PreProdEvents", Rownumber, 5);

            ClassNameXpath = "//li/a/span[text()='"+ ClassName +"']";
			 
			 if(eventName.contains("'"))
			 {
				 String evnt = eventName.replace("'", "XZ");
				 String event1 = evnt.substring(0,evnt.indexOf("XZ"));
				 eventName = event1;
			 }
			 
			 if (eventName.toLowerCase().contains("vs") || eventName.toLowerCase().contains("v") || eventName.contains("-"))
             {
                 if (eventName.toLowerCase().contains("vs"))
                     replaceItem = "vs";
                 else if (eventName.toLowerCase().contains(" - "))
                     replaceItem = "-";
                 else
                     replaceItem = "v";
                 
                 String[] arr = eventName.replace(replaceItem, "xz").split("xz");
                 eventName = arr[0];
                 eventName = eventName.substring(0, eventName.length()-1);
               }
			 if(TypeName.contains("'"))
			 {
				 String type = TypeName.replace("'", "XZ");
				 String type1 = type.substring(0,type.indexOf("XZ"));
				 TypeName = type1;
			 }
			 if(SubType.contains("'"))
			 {
				 String sub = SubType.replace("'", "XZ");
				 String sub1 = sub.substring(0,sub.indexOf("XZ"));
				 eventName = sub1;
			 }
			 if(Selection.contains("'"))
			 {
				 String sel = Selection.replace("'", "XZ");
				 String sel1 = sel.substring(0,sel.indexOf("XZ"));
				 eventName = sel1;
			 }
			
			click(HomeGlobalElements.AZMenu , "AZMenu is not clickable");
			scrollToView(By.xpath(ClassNameXpath));
			scrollAndClick(getDriver(), By.xpath(ClassNameXpath));
            HGfunctions.VerifyBreadCrums(TextControls.Home, ClassName , "" , "");
            
		    String Type =  "//li[@class='tab']//span[contains(text(),"+"\"" + TypeName + "\"" + ")]";
		    String eventpath = "//div[@class='name']//span[contains(text(), '"+ eventName +"')]";
		  
		    Thread.sleep(2000);
		    //* click on Type 
		    click(By.xpath(Type), " "+TypeName+" type is not clickable");
		    
		    //* Click on SubType
		    if(!TextUtils.isEmpty(SubType))
		    {
		    System.out.println("//*[text()='"+ SubType +"']");
		    click(By.xpath("//div[@class='links']//a[contains(text(),"+"\"" + SubType + "\"" + ")]") ," "+ SubType +" Subtype is not clickable");		   
	    	//scrollToView(By.xpath(eventpath));
	    	//scrollToElement(By.xpath(eventpath))
		    if(!isElementPresent(BetslipElements.EDPHeader))
		     clickByJS(getDriver().findElement(By.xpath("//div[@class='name']//span[contains(text(), '"+ eventName +"')]")));
			 }
		   
		   else if(!isElementPresent(BetslipElements.EDPHeader))
		    {
		    	//* click on Event
		    	 WebElement Element = getDriver().findElement(By.xpath(eventpath));
		    	 scrollToView(Element);
		    	 clickByJS(getDriver().findElement(By.xpath(eventpath)));
		    	
		       String eventInfo = "//div[@class='header-wrapper']//span[@class='name'  and contains(text(), '"+  eventName +"')]" ;
		       Assert.assertTrue(isElementDisplayed(By.xpath(eventInfo)), "Event information is not present in event details page");
		    }
		    
		 		    String selxpath = "//span[text()='"+ Market +"']//../..//div[text()='"+ Selection +"']/..//span[@class='odds-convert']";
		            Thread.sleep(2000);
		            click(By.xpath(selxpath) , ""+ Selection +" selection is not found");
		 		    odds =  getDriver().findElement(By.xpath(selxpath)).getText();
		            VerifySelectionInBetslip( eventName, TypeName, Market, Selection, odds);      
		            
	}
	catch ( Exception | AssertionError  e){
		Assert.fail(e.getMessage(),e);
	}
	return  odds ;
	
	}
public void VerifySelectionInBetslip(String eventName , String typeName ,String marketName , String selection , String odds){
	String oddSPxPath ,selXpath = null ;
	
	try{
		
	Thread.sleep(2000);
	Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipTitle), "Betslip is not displayed");
	if (typeName.contains("'"))
    {
        String typeNme = typeName.replace("'", "XZ");
        String typeName1 = typeNme.substring(0,typeNme.indexOf("XZ"));
        typeName = typeName1;
    }
    if (eventName.contains("'"))
    {
        String evtName = eventName.replace("'", "XZ");
        String eventName1 =evtName.substring(0,evtName.indexOf("XZ"));
        eventName = eventName1 ;
   }
    selXpath = "//div[@class='market-information-selection']//span[contains(text(), '" + selection + "')]/../following-sibling::div[contains(text(), '" + marketName + "')]/following-sibling::div[@class='market-information-event'][normalize-space(text()='" + eventName + " " + typeName + "')]";
	Assert.assertTrue(isElementDisplayed(By.xpath(selXpath)),"Selection-Event-Market '" + selection + "-" + eventName + "-" + marketName + "' was not found in the Betslip");

	 oddSPxPath = selXpath + "/../following::div[@class='odds-container']//div[text()='" + odds + "']";
	  if (!isElementDisplayed(By.xpath(oddSPxPath)))
         oddSPxPath = selXpath + "/../following-sibling::div//select/option[text()='" + odds + "']";
	  Assert.assertTrue(isElementDisplayed(By.xpath(oddSPxPath)), "Selection-Event-Market-Odd '" + selection + "-" + eventName + "-" + marketName + "-" + odds + "' was not found in the Betslip");
    
}
	catch ( Exception | AssertionError  e){
		Assert.fail(e.getMessage(), e);
	}

}
					

public void scrollAndClick(WebDriver driver, By by)throws Exception {
	WebElement element = driver.findElement(by);
	int elementPosition = element.getLocation().getY();
	String js = String.format("window.scroll(0, %s)", elementPosition);
	  ((JavascriptExecutor)driver).executeScript(js);
	   Thread.sleep(3000);
	   element.click();
    }
	
}
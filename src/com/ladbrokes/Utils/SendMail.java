package com.ladbrokes.Utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;
 
public class SendMail {
 @Test
	public void SendmailwithReport() throws InvalidPropertiesFormatException, IOException {
		String frommail=ReadTestSettingConfig.getTestsetting("FromMail");
		String mailpass=ReadTestSettingConfig.getTestsetting("MailPass");
		String tomail=ReadTestSettingConfig.getTestsetting("ToMail");
		String browsertype=ReadTestSettingConfig.getTestsetting("BrowserType");
		String projectName=ReadTestSettingConfig.getTestsetting("ProjectName");
		String runid=ReadTestSettingConfig.getTestsetting("runID");
		String TestrailURL ="https://ladbrokescoral.testrail.com/index.php?/runs/view/"+runid+"&group_by=cases:section_id&group_order=asc";
 
		// Create object of Property file
		Properties props = new Properties();
		// this will set host of server- you can change based on your requirement 
		props.put("mail.smtp.host", "smtp.gmail.com");
		// set the port of socket factory 
		props.put("mail.smtp.socketFactory.port", "465");
		// set socket factory
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		// set the authentication to true
		props.put("mail.smtp.auth", "true");
		// set the port of SMTP server
		props.put("mail.smtp.port", "465");
		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(frommail, mailpass);
					}
				});

		try {
			// Create object of MimeMessage class
			Message message = new MimeMessage(session);
			// Set the from address
			message.setFrom(new InternetAddress(frommail));
			// Set the recipient address
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(tomail));
            // Add the subject link
			message.setSubject("SB Desktop Automation Report for: " + projectName + " (" + browsertype + ")" + " - " + getCurrentDate());
			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();
			// Set the body of email
			messageBodyPart1.setText("Hello All, " + "Screenshot of the Desktop P1 Automation Report is attached to this mail."+ "\n\n" + "TestRail Path of the run: "+TestrailURL);
			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			// Mention the file which you want to send
			String filename = System.getProperty("user.dir") + "//Reports//"+ projectName + "_" + browsertype + ".png";
			// Create data source and pass the filename
			DataSource source = new FileDataSource(filename);
			// set the handler
			messageBodyPart2.setDataHandler(new DataHandler(source));
			// set the file
			messageBodyPart2.setFileName(filename);
			// Create object of MimeMultipart class
			Multipart multipart = new MimeMultipart();
			// add body part 1
			multipart.addBodyPart(messageBodyPart1);
			// add body part 2
			multipart.addBodyPart(messageBodyPart2);
			// set the content
			message.setContent(multipart);
			// finally send the email
			Transport.send(message);
			System.out.println("=====Email Sent=====");
 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
 
	public String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
		Calendar cal = Calendar.getInstance();
		String getCurrentDate = sdf.format(cal.getTime());
		return getCurrentDate.toUpperCase();
	}
	
}

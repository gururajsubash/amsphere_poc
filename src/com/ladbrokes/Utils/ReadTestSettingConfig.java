package com.ladbrokes.Utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class ReadTestSettingConfig {

	public static String getTestsetting(String args1) throws InvalidPropertiesFormatException, IOException {
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream("TestConfig/TestSetting.xml");
		prop.loadFromXML(fis);
		String testsettingValue = prop.getProperty(args1);
		return testsettingValue;
	}

	public static void setTestsetting(String RunIDValue) throws InvalidPropertiesFormatException, IOException {
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "//TestConfig//TestSetting.xml//");
		prop.loadFromXML(fis);
		prop.setProperty("runID", RunIDValue);
		FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + "//TestConfig//TestSetting.xml//");
		prop.storeToXML(fos, "");
		fis.close();
		fos.close();
	}

}

package com.ladbrokes.GermanOR;

import org.openqa.selenium.By;

public class BetslipElements extends com.ladbrokes.EnglishOR.BetslipElements{
	
	public static final By stakeBox = By.xpath("//input[starts-with(@id,'slip-odds-stake')]");
	public static final By placeBet = By.xpath("//button[@class='button action' and text()='WETTE PLATZIEREN']");
	public static final By betSlipTitle = By.xpath("//header[contains(@class,'betslip-header')]//span[contains(text(), 'Wettschein')]");
	public static final By betSlipCounter = By.xpath("//span[@data-bind='text: singleBetCount()']");
	public static final By betSlipActive= By.xpath("//li[@class='betslip-tab active']//span[contains(text(),'Wettschein')]");
	public static final By betSlipTab= By.xpath("//li[@class='betslip-tab']//span[contains(text(),'Wettschein')]");
    public static final By acceptPlaceBet= By.xpath("//footer[contains(@class, 'acceptNeeded')]//button[contains(@class, 'button')]");
    public static final By betReceiptBanner =By.xpath("//div[@class='receipts']//betslip-reciept[contains(@params, 'bet')]");
    public static final By betSuccessfulMsg =By.xpath("//div[@id='betslip-container']/div/div[2]/span[1]");
    public static final By tickMarkOnBetslip =By.xpath("//div[@id='betslip-container']/div/div[2]/span[2]");
    public static final By doneOnBetslip = By.xpath("//button[contains(text(), 'Fertig')]");
    public static final By HomePage_RightColumnRaces = By.xpath("//div[@class='right-column']//next-races//selection-button//..//div[contains(@class, 'odds-button')]");
    public static final By HomePage_RightColumnEvents = By.xpath("//div[@class='right-column']//event-group//selection-button//..//div[contains(@class, 'odds-button')]");
    public static final By totalStake = By.xpath("//div[contains(text(), 'Gesamteinsatz')]/following-sibling::div[@class='amount']");
    public static final By SelInBetSlip = By.xpath("//div[@class='market-information-selection']");
    public static final By betInfoContainer = By.xpath("//div[@class='bet-expander']");
    public static final By minValueInBetslip = By.xpath("//div[@class='bet-expander-stakes']//span[contains(@data-bind,'minStakeErrorMessage')]");
    public static final By maxValueInBetslip = By.xpath("//div[@class='bet-expander-stakes']//span[contains(@data-bind,'maxStakeErrorMessage')]");
    public static final By minStakeErrorMessage = By.xpath("//div[@class='bet-error']//span[contains(@data-bind,'minStakeErrorMessage')]");
    public static final By maxStakeErrorMessage = By.xpath("//div[@class='bet-error maxStakeErrorMessage']//span[@data-bind='text: maxStakeErrorMessage']");
    public static final By inPlay = By.xpath("//span[text()='Live']");
    public static final By eventClick = By.xpath("(//div[@class='event']//div[@class='class'])[1]");
   public static final By edpOddClick = By.xpath("(//div[@class='odds-button'])[1]");
   public static final By reUseOnBetslip = By.xpath("//button[contains(text(), 'Gleiche Auswahl')]");
   public static final By totalPotentialReturns = By.xpath("//div[contains(text(), 'M�gliche Gewinne')]/following-sibling::div[@id='potential_return_total']");
   public static final By betReceiptNo = By.xpath("//span[@class='receipt-receiptno-value']");
   public static final By EDPHeader = By.xpath("//div[@class='basic-scoreboard']");
   public static final By removeSel = By.xpath("//div[@class='slip-remove']");
   public static final By removeAllBtn = By.xpath("//div[@class='betslip-container-remove-all-text' and contains(text(),'Alle entfernen')]");
   public static final By receiptOdds =By.xpath("//div[@class='receipt-odds']//span[@class='receipt-odds-value' and contains(@data-bind, 'text')]");
   public static final By betReceiptContainer =By.xpath("//div[@class='receipt']");
   public static final By freeBetsTitle = By.xpath("//span[text()='Gratiswetten']");
   public static final By anySportFreeBetRadio = By.xpath("//div[@class='information']//span[contains(text(),'Any')]/../following-sibling::input");
   public static final By freeBetOfYourChoiceRadio = By.xpath("//div[@class='information']//span[contains(text(),'FREE')]/../following-sibling::input");
   public static final By footballFreeBetRadio = By.xpath("//div[@class='information']//span[contains(text(),'Football')]/../following-sibling::input");
   public static final By totalFreeBets = By.xpath("//div[@class='slip-total-stake']/following-sibling::div/div[@id='display_freebet_total']");
   public static final By freebetDeductInfo = By.xpath("//span[@class='freebet-deduct' and text()='Der Gratsiswettenwert wird vom Gewinn abgezogen']");
   public static final By freebetErrorInfo = By.xpath("//div[@class='freebet-error freebet-error-invalid-stake' and text()='Bitte erh�hen Sie Ihren Einsatz, um die gew�hlte Gratiswette zu nutzen.']");
   public static final By betslipInPlay = By.xpath("//span[@data-bind and contains(text() ,'Live')]");
   
   //Odds Boost Controls
   public static final By oddsBoostTittle = By.xpath("//div[@class='text-header' and text()='Preis-Boost']");
   public static final By oddsBoostTooltip = By.xpath("//div[@class='text-container']/div[text()='Boosten Sie Ihre Quoten.']");
   public static final By oddsBoostTooltipInfo = By.xpath("//div[@class='info-header' and contains(text(),'Mit Boosten erh�hen Sie die Quoten der Wetten auf Ihrem Wettschein!')]");
   public static final By oddsBoostTooltipIcon = By.xpath("//div[@class='text-container']//oddsboost-tooltip[@params]");
   public static final By oddsBoostButton = By.xpath("//div[@class='odds-boost-button-container']/div[text()='boost']");
   public static final By oddsBoostButtonIcon = By.xpath("//div[@class='odds-boost-button-container']/div[@class='boost-icon']");
   public static final By BoostedButton = By.xpath("//div[@class='odds-boost-button-container']/div[text()='geboostet']");
   public static final By boostedValue = By.xpath("//div[@class='odds-container']//div//div[@class='boosted-value']");
   public static final By seceondSelBoost = By.xpath("//*[@id='betslip-container']/div[2]/div[3]/div[5]/div[3]/oddsboost-odds-animator/div/div[2]");
   public static final By oddsBoostReceipt = By.xpath("//span[@class='odds-boost-receipt-container-content-container-text' and contains(text(),'Diese Wette wurde ')]//b[text()='geboostet!']");
   public static final By oddsBoostReceiptIcon = By.xpath("//div[@class='odds-boost-receipt-container-content-container-icon']");
   public static final By ReceiptOdds = By.xpath("//div[@class='receipt-odds']//span[@class='receipt-odds-value']");
   public static final By MultiplesBoostedOdd = By.xpath("//*[@id='betslip-container']/div[2]/div[5]/div/div[1]/div[2]/oddsboost-odds-animator-multiples/div/div[2]");
   public static final By betslipHeaderRightButton = By.xpath("//span[text()='Betslip']");
   public static final By EWErrorMsgOB = By.xpath("//div[@class='alert-box alert-box-betslip']//div[@class='message' and contains(text(),'Odds Boost is unavailable for EW selections!')]");
   public static final By EWErrorMsgOB_OkBtn = By.xpath("//div[@class='alert-box alert-box-betslip']//div[@class='button ok enabled']");
}

package com.ladbrokes.GermanOR;
import org.openqa.selenium.By ;

public class MyBetsControls extends com.ladbrokes.EnglishOR.MyBetsControls{
	public static final By myBets = By.xpath("//li[starts-with(@class,'betslip-tab')]//span[text()='Meine Wetten']");
    public static final By myBetsCount = By.xpath("//span[@id='mybets-indicator']");
    public static final By openBets = By.xpath("//div[@class='title-container' and text()='Offene Wetten']");
    public static final By openBetsCount = By.xpath("//div[@class='title-container' and text()='Open Bets']/following-sibling::div[@class='count']");
    public static final By openBetsInfoMsg = By.xpath("//div[@class='tabs-content']//div[@class='mb-show-more-info' and text()[1]='Your open bets will appear here ' and text()[2]=' please login to view']");
    public static final By myAccas = By.xpath("//div[@class='title-container' and text()='Meine Kombiwetten']");
    public static final By myAccasCount = By.xpath("//div[@class='title-container' and text()='My Accas']/following-sibling::div[@class='count']");
    public static final By myAccasInfoMsg = By.xpath("//div[@class='my-accas-description-inner' and text()='Fu�ball-Kombiwetten erscheinen hier nach Platzierung']");
    public static final By createNewAcca = By.xpath("//div[@class='my-accas-new-acca-text' and text()='CREATE NEW ACCA']/../a/div[@class='my-accas-new-acca-button']");
    public static final By settledBets = By.xpath("//div[@class='title-container' and text()='Abgerechnete Wetten']");
    public static final By settledBetsCount = By.xpath("//div[@class='title-container' and text()='Settled Bets']/following-sibling::div[@class='count']");
    public static final By settledBetsInfoMsg = By.xpath("//div[@class='tabs-content']//div[@class='mb-show-more-info' and text()[1]='Your settled bets will appear here ' and text()[2]=' please login to view']");


    public static final By mybetHistroyItems = By.xpath("//div[@class='bet-detail']");
    public static final By myBetDownArrow = By.xpath("//div[@class='mb-header-arrow-container']");
    public static final By accountHistoryText = By.xpath("//span[text()[1]='For detailed bet history or to search by date,' and text()[2]=' go to']/../a[text()='Account History']");
    public static final By cashoutTermsAndCoText = By.xpath("//span[text()='Cash Out Terms &amp; Conditions can be found ']/../a[text()='here']");

    public static final By cashoutInOpenBets = By.xpath("//div[@class='button']/following::div[text()='AUSZAHLEN']/following-sibling::div[@class='cashout-holder-value']");
    public static final By cashoutNotification = By.xpath("//span[@class='cashout-flow-notifications black']");
    public static final By cashoutDecline = By.xpath("//div[contains(@class, 'button') and text()='ABLEHNEN']");
    public static final By cashoutAccept = By.xpath("//div[contains(@class, 'button') and text()='AKZEPTIEREN']");
    public static final By cashoutSpinner = By.xpath("//span[contains(text()[1],'BITTE WARTEN SIE, BIS ') and contains(text()[2],'WIR IHRE WETTE AUSGEZAHLT HABEN')]");
    public static final By cashoutTick = By.xpath("//div[@class='top-message-icon']");

    public static final By accaTitle = By.xpath("//div[@class='my-accas-header-dropdown-title' and contains(text(),'ACCA')]");
    public static final By hidePricesAcca = By.xpath("//span[contains(text(),'1X2 ausblenden')]");
    public static final By showPricesAcca = By.xpath("//span[contains(text(),'1X2 anzeigen')]");
    public static final By gamesValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Spiele')]/following::div[contains(@data-bind,'games')]");
    public static final By stakeValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Einsatz')]/following::div[contains(@data-bind,'stake')]");
    public static final By prValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Potenzial') or contains(text(),'Returns')]/following::div[contains(@data-bind,'accaPotentialReturn')]");
    public static final By accaActive = By.xpath("//div[@class='acca-status' and text()='YOUR ACCA IS ACTIVE']");
    public static final By historyLinkMyBets = By.xpath("//*[contains(text(),'Kontoverlauf')]");
    public static final By historyInfoTextAcca = By.xpath("//span[text()[1]='F�r einen ausf�hrlichen Wettverlauf' and text()[2]=' gehen Sie auf']");
    public static final By cashoutInfoTextAcca = By.xpath("//span[text()='Cash-out-Bedingungen finden Sie ']");
    public static final By cashoutLinkAcca = By.xpath("//a[@class='cashout-terms-link' and text()='hier']");
    public static final By cashoutBtnAcca = By.xpath("//div[@class='cashout-holder']//span[text()='CASH OUT:']/following-sibling::span[@class='cashout-holder-value'][1]");
    public static final By accaDescText = By.xpath("//div[@class='my-accas-description-inner' and text()='Fu�ball-Kombiwetten erscheinen hier nach Platzierung']");
    public static final By accaAddBtn = By.xpath("//div[@class='my-accas-new-acca-button']");
    public static final By createAccaText = By.xpath("//div[@class='my-accas-new-acca-text' and text()='NEUE KOMBIWETTE']");
    public static final By couponActiveTab = By.xpath("//div[@class='link' and contains(text(),'Coupons')]/ancestor::a");
    public static final By oddsColl = By.xpath("//div[@class='odds-button']");
    public static final By prReceiptValue = By.xpath("//div[contains(@data-bind,'receiptPotentialReturns')]");
    public static final By accaDropDown = By.xpath("//span[@class='my-accas-header-dropdown']");
    public static final By newAccaInDD = By.xpath("//*[text()='CREATE NEW ACCA']");
    public static final By betTypeAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Bet Type')]");
    public static final By prAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Potential Returns')]");
    public static final By dateAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Placement Date')]");
    public static final By oddsInAcca = By.xpath("//div[@class='acca-event-selections']");
    public static final By accasInDD = By.xpath("//span[@class='my-accas-dropdown-option']");
    public static final By cashedOutAcca = By.xpath("//div[@class='acca-status']//div[contains(text(),'Sie haben Ihre Kombiwette ausgezahlt')]");
    public static final By showMoreInHistory = By.xpath("//span[contains(text(),'Show more')]");
    public static final By moreMarketsInAccas = By.xpath("//span[contains(@data-bind,'numberOfMarketsMore')]");
    public static final By firstEventNameInAccas = By.xpath("(//div[contains(@data-bind,'homeTeamName')])[1]");
    public static final By cashoutIconInEDP = By.xpath("//div[@class='cash-out-badge']");
    public static final By settledAccasInDD = By.xpath("//span[@class='my-accas-dropdown-option']//span[contains(text(),'LOST') or contains(text(),'WON') or contains(text(),'CASHED OUT')]");
    public static final By openAccasInDD = By.xpath("//span[@class='my-accas-dropdown-option']//span[@class='my-accas-dropdown-option-title'][1]");
    public static final By balRefresh = By.xpath("//div[@class='balance refresh']");

}

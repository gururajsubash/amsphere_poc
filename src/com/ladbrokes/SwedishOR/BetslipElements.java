package com.ladbrokes.SwedishOR;

import org.openqa.selenium.By;

public class BetslipElements extends com.ladbrokes.EnglishOR.BetslipElements {
	public static final By stakeBox = By.xpath("//input[starts-with(@id,'slip-odds-stake')]");
	public static final By placeBet = By.xpath("//button[@class='button action' and text()='L�GG SPEL']");
	public static final By betSlipTitle = By
			.xpath("//header[contains(@class,'betslip-header')]//span[contains(text(), 'Spelkupong')]");
	public static final By betSlipCounter = By.xpath("//span[@data-bind='text: singleBetCount()']");
	public static final By betSlipActive = By
			.xpath("//li[@class='betslip-tab active']//span[contains(text(),'Spelkupong')]");
	public static final By betSlipTab = By.xpath("//li[@class='betslip-tab']//span[contains(text(),'Spelkupong')]");
	public static final By acceptPlaceBet = By.xpath("//button[contains(text(),'ACCEPTERA OCH FORTS�TT')]");
	public static final By betReceiptBanner = By
			.xpath("//div[@class='receipts']//betslip-reciept[contains(@params, 'bet')]");
	public static final By betSuccessfulMsg = By
			.xpath("//div[@class='slip-placed-success']/span[contains(text(), 'Spelet �r lagt')]");
	public static final By tickMarkOnBetslip = By.xpath("//div[@id='betslip-container']/div/div[2]/span[2]");
	public static final By doneOnBetslip = By
			.xpath("//footer[@class='betslip-footer']//button[contains(text(), 'Klar')]");
	public static final By HomePage_RightColumnRaces = By.xpath(
			"//div[@class='right-column']//next-races//selection-button//..//div[contains(@class, 'odds-button')]");
	public static final By HomePage_RightColumnEvents = By.xpath(
			"//div[@class='right-column']//event-group//selection-button//..//div[contains(@class, 'odds-button')]");
	public static final By totalStake = By
			.xpath("//div[contains(text(), 'Total insats')]/following-sibling::div[@class='amount']");
	public static final By SelInBetSlip = By.xpath("//div[@class='market-information-selection']");
	public static final By betInfoContainer = By.xpath("//div[@class='bet-expander']");
	public static final By minValueInBetslip = By
			.xpath("//div[@class='bet-expander-stakes']//span[contains(@data-bind,'minStakeErrorMessage')]");
	public static final By maxValueInBetslip = By
			.xpath("//div[@class='bet-expander-stakes']//span[contains(@data-bind,'maxStakeErrorMessage')]");
	public static final By minStakeErrorMessage = By
			.xpath("//div[@class='bet-error']//span[contains(@data-bind,'minStakeErrorMessage')]");
	public static final By maxStakeErrorMessage = By
			.xpath("//div[@class='bet-error maxStakeErrorMessage']//span[@data-bind='text: maxStakeErrorMessage']");
	public static final By inPlay = By.xpath("//span[text()='Live']");
	public static final By eventClick = By.xpath("(//div[@class='event']//div[@class='class'])[1]");
	public static final By edpOddClick = By.xpath("(//div[@class='odds-button'])[1]");
	public static final By reUseOnBetslip = By.xpath("//button[contains(text(), '�teranv�nd valen')]");
	public static final By totalPotentialReturns = By.xpath(
			"//div[contains(text(), 'Total m�jlig vinst')]/following-sibling::div[@id='potential_return_total']");
	public static final By betReceiptNo = By.xpath("//span[@class='receipt-receiptno-value']");
	public static final By EDPHeader = By.xpath("//div[@class='basic-scoreboard']");
	public static final By removeSel = By.xpath("//div[@class='slip-remove']");
	public static final By removeAllBtn = By
			.xpath("//div[@class='betslip-container-remove-all-text' and contains(text(),'Ta bort alla')]");
	public static final By receiptOdds = By
			.xpath("//div[@class='receipt-odds']//span[@class='receipt-odds-value' and contains(@data-bind, 'text')]");
	public static final By betReceiptContainer = By.xpath("//div[@class='receipt']");
	public static final By freeBetsTitle = By.xpath("//span[text()='Bonusspel']");
	public static final By anySportFreeBetRadio = By
			.xpath("//div[@class='information']//span[text()='Any']/../following-sibling::input");
	public static final By footballFreeBetRadio = By
			.xpath("//div[@class='information']//span[contains(text(),'Football')]/../following-sibling::input");
	public static final By totalFreeBets = By
			.xpath("//div[@class='slip-total-stake']/following-sibling::div/div[@id='display_freebet_total']");
	public static final By freebetDeductInfo = By.xpath(
			"//span[@class='freebet-deduct' and text()='Insats f�r bonusspel kommer att dras bort fr�n potentiell vinst']");
	public static final By freebetErrorInfo = By.xpath(
			"//div[@class='freebet-error freebet-error-invalid-stake' and text()='�ka insatsen f�r att anv�nda det valda bonusspelet.']");
	public static final By betslipInPlay = By.xpath("//span[@data-bind and contains(text() ,'Live')]");
	public static final By freeBetOfYourChoiceRadio = By.xpath("");
	// OddsBoost Related Elements
	public static final By oddsBoostTittle = By.xpath("//div[@class='text-header']");
	//public static final By oddsBoostTittle = By.xpath("//div[@class='text-header' and text()='Odds Boost']");
	public static final By oddsBoostTooltipInfo = By.xpath(
			"//div[@class='info-header' and contains(text(),'Tryck p� Boost f�r att h�ja oddsen p� spelen i spelkupongen!')]");
	public static final By oddsBoostTooltipIcon = By
			.xpath("//div[@class='text-container']//oddsboost-tooltip[@params]");
	public static final By oddsBoostTooltip = By
			.xpath("//div[@class='text-container']//div[contains(text(),'Tryck p� Boost f�r att h�ja oddsen p� spelen i spelkupongen!')]");
	public static final By oddsBoostButton = By
			.xpath("//div[@class='odds-boost-button-container']/div[text()='boost']");
	public static final By oddsBoostButtonIcon = By
			.xpath("//div[@class='odds-boost-button-container']/div[@class='boost-icon']");
	public static final By BoostedButton = By
			.xpath("//div[@class='odds-boost-button-container']/div[text()='boostad']");
	public static final By boostedValue = By.xpath("//div[@class='odds-container']//div//div[@class='boosted-value']");
	public static final By seceondSelBoost = By
			.xpath("//*[@id='betslip-container']/div[2]/div[3]/div[5]/div[3]/oddsboost-odds-animator/div/div[2]");
	public static final By oddsBoostReceipt = By.xpath(
			"//span[@class='odds-boost-receipt-container-content-container-text']/b[contains(text(),'boostat')]");
	public static final By oddsBoostReceiptIcon = By
			.xpath("//div[@class='odds-boost-receipt-container-content-container-icon']");
	public static final By ReceiptOdds = By.xpath("//div[@class='receipt-odds']//span[@class='receipt-odds-value']");
	public static final By betslipHeaderRightButton = By.xpath("//span[text()='Spelkupong']");
	public static final By MultiplesBoostedOdd = By.xpath(
			"//*[@id='betslip-container']/div[2]/div[5]/div/div[1]/div[2]/oddsboost-odds-animator-multiples/div/div[2]");
	public static final By EWErrorMsgOB = By.xpath(
			"//div[@class='alert-box alert-box-betslip']//div[@class='message' and contains(text(),'Odds Boost is unavailable for EW selections!')]");
	public static final By EWErrorMsgOB_OkBtn = By
			.xpath("//div[@class='alert-box alert-box-betslip']//div[@class='button ok enabled']");
}

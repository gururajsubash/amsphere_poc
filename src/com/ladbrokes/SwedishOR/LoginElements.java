package com.ladbrokes.SwedishOR;
import org.openqa.selenium.By;

public class LoginElements extends com.ladbrokes.EnglishOR.LoginElements{

	public static final By Login = By.xpath("//*[@class='button' and contains(text(), 'Logga in')]");
	public static final By Username = By.xpath("//input[@id='username']");
	public static final By Password = By.xpath("//input[@id='password']");
	public static final By NonSportsSiteUsername = By.xpath("//input[@name='username']");
	public static final By NonSportsSitePassword = By.xpath("//input[@name='password']");
	public static final By casinoLogin = By.xpath("//div//button[@title='Logga in']");
	public static final By BingoLogin=By.xpath("//button[@title='Logga in']");
	public static final By ladbrokesLogo = By.xpath("//h1[@class='logo']//a[contains(text(),'Ladbrokes')]");
	public static final By depositBtn = By.xpath("//div[contains(text(), 'Ins�ttning')]");
	public static final By depositIcon = By.xpath("//div[@class='icon-deposit']");
	public static final By settingsBtn = By.xpath("//div[@class='icon-settings']");
	public static final By settingsContent= By.xpath("//div[contains(@class, 'tooltip-content stngs')]");
	public static final By settingsStaticText = By.xpath("//div[@class='tooltip-header' and text()='Inst�llningar']/following-sibling::h2[text()='Visa odds som']");
	public static final By fractionalOdd = By.xpath("//span[@class='title' and text()='Br�kdelar']/following-sibling::input[@type='radio' and @value='fractional']");
	public static final By decimalOdd = By.xpath("//span[@class='title' and text()='Decimaler']/following-sibling::input[@type='radio' and @value='decimal']");
	public static final By ContactUsBtn = By.xpath("//div[contains(@class,'icon-contact')]");
	public static final By contactUsContent = By.xpath("//contact-us[1]");
	public static final By contactUsStaticText = By.xpath("//div[@class='tooltip-header' and text()='Kontakta oss']");
	public static final By liveChatLinkHeader = By.xpath("//div[@class='tooltip-section-link-content']//div[text()='Go to Live Chat']");
	public static final By liveChatFooterLink = By.xpath("//div[@class='footer-links-list']//a[contains(text(),'Starta livechatt')]");
	public static final By loginErrorpanel = By.xpath("//div[@class='login-error']/p");
	public static final By userBalance =By.xpath("//div[contains(@class, 'balance')]/following-sibling::div[@data-bind='currencyFormat: user.balance']");
	public static final By insuffFundsText = By.xpath("//li[@class='bet-error' and contains(text(), 'S�tt in mer pengar p� ditt konto f�r att l�gga spelet.')]");
	public static final By userNameAfterLogin = By.xpath("//div[contains(@class, 'user')]//following-sibling::div[starts-with(@class, 'name')]");
	public static final By logOutBtn = By.xpath("//div[@id='logoutSubmit']");
	public static final By forgotLoginDetails = By.xpath("//span[@class='forgot-login']//span");
	public static final By loginPassword = By.xpath("//input[@id='password']");
    public static final By forgotUsernameField = By.xpath("//input[@name='userName']");
    public static final By forgotEmail = By.xpath("//input[@name='email']");
    public static final By successMessage = By.xpath("//p[@class='success-message']");
    public static final By forgotPwdSubmitBtn = By.xpath("//strong[text()='SKICKA']");
    public static final By forgotUsrSubmitBtn = By.xpath("//strong[text()='SKICKA']");
    public static final By lostusrNameTab=By.xpath("//*[@id='lost-name-tab']");
    //public static final By forgotUsrSubmitBtn = By.xpath("//div[@class='popup-tab lost-name-tab']//button/strong[text()='SKICKA']");
    public static final By lostUNameMessage = By.xpath("//div[@class='portlet-msg portlet-msg-error']/p");
    public static final By dateDropDown = By.xpath("//select[@name='birthDay']");
    public static final By monthDropDown = By.xpath("//select[@name='birthMonth']");
    public static final By yearDropDown = By.xpath("//select[@name='birthYear']"); 
    public static final By forgottenUsernameTab = By.xpath("//a[contains(text(),'Forgotten Username?')]");
    public static final By rememberMe = By.xpath("//span[text()= 'Kom ih�g mig']/../input[@id='remember-me']");
    public static final By email=By.xpath("//input[@name='email']");
}
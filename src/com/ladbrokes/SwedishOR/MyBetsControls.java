package com.ladbrokes.SwedishOR;
import org.openqa.selenium.By ;

public class MyBetsControls extends com.ladbrokes.EnglishOR.MyBetsControls{
	public static final By myBets = By.xpath("//li[starts-with(@class,'betslip-tab')]//span[text()='Mina spel']");
    public static final By myBetsCount = By.xpath("//span[@id='mybets-indicator']");
    public static final By openBets = By.xpath("//div[@class='title-container' and text()='�ppna spel']");
    public static final By openBetsCount = By.xpath("//div[@class='title-container' and text()='�ppna spel']/following-sibling::div[@class='count']");
    public static final By openBetsInfoMsg = By.xpath("//div[@class='tabs-content']//div[@class='mb-show-more-info' and text()[1]='Dina �ppna spel kommer att visas h�r, ' and text()[2]=' logga in f�r att se dem']");
    public static final By myAccas = By.xpath("//div[@class='title-container' and text()='Mina kombispel']");
    public static final By myAccasCount = By.xpath("//div[@class='title-container' and text()='Mina kombispel']/following-sibling::div[@class='count']");
    public static final By myAccasInfoMsg = By.xpath("//div[@class='my-accas-description-inner' and text()='Fotbollsackumulatorer kommer visas h�r n�r du lagt dem']");
    public static final By createNewAcca = By.xpath("//div[@class='my-accas-new-acca-text' and text()='SKAPA NYTT KOMBISPEL']/../a/div[@class='my-accas-new-acca-button']");
    public static final By settledBets = By.xpath("//div[@class='title-container' and text()='R�ttade spel']");
    public static final By settledBetsCount = By.xpath("//div[@class='title-container' and text()='R�ttade spel']/following-sibling::div[@class='count']");
    public static final By settledBetsInfoMsg = By.xpath("//div[@class='tabs-content']//div[@class='mb-show-more-info' and text()[1]='Dina r�ttade spel kommer att visas h�r, ' and text()[2]=' logga in f�r att se dem']");

    public static final By mybetHistroyItems = By.xpath("//div[@class='bet-detail']");
    public static final By myBetDownArrow = By.xpath("//div[@class='mb-header-arrow-container']");
    public static final By accountHistoryText = By.xpath("//span[text()[1]='F�r att se detaljerad spelhistorik eller s�ka p� datum,' and text()[2]=' g� till']/../a[text()='Kontohistorik']");
    public static final By cashoutTermsAndCoText = By.xpath("//span[text()='Du hittar regler och villkor f�r Cash Out ']/../a[text()='h�r']");

    public static final By cashoutInOpenBets = By.xpath("//div[@class='button']/div[text()='CASH OUT']/following-sibling::div[@class='cashout-holder-value']");
    public static final By cashoutNotification = By.xpath("//span[@class='cashout-flow-notifications black']");
    public static final By cashoutDecline = By.xpath("//div[contains(@class, 'button') and text()='TACKA NEJ']");
    public static final By cashoutAccept = By.xpath("//div[contains(@class, 'button') and text()='ACCEPTERA']");
    public static final By cashoutSpinner = By.xpath("//span[contains(text()[1],'V�NLIGEN V�NTA MEDAN') and contains(text()[2],' DITT SPEL CASHAS UT')]");
    public static final By cashoutTick = By.xpath("//div[@class='top-message-icon']");

    public static final By accaTitle = By.xpath("//div[@class='my-accas-header-dropdown-title' and contains(text(),'ACCA')]");
    public static final By hidePricesAcca = By.xpath("//span[contains(text(),'G�m 1X2')]");
    public static final By showPricesAcca = By.xpath("//span[contains(text(),'Visa 1X2')]");
    public static final By gamesValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Spel')]/following::div[contains(@data-bind,'games')]");
    public static final By stakeValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Insats')]/following::div[contains(@data-bind,'stake')]");
    public static final By prValueAcca = By.xpath("//div[@class='acca-info-title' and contains(text(),'Potential') or contains(text(),'M�jlighet:')]/following::div[contains(@data-bind,'accaPotentialReturn')]");
    public static final By accaActive = By.xpath("//div[@class='acca-status' and text()='DITT KOMBISPEL �R AKTIVT']");
    public static final By historyLinkMyBets = By.xpath("//*[contains(text(),'ontohistorik')]");
                                                               
    public static final By historyInfoTextAcca = By.xpath("//span[text()[1]='F�r att se detaljerad spelhistorik eller s�ka p� datum,' and text()[2]=' g� till']");
    public static final By cashoutInfoTextAcca = By.xpath("//span[text()='Du hittar regler och villkor f�r Cash Out  ']");
    public static final By cashoutLinkAcca = By.xpath("//a[@class='cashout-terms-link' and text()='h�r']");
    public static final By cashoutBtnAcca = By.xpath("//div[@class='cashout-holder']//span[text()='CASH OUT:']/following-sibling::span[@class='cashout-holder-value'][1]");
    public static final By accaDescText = By.xpath("//div[@class='my-accas-description-inner' and text()='Fotbollsackumulatorer kommer visas h�r n�r du lagt dem']");
    public static final By accaAddBtn = By.xpath("//div[@class='my-accas-new-acca-button']");
    public static final By createAccaText = By.xpath("//div[@class='my-accas-new-acca-text' and text()='SKAPA NYTT KOMBISPEL']");
    public static final By couponActiveTab = By.xpath("//div[@class='link' and text()='Fotbollskuponger']/ancestor::a");
    public static final By oddsColl = By.xpath("//div[@class='odds-button']");
    public static final By prReceiptValue = By.xpath("//div[contains(@data-bind,'receiptPotentialReturns')]");
    public static final By accaDropDown = By.xpath("//span[@class='my-accas-header-dropdown']");
    public static final By newAccaInDD = By.xpath("//*[text()='CREATE NEW ACCA']");
    public static final By betTypeAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Speltyp')]");
    public static final By prAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Potentiell utbetalning')]");
    public static final By dateAccaDD = By.xpath("//span[@class='my-accas-dropdown-header-title']/div[contains(text(),'Datum f�r lagt spel')]");
    public static final By oddsInAcca = By.xpath("//div[@class='acca-event-selections']");
    public static final By accasInDD = By.xpath("//span[@class='my-accas-dropdown-option']");
    public static final By cashedOutAcca = By.xpath("//div[@class='acca-status']//div[contains(text(),'Du har cashat ut ditt kombispel')]");
    public static final By showMoreInHistory = By.xpath("//span[contains(text(),'Visa mer')]");
    public static final By moreMarketsInAccas = By.xpath("//span[contains(@data-bind,'numberOfMarketsMore')]");
    public static final By firstEventNameInAccas = By.xpath("(//div[contains(@data-bind,'homeTeamName')])[1]");
    public static final By cashoutIconInEDP = By.xpath("//div[@class='cash-out-badge']");
    public static final By settledAccasInDD = By.xpath("//span[@class='my-accas-dropdown-option']//span[contains(text(),'LOST') or contains(text(),'WON') or contains(text(),'CASHED OUT')]");
    public static final By openAccasInDD = By.xpath("//span[@class='my-accas-dropdown-option']//span[@class='my-accas-dropdown-option-title'][1]");
    public static final By balRefresh = By.xpath("//div[@class='balance refresh']");
}

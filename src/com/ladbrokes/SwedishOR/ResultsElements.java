package com.ladbrokes.SwedishOR;
import org.openqa.selenium.By;


public class ResultsElements {

		public static final By resultsTab = By.xpath("//span[@class='results-text' and contains(text(),'Resultat')]");
		public static final By resultsTabIcon = By.xpath("//span[@class='icon-results']");
		public static final By resultsTitle = By.xpath("//div[@class='icon-arrow expand']/..//div[@class='results-container__title' and text()='Resultat']");
		public static final By moreTab = By.xpath("//li[@class='tab tabs-dropdown dropdown']//div[@class='title-container' and text()='Fler']");
		public static final By type = By.xpath("//h2[@class='expand-list results-group__type expanded']");
		public static final By subType = By.xpath("//div[@class='results-sub-group']//div[@class='expand-list expanded']");
		public static final By time = By.xpath("//div[@class='results-sport-events__time']");		
		public static final By teamNames = By.xpath("//div[@class='results-sport-events__teams']");		
		public static final By teamHome = By.xpath("//div[@class='results-sport-events__teams']//div[@class='results-sport-events__team-name home']");
		public static final By scores = By.xpath("//div[@class='results-sport-events__score-container']");		
		public static final By settled = By.xpath("//div[@class='results-sport-events__status' and contains(text(),'Bekr�ftade')]");		
		public static final By todaysTab = By.xpath("//li[@class='tab active']//div[@class='text-container']//span[@class='title-container' and contains(text(),'IDAG')]");
		public static final By YesterdaysTab = By.xpath("//span[@class='title-container' and contains(text(),'IG�R')]");
		public static final By Last3DaysTab = By.xpath("//span[@class='title-container' and contains(text(),'Senaste 3 dagarna')]");
		public static final By emptyText = By.xpath("//div[@class='results-container__empty-text']");
		public static final By date = By.xpath("//div[@class='results-sport-events__date']");		
		public static final By win = By.xpath("//span[@class='win']");
		public static final By place = By.xpath("//span[@class='place']");
		public static final By vOid = By.xpath("//span[@class='void']");
		public static final By unnamedFav = By.xpath("//span[@title='Unnamed Favourite']");
		public static final By raceTime = By.xpath("//div[@class='results-outright-events__time']");
		public static final By HRtab = By.xpath("//span[@title='Galopp']");
		public static final By calenderIcon = By.xpath("//span[@class='calendar-icon']");
		public static final By PickaDate =By.xpath("//span[@class='form-control-text' and contains (text(),'V�lj ett datum')]");
		//added for golfresult page
		public static final By golftime=By.xpath("//div[@class='results-outright-events__time']");
		public static final By golfteamNames = By.xpath("//div[@class='results-outright-events__event-name']");
		public static final By golfsettled = By.xpath("//div[@class='results-outright-events__placement']");
		public static final By golfscores = By.xpath("//div[@class='results-outright-events__price']");
		public static final By golfbadge = By.xpath("//span[@class='results-outright-events__badge']");
		public static final By golfdate = By.xpath("//div[@class='results-outright-events__date']");
		
	}




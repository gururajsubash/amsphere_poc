package com.ladbrokes.testsuite;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.ReporterNG.ReporterNG;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;

public class HomeAndGlobalTests extends DriverCommon {
	Common common= new Common();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions(); 
	BetslipFunctions betSlipFuns = new BetslipFunctions();
	LoginLogoutFunctions loginFun = new LoginLogoutFunctions();


	/** 
	 * TC-1 Verify that following IMS Links are configured with correct IMS pages when user logged in 
     1- Deposit(on header)
     2 - My Account (on drop down)
     3 - The Grid
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyHeaderLinks() throws Exception {

		String testCase = "VerifyHeaderLinks";
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");

		String getTitle ;
		try{
			launchweb();
			String mainwindow = getDriver().getWindowHandle();
			Common.Login();
			// ** Verifying Deposit
			Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Deposit on Header is not Found");
			click(LoginElements.depositBtn);
			Thread.sleep(2000);
			switchwindow();
			getTitle = getDriver().getTitle();
			Assert.assertTrue(getTitle.contains(TextControls.deposit),"Failed to Open Deposit Page");
			//Assert.assertTrue(isElementDisplayed(HomeGlobalElements.addCardBtn),"Add a Credit / Debit Card button is not found");
			getDriver().close();
			getDriver().switchTo().window(mainwindow);

			// ** Verifying My Account Page
			click(LoginElements.userNameAfterLogin);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.myAccount),"My account link is not displayed under userAccount dropDown");
			click(HomeGlobalElements.myAccount);
			switchwindow();
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.myAccounttitle),"My account page default view 'Sports Bet History' is not found");
			getDriver().close();
			getDriver().switchTo().window(mainwindow);

			// ** Verifying The Grid Page
			if(ProjectName.contains("Desktop_English")){
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.theGrid),"Grid link is not displayed under userAccount dropDown");
				click(HomeGlobalElements.theGrid);
				switchwindow();
				getTitle = getDriver().getTitle();
				Assert.assertTrue(getTitle.contains("The Grid"),"Failed to Open Deposit Page");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);
			}

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage() ,e);

		}

	}
	/** 
	 * TC-2 Footer - Click on few of the links available in footer
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyFooterOnHomePage() throws Exception{

		String testCase = "VerifyFooterOnHomePage";
		String projectName=ReadTestSettingConfig.getTestsetting("ProjectName");
		String getTitle ;
		try{
			launchweb();
			//writing this condition as pages are empty in all footer links of other languages
			if(projectName.equals("Desktop_English"))
			{
				String mainwindow = getDriver().getWindowHandle();
				String actualUrl = getDriver().getCurrentUrl();

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.termsConditionsLink), "'Terms & Conditions' link is not present");
				scrollAndClick(HomeGlobalElements.termsConditionsLink);
				//click(HomeGlobalElements.termsConditionsLink);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.termsConditionsPage), "Failed to switch to'Terms & Conditions' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.responsibleGamblingLink), "'Responsible Gambling' link is not present");
				scrollAndClick(HomeGlobalElements.responsibleGamblingLink);
				switchwindow();
				Thread.sleep(2000);
				getTitle = getDriver().getTitle();
				Assert.assertTrue(getTitle.contains(TextControls.ResponsibleGambling),"Failed to switch to Responsible Gambling page");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.cookiePolicyLink), "'cookiePolicy' link is not present");
				scrollAndClick(HomeGlobalElements.cookiePolicyLink);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.cookiePolicyPage), "Failed to switch to'cookiePolicy' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				//**Verify social media links
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.legalIcon), "Leagal informations are not present On Home Page Footer");
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.legalIconEighteen), "legal-icons eighteen is not present on footer of home page");
				scrollAndClick(HomeGlobalElements.legalIconEighteen);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.legalIconEighteenPage), "Failed to switch to'legalIconEighteen' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.essaIcon), "legal-icons ESSA is not present on footer of home page");
				scrollAndClick(HomeGlobalElements.essaIcon);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.essaIconPage), "Failed to switch to'essaIcon' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.therapyIcon), "legal-icons Therapy is not present on footer of home page");
				scrollAndClick(HomeGlobalElements.therapyIcon);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.therapyIconPage), "Failed to switch to'Therapy Icon' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.gamcareIcon), "legal-icons Game Care icon is not present on footer of home page");
				scrollAndClick(HomeGlobalElements.gamcareIcon);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.gamcareIconPage), "Failed to switch to'Game Care Icon' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);

				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.gibraltarIcon), "legal-icons Gibraltar icon is not present on footer of home page");
				scrollAndClick(HomeGlobalElements.gibraltarIcon);
				switchwindow();
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.gibraltarIconPage), "Failed to switch to'GibraltarI Icon' window");
				getDriver().close();
				getDriver().switchTo().window(mainwindow);
			}

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage() , e);
		}
	}
	/** 
	 * TC-3 In-play module - Check the number of events, the markets and odds getting displayed for the events
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyCountdownMessageAndTimerForNonRacingInplayEvents() throws Exception{

		String testCase = "VerifyCountdownMessageAndTimerForNonRacingInplayEvents";


		try{
			//logger.log(LogStatus.INFO, "Test case has Started");
			launchweb();
			Common.Login();
			common.OddSwitcher(TextControls.decimal);
			HGfunctionObj.NavigateAddInplaySelection(1);
			if(isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.countdownMessage), "Please bear with us for a few moment message not present in betslip");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.countdownTimer),"Countdown timer message not present in betslip");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(2000);
			Assert.assertTrue(isElementDisplayed(BetslipElements.betReceiptBanner), "Bet receipt banner is not displayed in Bet Receipt");
			Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip), "Tick mark on Bet Receipt was not found");
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg), "Bet successfull message is not displayed in Bet Receipt");
			click(BetslipElements.doneOnBetslip);

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage() , e);

		}
	}

	/** 
	 * TC-4 Select Show More/Less option to Expand/collapse the list
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyShowmoreAndShowLessOnInplayLP() throws Exception {

		String testCase = "VerifyShowmoreAndShowLessOnInplayLP";

		String showMoreTab  ;
		String showLessTab;
		try{
			//logger.log(LogStatus.INFO, "Test case has Started");
			launchweb();
			click(HomeGlobalElements.inPlayLink);

			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);


			List <WebElement> inPlayFiltersList = getDriver().findElements(By.xpath("//div[@class='module'][1]//tabs[contains(@params, 'inplay')]/nav[@class='tabs']/ul//li[contains(@class, 'tab')]"));

			for (int i = 1; i < inPlayFiltersList.size(); i++)
			{
				inPlayFiltersList = getDriver().findElements(By.xpath("//div[@class='module'][1]//tabs[contains(@params, 'inplay')]/nav[@class='tabs']/ul//li[contains(@class, 'tab')]"));
				String tabName = inPlayFiltersList.get(i).getText();
				if (tabName.equalsIgnoreCase(TextControls.moreText))
				{
					break;
				}
				else
				{
					showMoreTab = "//*[@id='content']/div/div/div[1]/load-handler/div/tabs/event-group-simple/div[2]/div/div";
					inPlayFiltersList.get(i).click();

					List <WebElement> inplayEvntList = getDriver().findElements(HomeGlobalElements.inPlayEventList);

					if(isElementPresent(By.xpath(showMoreTab)))
					{	
						//scrollAndClick(By.xpath(showMoreTab));
						scrollToView(By.xpath(showMoreTab));
						click(By.xpath(showMoreTab));
						List <WebElement> eventList = getDriver().findElements(HomeGlobalElements.inPlayEventList);
						Assert.assertTrue((eventList.size() > inplayEvntList.size()), "Remaining events not displayed on click of Show More tab");

						Assert.assertTrue(isElementDisplayed(HomeGlobalElements.showLessTab), "Show Less button not found");
						scrollToView(HomeGlobalElements.showLessTab);
						click(HomeGlobalElements.showLessTab);
						//click(HomeGlobalElements.showLessTab);
						List <WebElement> showLessEventList = getDriver().findElements(HomeGlobalElements.inPlayEventList);
						getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
						Assert.assertTrue((showLessEventList.size() == inplayEvntList.size()), "Few events are not displayed on click of Show Less tab");
						System.out.println("On Click of Show Less tab few events are displayed successfully");
					}
					else
					{
						System.out.println("Show more is not displayed for " + inPlayFiltersList.get(i).getText() + " ");
					}
					scrollPage("top");
				}
			}

		}
		catch ( Exception | AssertionError  e){
			String screenShotPath = getScreenshot(testCase);


			Assert.fail(e.getMessage(),e);
		}
	}
	/** 
	 * TC-5 Use A- Z Sports to navigate to different class landing Pages
	 * Open up ALL Sports Class Pages and have a look to see that they are displayed correctly with the correct data 
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyAZSportsLink() throws Exception {

		String testCase = "VerifyAZSportsLink";

		String className , actBreadCrums ;

		try{
			launchweb();
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.AZMenu), "A-Z sports link is not found");
			click(HomeGlobalElements.AZMenu);
			List <WebElement> sportsList = getDriver().findElements(By.xpath("//ul[contains(@data-bind,'allSports')]//span[@class='name']"));
			int counter = sportsList.size();
			for(int i= 0 ; i < sportsList.size() ; i++)
			{
				if (i > counter)
					break ;
				sportsList = getDriver().findElements(By.xpath("//ul[contains(@data-bind,'allSports')]//span[@class='name']"));
				className = sportsList.get(i).getText().toLowerCase();
				sportsList.get(i).click();
				Thread.sleep(2000);
				actBreadCrums = GetElementText(HomeGlobalElements.breadcrumbs).toLowerCase();
				boolean status = actBreadCrums.contains(className);
				if(status == true)
					System.out.println(className + " class page is opened");
				else
					Assert.assertFalse(status,className + " sport link failed to open");
				click(HomeGlobalElements.AZMenu);
				Thread.sleep(2000);
			}


		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(),e);

		}
	}
	/** 
	 * TC-06 Switch between Time and Competitions tab and navigate to different event details page
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyUpComingMatchesOnSportsPage() throws Exception {

		String testCase = "VerifyUpComingMatchesOnSportsPage";

		try{
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			HGfunctionObj.VerifyClassLandingPage();

			common.NavigateToSportsPage(TextControls.Basketball);
			HGfunctionObj.VerifyClassLandingPage();

			common.NavigateToSportsPage("Tennis");
			HGfunctionObj.VerifyClassLandingPage();



		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage() , e);


		}

	}
	/** 
	 * TC-07 Choose my start page Football/horse racing page
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyChooseMyStartPage() throws Exception {

		String testCase = "VerifyChooseMyStartPage";

		try{

			launchweb();
			HGfunctionObj.ChooseMyStartPage_function(TextControls.Football);
			String project = ReadTestSettingConfig.getTestsetting("ProjectName");
			if(!(project.contains("Desktop_German")) && (!project.contains("Desktop_Swedish")))
				HGfunctionObj.ChooseMyStartPage_function(TextControls.HorseRacing);

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);}
	}

	/** 
	 * TC-08 VerifyLiveChat - verifying from header and footer links 
	 * @return none
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLiveChat()throws Exception{

		String testCase = "VerifyLiveChat";
		try{
			launchweb();
			HGfunctionObj.VerifyLiveChat_Function("header");
			//Live chat from Footer is removed 
			//HGfunctionObj.VerifyLiveChat_Function("footer"); 


		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}



	/** 
	 * TC-09 Check links displayed under Contact us , Help , Settings and Language
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyCustomerEditAndViewDisplaySettings() throws Exception{

		String testCase = "VerifyCustomerEditAndViewDisplaySettings";
		try
		{
			launchweb();
			HGfunctionObj.VerifyContactUsUI();
			HGfunctionObj.VerifyHelpUI();
			HGfunctionObj.VerifySettingsUI();
			HGfunctionObj.VerifyLanguageUI();

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-10 Login from Casino/Vegas/Poker and then Navigate to Neslon. User should be logged in
	 * Check navigation to Non Sports site from header links
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyNavigation_Login_NonSportsSite() throws Exception{ 
		String testCase = SBP1Desktop.class.getName();
		try{
			launchweb();
			HGfunctionObj.NavigationAndLogin_NonSportsSite("casino");
			//HGfunctionObj.NavigationAndLogin_NonSportsSite("poker");
			HGfunctionObj.NavigationAndLogin_NonSportsSite("bingo");
		}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-11 VerifyBetslipPromoOnHomePage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test		
	public void VerifyBetslipPromoOnHomePage() throws Exception{
		String testCase = "VerifyBetslipPromoOnHomePage";
		try
		{
			launchweb();
			//  verify promotion Landing page
			click(HomeGlobalElements.promotions);
			Thread.sleep(5000);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.promotionText), "Promotions are not available on Promotion page");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.tcText), "T&Cs apply button is not displayed");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.claimNowText), "Claim now button is not displayed");
			System.out.println("Promotion LP verified successfully");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-12 VerifyFractionalToDecimalAppearOnAllModuleCouponPage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void VerifyFractionalToDecimalAppearOnAllModuleCouponPage() throws Exception
	{
		String testCase = "VerifyFractionalToDecimalAppearOnAllModuleCouponPage";
		String eventFractional, couponDecimalValue, betslipText, inPlayFractional, inPlaydecimalValue, inPlayBetslipText, tabName, selXpath;
		float num1, num2, decimalResult, actulDecimalValue, actulDecimalText, expectedResult;
		int intValue, selCount = 0;

		try
		{

			// Verify fractional is changing to decimal on coupon page. ", "Fractional should be changed to decimal on coupon page after chsnging in settings");
			launchweb();
			click(FootballElements.couponLink, "Coupons link is not found");
			common.OddSwitcher("Fractional");

			//HGTFloginLogoutObj.switchToFractional(browser, driver);
			eventFractional = GetElementText(FootballElements.coupon1stEvent1stSel);
			String[] footballnumber = eventFractional.split("/");
			num1 = Float.parseFloat(footballnumber[0]);
			num2 = Float.parseFloat(footballnumber[1]);
			decimalResult = (num1 / num2) + 1;
			intValue = (int)(decimalResult * 100);
			expectedResult = intValue / 100.0f;
			common.OddSwitcher("Decimal");
			couponDecimalValue = GetElementText(FootballElements.coupon1stEvent1stSel);
			actulDecimalValue = Float.parseFloat(couponDecimalValue);
			Assert.assertTrue(actulDecimalValue== expectedResult, "Fraction value is not changed to Decimal value");
			click(FootballElements.coupon1stEvent1stSel, "Coupons event is not found");
			betslipText = GetElementText(HomeGlobalElements.betslipsText);
			actulDecimalText = Float.parseFloat(betslipText);
			Assert.assertTrue(actulDecimalText == expectedResult, "Same decimal value is reflected in Betslip");
			//Logger.Pass("changing from fractional to decimal on coupon page was successfull");

			common.OddSwitcher("fractional");

			//Verify fractional is changing to decimal on In-Play page. ", "Fractional should be changed to decimal on In-Play  page after chsnging in settings");
			click(HomeGlobalElements.inPlayLink, "Inplay link is not found");
			Thread.sleep(2000);
			List<WebElement> inPlayFiltersList = getDriver().findElements(By.xpath("//load-handler[contains(@params, 'inplayEventGroupList')]//tabs[contains(@params, 'inplay-content')]//ul//li"));
			for (int i = 1; i < inPlayFiltersList.size(); i++)
			{
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football.toUpperCase()))
				{
					inPlayFiltersList.get(i).click();
					break;
				}
				else if (tabName.contains("MORE"))
				{
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++)
					{
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains(TextControls.Football.toUpperCase()))
						{
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			Thread.sleep(3000);
			List<WebElement> prePlayEventList = getDriver().findElements(By.xpath("//h2//div[@class='title' and contains(text(), '" + TextControls.Football + "')]/../..//div[contains(@class, 'event-list live')]//div[@class='selections']//span[@class='price']"));
			for (int i = 1; i <= prePlayEventList.size(); i++)
			{
				if (selCount == 1)
					break;
				selXpath = "(//h2//div[@class='title' and contains(text(), '" + TextControls.Football + "')]/../..//div[contains(@class, 'event-list live')]//div[@class='selection'])[" + i + "]//span[@class='price']";
				if (!isElementDisplayed(By.xpath(selXpath)))
				{
					System.out.println("selection not found for the event");
				}
				else
				{
					inPlayFractional = GetElementText(By.xpath(selXpath));
					if (inPlayFractional.contains("/"))
					{
						//change fractional to decimal
						common.OddSwitcher("Decimal");
					}
					inPlaydecimalValue = GetElementText(By.xpath(selXpath));
					if (inPlaydecimalValue.contains("."))
					{
						click(By.xpath(selXpath), " In-Play event is not found");
						inPlayBetslipText = GetElementText(HomeGlobalElements.betslipsText);
						Assert.assertTrue(inPlayBetslipText.contains("."), "Same decimal value is not reflected in Betslip");
					}
					selCount++;
				}
			}

			//Logger.Pass("changing from fractional to decimal on home page was successfull");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-13 VerifyFractionalToDecimalAppearOnAllModulesOnHomePage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */	
	@Test
	public void VerifyFractionalToDecimalAppearOnAllModulesOnHomePage() throws Exception
	{
		String testCase = "VerifyFractionalToDecimalAppearOnAllModulesOnHomePage";
		String inPlayFractional, inPlaydecimalValue, inPlayBetslipText, tabName, selXpath, sportsName;
		int selCount = 0;
		boolean isStatus = false;
		try
		{
			launchweb();
			Common.OddSwitcher("fractional");

			//In-Play Module
			if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
			{
				click(HomeGlobalElements.inPlayTab, "Inplay Module is not present on home page");
			}

			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++)
			{
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains("TENNIS"))
				{
					isStatus = true;
					inPlayFiltersList.get(i).click();
					break;
				}
				else if (tabName.contains(TextControls.moreText.toUpperCase()))
				{
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++)
					{
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains("TENNIS"))
						{
							isStatus = true;
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			if (isStatus == true)
				System.out.println("Event type is found");
			else
				Assert.fail("Event type is not found");
			List<WebElement> prePlayEventList = getDriver().findElements(By.xpath("//h2//div[@class='title' and contains(text(), 'Tennis')]/../..//div[contains(@class, 'event-list live')]//div[@class='selections']//span[@class='price']"));
			for (int i = 1; i <= prePlayEventList.size(); i++)
			{
				if (selCount == 1)
					break;
				selXpath = "(//h2//div[@class='title' and contains(text(), 'Tennis')]/../..//div[contains(@class, 'event-list live')]//div[@class='selection'])[" + i + "]//span[@class='price']";
				if (!isElementPresent(By.xpath(selXpath)))
				{
					System.out.println("selection not found for the event");
				}
				else
				{
					inPlayFractional = GetElementText(By.xpath(selXpath));
					if (inPlayFractional.contains("/"))
					{
						//change fractional to decimal
						common.OddSwitcher("Decimal");
					}
					inPlaydecimalValue = GetElementText(By.xpath(selXpath));
					if (inPlaydecimalValue.contains("."))
					{
						click(By.xpath(selXpath), " In-Play event is not found");
						inPlayBetslipText = GetElementText(HomeGlobalElements.betslipsText);
						Assert.assertTrue(inPlayBetslipText.contains("."), "Same decimal value is not reflected in Betslip");
					}
				}
				selCount++;
			}

			common.OddSwitcher("fractional");

			// Highlights Module
			if (!(getAttribute(HomeGlobalElements.highLightsTab, "class").contains("expanded")))
				click(HomeGlobalElements.highLightsTab, "HighLights Module is not present on home page");

			List<WebElement> highlightsFiltersList = getDriver().findElements(HomeGlobalElements.highLightFilters);
			for (int i = 1; i < highlightsFiltersList.size(); i++)
			{
				sportsName = highlightsFiltersList.get(i).getText();
				if (sportsName.toLowerCase().contains(TextControls.Football.toLowerCase()))
				{
					//tabName = sportsName.substring(0, sportsName.indexOf("\r"));
					scrollToView(highlightsFiltersList.get(i));
					highlightsFiltersList.get(i).click();
					break;
				}

				else if (sportsName.toUpperCase().contains(TextControls.moreText.toUpperCase()))
				{
					click(By.xpath("//h1[contains(@class, 'expand-list')]//span[contains(text(), '" + TextControls.hightlightText + "')]/following::load-handler//div[@class='load-handler']//nav[@class='tabs']//div[@class='title-container' and @title='More']"));
					List<WebElement> sportsNameInMore = getDriver().findElements(HomeGlobalElements.sportsNameInMore);
					for (int j = 0; j <= sportsNameInMore.size(); j++)
					{
						sportsName = sportsNameInMore.get(j).getText().toLowerCase();
						if (sportsName.contains(TextControls.Football.toLowerCase())){
							/*//tabName = tabName.substring(0, tabName.indexOf("\r"));
                          if (tabName.toUpperCase().equals(TextControls.Football.toUpperCase()))
                          {*/
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			List<WebElement> highlightsEventList = getDriver().findElements(By.xpath("//event-group-simple//div[@class='event-group']//event-list//div[@class='event-list pre']//div[@class='selection']//selection-button"));
			for (int i = 1; i <= highlightsEventList.size(); i++)
			{
				if (selCount == 2)
					break;
				selXpath = "(//event-list/div[@class='event-list pre'][" + i + "]//div[@class='selection']/selection-button)[1]//div[@class='odds-button']";
				if (!getDriver().findElement(By.xpath(selXpath)).isDisplayed())
				{
					System.out.println("selection not found for the event");
				}
				else
				{
					// selXpath);
					selCount++;
				}
			}
			// Logger.Pass("changing from fractional to decimal on home page was successfull");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-14 VerifyHighlightsOnHomePage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyHighlightsOnHomePage() throws Exception
	{

		String testCase = "VerifyHighlightsOnHomePage";
		try
		{
			launchweb();
			//Verifying the Highlight module on home page                                


			if (!(getAttribute(HomeGlobalElements.highLightsTab, "class").contains("expanded")))
				click(HomeGlobalElements.highLightsTab, "HighLights Module is not present on home page");

			List<WebElement> highLightFiltersList = getDriver().findElements(HomeGlobalElements.highLightFilters);
			for (int i = 1; i < highLightFiltersList.size(); i++)
			{
				String tabName = highLightFiltersList.get(i).getText();
				if (tabName.contains(TextControls.moreText.toUpperCase()))
				{
					//Click on more
					scrollToView(highLightFiltersList.get(i));
					highLightFiltersList.get(i).click();
					//Get sports listed under MORE
					List<WebElement> sportsNameInMore = getDriver().findElements(HomeGlobalElements.sportsNameInMore);

					//Verify Events count for each sports under MORE
					for (int j = 0; j < sportsNameInMore.size(); j++)
					{
						String moreTabName = sportsNameInMore.get(j).getText();
						int highLightEventCnt = Integer.parseInt(common.ExtractNumberFromString(moreTabName, "(", ")"));
						sportsNameInMore.get(j).click();

						List<WebElement> highlightEvents = getDriver().findElements(HomeGlobalElements.highLightEventList);
						Assert.assertTrue(highLightEventCnt == highlightEvents.size(), "Mismatch in Highlight events count");

						//click on more
						highLightFiltersList.get(i).click();

						//j++;
					}
				}
				else
				{
					int highLightEventCnt = Integer.parseInt(common.ExtractNumberFromString(tabName, "(", ")"));
					scrollToView(highLightFiltersList.get(i));
					highLightFiltersList.get(i).click();

					List<WebElement> highlightEvents = getDriver().findElements(HomeGlobalElements.highLightEventList);
					Assert.assertTrue(highLightEventCnt == highlightEvents.size(), "Mismatch in Highlight events count");
					//Logger.Pass("Successfully verified the Highlight Events list functionality");
				}
			}
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}


	/** 
	 * TC-15 VerifyInplayEventTitleOnEventDetailPage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test	
	public void VerifyInplayEventTitleOnEventDetailPage() throws Exception{
		String testCase = "VerifyInplayEventTitleOnEventDetailPage";
		String tabName;
		boolean isStatus = false;
		try
		{
			launchweb();
			//Logger.AddTestCase("Verifying the Inplay Event Title On Event Detail Page Functionality", "Inplay Event Title On Event Detail Page should be displayed on application as expected");
			if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
				click(HomeGlobalElements.inPlayTab, "Inplay Module is not present on home page");

			//getDriver().findElements((HomeGlobalElements.inPlayFilters));
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 0; i < inPlayFiltersList.size(); i++)
			{
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football.toUpperCase()))
				{
					isStatus = true;
					inPlayFiltersList.get(i).click();
					break;
				}
				else if (tabName.contains(TextControls.moreText))
				{
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++)
					{
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains(TextControls.Football.toUpperCase()))
						{
							isStatus = true;
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			if (isStatus == true)
				System.out.println("Football class is found");
			else
				Assert.fail("Football class is not found");

			//Click on football inplay event
			WebElement fbInplayEvent = getDriver().findElement(HomeGlobalElements.inPlayFBEvent);
			fbInplayEvent.click();

			Assert.assertTrue(isElementPresent(HomeGlobalElements.inPlayTabOnDetailPage), "LHN Inplay module is not present on event detail page");
			if (!(getAttribute(HomeGlobalElements.inPlayOnDetialPage, "class").contains("expanded")))
				click(HomeGlobalElements.inPlayOnDetialPage, "Inplay event section is not present");

			List<WebElement> inPlaySportList = getDriver().findElements((HomeGlobalElements.inPlaySportsOnDetailPage));
			for (int i = 0; i < inPlaySportList.size(); i++)
			{
				String tabName1 = inPlaySportList.get(i).getText();
				int inplayEventCnt = Integer.parseInt(common.ExtractNumberFromString(tabName1, "(", ")"));
				if ((getAttribute(HomeGlobalElements.inPlaySportsOnDetailPage, "class").contains("expanded")))
				{
					List<WebElement> inplayEvents = getDriver().findElements((HomeGlobalElements.inPlayEventsOnDetailPage));
					Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
				}
				else
				{
					inPlaySportList.get(i).click();
					List<WebElement> inplayEvents = getDriver().findElements((HomeGlobalElements.inPlayEventsOnDetailPage));
					Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
				}
				inPlaySportList.get(i).click();
			}
			// Logger.Pass("Inplay Event Title On event details page is displayed Successfully");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-16 VerifyInplayOnHomePage
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyInplayOnHomePage()throws Exception{
		String testCase = "VerifyInplayOnHomePage";
		String showMoreTab;
		try
		{
			launchweb();
			//Verifying the Inplay module on home page                                
			if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
				click(HomeGlobalElements.inPlayTab, "Inplay Module is not present on home page");

			List<WebElement> inPlayFiltersList =getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++)
			{
				String tabName = inPlayFiltersList.get(i).getText().toUpperCase();
				showMoreTab ="//span[contains(text(),'In-Play')]/following::div[@class='show-more']" ;
				if (tabName.contains(TextControls.moreText.toUpperCase()))
				{
					//Click on more
					scrollPage("top");
					inPlayFiltersList.get(i).click();
					Thread.sleep(3000);
					//Get sports listed under MORE
					List<WebElement> sportsNameInMore =getDriver().findElements(HomeGlobalElements.sportsNameInMore);

					//Verify Events count for each sports under MORE
					for (int j = 0; j < sportsNameInMore.size(); j++)
					{
						String moreTabName = sportsNameInMore.get(j).getText();
						int inplayEventCnt = Integer.parseInt(common.ExtractNumberFromString(moreTabName, "(", ")"));
						sportsNameInMore.get(j).click();
						Thread.sleep(3000);

						if (isElementPresent(By.xpath(showMoreTab))){
							scrollToView(By.xpath(showMoreTab));
							click(By.xpath(showMoreTab));}

						List<WebElement> inplayeventSuspended =getDriver().findElements(By.xpath("//span[contains(text(),'In-Play')]/following::div[@class='event-group'][1]//div[contains(@class, 'event-list live')]"));
						// if (inplayEventCnt != inplayEvents.Count)
						// {
						Assert.assertTrue(inplayEventCnt == inplayeventSuspended.size(), "Mismatch in inplay events count");
						// }

						//click on more
						inPlayFiltersList.get(i).click();
						j++;
					}
				}
				else
				{
					int inplayEventCnt = Integer.parseInt(common.ExtractNumberFromString(tabName, "(", ")"));
					scrollPage("top");
					inPlayFiltersList.get(i).click();
					if (isElementPresent(By.xpath(showMoreTab))){
						scrollToView(By.xpath(showMoreTab));
						click(By.xpath(showMoreTab));
						Thread.sleep(3000);}

					List<WebElement> inplayEvents =getDriver().findElements(HomeGlobalElements.inPlayEventList);
					Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
					//Logger.Pass("Successfully verified the Inplay Events list functionality");
				}
			}
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC-17 VerifyInplaySectionExpandCollapse
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void VerifyInplaySectionExpandCollapse() throws Exception
	{
		String testCase = "VerifyInplaySectionExpandCollapse";
		String sportsTitle, sportsTitleTemp;
		int i, j = 1;
		try
		{
			launchweb();
			//"Verifying the InPlay Event Title Is Expandable/Collapsible .             
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.sportsTabOnHeader), "By default Sports is not shown as highlighted in the top navigation bar");
			if (!(getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded")))
				click(HomeGlobalElements.inPlayTab, "Inplay Module is not present on home page");

			click(HomeGlobalElements.inPlayFootball);

			//Click on football inPlay event
			scrollPage("Top");
			click(HomeGlobalElements.inPlayFBEvent);

			//Verifying whether the inPlay section in expanded by default ot not
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.inPlayExpandedonEDP), "By default Inplay Section in LHS is not expanded");
			//Collapse InPlay Section
			click(HomeGlobalElements.inPlayOnDetialPage, "Inplay event section is not present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.inplayHeadedonEDP), "Inplay event title did not collapse on clicking on it");
			//Expand InPlay section
			click(HomeGlobalElements.inPlayOnDetialPage, "Inplay event section is not present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.inPlayExpandedonEDP), "By default Inplay Section in LHS is not expanded");
			System.out.println("Inplay event title is expanding and collapsing onclicking on it");

			//Logger.AddTestCase("Verifying Sports Title Under Inplay Is Expandable/Collapsable", "Sports titles for eg. Football (7), Tennis(4) etc. under the Inplay event title should expand and collapse on clicking on it");
			List<WebElement> sportsUnderInplay = getDriver().findElements(By.xpath("//div[@class='inplay-wrapper']//h2"));
			for (i = 0; i < sportsUnderInplay.size(); i++)
			{
				sportsTitleTemp = GetElementText(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "]//span[@class='title']"));
				int index = sportsTitleTemp.indexOf('(');
				sportsTitle = sportsTitleTemp.substring(0, index);

				sportsUnderInplay = getDriver().findElements(By.xpath("//div[@class='inplay-wrapper']//h2"));
				//check whether expanded
				if (getAttribute(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "]"), "class").contains("expanded"))
				{
					//Collapse
					sportsUnderInplay.get(i).click();
					Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "][@class='sport']//span[contains(text(), '" + sportsTitle + "')]")), "" + sportsTitle + " not collapsable");
					//Expand
					sportsUnderInplay.get(i).click();
					Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "][@class='sport expanded toggle-arrow-ie']//span[contains(text(), '" + sportsTitle + "')]")), "" + sportsTitle + " not expandable");

				}
				//Expand
				else
				{
					sportsUnderInplay.get(i).click();
					Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "][@class='sport expanded toggle-arrow-ie']//span[contains(text(), '" + sportsTitle + "')]")), "" + sportsTitle + " not expandable");
					//Collapse
				}
				sportsUnderInplay.get(i).click();
				Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='inplay-wrapper']//h2[" + j + "][@class='sport']//span[contains(text(), '" + sportsTitle + "')]")), "" + sportsTitle + " not collapsable");
				j++;
			}
			System.out.println("All the sports titles under the Inplay event title is expanding and collapsing on clicking on it.");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 * TC-18 VerifyLHSClassSortOrder
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyLHSClassSortOrder() throws Exception
	{
		String testCase = "VerifyLHSClassSortOrder";
		int p = 0;
		String[] arrClasses;
		try
		{
			launchweb();
			// Verifying the classes are sorted in LHS
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), "AZ Sports header is not present in LHN");
			click(HomeGlobalElements.AZMenu, "A-Z Sports header is not present in LHN");

			List<WebElement> aTozClassList = getDriver().findElements(By.xpath("//ul[contains(@data-bind,'foreach: allSports')]//span[@class='name']"));
			arrClasses = new String[aTozClassList.size()];
			for (WebElement ele : aTozClassList)
			{
				arrClasses[p++] = ele.getText();
			}
			boolean result = common.StringIsSortedAsc(arrClasses);//StringIsSorted(arrClasses);
			if (result == true)
				System.out.println("All the classes displayed in LHS are sorted alphabetically");

			else
				System.out.println("The classes displayed in LHS are not sorted alphabetically");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 * TC-19 To verify Left hand navigation (LHN) to be visible on all pages of the applicationyLHSClassSortOrder
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void VerifyLHSInAllPages()throws Exception
	{
		String testCase = "VerifyLHSInAllPages";

		try
		{
			launchweb();
			// "Verifying the LHS is displayed in all pages", "The LHS should be displayed in all the pages");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), "A-Z Sports header is not present in LHN in Homepage");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.azIcon), "A-Z Sports icon is not present in LHN in Homepage");
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@id, 'az-region')]")), "LHS is not present in HomePage");
			common.NavigateToSportsPage(TextControls.Football);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), "A-Z Sports header is not present in LHN in Homepage");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.azIcon), "A-Z Sports icon is not present in LHN in Homepage");
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@id, 'az-region')]")), "LHS is not present in Football class page");
			// HGTFhomeglobalObj.NavigateToEventSubType(browser, driver, TextControls.footballText, TextControls.englishText, "Premier League");
			click(HomeGlobalElements.eventTypeTabName);
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@id, 'az-region')]")), "LHS is not present in Football->EST page");

			common.SelectLinksFromAZ(TextControls.HorseRacing);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), "A-Z Sports header is not present in LHN in Homepage");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.azIcon), "A-Z Sports icon is not present in LHN in Homepage");
			scrollToView(HorseRacingElements.todayHRace1stRaceTime);
			click(HorseRacingElements.todayHRace1stRaceTime);			
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@id, 'az-region')]")), "LHS is not present in HorseRacing->RaceCard page");
			System.out.println("The LHS is displayed in all pages (Class, EST, RaceCard pages");
		}

		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 * TC-20 VerifyPaymentMethodLinksOnFooter
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyPaymentMethodLinksOnFooter() throws Exception{
		String testCase = "VerifyPaymentMethodLinksOnFooter";
		try
		{
			launchweb();

			String mainwindow = getDriver().getWindowHandle(), expURL;
			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'VISA.png')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'VISA.png')]")), "'Visa-electron' link is not present");
				// expURL = "https://help.ladbrokes.com/s/article/kaA0Y000000KzgSUAS/LB-Visa-Electron";
				expURL = "https://help.ladbrokes.com/s/article/LB-Visa-Electron";

				scrollPage("bottom");
				common.SwitchWindow_ValidateNewWindow("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'VISA.png')]", "", expURL);
				getDriver().switchTo().window(mainwindow);
			}

			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'visa-55x31.png')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'visa-55x31.png')]")), "'Visa' link is not present");
				//  expURL = "https://help.ladbrokes.com/s/article/kaA0Y000000KzgNUAS/LB-Visa";
				expURL = "https://help.ladbrokes.com/s/article/LB-Visa";
				scrollPage("bottom");
				common.SwitchWindow_ValidateNewWindow("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'visa-55x31.png')]", "", expURL);
				getDriver().switchTo().window(mainwindow);
			}

			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'mastercard')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'mastercard')]")), "'Mastercard' link is not present");
				// expURL = "https://help.ladbrokes.com/s/article/kaA0Y000000KzghUAC/LB-MasterCard";
				expURL = "https://help.ladbrokes.com/s/article/LB-MasterCard";
				scrollPage("bottom");
				common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'mastercard')]", "", expURL);
				getDriver().switchTo().window(mainwindow);
			}
			//Has been removed from Preprod and Prod
			//            if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'western-union')]")))
			//            {
			//               Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'western-union')]")), "'Western-union' link is not present");
			//                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/289";
			//                scrollPage("bottom");
			//                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'western-union')]", "", expURL);
			//                getDriver().switchTo().window(mainwindow);
			//            }

			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'neteller')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'neteller')]")), "'Neteller' link is not present");
				//expURL = "https://help.ladbrokes.com/s/article/kaA0Y000000KzOEUA0/LB-Neteller";
				expURL = "https://help.ladbrokes.com/s/article/LB-Neteller";
				scrollPage("bottom");
				common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'neteller')]", "", expURL);
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paypal')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paypal')]")), "'PayPal' link is not present");
				//expURL = "https://help.ladbrokes.com/s/article/kaA0Y000000KzOdUAK/LB-PayPal";
				expURL = "https://help.ladbrokes.com/s/article/LB-PayPal";
				scrollPage("bottom");
				common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paypal')]", "", expURL);
				getDriver().switchTo().window(mainwindow);
			}

			//Has been removed from Preprod and Prod
			//            if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ucash')]")))
			//            {
			//               Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ucash')]")), "'Ucash' link is not present");
			//                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/297";
			//                scrollPage("bottom");
			//                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ucash')]", "", expURL);
			//                getDriver().switchTo().window(mainwindow);
			//            }

			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'skrill')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'skrill')]")), "'Skrill' link is not present");
				/*expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/292";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'skrill')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'skrill')]"));
				//common.SwitchToPage("Skrill (Moneybookers)");
				 getDriver().switchTo().window(mainwindow);

			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'bank-transfer')]")))
			{
				/*Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'bank-transfer')]")), "'Bank Transfer' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/293";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'bank-transfer')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'bank-transfer')]"));
				//common.SwitchToPage("Bank Transfer");
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paysafe-card')]")))
			{
				/*Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paysafe-card')]")), "'Pay Safe Card' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/300";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paysafe-card')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'paysafe-card')]"));
				//common.SwitchToPage("Paysafecard");
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'maestro')]")))
			{
				/*Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'maestro')]")), "'Maestro' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/295";
                scrollPage("bottom");
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'maestro')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'maestro')]"));
				//common.SwitchToPage("Maestro");
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'sofort')]")))
			{
				Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'sofort')]")), "'Sofort' link is not present");
				/*  expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/301";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'sofort')]", "", expURL);*/
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'giropay')]")))
			{
				/*Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'giropay')]")), "'GiroPay' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/304";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'giropay')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'giropay')]"));
				//common.SwitchToPage("Giropay");
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ideal')]")))
			{

				/* Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ideal')]")), "'iDeal' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/303";
                common.SwitchWindow_ValidateNewWindow( "//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ideal')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'ideal')]"));
				//common.SwitchToPage("iDEAL");
				getDriver().switchTo().window(mainwindow);
			}


			if (isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'trustly')]")))
			{

				/*Assert.assertTrue(isElementPresent(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'trustly')]")), "'Trustly' link is not present");
                expURL = "http://helpcentre.ladbrokes.com/app/answers/detail/a_id/307";
                common.SwitchWindow_ValidateNewWindow("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'trustly')]", "", expURL);
                getDriver().switchTo().window(mainwindow);*/
				scrollPage("bottom");
				click(By.xpath("//div[@id='footer-container']//section/ul/li/a/img[contains(@src, 'trustly')]"));
				//common.SwitchToPage("Trustly");
				getDriver().switchTo().window(mainwindow);
			}
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 * TC-20 Registration 
	 * Navigate To Registration Page
       Verifying footerLinks On Registration Page
       Register Customer
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyRegistration_UKCustomer() throws Exception{

		String testCase = "VerifyRegistration_UKCustomer";
		try
		{
			launchweb();
			String mainWindow = getDriver().getWindowHandle();
			Assert.assertTrue(isElementPresent(HomeGlobalElements.registrationButton), "Registration button Join Me is not found");
			click(HomeGlobalElements.registrationButton, "Registration button Join Me is not found");
			Thread.sleep(2000);
			getDriver().switchTo().activeElement();
			WebElement iframe = getDriver().findElement(By.xpath("//iframe[@class='lightbox-registration-frame']"));
			getDriver().switchTo().frame(iframe);

			Assert.assertTrue(isElementPresent(HomeGlobalElements.registrationTitle), " Failed to navigate to Registration page, Registration title not found");
			//Assert.assertTrue(isElementPresent(HomeGlobalElements.quickpaypal), " QuickPayPal Registration is not found");

			//Verify footerLink On RegistrationPage 
			scrollPage("bottom");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regNeedHelpIcon), " Need Help icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regCallIcon), "Call icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regHelpIcon), "Help Icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regSecurityIcon), "Security icon is not found in footer");


			//Register Customer, New customer registration should be possible

			Random rnd = new Random();
			int rndNumber = rnd.nextInt(3000);
			String currencySymbol = TextControls.currencySymbol;

			String firstname = "TestFname";
			String surname = "TestLname";
			String username = "Desk_AutoUser_" + rndNumber;

			String postcode = "E18BT";
			String email = "test@playtech.com";
			String phonenumber = "1234567890";

			String Password = "Lbr123456";
			String secretQuestion = TextControls.favColorText;
			String secretAnswer = "Green";
			//Date dateForButton = Date.from()//Now.AddDays(-10958);

			//Select selectTitle = new Select(getDriver().findElement(HomeGlobalElements.title));
			//selectTitle.selectByValue("Mr");
			click(HomeGlobalElements.title);
			Entervalue(HomeGlobalElements.firstname, firstname);
			Entervalue(HomeGlobalElements.surname, surname);
			Select select = new Select(getDriver().findElement(By.xpath("//select[@id='day']")));
			select.selectByValue("10");
			Select selectmonth = new Select(getDriver().findElement(By.xpath("//select[@id='month']")));
			selectmonth.selectByValue("09");
			Select selectyear = new Select(getDriver().findElement(By.xpath("//select[@id='year']")));
			selectyear.selectByValue("1990");
			Thread.sleep(2000);
			Entervalue(HomeGlobalElements.email, email);

			//*****clicking on continue to step2 button *****
			click(By.xpath("//*[@id='p_p_id_registration_WAR_accountportlet_']/div/div[4]/button"), "continue to step2 button is not found");
			WebElement element = getDriver().findElement(HomeGlobalElements.postcode);
			element.sendKeys(postcode);
			element.sendKeys(Keys.ENTER);
			click(HomeGlobalElements.FindAddress);
			Thread.sleep(1000);
			Entervalue(HomeGlobalElements.phonenumber, phonenumber);

			//*****clicking on continue to last step button *****
			click(By.xpath("//*[@id='p_p_id_registration_WAR_accountportlet_']/div/div[4]/button"), "continue to last step button is not found");
			Entervalue(HomeGlobalElements.username, username);
			Entervalue(HomeGlobalElements.password, Password);

			Select selectSctQues = new Select(getDriver().findElement(HomeGlobalElements.secretQuestion));
			selectSctQues.selectByValue("Favourite colour");
			Entervalue(HomeGlobalElements.secretAnswer, secretAnswer);
			Select selectDepositLimit = new Select(getDriver().findElement(HomeGlobalElements.depositLimit));
			selectDepositLimit.selectByValue("GBP");
			Thread.sleep(2000);
			click(HomeGlobalElements.checkbox);
			scrollPage("bottom");
			click(HomeGlobalElements.createAccountButton, "Complete registration button is not present ");
			Thread.sleep(2000);

			// Verifying FirstDeposit window
			if(isElementPresent(HomeGlobalElements.gdprsubmit))
			click(HomeGlobalElements.gdprsubmit);
			Thread.sleep(3000);
			getDriver().switchTo().activeElement();
			common.SwitchToPage("Deposit");

			// Verifying user Auto loggedIn after registration
			Assert.assertTrue(GetElementText(LoginElements.userBalance).contains(currencySymbol), "Currency symbol is not present in balance");
			Assert.assertTrue(GetElementText(LoginElements.userNameAfterLogin).contains(firstname), "User name is not present afterlogin");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}
	}


	/** 
	 * TC-20 Registration for non Uk customer
	 * Navigate To Registration Page
       Verifying footerLinks On Registration Page
       Register Customer
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyRegistration_NonUKCustomer() throws Exception{
		String testCase = "VerifyRegistration_NonUKCustomer";
		try
		{
			launchweb();
			String mainWindow = getDriver().getWindowHandle();
			Assert.assertTrue(isElementPresent(HomeGlobalElements.registrationButton), "Registration button Join Me is not found");
			click(HomeGlobalElements.registrationButton, "Registration button Join Me is not found");
			Thread.sleep(2000);
			getDriver().switchTo().activeElement();
			WebElement iframe = getDriver().findElement(By.xpath("//iframe[@class='lightbox-registration-frame']"));
			getDriver().switchTo().frame(iframe);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.registrationTitle), " Failed to navigate to Registration page, Registration title not found");
			//Assert.assertTrue(isElementPresent(HomeGlobalElements.quickpaypal), " QuickPayPal Registration is not found");

			//Verify footerLink On RegistrationPage 
			scrollPage("bottom");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regNeedHelpIcon), " Need Help icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regCallIcon), "Call icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regHelpIcon), "Help Icon is not found in footer");
			Assert.assertTrue(isElementPresent(HomeGlobalElements.regSecurityIcon), "Security icon is not found in footer");


			//Register Customer, New customer registration should be possible

			Random rnd = new Random();
			int rndNumber = rnd.nextInt(3000);
			String currencySymbol = "€";

			String firstname = "TestFname";
			String surname = "TestLname";
			String username = "Desk_AutoUser_" + rndNumber;

			String postcode = "LZ3872YJ";
			String email = "test@playtech.com";
			String phonenumber = "1234567890";

			String houseno = rnd.toString();
			String Password = "Lbr123456";
			String secretQuestion = TextControls.favColorText;
			String secretAnswer = "Green";


			
			click(HomeGlobalElements.title);
			Entervalue(HomeGlobalElements.firstname, firstname);
			Entervalue(HomeGlobalElements.surname, surname);
			Select select = new Select(getDriver().findElement(By.xpath("//select[@id='day']")));
			select.selectByValue("10");
			Select selectmonth = new Select(getDriver().findElement(By.xpath("//select[@id='month']")));
			selectmonth.selectByValue("09");
			Select selectyear = new Select(getDriver().findElement(By.xpath("//select[@id='year']")));
			selectyear.selectByValue("1990");
			Thread.sleep(2000);
			Entervalue(HomeGlobalElements.email, email);

			//*****clicking on continue to step2 button *****
			click(By.xpath("//*[@id='p_p_id_registration_WAR_accountportlet_']/div/div[4]/button"), "continue to step2 button is not found");
			Select selectCountry = new Select(getDriver().findElement(By.xpath("//select[@id='countryCode']")));
			selectCountry.selectByValue("IE");
			Entervalue(HomeGlobalElements.postcode, postcode);
			Entervalue(HomeGlobalElements.address, "ImperialHouse");
			Entervalue(HomeGlobalElements.city, "Aldgate");
			Entervalue(HomeGlobalElements.phonenumber, phonenumber);
			Select selectState = new Select(getDriver().findElement(HomeGlobalElements.county));
			selectState.selectByVisibleText("Armagh");

			//*****clicking on continue to last step button *****
			click(By.xpath("//*[@id='p_p_id_registration_WAR_accountportlet_']/div/div[4]/button"), "continue to last step button is not found");
			Entervalue(HomeGlobalElements.username, username);
			Entervalue(HomeGlobalElements.password, Password);

			Select selectSctQues = new Select(getDriver().findElement(HomeGlobalElements.secretQuestion));
			selectSctQues.selectByValue("Favourite colour");
			Entervalue(HomeGlobalElements.secretAnswer, secretAnswer);
			Select selectDepositLimit = new Select(getDriver().findElement(HomeGlobalElements.depositLimit));
			selectDepositLimit.selectByValue("0");
			Select selectCurreny = new Select(getDriver().findElement(By.xpath("//select[@name='currencyCode']")));
			selectCurreny.selectByValue("EUR");
			click(HomeGlobalElements.checkbox);
			scrollPage("bottom");
			click(HomeGlobalElements.createAccountButton, "Complete registration button is not present ");
			Thread.sleep(5000);

			
			// Verifying FirstDeposit window
			click(HomeGlobalElements.gdprsubmit);
			Thread.sleep(3000);
			getDriver().switchTo().activeElement();
			common.SwitchToPage("Deposit");
			Thread.sleep(3000);

			// Verifying user loggedIn after registration
			Assert.assertTrue(GetElementText(LoginElements.userBalance).contains(currencySymbol), "Currency symbol is not present in balance");
			Assert.assertTrue(GetElementText(LoginElements.userNameAfterLogin).contains(firstname), "User name is not present afterlogin");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 *  Auto Highlights comparing elements in Sports tabs and sorting 
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */


	@Test
	public void AutoHighlights_SportsTabEvents() throws Exception{
		String testCase = "AutoHighlights_SportsTabEvents";
		try
		{

			launchweb();

			//Verify Highlights module on home page                                

			if (!(getAttribute(HomeGlobalElements.highLightsTab, "class").contains("expanded")))
				click(HomeGlobalElements.highLightsTab, "HighLights Module is not present on home page");

			List<WebElement> highLightFiltersList = getDriver().findElements(HomeGlobalElements.SportUnderhiglights);

			for(int i=0; i<highLightFiltersList.size();i++){

				highLightFiltersList = getDriver().findElements(HomeGlobalElements.SportUnderhiglights);
				String sport = highLightFiltersList.get(i).getText();
				System.out.println("The sport under Highlights is: "+sport );
				String events = "//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list";
				List<WebElement> listOfEvents = getDriver().findElements(By.xpath(events));

				for (int j=0;j<listOfEvents.size();j++){

					List<WebElement> highLightFL = getDriver().findElements(HomeGlobalElements.highLightFilters);
					scrollToView(highLightFL.get(1));  

					String eventDetailsName = "(//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list)["+(j+1)+"]/div/div/div/div";
					String eventName= getDriver().findElement(By.xpath(eventDetailsName)).getText();

					System.out.println("Events grouped under "+sport+"are:" +" "+ eventName);
					if((i==2) || (i>2))
					{
						String moreSport = "//tabs[contains(@params, 'featured-content')]/nav[@class='tabs']/ul/li/div/div/span[text()='"+sport+"']";
						if (j==0)
						{	
							click(HomeGlobalElements.moreHighlights);
							click(By.xpath(moreSport));
						}
						String eventOn =	"(//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list/div/div[@class='event-list-details']/div[1]/div)["+(j+1)+"]";
						String eventOnTab= getDriver().findElement(By.xpath(eventOn)).getText();

						if(eventOnTab.contains(eventName)){
							Reporter.log("Events match on both tabs");
							System.out.println("Events match on both tabs");
						}
						else
						{
							Assert.fail("Events do not match");
							System.out.println("Events do not match");
						}

					}
					else{
						String sportTab ="//tabs[contains(@params, 'featured-content')]/nav[@class='tabs']/ul/li/div/span[text()='"+sport+"']";
						click(By.xpath(sportTab));


						String eventOn =	"(//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list/div/div[@class='event-list-details']/div[1]/div)["+(j+1)+"]";
						String eventOnTab= getDriver().findElement(By.xpath(eventOn)).getText();

						if(eventOnTab.contains(eventName)){
							Reporter.log("Events match on both tabs");
							System.out.println("Events match on both tabs");
						}
						else
						{
							Assert.fail("Events do not match");
							System.out.println("Events do not match");
						}

					}          		

				}//j end

				click(HomeGlobalElements.allHighlights);

			}//i end


		}catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}

	}




	/** 
	 *  Auto Highlights and Grouping on the Home Page
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void AutoHighlights_Grouping() throws Exception{
		String testCase = "AutoHighlights_Grouping";
		try
		{

			launchweb();

			//Verify Highlights module on home page                                

			if (!(getAttribute(HomeGlobalElements.highLightsTab, "class").contains("expanded")))
				click(HomeGlobalElements.highLightsTab, "HighLights Module is not present on home page");

			List<WebElement> highLightFiltersList = getDriver().findElements(HomeGlobalElements.SportUnderhiglights);

			for(int i=0; i<highLightFiltersList.size();i++){
				String sport = highLightFiltersList.get(i).getText();
				System.out.println("The sports under higjlights is: "+sport );
				String events = "//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list";
				List<WebElement> listOfEvents = getDriver().findElements(By.xpath(events));

				for (int j=0;j<listOfEvents.size();j++){

					if(sport.equals("Cricket"))
					{
						String TeamAName="";
						String TeamA = "//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='Cricket']/../following-sibling::div/event-list[1]/div/div/div/div[1]";
						//String TeamB = "//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='Cricket']/../following-sibling::div/event-list[1]/div/div/div/div[2]";
						if(isElementPresent(By.xpath(TeamA)))
						TeamAName = getDriver().findElement(By.xpath(TeamA)).getText();
						

						System.out.println("Teams grouped under "+sport+"are:" +TeamAName);

					}
					else{
						String eventDetailsTime= "((//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list)["+(j+1)+"]/div/span[@class='time list-view'])";
						String eventTimeAndOdds= getDriver().findElement(By.xpath(eventDetailsTime)).getText();

						String eventDetailsName = "(//h1/span[text()='Highlights']/../../load-handler/div[@class='load-handler']//event-group-simple/h2/div[text()='"+sport+"']/../following-sibling::div/event-list)["+(j+1)+"]/div/div/div/div";
						String eventName= getDriver().findElement(By.xpath(eventDetailsName)).getText();

						System.out.println("Events grouped under "+sport+"are:" +eventTimeAndOdds +" "+ eventName);
					}

				}//j end


			}//i end


		}catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}

	}
	/** 
	 * TC-811219   Access the promotions page via the quick links
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void Verify_The_Promotions_Page_Via_Quick_Links() throws Exception {

		String testCase = "Verify_The_Promotions_Page_Via_Help_Links";

		try{
			//launches the url in the web browser
			launchweb();
			//user try to acccess the Promotions via QuckLinks
			Assert.assertTrue(isElementPresent(HomeGlobalElements.promotions), " Promotions link is not present in quck links");
			click(HomeGlobalElements.promotions);
			Thread.sleep(2000);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.promotionsText),"Promotions page is not present");

		}catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}

	}
	
	/** 
	 * TC- 811220  Click on Claim now for a promotion
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void Verify_Claim_Now_For_Promotion() throws Exception {

		String testCase = "Verify_Claim_Now_For_Promotion";

		try{
			//launches the url in the web browser
			launchweb();
			//user try to acccess the Promotions via QuckLinks
			Assert.assertTrue(isElementPresent(HomeGlobalElements.promotions), " Promotions link is not present in quck links");
			click(HomeGlobalElements.promotions);
			Thread.sleep(2000);
			//click claimNow for a promotion
			Assert.assertTrue(isElementPresent(HomeGlobalElements.claimNow),"Claim now for a promotion is not present");
			click(HomeGlobalElements.claimNow);
			Thread.sleep(3000);
			ArrayList<String>	tab = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(tab.get(1));
			Assert.assertTrue(isElementPresent(HomeGlobalElements.promotionPage),"promotion page is not present");
			

		}catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
		}

	}
	/** Check AEM banners on homepage
	 * 
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void Check_AEM_banners_on_homepage()throws Exception
	{
		String testCase = "Check_AEM_banners_on_homepage";

		try
		{
			launchweb();
			//Check for Aem Banners	
			Assert.assertTrue(isElementPresent(HomeGlobalElements.Aembanners), "Aem banners is not present in Home page");
			System.out.println("Aem Banners verified successfully on Home Page");
		}

		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}

	
	/** Check AEM banners on Football homepage
	 * 
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void Check_AEM_banners_on_FootballPage()throws Exception
	{
		String testCase = "Check_AEM_banners_on_FootballPage";

		try
		{
			launchweb();
			//Check for Aem Banners	on Football page
			common.NavigateToSportsPage(TextControls.Football);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.Aembanners), "Aem banners is not present in Football page");
			System.out.println("Aem Banners verified successfully on Football page");
		}

		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** Check AEM banners on HR and GH homepage
	 * 
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */

	@Test
	public void Check_AEM_banners_on_HRandGHPage()throws Exception
	{
		String testCase = "Check_AEM_banners_on_HRandGHPage";

		try
		{
			launchweb();
			//Check for Aem Banners	
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.Aembanners), "Aem banners is not present in GH page");
			click(HomeGlobalElements.ladbrokesLogo);
			common.NavigateToSportsPage(TextControls.HorseRacing);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.Aembanners), "Aem banners is not present in HR page");
			System.out.println("Aem Banners verified successfully on HR and GH page");
		}

		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

		Assert.fail(e.getMessage(), e);
		}
	}
	/** 
	 * TC- 811066  check Tennis Virtual Tab is not present
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void Verify_The_Tennis_Virtual_Tab() throws Exception {

		String testCase = "Verify_The_Tennis_Virtual_Tab";

		try{
			//launches the url in the web browser
			launchweb();
			//user try to login with valid credentials
			Common.Login();
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), " AZSports not present in header");
			click(HomeGlobalElements.AZMenu);
			Thread.sleep(2000);
			WebElement element = getDriver().findElement(HomeGlobalElements.Virtual);
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(2000);
			element.click();
			Thread.sleep(2000);
			List<WebElement> virtualTabs=getDriver().findElements(HomeGlobalElements.virtualHeaders);
			for(int i=0;i<=virtualTabs.size();i++)
			{
				virtualTabs=getDriver().findElements(HomeGlobalElements.virtualHeaders);
				String VirtualTabText=virtualTabs.get(i).getText().toLowerCase();
				if(VirtualTabText.contains("Tennis"))
				{
				  System.out.println("Tennis Tab is present in Virtual sports");	
			    }	
				else
					System.out.println("Tennis Tab is not present in Virtual sports");
				break;
			}
		}			
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);
	            }
        	}	


   /** 
    * TC- 811067  Verify Virtual bet placement
    * @return none
    * @throws Exception element not found
    */
   @Test
   public void Verify_Virtual_Bet_placement() throws Exception {

	String testCase = "Verify_Virtual_Bet_placement";

	try{
		//launches the url in the web browser
		launchweb();
		//user try to login with valid credentials
		Common.Login();
		Common.OddSwitcher(TextControls.decimal);
		Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), " AZSports not present in header");
		click(HomeGlobalElements.AZMenu);
		Thread.sleep(2000);
		//click on virtual sports tab
		WebElement element = getDriver().findElement(HomeGlobalElements.Virtual);
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(2000);
		element.click();
		Thread.sleep(2000);
		List<WebElement> virtualSportsList=getDriver().findElements(HomeGlobalElements.virtualSports);
		virtualSportsList.get(1).click();
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.virtualEventPicker));
		click(HomeGlobalElements.virtualEventPicker);
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.virtualSelection));
		click(HomeGlobalElements.virtualSelection);
		Thread.sleep(1000);
		Common.Enterstake(TextControls.stakeValue);
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		click(BetslipElements.placeBet);
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.virtualBetPlacementText));
		
	}			
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);
	            }
        	}
   
   
   
   /** 
    * TC- 811058  Verify the Results of perticular sport
    * @return none
    * @throws Exception element not found
    */
   @Test
   public void Verify_Results_Of_Perticular_Sport() throws Exception {

	String testCase = "Verify_Results_Of_Perticular_Sport";

	try{
		//launches the url in the web browser
		launchweb();
		//user try to login with valid credentials
		Common.Login();
		Thread.sleep(2000);
		//navigate to Football EDP
		common.NavigateToSportsPage(TextControls.Football);
		//click result link from Football EDP
		Assert.assertTrue(isElementPresent(ResultsElements.resultsTab), " Results tab not present in Football page");
		click(ResultsElements.resultsTab);
		Thread.sleep(2000);
		Assert.assertTrue(isElementPresent(ResultsElements.resultsTitle), " Results Title is not present in Results page");
		//user click on More option button
		Assert.assertTrue(isElementPresent(ResultsElements.MoreTab), " More tab is not present in Football Results page");
		click(ResultsElements.MoreTab);
		Thread.sleep(1000);
		//click the sports from More button 
		Assert.assertTrue(isElementPresent(ResultsElements.sports), " sports are not present in football more tab");
		click(ResultsElements.sports);
		Thread.sleep(2000);
		//navigate to results page of that perticular sport
		Assert.assertTrue(isElementPresent(ResultsElements.resultsPageText), " Results Text is not present in perticular sports Results page");
		Assert.assertTrue(isElementPresent(ResultsElements.resultsSportsText));
		Thread.sleep(1000);
		System.out.println("Football result page is displayed successfully");
		
		//navigate to Tennis EDP
		common.NavigateToSportsPage(TextControls.Tennis);
		//click result link from Tennis EDP
		Assert.assertTrue(isElementPresent(ResultsElements.resultsTab), " Results tab not present in Golf page");
		click(ResultsElements.resultsTab);
		Thread.sleep(2000);
		Assert.assertTrue(isElementPresent(ResultsElements.resultsTitle), " Results Title is not present in Results page");
		//user click on More option button
		Assert.assertTrue(isElementPresent(ResultsElements.MoreTab), " More tab is not present in Tennis Results page");
		click(ResultsElements.MoreTab);
		Thread.sleep(1000);
		//click the sports from More button 
		Assert.assertTrue(isElementPresent(ResultsElements.sports), " sports are  not present in Tennis more tab");
		click(ResultsElements.sports);
		Thread.sleep(2000);
		//navigate to results page of that perticular sport
		Assert.assertTrue(isElementPresent(ResultsElements.resultsSportsText));
		System.out.println("Tennis result page is displayed successfully");
		
		//navigate to Golf EDP
		common.NavigateToSportsPage(TextControls.golf);
		//click result link from Golf EDP
		Assert.assertTrue(isElementPresent(ResultsElements.resultsTab), " Results tab not present in Golf page");
		click(ResultsElements.resultsTab);
		Thread.sleep(2000);
		//user click on More option button
		Assert.assertTrue(isElementPresent(ResultsElements.MoreTab), " More tab is not present in Golf  Results page");
		click(ResultsElements.MoreTab);
		Thread.sleep(1000);
		//click the sports from More button
		Assert.assertTrue(isElementPresent(ResultsElements.sports), " sports are  not present in Golf more tab");
		click(ResultsElements.sports);
		Thread.sleep(2000);
		//navigate to results page of that perticular sport
		Assert.assertTrue(isElementPresent(ResultsElements.resultsSportsText));
		System.out.println("Golf result page is displayed successfully");
	}			
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);
	            }
        	}
   
   
   
   /** 
    * TC- 811144  Verify the functionality of different tabs on Deposit screen
    * @return none
    * @throws Exception element not found
    */
   @Test
   public void Verify_Functionality_Of_Different_Tabs_On_Deposit_Screen() throws Exception {

	String testCase = "Verify_Functionality_Of_Different_Tabs_On_Deposit_Screen";

	try{
		String initbal,laterbal;
		//launches the url in the web browser
		launchweb();
		//user try to login with valid credentials
		Common.Login();
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Deposit button is not present in Deposit page");
		click(LoginElements.depositBtn);
		Thread.sleep(3000);
		//check the functionality of Transfer tab
		ArrayList<String>	tab = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tab.get(1));
		Assert.assertTrue(isElementDisplayed(LoginElements.transferBtn), "Transfer button is not present in Deposit page");
		click(LoginElements.transferBtn);
		Thread.sleep(3000);
		Assert.assertTrue(isElementDisplayed(LoginElements.transferText), "Transfer page is not present");
		getDriver().findElement(LoginElements.balinBankingPage).click();
		scrollPage("bottom");
		Thread.sleep(4000);
		initbal=getDriver().findElement(LoginElements.balinBankingPage).getText();
		System.out.println(initbal);
		if(initbal.contains(","))
		{
			initbal=initbal.replace(",", "");
		}
		double initvalue = Double.parseDouble(initbal.replace(TextControls.currencySymbol, ""));
		System.out.println(initvalue);
		Entervalue(LoginElements.amount,"1");
		Thread.sleep(1000);
		click(LoginElements.Transfer);
		Assert.assertTrue(isElementDisplayed(LoginElements.TransferValidation), "amount has not been transfered");
		getDriver().findElement(By.xpath("//span[@class='popup-modal__button fn-close']")).click();
		laterbal=getDriver().findElement(LoginElements.balinBankingPage).getText();	
		System.out.println(laterbal);
	
		
		//check the functionality of Withdraw  Tab
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawlBtn), "Withdrawl button is not present in Deposit page");
		click(LoginElements.withdrawlBtn);
		getDriver().navigate().refresh();
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawText), "Withdrawl page is not present");
		Assert.assertTrue(isElementDisplayed(LoginElements.visa));
		click(LoginElements.visa);
		Thread.sleep(1000);
		//Select select=new Select(getDriver().findElement(LoginElements.withdrawFromdropdown));
		//select.selectByValue("Games & Lottos");
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawvisaAmount));
		Entervalue(LoginElements.withdrawvisaAmount,"1");
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawSuccessMsg), "withdraw button is not present");
		click(LoginElements.withdrawSuccessMsg);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawAccept), "Accept button is not present");
		click(LoginElements.withdrawAccept);		
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.withdrawlRequest));
		click(LoginElements.withdrawlRequest);
		
        //check the functionality of pending withdrawl tab
		Assert.assertTrue(isElementDisplayed(LoginElements.pendingwithdrawlBtn), "Pending withdrawl is not present in Deposit page");
		click(LoginElements.pendingwithdrawlBtn);
		Assert.assertTrue(isElementDisplayed(LoginElements.pendingwithdrawText), "Pending Withdrawl page is not present");
	
		////check the functionality of account History  Tab
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.accountHistoryBtn), "Account History button is not present in Deposit page");
		click(LoginElements.accountHistoryBtn);
		Thread.sleep(1000);
		Assert.assertTrue(isElementDisplayed(LoginElements.accountHistoryText), "Account History page is not present");
		
		
}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);
	            }
        	}


	/**
	 * C811068 User Interaction is remembered when event transition is happening
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void Verify_Virtual_User_Interaction_is_remembered_when_event_transition() throws Exception {

		String testCase = "Verify_Virtual_User_Interaction_is_remembered_when_event_transition";

		try{
			//launches the url in the web browser
			launchweb();
			//user try to login with valid credentials
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementPresent(HomeGlobalElements.AZMenu), " AZSports not present in header");
			click(HomeGlobalElements.AZMenu);
			Thread.sleep(2000);
			//click on virtual sports tab
			WebElement element = getDriver().findElement(HomeGlobalElements.Virtual);
			((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(2000);
			element.click();
			Thread.sleep(2000);

			String eventTime = getDriver().findElement(HomeGlobalElements.virtualEventPickerNextEvent).getText();
			System.out.println(eventTime);
			click(HomeGlobalElements.virtualEventPickerNextEvent);
			String title = getDriver().findElement(HomeGlobalElements.virtualTitle).getText();

			//Wait until event in first position disappears
			String eventTimeFirstPosition = getDriver().findElement(HomeGlobalElements.virtualEventPickerNextEvent).getText();
			By firstEvent = By.xpath("//li[@class='virtuals-event-selector__event' and contains(text(),'"+eventTimeFirstPosition+"')]");

			new WebDriverWait(getDriver(), 300).until(ExpectedConditions.invisibilityOfElementLocated(firstEvent));

			Thread.sleep(4000);

			String title2 = getDriver().findElement(HomeGlobalElements.virtualTitle).getText();
			Assert.assertEquals(title, title2, "Selected event not displayed after transition");

			click(HomeGlobalElements.virtualEventPicker);

			//Check that first event is equal to what was selected from the beginning
			String eventTime2 = getDriver().findElement(HomeGlobalElements.virtualEventPickerNextEvent).getText();
			Assert.assertEquals(eventTime, eventTime2, "Event transition to first position failed");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

}
		
		
	
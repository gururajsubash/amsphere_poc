package com.ladbrokes.testsuite;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.HorseRacing_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;

public class HorseRacingTests extends DriverCommon {
	Common common= new Common();
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions(); 
	LoginLogoutFunctions LoginfunctionObj = new LoginLogoutFunctions();
	TestdataFunction testdata = new TestdataFunction();
	HorseRacing_Functions HRfunctionObj = new HorseRacing_Functions();
	
	/** 
	 * TC-01  HorseRacingForecastPlaceBet
	 * Validate Bet Information
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	 public void VerifyHorseRacingForecastPlaceBet() throws Exception {
		
		String testCase = "VerifyHorseRacingForecastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
		String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
		
		Boolean statusBeforeClear, statusBeforeClear1;
		String valueText, horseOne, horseTwo, evntName, raceName, eventsInfo, betInformation , receiptInformation , marketName = "Straight Forecast";
		int betSlipCount,  initBetSlipCount;
		
		 try{
			 launchweb();
			 if(germanLanguage.equals("Desktop_German"))
				 System.out.println("Horse Racing is not available for German");
			 else{
				
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
				
				// ** Verifying AddToBetslip is enabled after selecting radio button
			   click(HorseRacingElements.checkbox);
			   click(HorseRacingElements.checkbox1);
			   Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
			   WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkbox);
			   WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkbox1);
			   
			   statusBeforeClear  = checkbox1.isSelected();
		       statusBeforeClear1  = checkbox2.isSelected();
		     // ** Verifying Betting value after selecting the selections in Forecast 
	           if (statusBeforeClear == statusBeforeClear1 == true)
	        	   Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue),"Betting value is not visible after selecting boxes");
		 
	         //Verifying selections clear after clicking on addToBetslip button
	           horseOne =  GetElementText(HorseRacingElements.forecastHorseOne);
	           horseTwo = GetElementText(HorseRacingElements.forecastHorseTwo);
	           raceName = GetElementText(HorseRacingElements.raceName);
	           evntName = GetElementText(HorseRacingElements.eventName);
	           eventsInfo = evntName + " " + raceName;
		
	           WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
	           scrollToView(element);
	         click(HorseRacingElements.addToBetslipButton);
	           
	           valueText = GetElementText(HorseRacingElements.bettingValue);
	           if (valueText.equals(""))
	           System.out.println("Value is cleared and not showing");
	        else
	          Assert.fail("Value is not cleared after adding bet to betSlip");

	           //**Getting count from BetSlip
	           betSlipCount = common.GetBetSlipCount();
	           if (betSlipCount == initBetSlipCount + 1)
	           {
	        	   betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
	        	   System.out.println("Bet Info : " + betInformation);
	        	   Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
	               common.Enterstake(TextControls.stakeValue);
	               BTbetslipObj.BetPlacement(true);
	             //Verifying bet receipt                    
	               receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
	          Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
	           }
			 }
	           }
		 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			 
			 Assert.fail(e.getMessage() , e);
			 }
		 }

	/** 
	 * TC-02  HorseRacing ReverseForecastPlaceBet
	 * Validate Bet Information
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	 public void VerifyHorseRacingReverseForecastPlaceBet() throws Exception {
		
		String testCase = "VerifyHorseRacingReverseForecastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
		String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
		
		Boolean statusBeforeClear, statusBeforeClear1;
		String valueText, horseOne, horseTwo, evntName, raceName, eventsInfo, betInformation , receiptInformation , marketName = "Reverse Forecast";
		int betSlipCount,  initBetSlipCount;
		
		 try{
			launchweb();
			 if(germanLanguage.equals("Desktop_German"))
				 System.out.println("Horse Racing is not available for German");
			 else{
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
				
				// ** Verifying AddToBetslip is enabled after selecting radio button
				   click(HorseRacingElements.checkboxAny);
				  click(HorseRacingElements.checkboxAny1);
				  Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
				   WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkboxAny);
				   WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkboxAny1);
				   statusBeforeClear  = checkbox1.isSelected();
			       statusBeforeClear1  = checkbox2.isSelected();
			     
			       // ** Verifying Betting value after selecting the selections in Forecast 
	               if (statusBeforeClear == statusBeforeClear1 == true)
	            	   Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue),"Betting value is not visible after selecting boxes");
			 
	             //Verifying selections clear after clicking on addToBetslip button
	               horseOne =  GetElementText(HorseRacingElements.forecastHorseOne);
	               horseTwo = GetElementText(HorseRacingElements.forecastHorseTwo);
	               raceName = GetElementText(HorseRacingElements.raceName);
	               evntName = GetElementText(HorseRacingElements.eventName);
	               eventsInfo = evntName + " " + raceName;
			
	               WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
	               scrollToView(element);
	             click(HorseRacingElements.addToBetslipButton);
	               
	               valueText = GetElementText(HorseRacingElements.bettingValue);
	               if (valueText.equals(""))
	               System.out.println("Value is cleared and not showing");
	               else
	                   Assert.fail("Value is not cleared after adding bet to betSlip");

	                    //**Getting count from BetSlip
	                    betSlipCount = common.GetBetSlipCount();
	                    if (betSlipCount == initBetSlipCount + 1)
	                    {
	                 	   betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
	                 	   Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
	                        Common.Enterstake(TextControls.stakeValue);
	                        BTbetslipObj.BetPlacement(true);
	                      //Verifying bet receipt                    
	                        receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
	                   Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
	                    }
			 }
	                    }
	     		 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	     			
	     			 Assert.fail(e.getMessage() , e);
	     			 }
	}
	/** 
	 * TC-03  HorseRacing TriCast PlaceBet
	 * Validate Bet Information
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	 public void VerifyHorseRacingTricastPlaceBet() throws Exception {
		
		String testCase = "VerifyHorseRacingTricastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
		String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
		
		Boolean statusBeforeClear, statusBeforeClear1 , statusBeforeClear2;
		String valueText, horseOne, horseTwo , horseThree, evntName, raceName, eventsInfo, betInformation , receiptInformation , marketName = "Tricast";
		int betSlipCount,  initBetSlipCount;
		
		 try{
				launchweb();
			 if(germanLanguage.equals("Desktop_German"))
				 System.out.println("Horse Racing is not available for German");
			 else{
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
				
				// ** Verifying AddToBetslip is enabled after selecting radio button
				   click(HorseRacingElements.checkbox2);
				   click(HorseRacingElements.checkbox3);
				   click(HorseRacingElements.checkbox4);
				   Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
				   
				   WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkbox2);
				   WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkbox3);
				   WebElement checkbox3 = getDriver().findElement(HorseRacingElements.checkbox4);
				   
				   statusBeforeClear  = checkbox1.isSelected();
			       statusBeforeClear1  = checkbox2.isSelected();
			       statusBeforeClear2  = checkbox3.isSelected();
			     
			       // ** Verifying Betting value after selecting the selections in Forecast 
	               if (statusBeforeClear == statusBeforeClear1 == statusBeforeClear2 == true)
	            	   Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue),"Betting value is not visible after selecting boxes");
			 
	             //Verifying selections clear after clicking on addToBetslip button
	               horseOne =  GetElementText(HorseRacingElements.forecastHorseOne);
	               horseTwo = GetElementText(HorseRacingElements.forecastHorseTwo);
	               horseThree = GetElementText(HorseRacingElements.tricastHorseThree);
	               raceName = GetElementText(HorseRacingElements.raceName);
	               evntName = GetElementText(HorseRacingElements.eventName);
	               eventsInfo = evntName + " " + raceName;
	   			
	               WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
	               scrollToView(element);
	             click(HorseRacingElements.addToBetslipButton);
	               
	               valueText = GetElementText(HorseRacingElements.bettingValue);
	               if (valueText.equals(""))
	               System.out.println("Value is cleared and not showing");
	               else
	                   Assert.fail("Value is not cleared after adding bet to betSlip");

	                    //**Getting count from BetSlip
	                    betSlipCount = common.GetBetSlipCount();
	                    if (betSlipCount == initBetSlipCount + 1)
	                    {
	                 	   betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
	                 	   Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
	                        Common.Enterstake(TextControls.stakeValue);
	                        BTbetslipObj.BetPlacement(true);
	                      //Verifying bet receipt                    
	                        receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
	                   Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
	                    }
	                    }
		 }
	     		 catch ( Exception | AssertionError  e){
	     			 String screenShotPath = getScreenshot(testCase); 
	     			 Assert.fail(e.getMessage() , e);
	     			 }
	}

	
	/** 
	 * TC-04  HorseRacing Combination TricastPlaceBet
	 * Validate Bet Information
     * @return none
	 * @throws Exception element not found
	 */
	
@Test
	 public void VerifyHorseRacingCombinationTricastPlaceBet() throws Exception {
	
	String testCase = "VerifyHorseRacingCombinationTricastPlaceBet";
	String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
		String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
		
		Boolean statusBeforeClear, statusBeforeClear1 , statusBeforeClear2;
		String valueText, horseOne, horseTwo , horseThree, evntName, raceName, eventsInfo, betInformation , receiptInformation , marketName = "Combination Tricast";
		int betSlipCount,  initBetSlipCount;
		
		 try{
			 launchweb();
			 if(germanLanguage.equals("Desktop_German"))
				 System.out.println("Horse Racing is not available for German");
			 else{
				
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
				
				// ** Verifying AddToBetslip is enabled after selecting radio button
				
				   click(HorseRacingElements.checkboxTricastAny);
				   click(HorseRacingElements.checkboxTricastAny1);
				  click(HorseRacingElements.checkboxTricastAny2);
				  Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
				   
				   WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkboxTricastAny);
				   WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkboxTricastAny1);
				   WebElement checkbox3 = getDriver().findElement(HorseRacingElements.checkboxTricastAny2);
				   
				   statusBeforeClear  = checkbox1.isSelected();
			       statusBeforeClear1  = checkbox2.isSelected();
			       statusBeforeClear2  = checkbox3.isSelected();
			     
			       // ** Verifying Betting value after selecting the selections in Forecast 
	               if (statusBeforeClear == statusBeforeClear1 == statusBeforeClear2 == true)
	            	   Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue),"Betting value is not visible after selecting boxes");
	             //Verifying selections clear after clicking on addToBetslip button
	               horseOne =  GetElementText(HorseRacingElements.forecastHorseOne);
	               horseTwo = GetElementText(HorseRacingElements.forecastHorseTwo);
	               horseThree = GetElementText(HorseRacingElements.tricastHorseThree);
	               raceName = GetElementText(HorseRacingElements.raceName);
	               evntName = GetElementText(HorseRacingElements.eventName);
	               eventsInfo = evntName + " " + raceName;
	   			
	               WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
	               scrollToView(element);
	             click(HorseRacingElements.addToBetslipButton);
	               
	               valueText = GetElementText(HorseRacingElements.bettingValue);
	               if (valueText.equals(""))
	               System.out.println("Value is cleared and not showing");
	               else
	                   Assert.fail("Value is not cleared after adding bet to betSlip");

	                    //**Getting count from BetSlip
	                    betSlipCount = common.GetBetSlipCount();
	                    if (betSlipCount == initBetSlipCount + 1)
	                    {
	                 	   betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
	                 	   Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
	                        common.Enterstake(TextControls.stakeValue);
	                        BTbetslipObj.BetPlacement(true);
	                      //Verifying bet receipt                    
	                        receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
	                   Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
	                    }
			 }
	                    }
	     		 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	     			
	     			 Assert.fail(e.getMessage() , e);
	     			 }
		 
		 }
/** 
 * TC-05  SP Bet Placement
* @return none
 * @throws Exception element not found
 */

@Test
public void VerifyHRBetPlacement_SP() throws Exception {
	
	String testCase = "VerifyHRBetPlacement_SP";
	String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	String selection = testdata.Readtestdata("PreProdEvents", 10, 5);
	int initBetSlipCount , laterBetslipCnt ;
	String oddBtn ;
	
	try{
		launchweb();
		if(germanLanguage.equals("Desktop_German"))
			 System.out.println("Horse Racing is not available for German");
		 else{
		Common.Login();
		Common.OddSwitcher("decimal");
		initBetSlipCount = common.GetBetSlipCount();
		common.NavigateToSportsPage(className);
        String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
        scrollToView(Element);
        click(By.xpath(hrEvent));
        oddBtn ="//*[@class='horse']//*[@class='name' and contains(text(), '"+ selection +"')]/../../../../../../..//div[@class='odds-button']/span[text()='SP']";
        click(By.xpath(oddBtn));
	    laterBetslipCnt = common. GetBetSlipCount();
		Assert.assertTrue((initBetSlipCount + 1 == laterBetslipCnt),"Mismatch in Betslip count on adding a selction, Expected:" + (initBetSlipCount + 1) + ", Actual:" + laterBetslipCnt + ".");
		Common.Enterstake(TextControls.stakeValue);
		BTbetslipObj.BetPlacement(true);
		
		 }
		}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		
		Assert.fail(e.getMessage() , e);
		
	}
	}
/** 
 * TC-06  Build a RaceCard - Custom and Next 3 races
* @return none
 * @throws Exception element not found
 */

@Test
public void VerifyHREventBuildRaceCard() throws Exception {
	
	String testCase = "VerifyHREventBuildRaceCard";
	String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
	
	try{
		launchweb();
		if(germanLanguage.equals("Desktop_German"))
			 System.out.println("Horse Racing is not available for German");
		 else{
		common.SelectLinksFromAZ(TextControls.HorseRacing);		 
		HRfunctionObj.VerifyRacingEventBuildRaceCard_function(TextControls.HorseRacing);
		 }
	}
	
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		
		Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-07  Add a selection from horse racing event.
 Check and confirm the following is displayed:
 name of the runner, 
 time of the event,
 market name,
 event name,
 maximum amount,
 minimum amount,
 Each way.
* @return none
 * @throws Exception element not found
 */

@Test
public void VerifyHRSelctionInfoBetSlip() throws Exception {
	
	String testCase = "VerifyHRSelctionInfoBetSlip";
	
	String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	String selection = testdata.Readtestdata("PreProdEvents", 10, 5);
	String marketName = testdata.Readtestdata("PreProdEvents", 10, 6);
	int initBetSlipCount , laterBetslipCnt ;
	String oddBtn , betInformation , eventsInfo , EachWay ;
	
	try{
		launchweb();
		if(germanLanguage.equals("Desktop_German"))
			 System.out.println("Horse Racing is not available for German");
		 else{
		common.SelectLinksFromAZ(TextControls.HorseRacing);
		initBetSlipCount = common.GetBetSlipCount();
		
		//** Adding Selection
		String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
        scrollToView(Element);
        click(By.xpath(hrEvent));
        oddBtn ="//*[@class='horse']//*[@class='name' and contains(text(), '"+ selection +"')]/../../../../../../..//div[@class='odds-button']/span[text()='SP']";
        click(By.xpath(oddBtn));
	    laterBetslipCnt = common. GetBetSlipCount();
	    Assert.assertTrue((initBetSlipCount + 1 == laterBetslipCnt),"Mismatch in Betslip count on adding a selction, Expected:" + (initBetSlipCount + 1) + ", Actual:" + laterBetslipCnt + ".");
	    
	    // Verifying name of the runner, time of the event, market name,  event name, EachWay
	   eventsInfo = eventName + " " + typeName ;
	    betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + selection + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
  	   Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
  	   EachWay = "//input[@id='betslip-eachway-single-0']";
  	   Assert.assertTrue(isElementDisplayed(By.xpath(EachWay)),"Each way is not found on betSlip");
  	   
  	   // Verifying Min/max amount 
  	 if(isElementPresent(BetslipElements.oddsBoostTooltipInfo))
   	   click(BetslipElements.oddsBoostTooltipIcon);
  	   click(By.xpath(betInformation));
  	   Thread.sleep(1000);
  	   Assert.assertTrue(isElementDisplayed(BetslipElements.minValueInBetslip), "Minimum value is not found on betslip");
  	  Assert.assertTrue(isElementDisplayed(BetslipElements.maxValueInBetslip), "Maximum value is not found on betslip");
  	
		 }
	}
	
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		
		Assert.fail(e.getMessage() , e);
	}
}
/** 
 * TC-08 Check the content on horse racing landing page
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyHorseRacingLandingPage() throws Exception{
	
	String testCase = "VerifyHorseRacingLandingPage";
	String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
	String  actBreadcrumbs, expBreadcrumbs;
	int count;
try{
	launchweb();
	if(germanLanguage.equals("Desktop_German"))
		 System.out.println("Horse Racing is not available for German");
	 else{
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	//*** Verifying Horse Racing Landing Page components
	Assert.assertTrue(isElementDisplayed(HorseRacingElements.nextRaces), "Next Races not displayed in Horse Racing Landing Page");
	Assert.assertTrue(isElementDisplayed(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
	Assert.assertTrue(isElementDisplayed(HorseRacingElements.futureHorseRaces), "Future Horse Races not displayed in Horse Racing Landing Page");
	System.out.println("Horse Racing Landing Page components are displayed as expected");
	
	// **** Verifying BreadCrums
	HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.HorseRacing, "", "");
	System.out.println("The breadcrumb is shown on the top as Home > Horse Racing as expected");
	
	//** Verify Next Races Modules
   List<WebElement> nextRaces =  getDriver().findElements(HorseRacingElements.nextRacesModules);
   count = nextRaces.size();
 if (count == 0)
	 Assert.fail("No Races are Present");
 else
	 System.out.println(count +" races are displayed in the Next Races module");
	 }
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	
	Assert.fail(e.getMessage(), e);}
}

/** 
 * TC-09 Check the Contextual Menu for next Races 
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyEventNavContextualMenuForNextRacesEvent_HR() throws Exception{
	String testCase = "VerifyEventNavContextualMenuForNextRacesEvent_HR";
	String  eventXpath;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));
   if ((getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),'" + TextControls.nextThreeRaces + "')]"), "class").contains("expanded")))
       click( By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),'" + TextControls.nextThreeRaces + "')]"), "Next races Meeting is not present on Racecard page");

   eventXpath = "//div[@class='wrapper sidebar']//div[@class='next-races-group']/a[1]/span[@class='time']";
   click(By.xpath(eventXpath));
    

    List<WebElement>venueList = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2"));
    for (int j = 1; j < venueList.size(); j++)
    {
        String xPath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[@class='meetings']";
       Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'" + venueList.get(j) + "' Venue is not dispalyed in collapsed mode");
    }
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	
	Assert.fail(e.getMessage(), e);}
}

/** 
 * TC-10 Check the Contextual Menu for today's Racing 
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyEventNavContextualMenuForTodayEvent_HR() throws Exception{
	String testCase = "VerifyEventNavContextualMenuForTodayEvent_HR";
	String  eventXpath;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));
   
if ((getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "class").contains("expanded")))
        click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "Today's Meeting is not present on Racecard page");

    List<WebElement> venueName1 = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), 'today')]/../following-sibling::div[@class='meetings-group']//h2"));

    for (int i = 1; i <= venueName1.size(); i++)
    {
        if (i == 3)
            i = venueName1.size();

        String test = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title']";
        String venueName = GetElementText(By.xpath(test));
        if (!isElementPresent(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings expanded')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]")))
           click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]"));

        //Verifying the venue and events under selection venue from Today's Meeting 
        eventXpath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[1]/following-sibling::div[@class='meetings-container'][1]/a[1]";
        click(By.xpath(eventXpath));
       
        List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2"));
        for (int j = 1; j < venueList.size(); j++)
        {
            String xPath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[@class='meetings']";
           Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'" + venueList.get(j) + "' Venue is not dispalyed in collapsed mode");
        }
        click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]"));
    }
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	
	Assert.fail(e.getMessage(), e);}
}

/** 
 * TC-11 Check the Contextual Menu for tomorrow Racing 
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyEventNavContextualMenuForTomorrowEvent() throws Exception{
	String testCase = "VerifyEventNavContextualMenuForTomorrowEvent";
	String  eventXpath;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.tomorrowText + "')]")), "Tomorrow's Races not displayed in Horse Racing Landing Page");
	click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));
    
 if ((getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.tomorrowText + "')]"), "class").contains("expanded")))
        click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.tomorrowText + "')]"), "Tomorrow's Meeting is not present on Racecard page");

   List<WebElement> venueName1 = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2"));

    for (int i = 1; i <= venueName1.size(); i++)
    {
        if (i == 3)
            i = venueName1.size();

        String test = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title']";
        String venueName = GetElementText(By.xpath(test));
        if (!isElementPresent(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings expanded')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]")))
            click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]"));

        //Verifying the venue and events under selection venue from Today's Meeting 
        eventXpath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[1]/following-sibling::div[@class='meetings-container'][1]/a[1]";
        click(By.xpath(eventXpath));
        
    List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2"));
        for (int j = 1; j < venueList.size(); j++)
        {
            String xPath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[@class='meetings']";
           Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'" + venueList.get(j) + "' Venue is not dispalyed in collapsed mode");
        }
        click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.tomorrowText.toLowerCase() + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][" + i + "]//span[@class='title' and contains(text(), '" + venueName + "')]"));
    }
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	
	Assert.fail(e.getMessage(), e);}
}


/** 
 * TC-12 FavouriteIndexAndAggregateDistanceDisplayed 
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyFavouriteIndexAndAggregateDistanceDisplayedBelowRacecard() throws Exception{
	String testCase = "VerifyFavouriteIndexAndAggregateDistanceDisplayedBelowRacecard";
	String actBreadcrumbs, expBreadcrumbs;
    
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	if(isTextPresent(TextControls.favIndexText)){
		Assert.assertTrue(isElementDisplayed(HorseRacingElements.FavouritesIndex), "Favourite Index is not displayed");
	click(HorseRacingElements.winningDistances);
    actBreadcrumbs = GetElementText(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"));
    actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
    expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.HorseRacing + "";
    Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
    common.SelectLinksFromAZ(TextControls.HorseRacing);
	}
	if(isTextPresent(TextControls.aggWinningDistanceText)){
		WebElement element = getDriver().findElement(HorseRacingElements.winningDistances);
		scrollToView(element);
	 click(HorseRacingElements.winningDistances);
      actBreadcrumbs = GetElementText(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"));
      actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
      expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.HorseRacing + "";
    Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
	}
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);}
}

/** 
 * TC-13  HorseRacingForecastClearFunctionality
 * @return none
 * @throws Exception element not found
 */

@Test
 public void VerifyForecastClearFunctionality() throws Exception {
	String testCase = "VerifyForecastClearFunctionality";
	
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	
	Boolean statusBeforeClear, statusBeforeClear1 , statusAfterClear, statusAfterClear1;
    String xPath, valueText;
    try{
		 launchweb();
		 HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
		 
		//Verifying radio buttons are selected 
		 click(HorseRacingElements.checkbox);
         WebElement element = getDriver().findElement(HorseRacingElements.checkbox);
         statusBeforeClear = element.isSelected();
         if (statusBeforeClear == true)
         {
            
            //Verifying Other radio buttons state(disabled state) and Clear button
             List<WebElement> forecasrColList =getDriver().findElements(HorseRacingElements.forecastColRadioBtn);
             for (int j = 1; j <= forecasrColList.size() - 3; j++)
             {
                 xPath = "//span[@class='forecast']//span//input[@class='forecast-radio' and @data-col='0' and @data-row='" + j + "']";
                Assert.assertTrue(getAttribute(By.xpath(xPath), "disabled").contains("true"), "Radio button is not disabled");
             }
            Assert.assertTrue(isElementPresent(HorseRacingElements.clearButton), "Clear button is not found");
            click(HorseRacingElements.checkbox1);
             WebElement element1 = getDriver().findElement(HorseRacingElements.checkbox1);
             statusBeforeClear1 = element1.isSelected();
             List<WebElement> forecasrCol1List =getDriver().findElements(HorseRacingElements.forecastColRadioBtn);
             for (int k = 2; k <= forecasrCol1List.size() - 3; k++)
             {
                 xPath = "//span[@class='forecast']//span//input[@class='forecast-radio' and @data-col='1' and @data-row='" + k + "']";
                 Assert.assertTrue(getAttribute(By.xpath(xPath), "disabled").contains("true"), "Radio button is not disabledt");
             }
            Assert.assertTrue(isElementPresent(HorseRacingElements.clearButton), "Clear button is not found");
             //Verifying Betting value after selecting the Radio buttons 
            Assert.assertTrue(isElementPresent(HorseRacingElements.bettingValue), "Betting value is not visible");

             //Verifying betting value clicking on clear button
            scrollToView(HorseRacingElements.clearButton);
             click(HorseRacingElements.clearButton, "Clear button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
             if (valueText.equals(""))
             {
                System.out.println("Value is cleared and not showing");
             }
             else
               Assert.fail("Value is not cleared");

             //Verifying radio buttons are not selected after Clear 
             statusAfterClear = element.isSelected();
             statusAfterClear1 = element1.isSelected();
             if (statusAfterClear == false && statusAfterClear1 == false)
             {
                Assert.assertFalse(isElementPresent(HorseRacingElements.clearButton), "Clear button is found");
             }
         }
		 }
		 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		 	
		 	
		 	Assert.fail(e.getMessage(), e);}
		 }
/** 
 * TC-14  Verifying Form, Result and Commentary on Horse racing page
 * @return none
 * @throws Exception element not found
 */
@Test
public void VerifyFormResultAndCommentaryOnHorseRacingPage() throws Exception{
	String testCase ="VerifyFormResultAndCommentaryOnHorseRacingPage";
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
	    WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	    scrollToView(Element);
	    click(By.xpath(hrEvent));
	    
	  //Verify Form, Results and Commentary tabs
        Assert.assertTrue(isElementPresent((HorseRacingElements.formTab)), "Form Tab is not found");
        Assert.assertTrue(isElementPresent((HorseRacingElements.resultsTab)), "Results Tab is not found");
        Assert.assertTrue(isElementPresent((HorseRacingElements.commentaryTab)), "Commentary Tab is not found");
        
      //Popup for the Form tab
        click(HorseRacingElements.formTab);
        SwitchToPage("racingpost.com");

        //Popup for the Results tabs
        click(HorseRacingElements.resultsTab);
        SwitchToPage( "racingpost.com");

        //Popup for the Commentary tab
        click(HorseRacingElements.commentaryTab);
        SwitchToPage( "Live Commentaries");
        System.out.println("Form,Result and Commentary on Horse racing page was successful");
        
      //Verify Tips tab
        Assert.assertTrue(isElementPresent((HorseRacingElements.tipsTab)), "Tips Tab is not found");
        click(HorseRacingElements.tipsTab);
        Assert.assertTrue(isElementPresent((HorseRacingElements.verdictImage)), "verdict image is not found");
        
        //Verify Verdict tab
        Assert.assertTrue(isElementPresent((HorseRacingElements.verdictTab)), "Verdict Tab is not found");
        click(HorseRacingElements.verdictTab);
        Assert.assertTrue(isElementPresent((HorseRacingElements.verdictImage)), "verdict image is not found");
        

	}
	 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	 Assert.fail(e.getMessage(), e);}
	 }
/** 
 * TC-15  Verifying fractional to decimal appear on all modules on Horse racing page
 * <TestCaseID>4(sprint 5 NLSN-694)</TestCaseID>
 * @return none
 * @throws Exception element not found
 */
@Test
public void VerifyFractionalToDecimalAppearOnAllModulesOnHorseRacingPage() throws Exception{
	String testCase ="VerifyFractionalToDecimalAppearOnAllModulesOnHorseRacingPage";
	String typeName =  testdata.Readtestdata("PreProdEvents",9, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 9, 4);
	String price;
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
	    WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	    scrollToView(Element);
	    click(By.xpath(hrEvent));
	    
	  //Fraction to decimal in Race event
        price = GetElementText(HorseRacingElements.LP);
        if (common.isAlpha(price))   //has  no odds only SP
        {
          Assert.fail("Live Price not present in raceCard page");
        }
        else
        {
            // Event Sports module
        	common.fractToDec(HorseRacingElements.LP);
           //Logger.Pass("changing from fractional to decimal for Today's Racing event was successfull");
        }

        //Fraction to decimal in Next race
       common.SelectLinksFromAZ(TextControls.HorseRacing);
        price = GetElementText(HorseRacingElements.nextRaceOdds);
        if (common.isAlpha(price))   //has  no odds only SP
        {
          Assert.fail("Live Price not present in Next raceCard page");
        }
        else
        {
            // Event Sports module
        	common.fractToDec(HorseRacingElements.nextRaceOdds);
        }

	}
	 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	 Assert.fail(e.getMessage(), e);}
	 }


/** 
* TC-16 VerifyFutureHorseRacingEventDetailsPage
* @return none
* @throws Exception 
* @throws Exception element not found
*/
@Test
public void VerifyFutureHorseRacingEventDetailsPage() throws Exception{
	String testCase = "VerifyFutureHorseRacingEventDetailsPage";
	String eventName;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	Assert.assertTrue(isElementPresent(HorseRacingElements.futureHorseRaces), "Future Horse Races not displayed in Horse Racing Landing Page");
    Assert.assertTrue(isElementPresent(By.xpath("//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']")), "No Future Horse racing events found");
     
    //Check whether FHR events are present and navigate to an event                                                
     String eventXPath = "//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']//div[1]//a//span[@class='racing-upcoming-meeting-name']";
     eventName = GetElementText(By.xpath(eventXPath));
     WebElement evnt = getDriver().findElement(By.xpath(eventXPath));
     scrollToView(evnt);
     click(By.xpath(eventXPath), "Required Future Racing Event not found");
      Thread.sleep(2000);
     if (eventName.contains("'"))
     {
         String evtName = eventName.replace("'", "XZ");
         String eventName1 = evtName.substring(0,evtName.indexOf("XZ"));
         eventName = eventName1;
     }
     //Check whether navigated to the event details page 
    Assert.assertTrue(isElementPresent(By.xpath("//div[@class='basic-scoreboard']//span[@class='name' and contains(text(), '" + eventName + "')]")), "Navigation to Future Horse racing event failed");
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);}
}

/** 
* TC-16 VerifyHorseRacingAllRacingUkIrish
* @return none
* @throws Exception 
* @throws Exception element not found
*/
@Test
public void VerifyHorseRacingAllRacingUkIrish() throws Exception{
	String testCase = "VerifyHorseRacingAllRacingUkIrish";
	String browserType = ReadTestSettingConfig.getTestsetting("BrowserType");
	String allRacingDropdownText;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	Assert.assertTrue(isElementPresent(HorseRacingElements.allRacingDropdown), "All Racing dropdown in Today's Races is not displayed on Horse Racing Landing Page");
    allRacingDropdownText = GetElementText(HorseRacingElements.allRacingDropdown);
  
       if (allRacingDropdownText.contains(TextControls.allRacingText))
    {
       click(HorseRacingElements.allRacingDropdown, "All Racing dropdown in Today's Races is not displayed on Horse Racing Landing Page");
       Thread.sleep(1000);
       Assert.assertTrue(isElementPresent(HorseRacingElements.allRacing), "All Racing option is not displayed in the dropdown");
       Assert.assertTrue(isElementPresent(HorseRacingElements.ukIrish), "UK & Irish option is not displayed in the dropdown");

        WebElement elem = getDriver().findElement(HorseRacingElements.allRacingDropdown);
        List<WebElement> options = elem.findElements(By.tagName("option"));
        String[] arr = new String[2]; 
        int j = 0;
        for (WebElement option : options)
            arr[j++] = option.getText();
       Assert.assertTrue(arr[0].equalsIgnoreCase(TextControls.allRacingText), "All Racing option is not displayed in the dropdown");
       Assert.assertTrue(arr[1].equalsIgnoreCase(TextControls.ukText + " & " + TextControls.irishText), "UK & Irish option is not displayed in the dropdown");
       
       // Logger.Pass("The dropdown is available with All Racing and UK&Irish optioins and by default All Racing is shown on it");
    }
    else
     Assert.fail("The dropdown is not available with All Racing and UK&Irish optioins OR by default All Racing is not shown on it");
    

    //Logger.AddTestCase("VerifyHorseRacingUKRaces", "UK & Irish dropdown option should display only UK & Irish races");
    click(HorseRacingElements.allRacingDropdown, "All Racing dropdown in Today's Races is not displayed on Horse Racing Landing Page");
    Thread.sleep(1000);
    click(HorseRacingElements.ukIrish, "UK & Irish option is not displayed in the dropdown");
    Thread.sleep(1000);
    List<WebElement> ukRaceTimes = getDriver().findElements(HorseRacingElements.todayHRModuleVenuesList);
    String[] arrUkRaces = HRfunctionObj.add_list_items_to_Array(ukRaceTimes);

    if (browserType.equals("Firefox"))
    {
        getDriver().findElement(HorseRacingElements.futureHorseRaces).sendKeys(Keys.PAGE_UP);
        Thread.sleep(3000);
    }

    click(HorseRacingElements.allRacingDropdown, "All Racing dropdown in Today's Races is not displayed on Horse Racing Landing Page");
    Thread.sleep(1000);
    click(HorseRacingElements.allRacing, "All Racing option is not displayed in the dropdown");
    Thread.sleep(1000);
    List<WebElement> allRacingRaceTimes = getDriver().findElements(HorseRacingElements.todayHRModuleVenuesList);
    String[] arrAllRaces = HRfunctionObj.add_list_items_to_Array(allRacingRaceTimes);


    for (int k = 0; k < ukRaceTimes.size() - 1; k++)
    {
       Assert.assertTrue((arrUkRaces[k] == arrAllRaces[k]), "UK & Irish races are not displayed first as expected");

    }
  // Logger.Pass("UK & Irish races are displayed first as expected");


    //Logger.AddTestCase("VerifyHorseRacingAllRaces", "All Racing dropdown option should display all races both UK and Non UK races");
    if (ukRaceTimes.size() == allRacingRaceTimes.size())
      System.out.println("No Non-UK races available");
    else
    System.out.println("All races are displayed correctly as expected (Non UK races are displayed after UK races)");
    
}

catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);
}
} 


/** 
 * TC-17  VerifyHorseracingFCTC_SelectionInfo
 * @return none
 * @throws Exception element not found
 */

@Test
 public void VerifyHorseracingFCTC_SelectionInfo() throws Exception {
	String testCase = "VerifyHorseracingFCTC_SelectionInfo";
	
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	
	try{
		 launchweb();
		 Common.OddSwitcher("decimal");
		 HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
		 
		 // Ascending order
         List<WebElement>sortListAsc = getDriver().findElements(By.xpath("//div[@class='runners']//span[@class='selections']//span"));
         List<Double> ascList = new ArrayList<>();
         for (WebElement ele : sortListAsc)
         {
             if (!TextUtils.isEmpty(ele.getText()))
                 ascList.add(Double.parseDouble(ele.getText()));
         }
         //check whether its displayed in ascending order
         boolean ascendingOrder = common.AscendingOrder(ascList);
         Assert.assertTrue(ascendingOrder == true, "Odds not sorted in ascending order");


         //Logger.AddTestCase("Verify  forecast/tricast Racecard ", "Racecard should be having selection and jocky name, age, weight and race winner odds ");
         List<WebElement>selectionNames = getDriver().findElements(By.xpath("//div[@class='race-card FC_TC']//div[@class='runners']//span/div//div//span[@class='name' and contains(@data-bind, 'text')]"));
         for (int k = 1; k <= selectionNames.size() - 1; k++)
         {
           Assert.assertTrue(isElementPresent(HorseRacingElements.runnerName), "Runner name is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.jockyName), "Jocky name is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.horseAge), "Horse age is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.weight), "weight is not found");
           // Logger.Pass("Verification of forecast/tricast Racecard information was successful");
         }
    }
    catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);}
	} 

/** 
* TC-18 VerifyHorseRacingHierarchyDisplayOnContextualMenu
* @return none
* @throws Exception 
* @throws Exception element not found
*/
@Test
public void VerifyHorseRacingHierarchyDisplayOnContextualMenu() throws Exception{
	String testCase = "VerifyHorseRacingHierarchyDisplayOnContextualMenu";
	String contextualMenu;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	 Assert.assertTrue(isElementPresent(HorseRacingElements.nextRaces), "Next Races not displayed in Horse Racing Landing Page");
     Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
      if (!isElementPresent(HorseRacingElements.tomorrowsHorseraces))
      {
    	  WebElement element = getDriver().findElement(HorseRacingElements.tomorrowsHorseraces);
          scrollToView(element);
      }

    Assert.assertTrue(isElementPresent(HorseRacingElements.tomorrowsHorseraces), "Tomorrow's Races not displayed in Horse Racing Landing Page");
    Assert.assertTrue(isElementPresent(HorseRacingElements.futureHorseRaces), "Future Horse Races not displayed in Horse Racing Landing Page");
      if (isTextPresent(TextControls.nextRacesText) && isTextPresent(TextControls.HorseRacingToday) && isTextPresent(TextControls.HRtomorrow) && isTextPresent(TextControls.futureText))
      {
    	  scrollToView(HorseRacingElements.raceEvent);
          //When Today, Tomorrow, Future Racing and Specials(if available) is available
          click(HorseRacingElements.raceEvent, "Event in Today's Races module on Horse Racing Landing Page not found");
          contextualMenu = "//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),   '" + TextControls.nextThreeRaces + "')]/following::header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText+ "')]/following::header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText + "')]/following::header[contains(@class, 'specials')]//span[contains(text(), '" + TextControls.futureText + "')]";
          Assert.assertTrue(isElementDisplayed(By.xpath(contextualMenu)), "Contextual Menu is not displayed in the correct hierarchy");
         //Logger.Pass("Contextual menu Hierarchy is displayed as NextRaces, Todays events, Tomorrow's events and Future Racing and Specials(if available)");
      }
      else
        Assert.fail("All Horse Racing Modules are not displayed on Horse Racing landing Page");
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);}
} 


/** 
* TC-19 VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces
* @return none
* @throws Exception 
* @throws Exception element not found
*/
@Test
public void VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces() throws Exception{
	String testCase = "VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces";
	String contextualMenu;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	Assert.assertTrue(isElementPresent(HorseRacingElements.nextRaces), "Next Races not displayed in Horse Racing Landing Page");
    Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
                   
    /*WebElement Element = getDriver().findElement(HorseRacingElements.tomorrowsHorseraces);
    scrollToView(Element);
    Assert.assertTrue(isElementPresent( HorseRacingElements.tomorrowsHorseraces), "Tomorrow's Races not displayed in Horse Racing Landing Page");*/
    
    if (isTextPresent(TextControls.nextRacesText) && isTextPresent(TextControls.HorseRacingToday) && isTextPresent(TextControls.HRtomorrow) && isTextPresent(TextControls.futureText)) 
        {
    	scrollToView(HorseRacingElements.raceEvent);
        // When Future  racing and Specials event is not available (Today, Tomorrow is available)
         click(HorseRacingElements.raceEvent, "Event in Today's Races module on Horse Racing Landing Page not found");
         contextualMenu = "//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header expanded')]//span[contains(text(),   '" + TextControls.nextThreeRaces + "')]/following::header[contains(@class, 'meeting-header expanded')]//span[contains(text(), '" + TextControls.todayText + "')]";
        if (isElementPresent(HorseRacingElements.tomorrowsHorseraces))
         contextualMenu = contextualMenu + "/following::header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.tomorrowText+ "')]";
        Assert.assertTrue(isElementDisplayed(By.xpath(contextualMenu)), "Contextual Menu is not displayed in the correct hierarchy");
         
        //Logger.Pass("Contextual menu Hierarchy is displayed as NextRaces, Todays events and Tomorrow's events");
     }
     else
       System.out.println("Future Races are available , hence ' VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces' is not possible");
	
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);}
} 

/** 
* TC-20 Verify that Tomorrow's racing is not displayed on the contextual menu in meeting venue view 
* @return none
* @throws Exception 
* @throws Exception element not found
*/
@Test
public void VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoTmrwRaces() throws Exception{
	String testCase = "VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoTmrwRaces";
	String contextualMenu;
	
try{
	launchweb();
	common.SelectLinksFromAZ(TextControls.HorseRacing);
	
	Assert.assertTrue(isElementPresent(HorseRacingElements.nextRaces), "Next Races not displayed in Horse Racing Landing Page");
    Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
    Assert.assertTrue(isElementPresent(HorseRacingElements.futureHorseRaces), "Future Horse Races not displayed in Horse Racing Landing Page");
    
    
    if (isTextPresent(TextControls.nextRacesText) && isTextPresent(TextControls.HorseRacingToday) && isTextPresent(TextControls.HRtomorrow) && !isTextPresent(TextControls.futureText)) 
        {
    	//When Tomorrow's racing is not available (Today, Future Racing and Specials(if available) is available)
    	
         click(HorseRacingElements.raceEvent, "Event in Today's Races module on Horse Racing Landing Page not found");
         contextualMenu = "//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),   '" + TextControls.nextThreeRaces + "')]/following::header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText + "')]/following::header[contains(@class, 'specials')]//span[contains(text(), '" + TextControls.futureText + "')]";
        Assert.assertTrue(isElementDisplayed(By.xpath(contextualMenu)), "Contextual Menu is not displayed in the correct hierarchy");
     }
     else
       System.out.println("Tomorrow Races are available , hence ' VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoTmrwRaces' is not possible");
	
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage(), e);}
} 


/** 
 * TC-21  Validate Contextual menu in HorseRacing event detail page
* @return none
 * @throws Exception element not found
 */

@Test
public void VerifyHorseracingMeetingVenueTabAndTimeInContextualMenu() throws Exception {
	String testCase = "VerifyHorseracingMeetingVenueTabAndTimeInContextualMenu";
	String venuNameXPath, venuTimeXPath ;
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
        String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
        scrollToView(Element);
        click(By.xpath(hrEvent));
        
        venuNameXPath = "//div[contains(@class, 'meetings-group')]//h2[contains(@class, 'meetings') and contains(@data-bind, 'toggle')]//span[@class='title' and contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]";
        Assert.assertTrue(isElementPresent(By.xpath(venuNameXPath)), "Required venu name not found in Contextual Menu");

         venuTimeXPath = "//div[contains(@class, 'meetings-group')]//div[contains(@class, 'meetings-container')]//a[@class ='meetings-link selected' and contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
        Assert.assertTrue(isElementPresent(By.xpath(venuTimeXPath)), "Required race time not found in Contextual Menu");
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);}
	}

/** 
* TC-22  Validate Contextual menu in HorseRacing event detail page
* @return none
* @throws Exception element not found
*/

@Test
public void VerifyHorseracingMeetingVenueTabInContextualMenu() throws Exception {
	String testCase = "VerifyHorseracingMeetingVenueTabInContextualMenu";
	String raceTime1, raceTime2, raceTime3, raceName1, raceName2, raceName3, nextRaces, raceName;
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		
		
		raceTime1 = GetElementText(HorseRacingElements.raceTime1);
        raceName = GetElementText(HorseRacingElements.raceName1).toLowerCase();
        if(raceName.contains("("))
        	raceName = raceName.substring(0, raceName.indexOf('('));
        raceName1 = toTitleCase(raceName);
        
        raceTime2 = GetElementText(HorseRacingElements.raceTime2);
        raceName = GetElementText(HorseRacingElements.raceName2).toLowerCase();
        if(raceName.contains("("))
        	raceName = raceName.substring(0, raceName.indexOf('('));
        raceName2 = toTitleCase(raceName);
        
        raceTime3 = GetElementText(HorseRacingElements.raceTime3);
        raceName = GetElementText(HorseRacingElements.raceName3).toLowerCase();
        if(raceName.contains("("))
        	raceName = raceName.substring(0, raceName.indexOf('('));
        raceName3 = toTitleCase(raceName);
       
      click(HorseRacingElements.nextRaceEvent, "Next Races event not found");
        Thread.sleep(1000);
        // Verifying Next 03 Races is displaying on the contextual menu
        nextRaces = "(//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),   '" + TextControls.nextThreeRaces + "')]/following::span[@class='time' and contains(text(), '" + raceTime1 + "')]/following::span[@class='name' and contains(text(), '" + raceName1 + "')][1]/following::span[@class='time' and contains(text(), '" + raceTime2 + "')]/following::span[@class='name' and contains(text(), '" + raceName2 + "')]/following::span[@class='time' and contains(text(), '" + raceTime3 + "')]/following::span[@class='name' and contains(text(), '" + raceName3 + "')])[1]";
        Assert.assertTrue(isElementDisplayed(By.xpath(nextRaces)), "Next Races are not found");
      
        click(By.xpath("//div[@class='next-races-group']//a[@class='next-races-link'][1]"), "Next Races event not found");
         String eventPath = "//div[@id='content']//div[@class='header-navigation race-card']//div[@class='wrap-time-name']//span[contains(text(), '" + raceTime1 + "')]/following::span[contains(text(), '" + raceName1 + "')][1]";
         Assert.assertTrue(isElementPresent(By.xpath(eventPath)), "Navigation to Next races event - " + raceTime1 + " " + raceName1 + " failed");

	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage(), e);}
	}

/** 
* TC-23  Verify HorseRacing Todays Module Form and Result tabs
* @return none
* @throws Exception element not found
*/
@Test
public void VerifyHorseRacingTodaysModuleFormResult() throws Exception{
	String testCase ="VerifyHorseRacingTodaysModuleFormResult";
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		Assert.assertTrue(isElementPresent(HorseRacingElements.hrTodaysModuleResult), "Results tab in Today's Races not displayed on Greyhounds Landing Page");
        Assert.assertTrue(isElementPresent(HorseRacingElements.hrTodaysModuleForm), "Form tab in Today's Races not displayed on Greyhounds Landing Page");
        System.out.println("Results and Form are displayed in Today's Module on Greyhounds Page");

         // Verify GreyhoundsTodays Module ResultTab , "Results tab should take the user to the popped out Results page" .
        
        WebElement element = getDriver().findElement(HorseRacingElements.hrTodaysModuleResult);
        scrollToView(element);
        click(HorseRacingElements.hrTodaysModuleResult, "Results tab not found in Today's Races module on Greyhounds Landing Page");
         Thread.sleep(3000);
         common.SwitchToPage("Ladbrokes | Racing_form | Today's Fast Results");
         System.out.println("Results tab is taking the user to the popped out Results page");

         
         // Verify HorseRacing Todays Module Form Tab, "Form tab should take the user to the popped out Results page"
         
         click(HorseRacingElements.hrTodaysModuleForm, "Form tab not found in Today's Races module on Greyhounds Landing Page");
         Thread.sleep(3000);
         common.SwitchToPage("Ladbrokes | Racing_form | Race Cards");
        System.out.println("Form tab is taking the user to the popped out Results page");

	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-24  Verify HorseRacing Win Only Market Place Bet
 * @return none
 * @throws Exception element not found
 */

@Test
 public void VerifyHorseracingWOMarketsPlaceBet() throws Exception {
	String testCase = "VerifyHorseracingWOMarketsPlaceBet";
	
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	
	try{
		 launchweb();
		 Common.Login();
		 Common.OddSwitcher("decimal");
		 HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , false);
		 Assert.assertTrue(isElementDisplayed(HorseRacingElements.winOnlyTab), "Win Only Market is not found");
		 click(HorseRacingElements.winOnlyTab ,"Win Only Market Tab is not clickable" );
		 String selection = "(//div[@class='odds-button'])[2]";
		 String selName = GetElementText(By.xpath("(//div[@class='horse']//span[@class='name'])[2]"));
		 click(By.xpath(selection));
		 String eventsInfo = GetElementText(By.xpath("//span[@class='time']")) + GetElementText(By.xpath("//span[@class='name']"));
		 String odds = GetElementText(By.xpath("//span[@class='selections']//div[@class='odds-button selected']//span[@class='price']"));
	     String [] arrayOdds = new String[1];
	     arrayOdds[0] = odds;
		
	     //checking Bet information
         BTbetslipObj.VerifyBetSlip(eventsInfo, selName, "", "", "", "Single", 1 ,arrayOdds);
         scrollPage("Top");
         //Verifying Place bet
       common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-25  HR_RaceCard Bar
* @return none
 * @throws Exception element not found
 */
@Test
public void VerifyHR_SelectDeselectTodayTomorrowFutureRaceEvents() throws Exception{
	String testCase = "VerifyHR_SelectDeselectTodayTomorrowFutureRaceEvents";
	 String check = "check";
	 try{
		 launchweb();
	     common.NavigateToSportsPage(TextControls.HorseRacing);
	     
	   //Selecting today's race event
	     List<WebElement> todayEventCheckbox = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][1]//input"));
	    scrollToView(todayEventCheckbox.get(1));
	    todayEventCheckbox.get(1).click();
	    WebElement checkBox =  todayEventCheckbox.get(1);
	    if (check == "check")
        {
          Assert.assertTrue(checkBox.isSelected(), "Today's Race is not selected");
            System.out.println("Clicked Today's Checkbox");
        }
        else
        	Assert.assertTrue(!checkBox.isSelected(), "Today's Race is selected");
           Thread.sleep(2000);
           
         //Deselecting today's race event
           todayEventCheckbox.get(1).click();
           
           WebElement checkBox1 = todayEventCheckbox.get(1);
           if (check == "uncheck")
           {
        	   Assert.assertTrue(checkBox1.isSelected(), "Today's Race is not selected");
        	   System.out.println("Unchecked Today's Checkbox");
           }
           else
             Assert.assertTrue(!checkBox1.isSelected(), "Today's Race is selected");
           WebElement todayEvent=getDriver().findElement(By.xpath("//span[starts-with(@data-bind, 'html') and contains(text(), '" + TextControls.todayText + "')]//preceding-sibling::div[@class='icon-arrow expand']"));
           scrollToView(todayEvent);
           click(todayEvent);
           Thread.sleep(2000);
           
           //Selecting and deselecting tomorrow's race events
           if (isElementPresent(HorseRacingElements.tomorrowsHorseraces))
           {
             String tomoEvent = "//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input";
              
               //Selecting tomorrow's race event
              
              click(By.xpath("//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input"), "Tommorow's Race event is not found");
               Thread.sleep(200);

               checkBox = getDriver().findElement(By.xpath(tomoEvent));
               if (check == "check")
               {
                 Assert.assertTrue(checkBox.isSelected(), "Tomorrow's Race is not selected");
                   System.out.println("Clicked Tomorrow's Checkbox");
               }
               else
            	   Assert.assertTrue(!checkBox.isSelected(), "Tomorrow's Race is selected");
               Thread.sleep(200);
               
               //Deselecting tomorrow's race event
             
             click(By.xpath("//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input"), "Tommorow's Race event is not found");
               checkBox = getDriver().findElement(By.xpath(tomoEvent));

               if (check == "uncheck")
               {
            	   Assert.assertTrue(checkBox.isSelected(), "Tomorrow's Race is not unselected");
                   System.out.println("Unchecked Tomorrow's Checkbox");
               }

               else
            	   Assert.assertTrue(!checkBox.isSelected(), "Tomorrow's Race is selected");

               click(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.tomorrowText + "')]"));
               Thread.sleep(200);
           }
           else
           System.out.println("Tomorrow's Race Tab is not present");
           
         //Selecting and deselecting Future's race events
           
           String futureEvent = "//div[@class='module']//span[contains(text(), '"+ TextControls.futureText +"')]/../..//div[@class='racing-upcoming']/div[1]//input";
           //Selecting Future's race event
          click(By.xpath("//div[@class='module']//span[contains(text(), '"+ TextControls.futureText +"')]/../..//div[@class='racing-upcoming']/div[1]//input"), "Future Racing Event not found");
           checkBox = getDriver().findElement(By.xpath(futureEvent));
           if (check == "check")
           {
        	   Assert.assertTrue(checkBox.isSelected(), "Future Race is not selected");
               System.out.println("Clicked Future Checkbox");
           }
           else
              Assert.assertTrue(!checkBox.isSelected(), "Future Race is selected");
           Thread.sleep(2000);
           //Deselecting Future's race event
           click(By.xpath("//div[@class='module']//span[contains(text(), '"+TextControls.futureText+"')]/../..//div[@class='racing-upcoming']/div[1]//input"), "Future Racing Event not found");
           checkBox = getDriver().findElement(By.xpath(futureEvent));
           if (check == "uncheck")
           {
              Assert.assertTrue(checkBox.isSelected(), "Future Race is not selected");
               System.out.println("Unchecked Future's Checkbox");
           }
           else
              Assert.assertTrue(!checkBox.isSelected(), "Future Race is selected");
	 }
	 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage() , e);
		}
}

/** 
 * TC-26  VerifyHR_TodayTomorrowFutureRaceCard
* @return none
 * @throws Exception element not found
 */
@Test
public void VerifyHR_TodayTomorrowFutureRaceCard() throws Exception{
	String testCase = "VerifyHR_TodayTomorrowFutureRaceCard";
	String actBreadcrumbs, expBreadcrumbs;
	 try{
		 launchweb();
	     common.NavigateToSportsPage(TextControls.HorseRacing);
	     

         //Select the races form Today, Tomorrow and Future Horse Racing
         HRfunctionObj.selectDeslectRaces("check");
        Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is not displayed");
        Assert.assertFalse(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard displayed by unchecking the races");
        scrollPage("Top"); 
        
        //De-select the races form Today, Tomorrow and Future Horse Racing
         HRfunctionObj.selectDeslectRaces("uncheck");
       Assert.assertFalse(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is displayed");
       Assert.assertTrue(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard not displayed by unchecking the races");
       scrollPage("Top");  // Edge adjustment
        
        //Select the races form Today, Tomorrow and Future Horse Racing
         HRfunctionObj.selectDeslectRaces("check");
        Assert.assertFalse(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard displayed by unchecking the races");
        Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is not displayed");
        click(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']"), "Failed to click on Build race card button");
        Thread.sleep(3000);
        Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
         actBreadcrumbs = GetElementText(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"));
         actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
         expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.HorseRacing + ">" + TextControls.buildRaceCard + "";
       Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
        //Logger.Pass("Build Horse Racing Today Tomorrow Future Races verified successfully");
	 }
	 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage() , e);
		}
}

/** 
 * TC-27  VerifyHRMarketTabsForSelectedRacesOnRaceCardPage
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyHRMarketTabsForSelectedRacesOnRaceCardPage() throws Exception
{
	String testCase = "VerifyHRMarketTabsForSelectedRacesOnRaceCardPage";
    String actBreadcrumbs, expBreadcrumbs, raceNum;
    boolean iStatus = false;
    int j = 1, k = 3, raceCount, counter = 0; 
    try
    {
    	launchweb();
    	common.NavigateToSportsPage(TextControls.HorseRacing);
    	 
        //"Verify Select HR Events and build Race card", "User should be able to select and build the race card");
       
       List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
        for (int x = 0; x < venueList.size(); x++)
        {
            venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));

           List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']"));
            String[] buildRacesList = HRfunctionObj.add_list_items_to_Array(raceList);
            if (raceList.size() >= 3)
            {
                raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));

                for (int i = 0; i <= 3; i++)
                {
                    if (counter == 1)
                       break;
                    if (!raceList.get(i).isSelected())
                    {
                        //raceList.get(i).click();
                        clickByJS(raceList.get(i));
                       List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span//input"));
                      Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");
                      Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
                        raceNum = GetElementText(By.xpath("//a[@class='button action']/span"));
                        raceCount = i + 1;
                      Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
                        j = j + 1;
                    }
                }
                Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']")), "Build race card button is not enabled");
                click(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']"), "Failed to clcik on Build race card button");
                Thread.sleep(2000);
                Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
               
                actBreadcrumbs = GetElementText(By.xpath("//nav[contains(@class,'breadcrumbs')]"));
                actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
                expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.HorseRacing + ">" + TextControls.buildRaceCard + "";
                Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
                
                for (int i = 0; i <3; i++)
                {
                   WebElement element = getDriver().findElement(By.xpath("//div[@class='wrap-time-name']//span[@class='time' and contains(text(), '" + buildRacesList[i] + "')]"));
                  scrollToView(element);
                  Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='wrap-time-name']//span[@class='time' and contains(text(), '" + buildRacesList[i] + "')]")), buildRacesList[i] + " event Time is not found in BUild Race Card Page");
                  Assert.assertTrue(isElementDisplayed(By.xpath("(//div[@class='wrap-time-name']/following::div[@class='tab-container'])[" + (i + 1) + "]//nav[@class='tabs']//ul//li")), buildRacesList[i] + " Market tab is not found in BUild Race Card Page");
                    Thread.sleep(1000);
                                                                          
                    iStatus = true;
                }
            }
            if (iStatus == true)
                break;
        }
}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-28  VerifyHRSelectedRacesListedInTimeOrderOnRaceCardPage
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyHRSelectedRacesListedInTimeOrderOnRaceCardPage() throws Exception{
	String testCase ="VerifyHRSelectedRacesListedInTimeOrderOnRaceCardPage";
	String actBreadcrumbs, expBreadcrumbs, raceNum;
    int j = 1, k = 2, raceCount;
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
		 List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
         for (int x = 0; x < venueList.size(); x++)
         {
             venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));

             List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']"));
             String[] buildRacesList = HRfunctionObj.add_list_items_to_Array(raceList);
             if (raceList.size() >= 3)
             {
                 raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));

                 for (int i = 0; i <= 4; i++)
                 {
                     
                     if (!raceList.get(i).isSelected())
                     {
                    	 clickByJS(raceList.get(i));
                         List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span//input"));
                        Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");
                        Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
                         raceNum = getDriver().findElement(By.xpath("//a[@class='button action']/span")).getText().toLowerCase();
                         raceCount = i + 1;
                         Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
                         j = j + 1;
                     }
                 }
               Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']")).isDisplayed(), "Build race card button is not enabled");
                 click(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']"), "Failed to clcik on Build race card button");
                 Thread.sleep(3000);
               Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
                 actBreadcrumbs = (getDriver().findElement(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"))).getText();
                 actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
                 expBreadcrumbs = expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.HorseRacing + ">" + TextControls.buildRaceCard + "";
               Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
                 Thread.sleep(200);

                 // Check selected races displayed in time order
                 List<WebElement> sortListAsc = getDriver().findElements(By.xpath("//div[@class='racing-page']//div[@class='header-navigation race-card']//span[@class='time']"));
                   System.out.println(sortListAsc.get(0).getText());                                                        
                 //Check selected race count and number races displayed on racecard page 
                 List<String>  ascList = new ArrayList<>();
                  for (WebElement ele : sortListAsc)
                 {
                     if (!TextUtils.isEmpty(ele.getText())){
                    	 System.out.println("--->>>" + ele.getText());
                        ascList.add(ele.getText());}
                 }
                 //check whether its displayed in ascending order
                boolean ascendingOrder = common.isSorted(ascList);
                Assert.assertTrue(ascendingOrder == true, "Selected races not displayed in time order");
                 
		}
         }
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-29  Verifying Market tabs on Horse racing page
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyMarketTabsOnHorseRacing() throws Exception{
	String testCase ="VerifyMarketTabsOnHorseRacing";
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
    
		 String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
	        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	        scrollToView(Element);
	        click(By.xpath(hrEvent));
	        
	        //Logger.AddTestCase("Verify the market tabs to be shown on the racecard on Page Load ", " market tabs should be shown on the racecard on Page Load");
           List<WebElement> marketTabs = getDriver().findElements(By.xpath("//nav[@class='tabs']//ul//li"));
            int count = (marketTabs.size())-1;
           String marketTabsText = marketTabs.get(count).getText();
            if (marketTabsText.toLowerCase().contains("totepool"))
                count = marketTabs.size() - 1;
            else
                count = marketTabs.size();
            for (int i = 0; i < count; i++)
            {
                marketTabs = getDriver().findElements(By.xpath("//nav[@class='tabs']//ul//li"));
                if (i == 0)
                {
                	String val = getAttribute(By.xpath("(//nav[@class='tabs']/following::li[@class='market-selector__item__container']//div)[" + (i + 1) + "]"), "class");
                    System.out.println("--->>>"+ val);
                	Assert.assertTrue((getAttribute(By.xpath("(//nav[@class='tabs']/following::li[@class='market-selector__item__container']//div)[" + (i + 1) + "]"), "class")).contains("active"));
                }
                else
                {
                    marketTabs.get(i).click();
                    Assert.assertTrue(isElementDisplayed(By.xpath("//nav[@class='tabs']/following::div[@class='market-selector__item']")), "" + marketTabs.get(i).getText() + " Market tab not active");
                }
               List<WebElement> selectionList = getDriver().findElements(HorseRacingElements.marketSellections);
              Assert.assertTrue(selectionList.size() > 0, "selection List is not found under market");
            }
           //Logger.Pass("The market tabs to be shown on the raceCard on Page Load was successful");

            //Logger.AddTestCase("Verify 'Show all spotlight' and 'Show all form' buttons are shown on the racecard", " 'Show all spotlight' and 'show all form' buttons should be shown on the racecard");
          Assert.assertTrue(isElementPresent(HorseRacingElements.showAllSpotlight), "Show All Spotlight tab is not found");
          Assert.assertTrue(isElementPresent(HorseRacingElements.showAllForm), "Show all form is not found");

            //============================================================================================================================================================//
           List<WebElement> selWithSpotlightIcon = getDriver().findElements(By.xpath("//*[@class='runners']//*[@class='item']//*[contains(@data-bind, 'spotlight')]"));
           List<WebElement> selWithFormIcon = getDriver().findElements(By.xpath("//*[@class='runners']//*[@class='item']//*[contains(@data-bind, 'getRunnerForm')]"));
           click(HorseRacingElements.showAllSpotlight);
            if ((GetElementText(HorseRacingElements.spotlightExpand)).contains("No spotlight information available at this time."))
            {
               System.out.println("No spotlight information available");
            }

            else
            {
               List<WebElement> selWithSpotlightInfo = getDriver().findElements(By.xpath("//*[@class='spotlights']"));
              Assert.assertTrue(selWithSpotlightIcon.size() == selWithSpotlightInfo.size(), "Every runner in the event does not have spotlight info");
                click(HorseRacingElements.hideAllSpotlight);
               Assert.assertFalse(isElementPresent(By.xpath("//*[@class='runners']//div[@class='item']/..//span[@class='spotlights']")), "Spotlight info is still visible even after clicking on hide All Spotlight tab");
            }


            click(HorseRacingElements.showAllForm);
           List<WebElement> selWithFormInfo = getDriver().findElements(By.xpath("//span[@class='forms']//ul"));
          Assert.assertTrue(selWithFormIcon.size() == selWithFormInfo.size(), "Every runner in the event does not have Form info");
            for (int z = 1; z <= selWithFormInfo.size(); z++)
            {
               Assert.assertTrue(isElementPresent(By.xpath("(//*[@class='runners']//div[@class='item']/..//span[@class='forms']//ul//li[1])[" + z + "]")), "Header info not present for selection " + z);
               Assert.assertTrue(isElementPresent(By.xpath("(//*[@class='runners']//div[@class='item']/..//span[@class='forms']//ul//li[2])[" + z + "]")), "Data not present under header info for selection " + z);
            }
           Assert.assertTrue(isElementPresent(HorseRacingElements.hideAllForm), "Hide all form tab is not found");
            click(HorseRacingElements.hideAllForm);
            
          Assert.assertFalse(isElementPresent(By.xpath("//span[@class='forms']//ul")), "Form info is still visible even after clicking on hide All Form tab");
            //============================================================================================================================================================//

           //Logger.Pass("'Show all spotlight' and 'Show all form' buttons are shown on the racecard was successful");
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-30  Verifying HR NextRaces More Link
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyHRNextRacesMoreLink() throws Exception{
	String testCase ="VerifyHRNextRacesMoreLink";
	String eventName, typeName;
   
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
		 Assert.assertTrue(isElementPresent(HorseRacingElements.nextRaces), "Next Races not displayed in Greyhounds Landing Page");

         Assert.assertTrue(isElementPresent(HorseRacingElements.nextRacesModules), "Events not present under Next Races module");
         List<WebElement> nextRaces = getDriver().findElements(HorseRacingElements.nextRacesModules);

          for (int i = 1; i <= nextRaces.size(); i++)
          {

              if (getDriver().findElement(By.xpath("//div[@class='next-race'][" + i + "]//header[@class='next-race-header']//a[2]")).getText().toLowerCase().contains(TextControls.moreText.toLowerCase()))
              {
                 List<WebElement> nextRaceEvents = getDriver().findElements(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//div[@class='next-race-participant']"));
                 Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-time']")), "Event name not found");
                  eventName = GetElementText(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-time']"));
                 Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-title']")), "Venue name not found");
                  typeName = GetElementText(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-title']")).toLowerCase();
                  typeName = toTitleCase(typeName);

                  click(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//a[@class='next-race-header-more']"));
                  
                 HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.HorseRacing, typeName, "" , eventName);
                // Logger.Pass("Next Races Event Details Page displayed successfully");

                 List<WebElement> runnersList = getDriver().findElements(By.xpath("//div[@class='race-container']//div[@class='item']"));
                  if (runnersList.size() > nextRaceEvents.size())
                  {
                    System.out.println("More link  is displayed as expected in Next Race module");
                      break;
                  }
                  else
                   Assert.fail("More Link is displayed inapprapriately");

              }
              else
            	  Assert.fail("More link is not displayed in the Next Races module");
          }

      }
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage() , e);
}
}

/** 
 * TC-31  Verifying Contextual Menu Racing
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_HR() throws Exception{
	String testCase ="VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_HR";
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));
        

        //Select the option to collapse today's racing on contextual men
        if (!(getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText+ "')]"), "class").contains("expanded")))
           click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText+ "')]"), "Today's Meeting is not present on Racecard page");

        //Select the option to expand today's racing on contextual menu
        if (!(getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]"), "class").contains("expanded")))
            click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText+ "')]"), "Today's Meeting is not present on Racecard page");

        //Verifying the expand/collapse and events under expanded Venue
        String test = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][1]//span[@class='title']";
        String venueName1 = GetElementText(By.xpath(test));

        if (!isElementPresent(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings expanded')][1]//span[@class='title' and contains(text(), '" + venueName1 + "')]")))
           click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][1]//span[@class='title' and contains(text(), '" + venueName1 + "')]"));

      Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText+ "')]/../following-sibling::div[@class='meetings-group']//h2[1]/following-sibling::div[@class='meetings-container'][1]/a")), "Venue tab is not expanded and events are not displayed");
       System.out.println("Option to Collapse and  Expand Todays Racing On Contextual menu is successfull");
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-32  Verifying RaceCard by sorting selection name
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyRacecardBySortingSelectionName() throws Exception{
	String testCase ="VerifyRacecardBySortingSelectionName";
	try{
		launchweb();
		Common.OddSwitcher("decimal");
		common.NavigateToSportsPage(TextControls.HorseRacing);
        String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
	        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	        scrollToView(Element);
	        click(By.xpath(hrEvent));
	        
	      //Sorting by Selection name 
           
          Assert.assertTrue(isElementPresent((HorseRacingElements.runnerLabel)), "Runner Label is not found");
            HRfunctionObj.verifyRaceCardSorting("//div[@class='horse']//span[@class='name']", "Runner label", "//span[contains(@class,'runner-header sortable')]");
           //Logger.Pass("Racecard sorting by Racecard selection names was successful");

           getDriver().navigate().refresh();
            //Sorting by Rating 
            //Logger.AddTestCase("Verify Racecard by sorting Racecard Rating", "Rating should be sorted from smallest to largest ");
          Assert.assertTrue(isElementPresent((HorseRacingElements.ratingLabel)), "Rating Label is not found");
            click(HorseRacingElements.ratingLabel, " Rating label not displayed in raceCard page");
            // Ascending order
            List<WebElement> ratingSortListDsc =  getDriver().findElements(By.xpath("//span[@class='rating']"));
            List<Double> RateDscList = new ArrayList<>();
            for(WebElement ele : ratingSortListDsc)
            {
                if (!TextUtils.isEmpty(ele.getText()))
                    RateDscList.add(Double.parseDouble(ele.getText()));
            }
            //check whether its displayed in ascending order
            boolean rateDscendingOrder = common.descendingOrder(RateDscList);
           Assert.assertTrue(rateDscendingOrder == true, "Ratings not sorted in descending order");
           //Logger.Pass("Racecard sorting by Racecard rating was successful");

          // getDriver().navigate().refresh();
            //Sorting by Odds 
            List<WebElement> oddsList =  getDriver().findElements(By.xpath("//span[@class='selections']//span[@class='price']"));
            List<String> list = new ArrayList<>();
            for (WebElement element : oddsList)
                list.add(element.getText());
            //if Odds has both SP and live price
            if ((list.contains("SP") && (list.contains(".")) || list.contains("/")) || (list.contains(".")))
            {
               // Ascending order
                List<WebElement> sortListAsc =  getDriver().findElements(By.xpath("//div[@class='odds-button']//span[@class='odds-convert']"));
                List<Double> ascList  = new ArrayList<>();
                for (WebElement ele : sortListAsc)
                {
                    if (!TextUtils.isEmpty(ele.getText()))
                        ascList.add(Double.parseDouble(ele.getText()));
                }
                //check whether its displayed in ascending order
                boolean ascendingOrder = common.AscendingOrder(ascList);
               Assert.assertTrue(ascendingOrder == true, "Odds not sorted by default in ascending order");
                click(HorseRacingElements.oddLabel, "Odds label is not found");
                Thread.sleep(1000);
                //Descending order
                List<WebElement> sortListDesc =  getDriver().findElements(By.xpath("//div[@class='odds-button']//span[@class='odds-convert']"));
                List<Double> descList = new ArrayList<>();
                for (WebElement ele : sortListDesc)
                {
                    if (!TextUtils.isEmpty(ele.getText()))
                        descList.add(Double.parseDouble(ele.getText()));
                }
                //check whether numbers are displayed in descending order or not
                boolean descendingOrder = common.descendingOrder(descList);
               Assert.assertTrue(descendingOrder == true, "Odds are not sorted in descending order");
           }
            //if odds contains only SP
            else if ((list.contains("SP")) && !(list.contains(".")) || list.contains("/"))
            {
                System.out.println("Verify Racecard by sorting Racecard number ,Number should be sorted from smallest to largest ");
            }
          getDriver().navigate().refresh();
        //Number in Descending order
            click(HorseRacingElements.noLabel, "Number Label is not found");
            List<WebElement> numSortList2 =  getDriver().findElements(By.xpath("//span[@class='number']//span[contains(@data-bind, 'text: number')]"));
            List<Integer> numDescList = new ArrayList<>();
            for(WebElement ele : numSortList2)
            {
            	if (!TextUtils.isEmpty(ele.getText()))
                    numDescList.add(Integer.parseInt(ele.getText()));
            }
            //check whether numbers are displayed in descending order or not
            boolean numDescOrder = common.descendingInteger(numDescList);
           Assert.assertTrue(numDescOrder == true, "Numbers not sorted in descending order");

            //Number in Descending order
            click(HorseRacingElements.noLabel, "Number Label is not found");
            List<WebElement> numSortList1 =  getDriver().findElements(By.xpath("//span[@class='number']//span[contains(@data-bind, 'text: number')]"));
            List<Integer> numAscList = new ArrayList<>();
            for (WebElement ele : numSortList1)
            {
                if (!TextUtils.isEmpty(ele.getText()))
                    numAscList.add(Integer.parseInt(ele.getText()));
            }
            //check whether its displayed in ascending order
            boolean numAscOrder = common.AscendingOrderInteger(numAscList);
           Assert.assertTrue(numAscOrder == true, "Number not sorted in ascending order"); ;
           
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-33 Verifying RaceCard on Horse racing page
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyRacecardForEachHorse() throws Exception{
	String testCase ="VerifyRacecardForEachHorse";
	String emptyNumber, emptyRating, unnamedSP, secondemptyNumber, secondemptyRating, secondUnnamedSP, favString, fav2String;
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
        String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
	        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	        scrollToView(Element);
	        click(By.xpath(hrEvent));
	        
	        //Verify the RaceCard details for each horse
           Assert.assertTrue(isElementPresent(HorseRacingElements.noLabel), "Number label is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.silkImages), "Silk images is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.runnerName), "Runner name is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.jockyName), "Jocky name is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.formNumber), "Form number is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.spotlight), "Spot Light is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.horseAge), "Horse age is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.weight), "weight is not found");
           Assert.assertTrue(isElementPresent(HorseRacingElements.ratingLabel), "Rating label is not found");
             
            //Verify Unnamed Favourite
             List<WebElement> runnersList =  getDriver().findElements(By.xpath("//div[@class='item']//span[@class='runner']"));
             for (int i = 0; i < runnersList.size(); i++)
             {
                if (runnersList.get(i).getText().contains(TextControls.unnamedFavourite))
                 {

                    Assert.assertFalse(runnersList.get(i).getText().contains("Form"), "Form details is displayed for unnamed favourite");
                     favString = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamedFavourite + "')]"));
                    Assert.assertEquals(favString, TextControls.unnamedFavourite);
                     emptyNumber = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamedFavourite + "')]/preceding::span[@class='number'][1]"));
                     emptyRating = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamedFavourite + "')]/following::span[@class='rating'][1]"));
                     unnamedSP = "//span[contains(text(),'" + TextControls.unnamedFavourite + "')]/following::span[@class='rating'][1]/../span[@class='selections']";
                    Assert.assertEquals(emptyNumber, "");
                    Assert.assertEquals(emptyRating, "");
                    Assert.assertTrue(GetElementText(By.xpath(unnamedSP)).equals("SP"), "SP is not found");
                    //Logger.Pass("Verification for Unnamed Favourite was successful");
                 }

                
                 else if (runnersList.get(i).getText().contains(TextControls.unnamed2ndFavourite))
                 {
                     //Verify Unnamed 2nd Favourite, Form details should not be displayed and only SP should be shown
                    Assert.assertFalse(runnersList.get(i).getText().contains("Form"), "Form details is displayed for unnamed favourite");
                     fav2String = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamed2ndFavourite + "')]"));
                    Assert.assertEquals(fav2String, TextControls.unnamed2ndFavourite);
                     secondemptyNumber = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamed2ndFavourite + "')]/preceding::span[@class='number'][1]"));
                     secondemptyRating = GetElementText(By.xpath("//span[contains(text(),'" + TextControls.unnamed2ndFavourite + "')]/following::span[@class='rating'][1]"));
                     secondUnnamedSP = "//span[contains(text(),'" + TextControls.unnamed2ndFavourite + "')]/following::span[@class='rating'][1]/../span[@class='selections']";
                    Assert.assertEquals(secondemptyNumber, "");
                    Assert.assertEquals(secondemptyRating, "");
                    Assert.assertTrue(GetElementText(By.xpath(secondUnnamedSP)).equals("SP"), "SP is not found");
                    //Logger.Pass("Verification for Unnamed 2nd Favourite was successful");
                 }
             }
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-34 Verifying Racing post result from Horse racing event details page
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyRacingPostResultFromHorseRacingEventDetailsPage() throws Exception{
	String testCase ="VerifyRacingPostResultFromHorseRacingEventDetailsPage";
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
        String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
	        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	        scrollToView(Element);
	        click(By.xpath(hrEvent));
	        
	        String mainWindow = getDriver().getWindowHandle();
	      //Verify the Results tabs
            click(HorseRacingElements.resultsTab, "Result tab not found in RaceCard page");
            //Switch to Result Pop up
           common.switchwindow();
           String title = getDriver().getTitle();
           Assert.assertTrue(title.contains("racingpost.com"), "Failed to Switch to Results Window");
           Assert.assertTrue(isElementPresent(HorseRacingElements.raceDetails), "Race details are not found");
          getDriver().close();
            //Switch back to parent page
           getDriver().switchTo().window(mainWindow);
    }
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-35 Horse Racing RaceCard
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifySelectHRSingleEventAndRefreshRaceCard() throws Exception{
	String testCase ="VerifySelectHRSingleEventAndRefreshRaceCard";
	String raceNum, buildRacecardMsg;
    int j = 1, k = 4, raceCount, counter = 0;
	
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
	    
		List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
        click(By.xpath("//div[@class='icon-arrow expand']"));
        a :  for (int x = 0; x < venueList.size(); x++)
        {
            venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
            if (counter == 1)
                break;
            List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
            if (raceList.size() >= 4)
            {
                raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
                for (int i = 0; i < raceList.size(); i++)
                {
                    if (counter == 1)
                        break;
                    if (!raceList.get(i).isSelected())
                    {
                        raceList.get(i).click();
                       List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span//input"));
                       Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");
                       Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
                        raceNum = GetElementText(By.xpath("//a[@class='button action']/span")).toLowerCase();
                        raceCount = i + 1;
                       Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
                       Assert.assertTrue(isElementDisplayed(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
                       Assert.assertTrue(isElementDisplayed(HorseRacingElements.clearAllButton), "Clear all button is not enabled");
                        //Refreshing the browser
                        getDriver().navigate().refresh();
                       Thread.sleep(2000);
                       Assert.assertFalse(isElementPresent(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
                       Assert.assertFalse(isElementPresent(HorseRacingElements.clearAllButton), "Clear all button is not enabled");
                        buildRacecardMsg = GetElementText(By.xpath("//div[@class='racing-card-footer-content']/span"));
                       Assert.assertTrue(buildRacecardMsg.contains(TextControls.createRacecardMsg), "Use the check boxes above to create a custom racecard not displayed after refresh");
 
                       break a ;
                   }
                    
                }
            }
           k = k + 1;
        }

    System.out.println("build next 3 races button on Next races module verified successfully");
    }
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage() , e);
		}
	}

/** 
 * TC-36 Horse Racing RaceCard
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifySelectingBetInForecastTricastOnRacecardPage() throws Exception{
String testCase = "VerifySelectingBetInForecastTricastOnRacecardPage";

	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	String valueText;
	
try{
			launchweb();
		    Common.Login();
			Common.OddSwitcher("decimal");
			HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
			
			 click(HorseRacingElements.checkbox, "Checkbox 1 not found/clickable for Forecast market");
             Assert.assertFalse(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is found");
             click((HorseRacingElements.checkbox1), "Checkbox 2 not found/clickable for Forecast market");
            
            Assert.assertTrue(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
            Assert.assertTrue(valueText.contains("1 " + TextControls.forecastBetText), "Betting value is not found");
             scrollAndClick(HorseRacingElements.addToBetslipButton);
            //click((HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
            Assert.assertTrue(valueText.equals(""), "Betting value is found");
             //Verify Selection added to BetSlip
             String mktBetSlip = "//div[@class='market-information-selection']/../..//following-sibling::div[contains(text(), 'Straight Forecast')]";
            Assert.assertTrue(isElementPresent(By.xpath(mktBetSlip)), "Selection not added to Betslip");

             scrollPage("Top");
             ///Verify for TriCast market
             click((HorseRacingElements.checkbox2), "Checkbox 1 not found/clickable for Tricast market");
            click((HorseRacingElements.checkbox3), "Checkbox 2 not found/clickable for Tricast market");
             Assert.assertFalse(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is found");
             click((HorseRacingElements.checkbox4), "Checkbox 3 not found/clickable for Tricast market");
            Assert.assertTrue(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
            Assert.assertTrue(valueText.contains("1 " + TextControls.tricastBetText), "Betting value is not found");
            scrollAndClick(HorseRacingElements.addToBetslipButton);
            //click((HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
            Assert.assertTrue(valueText.equals(""), "Betting value is found");
             //Verify Selection added to BetSlip
             mktBetSlip = "//div[@class='market-information-selection']/../..//following-sibling::div[contains(text(), 'Tricast')]";
            Assert.assertTrue(isElementPresent(By.xpath(mktBetSlip)), "Selection not added to Betslip");
           System.out.println("Verification of type of bet selected was successfull");
			
		 }
			catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
				Assert.fail(e.getMessage() , e);
				}
		}

/** 
 * TC-37 Verifying TodayRacesModuleFavIndex
* @return none
 * @throws Exception 
 * @throws Exception element not found
 */

@Test
public void VerifyTodayRacesModuleFavIndex() throws Exception{
	String testCase ="VerifyTodayRacesModuleFavIndex";
	 int i = 1;
     String venueName, favIndexVenue, todayRaceModule;
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
		Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");


        todayRaceModule = GetElementText(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled']"));
        
        if (todayRaceModule.contains(TextControls.favIndexText))
        {
            List<WebElement> hrTodayRacesAllTimes = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']"));

            for (i = 1; i <= hrTodayRacesAllTimes.size(); i++)
            {
                if (isElementPresent(By.xpath("//div[@class='racing-scheduled-meeting'][" + i + "]//div[@class='racing-scheduled-meeting-right']//a[contains(text(), '" + TextControls.favIndexText + "')]")))
                {
                    venueName = GetElementText(By.xpath("//div[@class='racing-scheduled-meeting'][1]//div[@class='racing-scheduled-meeting-left']//*[@class='racing-scheduled-meeting-name']"));
                    click(By.xpath("//div[@class='racing-scheduled-meeting'][" + i + "]//div[@class='racing-scheduled-meeting-right']//a[contains(text(), '" + TextControls.favIndexText + "')]"), "Favourites Index link not found");
                    favIndexVenue = GetElementText(By.xpath("//div[@class='basic-scoreboard']//span[@class='name']"));
                    if (venueName.toLowerCase() == favIndexVenue.toLowerCase())
                      Assert.assertTrue((GetElementText(By.xpath("//div[@class='basic-scoreboard']//span[@class='league']")).equals(TextControls.favouritesIndexText)), "Not navigated to Fav Index page of meeting " + venueName + ".");
                    else
                     Assert.fail("Not navigated to Fav Index page of meeting " + venueName + ".");

                    break;
                }

            }
           System.out.println("OnClick of Fav Index in Today's Races module, it navigates Fav Index page of that meeting");
        }
        else
        	Assert.fail("Fav Index is not available for any meeting in Today's Races module");
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage() , e);
		}
}

/** 
 * TC-38 Verifying TodaysModuleHorseRaces
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyTodaysModuleHorseRaces() throws Exception{
	String testCase = "VerifyTodaysModuleHorseRaces";
	String venueName, venueNameList, raceTimeList;
    
    try
    {
        // VerifyTodaysModuleHorseRaces
        launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
       Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");

        //Check whether it contains today's races only
        List<WebElement> hrTodayRacesAllTimes = getDriver().findElements(HorseRacingElements.todayHRModuleRaceTimes);
        for (int i = 0; i < hrTodayRacesAllTimes.size(); i++)
        {
            raceTimeList = hrTodayRacesAllTimes.get(i).getText();
            if ((common.containsDigit(raceTimeList)) && (!common.isAlpha(raceTimeList)) && (raceTimeList.contains(":")))
            {
              System.out.println("HR contains today's race");
            }
            else
              Assert.fail("HR does not contain today's race");
            
        }
       //Logger.Pass("Horse Racing Today module contains only today's races");
 }
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
}

/** 
 * TC-39 Verifying TodaysModuleHorseRaces are sorted
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyTodaysModuleHorseRacesSorted() throws Exception{
	String testCase = "VerifyTodaysModuleHorseRaces";
	String venueName, venueNameList , hrTodayRacesTimes;
	 int j = 1, o = 1, p, count;
    try
    {
       
        launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);

        // Verify GH Todays Module Races Are Sorted in order
		 Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
        List<WebElement> hrTodayRacesAllTimes = getDriver().findElements(HorseRacingElements.todayracesList);
        count =  hrTodayRacesAllTimes.size();
        for (o = 1; o <= count; o++)
        {
        	hrTodayRacesTimes = "//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + o + "]//div[@class='racing-scheduled-meeting-race-times']//a";
            venueNameList = "//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + o + "]//*[@class='racing-scheduled-meeting-name']";
            venueName = GetElementText(By.xpath(venueNameList));
             List<WebElement> raceTimes =getDriver().findElements(By.xpath(hrTodayRacesTimes));
            String[] arrRaces = new String[raceTimes.size()];
            p = 0;
            for (WebElement ele : raceTimes)
            {
                arrRaces[p++] = ele.getText();
            }
            boolean result = common.StringIsSortedAsc(arrRaces);
            if (result == true)
               System.out.println("For venue = " + venueName + ", the sorting of the races is according to the start time ");

            else
             Assert.fail("For venue = " + venueName + ", the sorting of the races is not according to the start time ");
        }
 }
    catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
    	Assert.fail(e.getMessage() , e);
    	}
    }

/** 
 * TC-40 Verifying TodaysModuleRaceCardPage
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyTodaysModuleRaceCardPage() throws Exception{
	String testCase ="VerifyTodaysModuleRaceCardPage";
	String venueName, raceTime;
   
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
		 Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in HR Landing Page");
         Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled']")), "Venues and Race times are not displayed in Today's Races module in HR Landing Page");

          //Get a race time (event) and venue details
         Assert.assertTrue(isElementPresent(HorseRacingElements.todayHRace1stVenue), "Greyhounds Venue not found");
         venueName = GetElementText(HorseRacingElements.todayHRace1stVenue);
         Assert.assertTrue(isElementPresent(HorseRacingElements.todayHRacesTimeList), "Race time not found under Venue");
         raceTime =  GetElementText(HorseRacingElements.todayHRacesTimeList);
          click(HorseRacingElements.todayHRacesTimeList);
          
          HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.HorseRacing, venueName, "", raceTime);
          System.out.println("OnClick of race time in Today's module, race card page should be displayed");
    }
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
Assert.fail(e.getMessage() , e);
}
}

/** 
 * TC-40 Verifying TodaysModuleHorseRaces are sorted
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyTomorrowsModuleHorseRaces() throws Exception{
	String testCase ="VerifyTomorrowsModuleHorseRaces";
	String venueName, venueNameList, raceTimeList;
    int i = 0, j = 1;
   
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
		 
		 Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.tomorrowText + "')]")), "Tomorrow's Races not displayed in Horse Racing Landing Page");

         //Check whether it contains tomorrow races only
        List<WebElement> hrTodayRacesAllTimes = getDriver().findElements(By.xpath("//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times']"));
         for (i = 0; i < hrTodayRacesAllTimes.size(); i++)
         {
             raceTimeList = hrTodayRacesAllTimes.get(i).getText();
             venueNameList = "//span[contains(text(), '" + TextControls.tomorrowText + "')]/../..//div[@class='racing-scheduled-meeting'][" + j + "]//span[contains(@class, 'racing-scheduled-meeting')]";
             venueName = GetElementText(By.xpath(venueNameList));
             if ((common.containsDigit(raceTimeList)) && (!common.isAlpha(raceTimeList)) && (raceTimeList.contains(":")))
             {
                //Logger.Pass(venueName + " contains tomorrow race");
             }
             else
               Assert.fail(venueName + " does not contain today's race");
             j++;
         }
	 }
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
	}

/** 
 * TC-41  VerifyTricastClearFunctionality
 * @return none
 * @throws Exception element not found
 */

@Test
 public void VerifyTricastClearFunctionality() throws Exception {
	String testCase = "VerifyTricastClearFunctionality";
	
	String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
	String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
	String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
	
	String xPath, valueText;
    boolean statusBeforeClear, statusBeforeClear1, statusBeforeClear2, statusAfterClear, statusAfterClear1, statusAfterClear2;
    int count;
    try{
		 launchweb();
		 HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName , true);
		 
		//Verifying radio buttons are selected 
        click(HorseRacingElements.checkbox2);
         WebElement element = getDriver().findElement(HorseRacingElements.checkbox2);
         statusBeforeClear = element.isSelected();
         if (statusBeforeClear == true)
         {
            //Verifying Other radio buttons state(disabled state) and Clear button
             List<WebElement>tricastColList = getDriver().findElements(HorseRacingElements.tricastColRadioBtn);
             if (tricastColList.size() > 5)
                 count = 5;
             else
                 count = tricastColList.size();
             for (int j = 1; j <= count; j++)
             {
                 xPath = "//span[@class='tricast']//span//input[@class='forecast-radio' and @data-col='0' and @data-row='" + j + "']";
                Assert.assertTrue(getAttribute(By.xpath(xPath), "disabled").contains("true"), "Radio button is not disabledt");
             }
            Assert.assertTrue(isElementPresent(HorseRacingElements.clearButton), "Clear button is not found");

             click(HorseRacingElements.checkbox3);
             WebElement element1 = getDriver().findElement(HorseRacingElements.checkbox3);
             statusBeforeClear1 = element1.isSelected();
             List<WebElement>tricastCol1List = getDriver().findElements(HorseRacingElements.tricastColRadioBtn);
             if (tricastCol1List.size() > 5)
                 count = 5;
             else
                 count = tricastCol1List.size();
             for (int k = 2; k <= count; k++)
             {
                 xPath = "//span[@class='tricast']//span//input[@class='forecast-radio' and @data-col='0' and @data-row='" + k + "']";
                Assert.assertTrue(getAttribute(By.xpath(xPath), "disabled").contains("true"), "Radio button is not disabledt");
             }
            Assert.assertTrue(isElementPresent(HorseRacingElements.clearButton), "Clear button is not found");

             click(HorseRacingElements.checkbox4);
             WebElement element2 = getDriver().findElement(HorseRacingElements.checkbox4);
             statusBeforeClear2 = element2.isSelected();
             List<WebElement>tricastCol2List = getDriver().findElements(HorseRacingElements.tricastColRadioBtn);
             if (tricastCol2List.size() > 5)
                 count = 5;
             else
                 count = tricastCol2List.size();
             for (int l = 3; l <= count; l++)
             {
                 xPath = "//span[@class='tricast']//span//input[@class='forecast-radio' and @data-col='0' and @data-row='" + l + "']";
                Assert.assertTrue(getAttribute(By.xpath(xPath), "disabled").contains("true"), "Radio button is not disabledt");
             }

             //Verifying Betting value after selecting the Radio buttons 
            Assert.assertTrue(isElementPresent(HorseRacingElements.bettingValue), "Betting value is not visible");

             //Verifying betting value clicking on clear button
            scrollToView(HorseRacingElements.clearButton);
            click(HorseRacingElements.clearButton, "Clear button is not found");
             valueText = GetElementText(HorseRacingElements.bettingValue);
             if (valueText.equals(""))
             {
               // Logger.Pass("Value is cleared and not showing");
             }
             else
               Assert.fail("Value is not cleared");

             //Verifying radio buttons are not selected after Clear 
             statusAfterClear = element.isSelected();
             statusAfterClear1 = element1.isSelected();
             statusAfterClear2 = element2.isSelected();
             if (statusAfterClear == false && statusAfterClear1 == false && statusAfterClear2 == false)
             {
                Assert.assertFalse(isElementPresent(HorseRacingElements.clearButton), "Clear button is found");
              }
         }
    }
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	Assert.fail(e.getMessage() , e);
	}
	}

/** 
 * TC-42  Verifying RaceCard on Horse racing page
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyUnNamedFavouriteDisplayedAfterNamedSelections() throws Exception{
	String testCase ="VerifyUnNamedFavouriteDisplayedAfterNamedSelections";
	String UnnamedFav, SecondUnnamedFav;
	try{
		launchweb();
		 common.NavigateToSportsPage(TextControls.HorseRacing);
		 String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 2) + "')]/../..//div//*[contains(text(), '" + testdata.Readtestdata("PreProdEvents",10, 4) + "')]";
	        WebElement Element = getDriver().findElement(By.xpath(hrEvent));
	        scrollToView(Element);
	        click(By.xpath(hrEvent));
	        
	        Assert.assertTrue(isElementDisplayed(By.xpath("//span[@class='runner']")), "Selections not displayed in raceCard page");
            List<WebElement> selectionNames = getDriver().findElements(By.xpath("//span[@class='runner']"));
            UnnamedFav = selectionNames.get(selectionNames.size()-2).getText();
            SecondUnnamedFav = selectionNames.get(selectionNames.size()-1).getText();

            //Verifying Unnamed Favourites are displaying after named selections
            Assert.assertTrue(UnnamedFav.equalsIgnoreCase(TextControls.unnamedFavourite), "Unnamed Favourite not displayed after selection names");
            Assert.assertTrue(SecondUnnamedFav.equalsIgnoreCase(TextControls.unnamed2ndFavourite), "Unnamed 2nd Favourite not displayed after selection names");
     }
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage() , e);
		}
		}
/** 
 * TC-43  Verifying RaceCard on Horse racing page
 * @return none
 * @throws Exception 
 * @throws Exception element not found
 */
@Test
public void VerifyUrlForSingleAndMultipleMeetings() throws Exception{
	String testCase ="VerifyUrlForSingleAndMultipleMeetings";
	String baseURL = ReadTestSettingConfig.getTestsetting("BaseURL1_xpath");
	String meetingName, expectedUrl, actualUrl;
	 
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.HorseRacing);
        
       //build Race card and compare Expected Url and Actual Url
        //Get meeting name
		meetingName = getAttribute(HorseRacingElements.allCheckBox1, "id");
		//WebElement ele = HorseRacingElements.allCheckBox1
		scrollToView(HorseRacingElements.allCheckBox1);
          click(HorseRacingElements.allCheckBox1);
            expectedUrl = baseURL + TextControls.HRMeetingURL + meetingName;
            Assert.assertTrue(isElementDisplayed(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
            Assert.assertTrue(isElementDisplayed(HorseRacingElements.clearAllButton), "Clear all button is not enabled");
            click(HorseRacingElements.buildRacecardButton, "Build racecard button is not found");
        //get actual raceard
           actualUrl = getDriver().getCurrentUrl();
           Assert.assertTrue(actualUrl.contains(expectedUrl), "Url is not displaying as expected");
           
        
       common.NavigateToSportsPage(TextControls.HorseRacing);
        //add few events
        List<WebElement> alliconlist =getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times-all']//input"));
        scrollToView(alliconlist.get(1));
        alliconlist.get(1).click();
        Thread.sleep(2000);
        scrollToView(alliconlist.get(2));
        alliconlist.get(2).click();
        Thread.sleep(2000);
        click(HorseRacingElements.buildRacecardButton, "Build racecard button is not found");
        //expected Url
        expectedUrl = baseURL + TextControls.HRMeetingURL;
        //get actual raceCard
        actualUrl = getDriver().getCurrentUrl();
       Assert.assertTrue(actualUrl.contains(expectedUrl), "Url is not displaying as expect after building racecard with different events");
       System.out.println("Url is displaying as expected");
		
	 }
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() , e);
			}
	}

	/**
	 * C811153  Check for results flag on resulted races in Landing and left menu on race card
	 * @return none
	 * @throws Exception
	 * @throws Exception element not found
	 */
	@Test
	public void verifyResultFlags() throws Exception{
		String testCase ="verifyResultFlags";

		try{
			launchweb();
			Common.Login();
			common.NavigateToSportsPage(TextControls.HorseRacing);
			//User shall Check for results flag on resulted races in Landing and left menu on race card
			Assert.assertTrue(isElementDisplayed(HorseRacingElements.resultedRaceFlagOnLadningPage), "No result flags on Horse racing landing page");
			scrollToView(HorseRacingElements.resultedRaceFlagOnLadningPage);
			click(HorseRacingElements.resultedRaceFlagOnLadningPage);
			Assert.assertTrue(isElementDisplayed(HorseRacingElements.resultedRaceFlagOnLeftMenu), "No result flags on leftmenu racecard");

		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage() , e);
		}
	}
}



package com.ladbrokes.testsuite;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import com.ladbrokes.EnglishOR.GreyhoundRacingElements;
import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.HorseRacing_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;

import com.relevantcodes.extentreports.LogStatus;

public class GreyHoundsTests extends DriverCommon {
	Common common = new Common();
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	LoginLogoutFunctions LoginfunctionObj = new LoginLogoutFunctions();
	TestdataFunction testdata = new TestdataFunction();
	HorseRacing_Functions HRfunctionObj = new HorseRacing_Functions();

	/**
	 * TC-01  GreyHounds Combination TricastPlaceBet
	 * Validate Bet Information
	 *
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	public void VerifyGreyhoundsCombinationTricastPlaceBet() throws Exception {

		String testCase = "VerifyGreyhoundsCombinationTricastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		Boolean statusBeforeClear, statusBeforeClear1, statusBeforeClear2;
		String valueText, horseOne, horseTwo, horseThree, evntName, raceName, eventsInfo, betInformation, receiptInformation, marketName = "Combination Tricast";
		int betSlipCount, initBetSlipCount;

		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, true);

				// ** Verifying AddToBetslip is enabled after selecting radio button

				click(HorseRacingElements.checkboxTricastAny);
				click(HorseRacingElements.checkboxTricastAny1);
				click(HorseRacingElements.checkboxTricastAny2);
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");

				WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkboxTricastAny);
				WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkboxTricastAny1);
				WebElement checkbox3 = getDriver().findElement(HorseRacingElements.checkboxTricastAny2);

				statusBeforeClear = checkbox1.isSelected();
				statusBeforeClear1 = checkbox2.isSelected();
				statusBeforeClear2 = checkbox3.isSelected();

				// ** Verifying Betting value after selecting the selections in Forecast
				if (statusBeforeClear == statusBeforeClear1 == statusBeforeClear2 == true)
					Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue), "Betting value is not visible after selecting boxes");
				//Verifying selections clear after clicking on addToBetslip button
				horseOne = GetElementText(HorseRacingElements.greyhoundsHorse1);
				horseTwo = GetElementText(HorseRacingElements.greyhoundsHorse2);
				horseThree = GetElementText(HorseRacingElements.greyhoundsHorse3);
				raceName = GetElementText(HorseRacingElements.raceName);
				evntName = GetElementText(HorseRacingElements.eventName);
				eventsInfo = evntName + " " + raceName;

				WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
				scrollToView(element);
				click(HorseRacingElements.addToBetslipButton);

				valueText = GetElementText(HorseRacingElements.bettingValue);
				if (valueText.equals(""))
					System.out.println("Value is cleared and not showing");
				else
					Assert.fail("Value is not cleared after adding bet to betSlip");

				//**Getting count from BetSlip
				betSlipCount = common.GetBetSlipCount();
				if (betSlipCount == initBetSlipCount + 1) {
					betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
					Common.Enterstake(TextControls.stakeValue);
					BTbetslipObj.BetPlacement(true);
					//Verifying bet receipt
					receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}

	}

	/**
	 * TC-02  GreyhoundsForecastPlaceBet
	 * Validate Bet Information
	 *
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	public void VerifyGreyhoundsForecastPlaceBet() throws Exception {

		String testCase = "VerifyGreyhoundsForecastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		Boolean statusBeforeClear, statusBeforeClear1;
		String valueText, horseOne, horseTwo, evntName, raceName, eventsInfo, betInformation, receiptInformation, marketName = "Straight Forecast";
		int betSlipCount, initBetSlipCount;

		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, true);

				// ** Verifying AddToBetslip is enabled after selecting radio button
				click(HorseRacingElements.checkbox);
				click(HorseRacingElements.checkbox1);
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
				WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkbox);
				WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkbox1);

				statusBeforeClear = checkbox1.isSelected();
				statusBeforeClear1 = checkbox2.isSelected();
				// ** Verifying Betting value after selecting the selections in Forecast
				if (statusBeforeClear == statusBeforeClear1 == true)
					Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue), "Betting value is not visible after selecting boxes");

				//Verifying selections clear after clicking on addToBetslip button
				horseOne = GetElementText(HorseRacingElements.greyhoundsHorse1);
				horseTwo = GetElementText(HorseRacingElements.greyhoundsHorse2);
				raceName = GetElementText(HorseRacingElements.raceName);
				evntName = GetElementText(HorseRacingElements.eventName);
				eventsInfo = evntName + " " + raceName;

				WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
				scrollToView(element);


				click(HorseRacingElements.addToBetslipButton);

				valueText = GetElementText(HorseRacingElements.bettingValue);
				if (valueText.equals(""))
					System.out.println("Value is cleared and not showing");
				else
					Assert.fail("Value is not cleared after adding bet to betSlip");

				//**Getting count from BetSlip
				betSlipCount = common.GetBetSlipCount();
				if (betSlipCount == initBetSlipCount + 1) {
					betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
					Common.Enterstake(TextControls.stakeValue);
					BTbetslipObj.BetPlacement(true);
					//Verifying bet receipt
					receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			test.log(LogStatus.FAIL, "testCase");
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-03  GreyHounds ReverseForecastPlaceBet
	 * Validate Bet Information
	 *
	 * @return none
	 * @throws Exception
	 * @throws Exception element not found
	 */

	@Test
	public void VerifyGreyhoundsReverseForecastPlaceBet() throws Exception {

		String testCase = "VerifyGreyhoundsReverseForecastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		Boolean statusBeforeClear, statusBeforeClear1;
		String valueText, horseOne, horseTwo, evntName, raceName, eventsInfo, betInformation, receiptInformation, marketName = "Reverse Forecast";
		int betSlipCount, initBetSlipCount;

		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, true);

				// ** Verifying AddToBetslip is enabled after selecting radio button
				click(HorseRacingElements.checkboxAny);
				click(HorseRacingElements.checkboxAny1);
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");
				WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkboxAny);
				WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkboxAny1);
				statusBeforeClear = checkbox1.isSelected();
				statusBeforeClear1 = checkbox2.isSelected();

				// ** Verifying Betting value after selecting the selections in Forecast
				if (statusBeforeClear == statusBeforeClear1 == true)
					Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue), "Betting value is not visible after selecting boxes");

				//Verifying selections clear after clicking on addToBetslip button
				horseOne = GetElementText(HorseRacingElements.greyhoundsHorse1);
				horseTwo = GetElementText(HorseRacingElements.greyhoundsHorse2);
				raceName = GetElementText(HorseRacingElements.raceName);
				evntName = GetElementText(HorseRacingElements.eventName);
				eventsInfo = evntName + " " + raceName;

				WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
				scrollToView(element);
				click(HorseRacingElements.addToBetslipButton);

				valueText = GetElementText(HorseRacingElements.bettingValue);
				if (valueText.equals(""))
					System.out.println("Value is cleared and not showing");
				else
					Assert.fail("Value is not cleared after adding bet to betSlip");

				//**Getting count from BetSlip
				betSlipCount = common.GetBetSlipCount();
				if (betSlipCount == initBetSlipCount + 1) {
					betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
					Common.Enterstake(TextControls.stakeValue);
					BTbetslipObj.BetPlacement(true);
					//Verifying bet receipt
					receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-04  GreyHounds TriCast PlaceBet
	 * Validate Bet Information
	 *
	 * @return none
	 * @throws Exception
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyGreyhoundsTricastPlaceBet() throws Exception {

		String testCase = "VerifyGreyhoundsTricastPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		Boolean statusBeforeClear, statusBeforeClear1, statusBeforeClear2;
		String valueText, horseOne, horseTwo, horseThree, evntName, raceName, eventsInfo, betInformation, receiptInformation, marketName = "Tricast";
		int betSlipCount, initBetSlipCount;

		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, true);

				// ** Verifying AddToBetslip is enabled after selecting radio button
				click(HorseRacingElements.checkbox2);
				click(HorseRacingElements.checkbox3);
				click(HorseRacingElements.checkbox4);
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.addToBetslipButton), "addToBetslip Button is not enabled");

				WebElement checkbox1 = getDriver().findElement(HorseRacingElements.checkbox2);
				WebElement checkbox2 = getDriver().findElement(HorseRacingElements.checkbox3);
				WebElement checkbox3 = getDriver().findElement(HorseRacingElements.checkbox4);

				statusBeforeClear = checkbox1.isSelected();
				statusBeforeClear1 = checkbox2.isSelected();
				statusBeforeClear2 = checkbox3.isSelected();

				// ** Verifying Betting value after selecting the selections in Forecast
				if (statusBeforeClear == statusBeforeClear1 == statusBeforeClear2 == true)
					Assert.assertTrue(isElementDisplayed(HorseRacingElements.bettingValue), "Betting value is not visible after selecting boxes");

				//Verifying selections clear after clicking on addToBetslip button
				horseOne = GetElementText(HorseRacingElements.greyhoundsHorse1);
				horseTwo = GetElementText(HorseRacingElements.greyhoundsHorse2);
				horseThree = GetElementText(HorseRacingElements.greyhoundsHorse3);
				raceName = GetElementText(HorseRacingElements.raceName);
				evntName = GetElementText(HorseRacingElements.eventName);
				eventsInfo = evntName + " " + raceName;

				WebElement element = getDriver().findElement(HorseRacingElements.addToBetslipButton);
				scrollToView(element);
				click(HorseRacingElements.addToBetslipButton);

				valueText = GetElementText(HorseRacingElements.bettingValue);
				if (valueText.equals(""))
					System.out.println("Value is cleared and not showing");
				else
					Assert.fail("Value is not cleared after adding bet to betSlip");

				//**Getting count from BetSlip
				betSlipCount = common.GetBetSlipCount();
				if (betSlipCount == initBetSlipCount + 1) {
					betInformation = "//div[@class='market-information']//div//span[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
					Common.Enterstake(TextControls.stakeValue);
					BTbetslipObj.BetPlacement(true);
					//Verifying bet receipt
					receiptInformation = "//div[@class='receipts']//div[@class='receipt-event-type']//span[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text()[1], '" + horseOne + "') and contains(text()[2], '" + horseTwo + "') and contains(text()[3], '" + horseThree + "')]/following::div[@class='receipt-stake']/following::div[@class='receipt-total-stake']/following::div//span[@class='receipt-potential-return-value' and contains(text(), 'N/A')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-05 Check the content on GreyHounds landing page
	 *
	 * @return none
	 * @throws NoSuchElementException
	 * @throws Exception              element not found
	 */
	@Test
	public void VerifyGreyHoundsRacingLandingPage() throws Exception {

		String testCase = "VerifyGreyHoundsRacingLandingPage";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String actBreadcrumbs, expBreadcrumbs;
		int count;
		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				common.SelectLinksFromAZ(TextControls.Greyhounds);

				//*** Verifying GreyHoundsRacing Landing Page components
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.nextRaces), "Next Races not displayed in Horse Racing Landing Page");
				Assert.assertTrue(isElementDisplayed(HorseRacingElements.todayRaces), "Today's Races not displayed in Horse Racing Landing Page");
				//Assert.assertTrue(isElementDisplayed(GHConstants.futureGreyhounds), "Future Horse Races not displayed in Horse Racing Landing Page");
				System.out.println("GreyHounds Racing Landing Page components are displayed as expected");

				// **** Verifying BreadCrums
				HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.Greyhounds, "", "");
				System.out.println("The breadcrumb is shown on the top as Home > Greyhound Racing as expected");

				//** Verify Next Races Modules
				List<WebElement> nextRaces = getDriver().findElements(HorseRacingElements.nextRacesModules);
				count = nextRaces.size();
				if (count == 0)
					Assert.fail("No Races are Present");
				else
					System.out.println(count + " races are displayed in the Next Races module");
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}

	}

	/**
	 * TC-06  GreyHounds TrapWinner PlaceBet
	 * Validate Bet Information
	 *
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyGreyhoundsTrapWinnerPlaceBet() throws Exception {

		String testCase = "VerifyGreyhoundsTrapWinnerPlaceBet";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		int betSlipCount, initBetSlipCount;
		String Trap1, raceName, evntName, eventsInfo, betInformation, receiptInformation, marketName = TextControls.trapWinner, betType = TextControls.single;

		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher("decimal");
				initBetSlipCount = common.GetBetSlipCount();
				HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, false);
				click(HorseRacingElements.trapWinner);
				Trap1 = GetElementText(HorseRacingElements.trap1);
				raceName = GetElementText(HorseRacingElements.raceName);
				evntName = GetElementText(HorseRacingElements.eventName);
				eventsInfo = evntName + " " + raceName;
				click(HorseRacingElements.SP);
				betSlipCount = common.GetBetSlipCount();
				if (betSlipCount == initBetSlipCount + 1) {
					betInformation = "//div[@class='selection-information']//div[@class='market-information-selection']//span[contains(text(), '" + Trap1 + "')]/following::div[contains(text(), '" + marketName + "')]/following::div[contains(text(), '" + eventsInfo + "')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(betInformation)), "Bet information is not found");
					Common.Enterstake(TextControls.stakeValue);
					BTbetslipObj.BetPlacement(true);

					receiptInformation = "//div[@class='receipt']//div[@class='receipt-event-type']//span[contains(text(), '" + betType + "')]/following::div[contains(text(), '" + eventsInfo + "')]/following::div[contains(text(), '" + Trap1 + "')]/following::div//span[contains(text(), '" + marketName + "')]/following::div/following::div/following::div/following::div/following::div[contains(text(), 'N/A')]";
					Assert.assertTrue(isElementDisplayed(By.xpath(receiptInformation)), "Receipt information is not found");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-07  Build a RaceCard - Custom and Next 3 races
	 *
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	public void VerifyGHEventBuildRaceCard() throws Exception {

		String testCase = "VerifyGHEventBuildRaceCard";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				common.SelectLinksFromAZ(TextControls.Greyhounds);
				HRfunctionObj.VerifyRacingEventBuildRaceCard_function(TextControls.Greyhounds);
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-08  Navigation through Contextual menu on racing event detail pages
	 * Contextual Menu For Today Event
	 *
	 * @return none
	 * @throws Exception element not found
	 */

	@Test
	public void VerifyEventNavContextualMenuForTodayEvent_GH() throws Exception {

		String testCase = "VerifyEventNavContextualMenuForTodayEvent_GH";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String meetingTime, eventInfo;
		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				common.NavigateToSportsPage(className);
				String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
				WebElement Element = getDriver().findElement(By.xpath(hrEvent));
				scrollToView(Element);
				click(By.xpath(hrEvent));
				String venueName = "//h2[@class='meetings expanded']/span[@class='title' and contains(text(),'" + typeName + "')]";
				Assert.assertTrue(isElementDisplayed(By.xpath(venueName)), " Venue is not dispalyed in collapsed mode");
				List<WebElement> meetingsLink = getDriver().findElements(By.xpath("//div[@class='meetings-container']/a[@class='meetings-link']"));
				for (int i = 1; i < meetingsLink.size(); i++) {
					meetingsLink = getDriver().findElements(By.xpath("//div[@class='meetings-container']/a[@class='meetings-link']"));
					meetingTime = meetingsLink.get(i).getText();
					meetingsLink.get(i).click();
					eventInfo = meetingTime + " " + typeName;
					HGfunctionObj.VerifyBreadCrums(TextControls.Home, className, eventInfo, "");
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);

			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-09  Contextual Menu for Next Races Racing
	 *
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyEventNavContextualMenuForNextRacesEvent_GH() throws Exception {
		String testCase = "VerifyEventNavContextualMenuForNextRacesEvent_GH";
		String eventXpath;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));
			if (getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),'Next 3 Races')]"), "class").contains("expanded"))
				click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),'Next 3 Races')]"));

			eventXpath = "//div[@class='wrapper sidebar']//div[@class='next-races-group']/a[1]/span[@class='time']";
			click(By.xpath(eventXpath));
			List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2"));
			for (int j = 1; j < venueList.size(); j++) {
				String xPath = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[@class='meetings']";
				Assert.assertTrue(isElementDisplayed(By.xpath(xPath)), "'" + venueList.get(j) + "' Venue is not dispalyed in collapsed mode");
			}

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-10  FutureGreyhoundsEventDetailsPage
	 *
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyFutureGreyhoundsEventDetailsPage() throws Exception {
		String testCase = "VerifyFutureGreyhoundsEventDetailsPage";
		String eventName;
		WebElement element = null;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementDisplayed(HorseRacingElements.futureGreyhounds), "Future Greyhounds not displayed in Greyhounds Landing Page");

			if (!isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]")))
				click(By.xpath("//section[@class='footer-top']"), "Future Racing not found");
			else if (isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]"))) {
				element = getDriver().findElement(By.xpath("//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]"));
				scrollToView(element);
				click(By.xpath("//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]"), "Future Racing not found");
			}

			eventName = GetElementText(HorseRacingElements.futureEvent);
			element = getDriver().findElement(HorseRacingElements.futureGhEvent);
			scrollToView(element);
			click(HorseRacingElements.futureGhEvent);
			Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='basic-scoreboard']//span[@class='name' and contains(text(), '" + eventName + "')]")), "Navigation to Future Greyhounds event failed");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-11  GH_RaceCard Bar
	 *
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyGH_SelectDeselectTodayTomorrowFutureRaceEvents() throws Exception {
		String testCase = "VerifyGH_SelectDeselectTodayTomorrowFutureRaceEvents";
		String check = "check";
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			//Selecting today's race event
			List<WebElement> todayEventCheckbox = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][1]//input"));
			todayEventCheckbox.get(1).click();
			WebElement checkBox = todayEventCheckbox.get(1);
			if (check == "check") {
				Assert.assertTrue(checkBox.isSelected(), "Today's Race is not selected");
				System.out.println("Clicked Today's Checkbox");
			} else
				Assert.assertTrue(!checkBox.isSelected(), "Today's Race is selected");
			Thread.sleep(2000);

			//Deselecting today's race event
			todayEventCheckbox.get(1).click();

			WebElement checkBox1 = todayEventCheckbox.get(1);
			if (check == "uncheck") {
				Assert.assertTrue(checkBox1.isSelected(), "Today's Race is not selected");
				System.out.println("Unchecked Today's Checkbox");
			} else
				Assert.assertTrue(!checkBox1.isSelected(), "Today's Race is selected");
			click(By.xpath("//span[starts-with(@data-bind, 'html') and contains(text(), '" + TextControls.todayText + "')]//preceding-sibling::div[@class='icon-arrow expand']"));
			Thread.sleep(2000);

			//Selecting and deselecting tomorrow's race events
			if (isElementPresent(HorseRacingElements.tomorrowsGreyhounds)) {
				String tomoEvent = "//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input";

				//Selecting tomorrow's race event

				click(By.xpath("//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input"), "Tommorow's Race event is not found");
				Thread.sleep(200);

				checkBox = getDriver().findElement(By.xpath(tomoEvent));
				if (check == "check") {
					Assert.assertTrue(checkBox.isSelected(), "Tomorrow's Race is not selected");
					System.out.println("Clicked Tomorrow's Checkbox");
				} else
					Assert.assertTrue(!checkBox.isSelected(), "Tomorrow's Race is selected");
				Thread.sleep(200);

				//Deselecting tomorrow's race event

				click(By.xpath("//div[@class='module'][2]//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'][1]//input"), "Tommorow's Race event is not found");
				checkBox = getDriver().findElement(By.xpath(tomoEvent));

				if (check == "uncheck") {
					Assert.assertTrue(checkBox.isSelected(), "Tomorrow's Race is not unselected");
					System.out.println("Unchecked Tomorrow's Checkbox");
				} else
					Assert.assertTrue(!checkBox.isSelected(), "Tomorrow's Race is selected");

				click(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.tomorrowText + "')]"));
				Thread.sleep(200);
			} else
				System.out.println("Tomorrow's Race Tab is not present");

			//Selecting and deselecting Future's race events

			String futureEvent = "//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']/div[1]//input";
			//Selecting Future's race event
			click(By.xpath("//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']/div[1]//input"), "Future Racing Event not found");
			checkBox = getDriver().findElement(By.xpath(futureEvent));
			if (check == "check") {
				Assert.assertTrue(checkBox.isSelected(), "Future Race is not selected");
				System.out.println("Clicked Future Checkbox");
			} else
				Assert.assertTrue(!checkBox.isSelected(), "Future Race is selected");
			Thread.sleep(200);
			//Deselecting Future's race event
			click(By.xpath("//div[@class='module']//span[contains(text(), '" + TextControls.futureText + "')]/../..//div[@class='racing-upcoming']/div[1]//input"), "Future Racing Event not found");
			checkBox = getDriver().findElement(By.xpath(futureEvent));
			if (check == "uncheck") {
				Assert.assertTrue(checkBox.isSelected(), "Future Race is not selected");
				System.out.println("Unchecked Future's Checkbox");
			} else
				Assert.assertTrue(!checkBox.isSelected(), "Future Race is selected");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-12  TodayTomorrowFutureRaceCard
	 *
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyGH_TodayTomorrowFutureRaceCard() throws Exception {
		String testCase = "VerifyGH_TodayTomorrowFutureRaceCard";
		String actBreadcrumbs, expBreadcrumbs;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);

			//Select the races form Today, Tomorrow and Future Horse Racing
			HRfunctionObj.selectDeslectRaces("check");
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is not displayed");
			Assert.assertFalse(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard displayed by unchecking the races");
			scrollPage("Top");

			//De-select the races form Today, Tomorrow and Future Horse Racing
			HRfunctionObj.selectDeslectRaces("uncheck");
			Assert.assertFalse(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is displayed");
			Assert.assertTrue(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard not displayed by unchecking the races");
			scrollPage("Top");  // Edge adjustment

			//Select the races form Today, Tomorrow and Future Horse Racing
			HRfunctionObj.selectDeslectRaces("check");
			Assert.assertFalse(isElementPresent(By.xpath("//div[@class='racing-card-footer-content']/span")), "Use the check boxes above to create a custom racecard displayed by unchecking the races");
			Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']")), "Build Race card button is not displayed");
			click(By.xpath("//div[contains(@class, 'racing-card-footer')]//a[@class='button action']"), "Failed to click on Build race card button");
			Thread.sleep(3000);
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
			actBreadcrumbs = GetElementText(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"));
			actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
			expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.Greyhounds + ">" + TextControls.buildRaceCard + "";
			Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGHMarketTabsForSelectedRacesOnRaceCardPage_GH() throws Exception {
		String testCase = "VerifyGHMarketTabsForSelectedRacesOnRaceCardPage_GH";
		String actBreadcrumbs, expBreadcrumbs, raceNum;
		boolean iStatus = false;
		int j = 1, k = 1, raceCount;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
			for (int x = 0; x < venueList.size(); x++) {
				venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));

				List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']"));
				String[] buildRacesList = HRfunctionObj.add_list_items_to_Array(raceList);
				if (raceList.size() >= 3) {
					raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));

					for (int i = 0; i <= 2; i++) {
						if (!raceList.get(i).isSelected()) {
							// scrollToView(raceList.get(i));
							//Thread.sleep(3000);
							raceList.get(i).click();

							List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//div[ @data-t-position='racing-today-module']//span[text()='Greyhounds Today']/following::span[@class='racing-scheduled-meeting-race-time']//input"));
							Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");

							Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
							raceNum = getDriver().findElement(By.xpath("//a[@class='button action']/span")).getText().toLowerCase();
							raceCount = i + 1;
							Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
							j = j + 1;
						}
					}

					click(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.nextRacesText + "')]"), "Next Races is not clicked to close on Greyhounds");
					click(By.xpath("//div//span[starts-with(@data-bind, '') and contains(text(), '" + TextControls.todayText + "')]"), "Next Races is not clicked to close on Greyhounds");
					Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']")).isDisplayed(), "Build race card button is not enabled");
					click(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']"), "Failed to clcik on Build race card button");
					Thread.sleep(3000);
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
					actBreadcrumbs = (getDriver().findElement(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"))).getText();
					actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
					expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.Greyhounds + ">" + TextControls.buildRaceCard + "";
					Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
					for (int i = 0; i <= 2; i++) {
						//click( By.xpath("//div[@class='wrap-time-name']//span[@class='time' and contains(text(), '" + buildRacesList[i] + "')]"), buildRacesList[i] + " event not found in BUild Race Card Page");
						Thread.sleep(1000);
						Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='wrap-time-name']//span[@class='time' and contains(text(), '" + buildRacesList[i] + "')]")).isDisplayed(), buildRacesList[i] + "event name and time not found");
						Thread.sleep(3000);
						Assert.assertTrue(getDriver().findElement(By.xpath("//ul[@class='market-selector__items'] | //nav[@class='tabs']")).isDisplayed(), buildRacesList[i] + "Market tab not found");
						Assert.assertTrue((getDriver().findElement(By.xpath("//div[@class='wrap-time-name']//span[@class='time' and contains(text(), '" + buildRacesList[i] + "')]/../../..//nav")).isDisplayed()), "Market tab not found");

						iStatus = true;
					}
				}
				if (iStatus == true)
					break;
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGHSelectedRacesListedInTimeOrderOnRaceCardPage() throws Exception {
		String testCase = "VerifyGHSelectedRacesListedInTimeOrderOnRaceCardPage";
		String actBreadcrumbs, expBreadcrumbs, raceNum;
		int j = 1, k = 2, raceCount;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
			for (int x = 0; x < venueList.size(); x++) {
				venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));

				List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']"));
				String[] buildRacesList = HRfunctionObj.add_list_items_to_Array(raceList);
				if (raceList.size() >= 3) {
					raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));

					for (int i = 0; i <= 4; i++) {

						if (!raceList.get(i).isSelected()) {
							raceList.get(i).click();
							List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span//input"));
							Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");
							Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
							raceNum = getDriver().findElement(By.xpath("//a[@class='button action']/span")).getText().toLowerCase();
							raceCount = i + 1;
							Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
							j = j + 1;
						}
					}
					Assert.assertTrue(getDriver().findElement(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']")).isDisplayed(), "Build race card button is not enabled");
					click(By.xpath("//div[@class='racing-card-footer-content']/a[@class='button action']"), "Failed to clcik on Build race card button");
					Thread.sleep(3000);
					Assert.assertTrue(isElementDisplayed(HomeGlobalElements.breadcrumbs), "Hierarchy (breadcrumbs) was not found");
					actBreadcrumbs = (getDriver().findElement(By.xpath("//nav[contains(@class,'breadcrumbs')]//ul"))).getText();
					actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
					expBreadcrumbs = expBreadcrumbs = "" + TextControls.Home + ">" + TextControls.Greyhounds + ">" + TextControls.buildRaceCard + "";
					Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs), "Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '" + expBreadcrumbs + "'");
					Thread.sleep(200);

					// Check selected races displayed in time order
					List<WebElement> sortListAsc = getDriver().findElements(By.xpath("//div[@class='racing-page']//div[@class='header-navigation race-card']//span[@class='time']"));

					//Check selected race count and number races displayed on raceCard page
					List<String> ascList = new ArrayList<>();
					for (WebElement ele : sortListAsc) {
						if (!TextUtils.isEmpty(ele.getText())) {
							System.out.println("--->>>" + ele.getText());
							ascList.add(ele.getText());
						}
					}
					//check whether its displayed in ascending order
					boolean ascendingOrder = common.isSorted(ascList);
					Assert.assertTrue(ascendingOrder == true, "Selected races not displayed in time order");

				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGreyhoundsNextRacesMoreLink() throws Exception {
		String testCase = "VerifyGreyhoundsNextRacesMoreLink";
		String eventName, typeName;

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(HorseRacingElements.nextRaces), "Next Races not displayed in Greyhounds Landing Page");

			Assert.assertTrue(isElementPresent(HorseRacingElements.nextRacesModules), "Events not present under Next Races module");
			List<WebElement> nextRaces = getDriver().findElements(HorseRacingElements.nextRacesModules);

			for (int i = 1; i <= nextRaces.size(); i++) {

				if (getDriver().findElement(By.xpath("//div[@class='next-race'][" + i + "]//header[@class='next-race-header']//a[2]")).getText().toLowerCase().contains(TextControls.moreText.toLowerCase())) {
					List<WebElement> nextRaceEvents = getDriver().findElements(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//div[@class='next-race-participant']"));
					Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-time']")), "Event name not found");
					eventName = GetElementText(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-time']"));
					Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-title']")), "Venue name not found");
					typeName = GetElementText(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//span[@class='next-race-header-title']")).toLowerCase();
					typeName = toTitleCase(typeName);

					click(By.xpath("//div[@class='next-races']//div[@class='next-race'][" + i + "]//a[@class='next-race-header-more']"));

					HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.Greyhounds, typeName, "", eventName);
					// Logger.Pass("Next Races Event Details Page displayed successfully");

					List<WebElement> runnersList = getDriver().findElements(By.xpath("//div[@class='race-container']//div[@class='item']"));
					if (runnersList.size() > nextRaceEvents.size()) {
						System.out.println("More link should is displayed as expected in Next Race module");
						break;
					} else
						Assert.fail("More Link is displayed inapprapriately");

				} else
					Assert.fail("More link is not displayed in the Next Races module");
			}

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGreyhoundsTodaysModuleRaceCardPage() throws Exception {
		String testCase = "VerifyGreyhoundsTodaysModuleRaceCardPage";
		String venueName, raceTime;

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Greyhounds Landing Page");
			Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled']")), "Venues and Race times are not displayed in Today's Races module in Greyhounds Landing Page");

			//Get a race time (event) and venue details
			Assert.assertTrue(isElementPresent(HorseRacingElements.todayHRace1stVenue), "Greyhounds Venue not found");
			venueName = GetElementText(HorseRacingElements.todayHRace1stVenue);
			Assert.assertTrue(isElementPresent(HorseRacingElements.todayHRacesTimeList), "Race time not found under Venue");
			raceTime = GetElementText(HorseRacingElements.todayHRacesTimeList);
			click(HorseRacingElements.todayHRacesTimeList);

			HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.Greyhounds, venueName, "", raceTime);
			System.out.println("OnClick of race time in Today's module, race card page should be displayed");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGreyhoundsTodaysModuleRaces() throws Exception {
		String testCase = "VerifyGreyhoundsTodaysModuleRaces";
		String venueName, venueNameList, raceTimeList, ghTodayRacesTimes;
		int i = 0, j = 1, o = 1, p, count;

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(HorseRacingElements.todayRaces), "Today's Races not displayed in Greyhounds Landing Page");

			//Check whether it contains today's races only
			List<WebElement> ghTodayRacesAllTimes = getDriver().findElements(HorseRacingElements.todayracesList);
			for (i = 0; i < ghTodayRacesAllTimes.size(); i++) {
				raceTimeList = ghTodayRacesAllTimes.get(i).getText();
				venueNameList = "//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + j + "]//*[@class='racing-scheduled-meeting-name']";
				venueName = GetElementText(By.xpath(venueNameList));
				if ((common.containsDigit(raceTimeList)) && (!common.isAlpha(raceTimeList)) && (raceTimeList.contains(":"))) {
					System.out.println(venueName + " contains today's race");
				} else
					Assert.fail(venueName + " does not contain today's race");
				j++;
			}
			System.out.println("Greyhounds Today module contains only today's races");


			// Verify GH Todays Module Races Are Sorted in order
			count = ghTodayRacesAllTimes.size();
			for (o = 1; o <= count; o++) {
				ghTodayRacesTimes = "//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + o + "]//div[@class='racing-scheduled-meeting-race-times']//a";
				venueNameList = "//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + o + "]//*[@class='racing-scheduled-meeting-name']";
				venueName = GetElementText(By.xpath(venueNameList));
				List<WebElement> raceTimes = getDriver().findElements(By.xpath(ghTodayRacesTimes));
				String[] arrRaces = new String[raceTimes.size()];
				p = 0;
				for (WebElement ele : raceTimes) {
					arrRaces[p++] = ele.getText();
				}
				boolean result = common.StringIsSortedAsc(arrRaces);
				if (result == true)
					System.out.println("For venue = " + venueName + ", the sorting of the races is according to the start time ");

				else
					Assert.fail("For venue = " + venueName + ", the sorting of the races is not according to the start time ");
			}

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyGreyhoundsTodaysModuleResultsForm() throws Exception {
		String testCase = "VerifyGreyhoundsTodaysModuleResultsForm";
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(HorseRacingElements.hrTodaysModuleResult), "Results tab in Today's Races not displayed on Greyhounds Landing Page");
			Assert.assertTrue(isElementPresent(HorseRacingElements.hrTodaysModuleForm), "Form tab in Today's Races not displayed on Greyhounds Landing Page");
			System.out.println("Results and Form are displayed in Today's Module on Greyhounds Page");

			// Verify GreyhoundsTodays Module ResultTab , "Results tab should take the user to the popped out Results page" .

			WebElement element = getDriver().findElement(HorseRacingElements.hrTodaysModuleResult);
			scrollToView(element);
			click(HorseRacingElements.hrTodaysModuleResult, "Results tab not found in Today's Races module on Greyhounds Landing Page");
			Thread.sleep(3000);
			common.SwitchToPage("Ladbrokes | Greyhounds | Fast Results");
			System.out.println("Results tab is taking the user to the popped out Results page");


			// Verify HorseRacing Todays Module Form Tab, "Form tab should take the user to the popped out Results page"

			click(HorseRacingElements.hrTodaysModuleForm, "Form tab not found in Today's Races module on Greyhounds Landing Page");
			Thread.sleep(3000);
			common.SwitchToPage("Ladbrokes | Greyhounds");
			System.out.println("Form tab is taking the user to the popped out Results page");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}


	@Test
	public void VerifyMarketTabsOnGreyhoundsPage() throws Exception {
		String testCase = "VerifyMarketTabsOnGreyhoundsPage";

		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
			WebElement Element = getDriver().findElement(By.xpath(hrEvent));
			scrollToView(Element);
			click(By.xpath(hrEvent));

			// Verify the market tabs to be shown on the Greyhounds racecard on Page Load , market tabs should be shown on the race card on Page Load .
			List<WebElement> marketTabs = getDriver().findElements(By.xpath("//nav[@class='tabs']//ul//li"));

			for (int i = 0; i < marketTabs.size(); i++) {
				marketTabs = getDriver().findElements(By.xpath("//nav[@class='tabs']//ul//li"));
				if (i == 0) {
					Assert.assertTrue(getAttribute(By.xpath("//nav[@class='tabs']/following::div[@class='market-selector__item'][" + (i + 1) + "]"), "data-bind").contains("active"), "1st market tab not active by default");
				} else {
					marketTabs.get(i).click();
					Assert.assertTrue(isElementDisplayed(By.xpath("//nav[@class='tabs']/following::div[@class='market-selector__item']")), "" + marketTabs.get(i).getText() + " Market tab not active");
				}
				List<WebElement> selectionList = getDriver().findElements(HorseRacingElements.marketSellections);
				int count = selectionList.size();
				Assert.assertTrue(count > 0, "selection List is not found under market");
			}
			System.out.println("The market tabs to be shown on the Greyhounds racecard on Page Load was successful");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}


	@Test
	public void VerifyMeetingVenueTabAndTimeInContextualMenuInGreyhounds() throws Exception {
		String testCase = "VerifyMeetingVenueTabAndTimeInContextualMenuInGreyhounds";

		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String venuNameXPath, venuTimeXPath;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
			WebElement Element = getDriver().findElement(By.xpath(hrEvent));
			scrollToView(Element);
			click(By.xpath(hrEvent));

			venuNameXPath = "//div[contains(@class, 'meetings-group')]//h2[contains(@class, 'meetings') and contains(@data-bind, 'toggle')]//span[@class='title' and contains(text(), '" + typeName + "')]";
			Assert.assertTrue(isElementPresent(By.xpath(venuNameXPath)), "Required venu name not found in Contextual Menu");

			venuTimeXPath = "//div[contains(@class, 'meetings-group')]//div[contains(@class, 'meetings-container')]//a[@class ='meetings-link selected' and contains(text(), '" + eventName + "')]";
			Assert.assertTrue(isElementPresent(By.xpath(venuTimeXPath)), "Required race time not found in Contextual Menu");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyMeetingVenueTabInContextualMenuInGreyhounds() throws Exception {
		String testCase = "VerifyMeetingVenueTabInContextualMenuInGreyhounds";
		String raceTime1, raceTime2, raceTime3, raceName1, raceName2, raceName3, nextRaces, raceName;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);

			raceTime1 = GetElementText(HorseRacingElements.raceTime1);
			raceName = GetElementText(HorseRacingElements.raceName1).toLowerCase();
			if (raceName.contains("("))
				raceName = raceName.substring(0, raceName.indexOf('('));
			raceName1 = toTitleCase(raceName);

			raceTime2 = GetElementText(HorseRacingElements.raceTime2);
			raceName = GetElementText(HorseRacingElements.raceName2).toLowerCase();
			if (raceName.contains("("))
				raceName = raceName.substring(0, raceName.indexOf('('));
			raceName2 = toTitleCase(raceName);

			raceTime3 = GetElementText(HorseRacingElements.raceTime3);
			raceName = GetElementText(HorseRacingElements.raceName3).toLowerCase();
			if (raceName.contains("("))
				raceName = raceName.substring(0, raceName.indexOf('('));
			raceName3 = toTitleCase(raceName);

			click(HorseRacingElements.nextRaceEvent, "Next Races event not found");
			Thread.sleep(1000);
			// Verifying Next 03 Races is displaying on the contextual menu
			nextRaces = "(//div[@class='wrapper sidebar']//header[contains(@class, 'next-races-header')]//span[contains(text(),   '" + TextControls.nextThreeRaces + "')]/following::span[@class='time' and contains(text(), '" + raceTime1 + "')]/following::span[@class='name' and contains(text(), '" + raceName1 + "')][1]/following::span[@class='time' and contains(text(), '" + raceTime2 + "')]/following::span[@class='name' and contains(text(), '" + raceName2 + "')]/following::span[@class='time' and contains(text(), '" + raceTime3 + "')]/following::span[@class='name' and contains(text(), '" + raceName3 + "')])[1]";
			Assert.assertTrue(isElementDisplayed(By.xpath(nextRaces)), "Next Races are not found");

			click(By.xpath("//div[@class='next-races-group']//a[@class='next-races-link'][1]"), "Next Races event not found");
			String eventPath = "//div[@id='content']//div[@class='header-navigation race-card']//div[@class='wrap-time-name']//span[contains(text(), '" + raceTime1 + "')]/following::span[contains(text(), '" + raceName1 + "')][1]";
			Assert.assertTrue(isElementPresent(By.xpath(eventPath)), "Navigation to Next races event - " + raceTime1 + " " + raceName1 + " failed");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_GH() throws Exception {
		String testCase = "VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_GH";

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			click(By.xpath("//div[@class='next-races']//div[@class='next-race'][1]//a[1]"));


			//Select the option to collapse today's racing on contextual men
			if (!(getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "class").contains("expanded")))
				click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "Today's Meeting is not present on Racecard page");

			//Select the option to expand today's racing on contextual menu
			if (!(getAttribute(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "class").contains("expanded")))
				click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),   '" + TextControls.todayText + "')]"), "Today's Meeting is not present on Racecard page");

			//Verifying the expand/collapse and events under expanded Venue
			String test = "//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][1]//span[@class='title']";
			String venueName1 = GetElementText(By.xpath(test));

			if (!isElementPresent(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings expanded')][1]//span[@class='title' and contains(text(), '" + venueName1 + "')]")))
				click(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(),'" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[contains(@class, 'meetings')][1]//span[@class='title' and contains(text(), '" + venueName1 + "')]"));

			Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='wrapper sidebar']//header[contains(@class, 'meeting-header')]//span[contains(text(), '" + TextControls.todayText + "')]/../following-sibling::div[@class='meetings-group']//h2[1]/following-sibling::div[@class='meetings-container'][1]/a")), "Venue tab is not expanded and events are not displayed");
			System.out.println("Option to Collapse and  Expand Todays Racing On Contextual menu is successfull");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}


	@Test
	public void VerifyResultsLinkOnGreyhoundsEventDetailsPage() throws Exception {
		String testCase = "VerifyResultsLinkOnGreyhoundsEventDetailsPage";

		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
			WebElement Element = getDriver().findElement(By.xpath(hrEvent));
			scrollToView(Element);
			click(By.xpath(hrEvent));
			String mainWindow = getDriver().getWindowHandle();
			// Verify Results link on Greyhounds event details page , New pop up window should be displayed today's meeting and today's tab should be highlighted
			Thread.sleep(1000);
			//Verify the Results tabs
			click(HorseRacingElements.resultsTab, "Result tab not found on Race Card Page");
			Thread.sleep(2000);
			//Switch to Result Popup
			common.switchwindow();
			click(By.xpath("//a[@href='/greyhounds/results']"));
			Assert.assertTrue(getAttribute(HorseRacingElements.todaytab, "class").contains("on"), "today tab is not highlighted by default");
			Assert.assertTrue(isElementPresent(HorseRacingElements.todayMeetings), "Today meetings are not found");
			//Switch back to sports page
			getDriver().close();
			getDriver().switchTo().window(mainWindow);

			click(HorseRacingElements.formTab, "Form tab not found on Race Card Page");
			Thread.sleep(2000);
			//Switch to Form Popup
			common.switchwindow();
			getDriver().close();
			getDriver().switchTo().window(mainWindow);
			Thread.sleep(1000);
			click(HorseRacingElements.commentaryTab);
			Thread.sleep(2000);
			//Switch to Commentary Pop up
			common.SwitchToPage("Commentaries");


		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifySelectGHSingleEventAndRefreshRaceCard() throws Exception {
		String testCase = "VerifySelectGHSingleEventAndRefreshRaceCard";
		String raceNum, buildRacecardMsg;
		int j = 1, k = 4, raceCount, counter = 0;

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);

			List<WebElement> venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
			click(By.xpath("//div[@class='icon-arrow expand']"));
			a:
			for (int x = 0; x < venueList.size(); x++) {
				venueList = getDriver().findElements(By.xpath("//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times']"));
				//venueList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-right']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
				if (counter == 1)
					break;
				List<WebElement> raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
				if (raceList.size() >= 4) {
					raceList = getDriver().findElements(By.xpath("(//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times'])[" + k + "]//span[@class='racing-scheduled-meeting-race-time']//input"));
					for (int i = 0; i < raceList.size(); i++) {
						if (counter == 1)
							break;
						if (!raceList.get(i).isSelected()) {
							raceList.get(i).click();

							List<WebElement> raceChkbox = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span//input"));
							Assert.assertTrue(isChecked(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting'][" + k + "]//div[@class='racing-scheduled-meeting-race-times']//span[@class='racing-scheduled-meeting-race-time'][" + j + "]//input")), "Check box is not checked");
							Assert.assertTrue(isElementPresent(By.xpath("//a[@class='button action']")), "Build race card button is not enabled");
							raceNum = GetElementText(By.xpath("//a[@class='button action']/span")).toLowerCase();
							raceCount = i + 1;
							Assert.assertTrue(raceNum.contains("" + raceCount + " "), "Mismatch in number of races displayed on racecard button");
							Assert.assertTrue(isElementDisplayed(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
							Assert.assertTrue(isElementDisplayed(HorseRacingElements.clearAllButton), "Clear all button is not enabled");
							//Refreshing the browser
							getDriver().navigate().refresh();
							Thread.sleep(2000);
							Assert.assertFalse(isElementPresent(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
							Assert.assertFalse(isElementPresent(HorseRacingElements.clearAllButton), "Clear all button is not enabled");
							buildRacecardMsg = GetElementText(By.xpath("//div[@class='racing-card-footer-content']/span"));
							Assert.assertTrue(buildRacecardMsg.contains(TextControls.createRacecardMsg), "Use the check boxes above to create a custom racecard not displayed after refresh");

							break a;
						}

					}
				}
				k = k + 1;
			}

			System.out.println("build next 3 races button on Next races module verified successfully");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifySpecialGreyhoundsEventDetailsPage() throws Exception {
		String testCase = "VerifySelectGHSingleEventAndRefreshRaceCard";

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);
			Assert.assertTrue(isElementPresent(By.xpath("//div[@class='module']//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]")), "Greyhound Specials module not found in Greyhounds Landing Page");

			//eventName = GetElementText(By.xpath("(//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]/../..//div[@class='racing-upcoming']//a)[1]//span[@class='racing-upcoming-meeting-name']"));
			List<WebElement> eventList = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.greyhoundsSpcls + "')]/following::div[@class='racing-upcoming']//a"));
			int index = eventList.size() - 3;
			scrollToView(eventList.get(index));
			eventList.get(index).click();
			//Check whether navigated to the event details page
			Assert.assertTrue(isElementPresent(By.xpath("//header[@class='specials expanded']/following::a[@class='sidebar-racing-link selected']")), "Navigation to Greyhounds Specials event failed");
			Assert.assertTrue(isElementPresent(BetslipElements.EDPHeader), "Greyhounds Specials event header not found");
			//logger.log(LogStatus.PASS, "Greyhound Specials Event Details Page displayed successfully");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyTrapWinnerMarketOnGreyhoundsPage() throws Exception {
		String testCase = "VerifyTrapWinnerMarketOnGreyhoundsPage";
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher("decimal");
			common.NavigateToSportsPage(className);
			String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName + "')]/../..//div//*[contains(text(), '" + eventName + "')]";
			WebElement Element = getDriver().findElement(By.xpath(hrEvent));
			scrollToView(Element);
			click(By.xpath(hrEvent));

			//Verify RaceCard is displaying only Unnamed Favourite not Unnamed 2nd Favourite
			List<WebElement> nameListEle = getDriver().findElements(By.xpath("//span[@class='horse']//span[@class='name']"));

			List<String> nameList = new ArrayList<>();
			for (WebElement name : nameListEle) {
				nameList.add(name.getText());
			}

			Assert.assertTrue(nameList.contains(TextControls.unnamedFavourite), "Unnamed Favourite not displayed");
			Assert.assertTrue(!nameList.contains(TextControls.unnamed2ndFavourite), "Unnamed 2nd Favourite displayed");
			System.out.println("Racecard is displaying only Unnamed Favourite not Unnamed 2nd Favourite");

			// Verify Trap winner number is displaying in ascending order on page load
			click(HorseRacingElements.otherMarketTab);
			click(HorseRacingElements.trapWinner, "Trap winner market not found");
			click(HorseRacingElements.trapWinnerNoBtn, "Trap winner No. button is not found");

			//Verifying Selection names are displaying as Trap name and not the actual runner's name.
			List<WebElement> trapNameList = getDriver().findElements(By.xpath("//span[@class='horse']//span[@class='name']"));

			for (WebElement item : trapNameList) {
				Assert.assertTrue(item.getText().contains("Trap"), "Selection names are not displaying Trap name");
				String result = item.getText().split(" ")[1];
				//int IsNumeric = Integer.parseInt(result);
				boolean IsNumeric = common.isNumeric(result);
				Assert.assertTrue(IsNumeric == true, "Numeric is not found");
			}


			//Sorting by Odds
			List<WebElement> oddsList = getDriver().findElements(By.xpath("//span[@class='selections']//span[@class='price']"));
			List<String> list = new ArrayList<>();
			for (WebElement element : oddsList)
				list.add(element.getText());
			//if Odds has both SP and live price
			if ((list.contains("SP") && (list.contains(".")) || list.contains("/")) || (list.contains(".")) || list.contains("/")) {
				click(HorseRacingElements.trapWinnerNoBtn, "trap Winner No button is not found");
				//Verify RaceCard by sorting Race Card number
				List<WebElement> sortList = getDriver().findElements(By.xpath("//span[@class='number']"));
				List<Double> ascList = new ArrayList<>();
				for (WebElement ele : sortList) {
					if (!TextUtils.isEmpty(ele.getText()))
						ascList.add(Double.parseDouble(ele.getText()));
				}
				//check whether Trap winner numbers are displayed in descending order or not
				boolean ascendingOrder = common.AscendingOrder(ascList);
				Assert.assertTrue(ascendingOrder == true, "Trap Numbers not sorted in descending order");
				click(HorseRacingElements.trapWinnerNoBtn, "trap Winner Number button is not found");


				List<WebElement> descSortList = getDriver().findElements(By.xpath("//span[@class='number']"));
				List<Double> desclist = new ArrayList<>();
				for (WebElement ele : descSortList) {
					if (!TextUtils.isEmpty(ele.getText()))
						desclist.add(Double.parseDouble(ele.getText()));
				}
				boolean descendingOrder = common.descendingOrder(desclist);
				Assert.assertTrue(descendingOrder == true, "Trap Numbers not sorted in ascending order");
				System.out.println("Racecard sorting by Racecard number was successful");

			}
			//if odds contains only SP
			else if ((list.contains("SP")) && !(list.contains(".")) || list.contains("/")) {
				//check whether Trap winner number list is in ascending order on page load
				List<WebElement> descSortList1 = getDriver().findElements(By.xpath("//span[@class='number']"));
				List<Double> desclist1 = new ArrayList<>();
				for (WebElement ele : descSortList1) {
					if (!TextUtils.isEmpty(ele.getText()))
						desclist1.add(Double.parseDouble(ele.getText()));
				}
				boolean descendingOrder1 = common.descendingOrder(desclist1);
				Assert.assertTrue(descendingOrder1 == true, "Trap Numbers not sorted in ascending order");

				click(HorseRacingElements.trapWinnerNoBtn, "trap Winner Number button is not found");
				List<WebElement> sortList1 = getDriver().findElements(By.xpath("//span[@class='number']"));
				List<Double> ascList1 = new ArrayList<>();
				for (WebElement ele : sortList1) {
					if (!TextUtils.isEmpty(ele.getText()))
						ascList1.add(Double.parseDouble(ele.getText()));
				}
				//check whether Trap winner numbers are displayed in descending order or not
				boolean ascendingOrder1 = common.AscendingOrder(ascList1);
				Assert.assertTrue(ascendingOrder1 == true, "Trap Numbers not sorted in descending order");
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}


	@Test
	public void VerifyTypeOFBetSelectedtInForecastTricastInGH() throws Exception {
		String testCase = "VerifyTypeOFBetSelectedtInForecastTricastInGH";

		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String valueText;

		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher("decimal");
			HRfunctionObj.NavigateToRacingEventWith_FcTcMarket(className, typeName, eventName, true);

			click(HorseRacingElements.checkbox, "Checkbox 1 not found/clickable for Forecast market");
			click((HorseRacingElements.checkbox1), "Checkbox 2 not found/clickable for Forecast market");

			Assert.assertTrue(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
			valueText = GetElementText(HorseRacingElements.bettingValue);
			Assert.assertTrue(valueText.contains("1 " + TextControls.forecastBetText), "Betting value is not found");
			scrollToView(HorseRacingElements.addToBetslipButton);
			click(HorseRacingElements.addToBetslipButton);
			valueText = GetElementText(HorseRacingElements.bettingValue);
			Assert.assertTrue(valueText.equals(""), "Betting value is found");
			//Verify Selection added to BetSlip
			String mktBetSlip = "//div[@class='market-information-selection']/../..//following-sibling::div[contains(text(), 'Straight Forecast')]";
			Assert.assertTrue(isElementPresent(By.xpath(mktBetSlip)), "Selection not added to Betslip");

			scrollPage("Top");
			///Verify for TriCast market
			click((HorseRacingElements.checkbox2), "Checkbox 1 not found/clickable for Tricast market");
			click((HorseRacingElements.checkbox3), "Checkbox 2 not found/clickable for Tricast market");
			click((HorseRacingElements.checkbox4), "Checkbox 3 not found/clickable for Tricast market");
			Assert.assertTrue(isElementPresent(HorseRacingElements.addToBetslipButton), "AddToBetslip button is not found");
			valueText = GetElementText(HorseRacingElements.bettingValue);
			Assert.assertTrue(valueText.contains("1 " + TextControls.tricastBetText), "Betting value is not found");
			scrollToView(HorseRacingElements.addToBetslipButton);
			click(HorseRacingElements.addToBetslipButton);
			valueText = GetElementText(HorseRacingElements.bettingValue);
			Assert.assertTrue(valueText.equals(""), "Betting value is found");
			//Verify Selection added to BetSlip
			mktBetSlip = "//div[@class='market-information-selection']/../..//following-sibling::div[contains(text(), 'Tricast')]";
			Assert.assertTrue(isElementPresent(By.xpath(mktBetSlip)), "Selection not added to Betslip");
			System.out.println("Verification of type of bet selected was successfull");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}


	@Test
	public void VerifyUrlForGHSingleAndMultipleMeetings() throws Exception {
		String testCase = "VerifyUrlForGHSingleAndMultipleMeetings";
		String baseURL = ReadTestSettingConfig.getTestsetting("BaseURL1_xpath");
		String meetingName, expectedUrl, actualUrl;

		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Greyhounds);

			click(By.xpath("//div[@class='icon-arrow expand']"));
			//Get meeting name
			meetingName = getAttribute(HorseRacingElements.allCheckBox1, "id");
			expectedUrl = baseURL + TextControls.ghMeetingURL + meetingName;
			//build Race card and compare Expected Url and Actual Url
			scrollToView(HorseRacingElements.allCheckBox1);
			click(HorseRacingElements.allCheckBox1, "All check box is not found");
			Assert.assertTrue(isElementDisplayed(HorseRacingElements.buildRacecardButton), "Build race card button is not enabled");
			Assert.assertTrue(isElementDisplayed(HorseRacingElements.clearAllButton), "Claer all button is not enabled");
			click(HorseRacingElements.buildRacecardButton, "Build racecard button is not found");
			//get actual racecard
			actualUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(actualUrl.contains(expectedUrl), "Url is not displaying as expected");
			common.NavigateToSportsPage(TextControls.Greyhounds);
			//add few events
			List<WebElement> alliconlist = getDriver().findElements(By.xpath("//span[contains(text(), '" + TextControls.todayText + "')]/../..//div[@class='racing-scheduled-meeting']//div[@class='racing-scheduled-meeting-race-times-all']//input"));
			scrollToView(alliconlist.get(1));
			alliconlist.get(1).click();
			Thread.sleep(2000);
			scrollToView(alliconlist.get(2));
			alliconlist.get(2).click();
			Thread.sleep(2000);
			click(HorseRacingElements.buildRacecardButton, "Build racecard button is not found");
			//expected Url
			expectedUrl = baseURL + TextControls.ghMeetingURL;
			//get actual raceCard
			actualUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(actualUrl.contains(expectedUrl), "Url is not displaying as expect after building racecard with different events");
			System.out.println("Url is displaying as expected");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC - 811162 - Verify Check For Results Flag On Resulted Races In Landing And Left Menu On Racecard
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyCheckForResultsFlagOnResultedRacesInLandingAndLeftMenuOnRacecard() throws Exception {
		String testCase = "VerifyCheckForResultsFlagOnResultedRacesInLandingAndLeftMenuOnRacecard";

		try {
			launchweb();
			Common.Login();
			common.NavigateToSportsPage(TextControls.Greyhounds);

			//User shall Check for results flag on resulted races in Landing and left menu on race card
			Assert.assertTrue(isElementDisplayed(GreyhoundRacingElements.GHresultedRaceFlagOnLadningPage), "No result flags on Greyhound racing landing page");
			scrollToView(GreyhoundRacingElements.GHresultedRaceFlagOnLadningPage);
			click(GreyhoundRacingElements.GHresultedRaceFlagOnLadningPage);
			Assert.assertTrue(isElementDisplayed(GreyhoundRacingElements.GHresultedRaceFlagOnLeftMenu), "No result flags on left menu racecard");

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
}



package com.ladbrokes.testsuite;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.EnglishOR.*;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.Convert;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;

public class FootballTests extends DriverCommon{

	Common common= new Common();
	FootballElements FootballElements = new FootballElements();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions(); 
	TestdataFunction TD = new TestdataFunction();

	
	
	/** 
	 * TC-001  A quick check on the content on FootBal landing page
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
		
	@Test
	public void VerifyFootballLandingPage() throws Exception{
		
		String testCase = "VerifyFootballLandingPage";
		 
		try{
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			HGfunctionObj.VerifyBreadCrums(TextControls.Home,TextControls.Football , "" , "");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.upcomingTab), "UpComing is not Present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.timeFilter), "Time filter is not Present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.competitionFilter), "Competitions filter is not Present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.navigationTabs), "Type tabs are not Present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.resultsIcon), " Results Icon is not Present");
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.audioIcon), "Audio are not Present");
			 
			
	}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			 
			 
			Assert.fail(e.getMessage(), e);
		}

		}
	
	/** 
	 * TC-2 Football landing page - Login from Header,add selection to Bet slip and Place bet
	 * @return none
	 * @throws Exception | AssertionError  element not found
	 */
	@Test	
    public void VerifyFootballLandingPagePlaceBet() throws Exception{
		
		String testCase = "VerifyFootballLandingPagePlaceBet";
		
		try
	{
		    launchweb();
		    //logger.log(LogStatus.INFO, "Application Launched Successfuly");
		    Common.Login(); 
		    Common.OddSwitcher(TextControls.decimal);
			common.NavigateToSportsPage(TextControls.Football);
			//** Get the Bet slip Count
			 int initBetslipCount = common.GetBetSlipCount();
			click(HomeGlobalElements.SportLandingPageOdd);
			if(isElementPresent(BetslipElements.oddsBoostButtonIcon))
				  click(BetslipElements.oddsBoostTooltipIcon);
			int laterBetslipCnt = common. GetBetSlipCount();
			Assert.assertTrue((initBetslipCount+1 == laterBetslipCnt),"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:" + laterBetslipCnt + ".");
			HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.Football,"" , "" );
            common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
		    //logger.log(LogStatus.INFO,"BetPlacement from Class page(Football) successfull");
		     
		    
	}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			 
			 
			Assert.fail(e.getMessage(), e);
		}


}
	
@Test 
public void VerifyMarketSwitcherInCoupons() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	try
    {
		launchweb();
		
		common.Login();
		common.OddSwitcher(TextControls.decimal);
        click(FootballElements.couponLink);
        Assert.assertTrue(isElementPresent(FootballElements.marketDropDown), "Market dropdown switcher is not present in coupons page");
        Select select = new Select(getDriver().findElement(FootballElements.marketDropDown));
        select.selectByValue("WDW");
       Assert.assertTrue(isElementPresent(FootballElements.wdwRightColumn), "WDW market is not selected in switcher");

    }
	catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
  	
  	Assert.fail(e.getMessage() , e);
  	}
	}

@Test 
public void VerifyMoreOptionOnEventType() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	  try
      {
          String moreTypeName;
          int count;
          launchweb();
          common.NavigateToEventSubType(TextControls.Football, false, true);
          
          click(FootballElements.moreOption);              
          List <WebElement> typeNameInMore = getDriver().findElements(FootballElements.moreOnOutrightwithType);
          if (typeNameInMore.size() > 8)
              count = 8;
          else
              count = typeNameInMore.size();

          for (int j = 1; j < count; j++)
          {
        	 
              typeNameInMore = getDriver().findElements(FootballElements.moreOnOutrightwithType);
              String xyz = typeNameInMore.get(j).getText();
              if (xyz.contains("DAILY BET BUNDLES"))
              {
                  j++;
              }
              else
              {
                  (typeNameInMore.get(j)).click();
                  if (isElementPresent(By.xpath("//div[@class='scoreboard-container']")))
                  {
                      //GoBack();
                      xyz = getDriver().findElement(FootballElements.moreOption).getText();
                  }
                 
                      moreTypeName = getDriver().findElement(FootballElements.moreOption).getText();
                 
                  if (moreTypeName.contains(xyz))
                  {
                      System.out.println(" more tab is displayed  ");
                     // Logger.Pass(TextControls.moreText + " tab is displayed with Type Name '" + xyz + "'");
                  }
                  else
                	  System.out.println(" more tab is not displayed  ");
                      //Logger. Fail(TextControls.moreText + " tab is not displayed with Type Name '" + xyz + "'");

                  //click on More
                  click(FootballElements.moreOption);
                  
              }
          }

      }
      catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
  	
  	Assert.fail(e.getMessage() , e);
  	}
	}

@Test 
public void VerifyMoreTabOnFootbalPage() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	String moreAfterSelecting,breadcrums,typexpath;
	String moreTypeListCnt;
    int count,subtypecount;
    try
    {
        moreAfterSelecting = "//li[@class='tab tabs-dropdown dropdown active']//div[@class='title-container']";

        launchweb();              
        common.NavigateToSportsPage(TextControls.Football); 
       Assert.assertTrue(isElementPresent(FootballElements.more), "More tab not found");

       click(FootballElements.moreOption);

        List <WebElement> moreTypeList = getDriver().findElements(FootballElements.MoreTypes);
        if (moreTypeList.size() > 8)
            count = 8;
        else
            count = moreTypeList.size();
        
        //Verify Events count for each sports under MORE
        for (int j = 0; j < count; j++)
        {
        	moreTypeList = getDriver().findElements(FootballElements.MoreTypes);
            String moreTabName = moreTypeList.get(j).getText();
            
            if (moreTabName.contains("DAILY BET BUNDLES"))
            {
                break;                        
            }
            else
            {
            	if (moreTabName.contains("("))
                {
                    /*moreTypeListCnt = ValNeeded[ValNeeded.length - 1];
                    moreTypeListCnt = moreTypeListCnt.substring(0, moreTypeListCnt.length() - 1);
                    int val = (int) Convert.ToDouble(moreTypeListCnt);*/
                	moreTypeListCnt = Common.ExtractNumberFromString(moreTabName , "(" , ")");
                    int moreTypeListCn = Integer.parseInt(moreTypeListCnt);
                    int val = moreTypeListCn;
                    
                    (moreTypeList.get(j)).click();

                    /*if (isElementPresent(By.xpath("//div[@class='basic-scoreboard']")))
                    {
                        breadcrums = getDriver().findElement(By.xpath("//ul[@class='breadcrumbs-list']")).getText();
                        if (breadcrums.contains("In-Play"))
                        {
                            typexpath = "(//a[@data-bind='html: title, href: href'])[3]";
                        }
                        else
                            typexpath = "(//a[@data-bind='html: title, href: href'])[2]";
                        click(By.xpath(typexpath));
                        val = 0;
                        subtypecount = 0;
                        moreAfterSelecting = "//nav[@class='tabs']//li[@class='tab tabs-dropdown dropdown']";

                    }
                    else
                    {*/
                        List <WebElement> moreSubTypes = getDriver().findElements(FootballElements.moreSubTypes);
                          subtypecount = moreSubTypes.size();
                          moreAfterSelecting = "//li[@class='tab tabs-dropdown dropdown active']//div[@class='title-container']";
                          Assert.assertTrue(val == subtypecount, "Mismatch in events count");
                }
            }
            click(By.xpath(moreAfterSelecting));
            Thread.sleep(2000);
        }
       
    }
    catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	Assert.fail(e.getMessage() , e);
	}
	}

@Test 
public void VerifyShowMoreEvents() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	try{
	launchweb();
	common.NavigateToEventSubType(TextControls.Football, false, true);
    if (!isElementPresent(HomeGlobalElements.upcomingTab))
    {
        isElementPresent(By.xpath("//span[contains(text(), 'Inplay & Anstehend')]"));
        System.out.println("\nUpcoming tab not present");
    }
    else
    	Assert.assertTrue(isElementPresent(HomeGlobalElements.upcomingTab), "Upcoming Tab is not present");

	
    if(isElementPresent(FootballElements.showMoreEvents))
        {
    		Assert.assertTrue(isElementPresent(FootballElements.showMoreEvents), "Show More link/tab not found");
        }
        else
        {
            click(By.xpath("//div[@class='left-column']//div[@class='outright']//nav[@class='tabs']//ul//li[2]"));
            Assert.assertTrue(isElementPresent(FootballElements.showMoreEvents), "Show More link/tab not found");
        }
        
        System.out.println("Show Less");
        List <WebElement> intEventList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
        scrollToView(FootballElements.showMoreEvents);
        click(FootballElements.showMoreEvents);
        List <WebElement> eventList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
        Assert.assertTrue((eventList.size() > intEventList.size()), "Remaining events not displayed on click of Show More tab");
       
        Assert.assertTrue(isElementPresent(FootballElements.showLessEvents), "Show Less link/tab not found");
        scrollToView(FootballElements.showLessEvents);
        click(FootballElements.showLessEvents);
        List <WebElement> showLessList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
        Assert.assertTrue((showLessList.size() == intEventList.size()), "Few not displayed on click of Show Less tab");
	}
	catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
		Assert.fail(e.getMessage() , e);
		}
	}

@Test 
public void VerifyOutrightMarketsOnFBSubtypePage() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	String showMoreTab, showLessTab, showMoreOnTabComp, xPath, marketName, marketNameTxt;
	try{
		launchweb();
		common.NavigateToSportsPage(TextControls.Football);
		String Type =  "//li[@class='tab']//span[text()="+"\"" + TD.Readtestdata("PreProdEvents", 1, 2)+ "\"" + "]";
		String subType = "//div[@class='links']//a[contains(text(),'" + TD.Readtestdata("PreProdEvents", 1, 3) + "')]";
		click(By.xpath(Type));
		click(By.xpath(subType));

		String tabXpath = "//header[@class='event-header Football']/..//nav[@class='tabs']";
       Assert.assertTrue(isElementPresent(By.xpath(tabXpath)), "Tabbed component(Outright Market) is not present");
        List <WebElement> tabList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='event']//tabs//nav//ul/li"));
        for (int i = 0; i < tabList.size(); i++)
        {
            String tabName = tabList.get(i).getText();
            if (tabName.contains(TextControls.moreText))
            {
                tabList.get(i).click();

                List <WebElement> marketListOfMore = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='event']//tabs//nav//ul/li//div[@class='more']/div"));
                for (int j = 0; j < marketListOfMore.size(); j++)
                {
                    if (j == 2)
                        break;
                    //marketListOfMore = driver.FindElements(By.XPath(xPath));
                    marketName = marketListOfMore.get(j).getText();
                    marketNameTxt = toTitleCase(marketName);
                    if (marketNameTxt.contains("/"))
                    {
                        break;
                    }
                    else
                    {
                        marketListOfMore.get(j).click();
                        tabList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='event']//tabs//nav//ul/li"));
                        String marketNameinMore = tabList.get(i).getText().toLowerCase();
                        if (marketNameinMore.contains(marketName.toLowerCase()))
                        {
                            showMoreTab = "//span[contains(text(), 'Show more')]";
                            showLessTab = "//span[contains(text(), 'Show less')]";
                            showMoreOnTabComp = getDriver().findElement(By.xpath("//div[@class='event'][1]")).getText();

                            if (showMoreOnTabComp.contains("SHOW MORE"))
                            {
                                List <WebElement> intEventList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
                                click(By.xpath(showMoreTab));
                                List <WebElement> eventList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
                               Assert.assertTrue((eventList.size() > intEventList.size()), "Remaining events not displayed on click of Show More tab");
                              
                               Assert.assertTrue(isElementPresent(By.xpath(showLessTab)), "Show Less link/tab not found");
                                click(By.xpath(showLessTab));
                                List <WebElement> showLessList = getDriver().findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
                              Assert.assertTrue((showLessList.size() == intEventList.size()), "Few not displayed on click of Show Less tab");
                              
                                //ScrollPage("Top");
                                tabList.get(i).click();
                            }
                        }
                       
                    }
                }

            }
        }
      
	}
	catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	Assert.fail(e.getMessage() , e);
	}
}


@Test
public void VerifyETHighlighted() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	try{
	launchweb();
	
	//Verify Event Types in title bar When Event Class page is loaded
	common.NavigateToEventSubType(TextControls.Basketball, true, false);
	common.VerifyEventTypesInTitleBar();
	System.out.println("basketball");
	
	common.NavigateToEventSubType("Baseball", true, false);
	common.VerifyEventTypesInTitleBar();
	
	common.NavigateToEventSubType(TextControls.Football, true, false);
	common.VerifyEventTypesInTitleBar();
	
	//Verify ET is Highlighted based on the corresponding EST
	
	String typeTab = GetElementText(By.xpath("//li[@class='tab active']"));//Active type tab name 
    Assert.assertTrue(typeTab.toLowerCase().contains(getDriver().findElement(By.xpath("//span[@data-bind='html: title']")).getText().toLowerCase()),"Event type  is highlighted by default on Main List");
   
	}
catch( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	
	Assert.fail(e.getMessage() , e);
	}
}


@Test
public void VerifyEventDetailPageNavigationForClassType() throws Exception{
	String testCase = "VerifyEventDetailPageNavigationForClassType";
	try{
	launchweb();
	common.NavigateToSportsPage(TextControls.Football);
	HGfunctionObj.VerifyEventDetailPageNavigation(TextControls.Football);
	
	common.NavigateToSportsPage(TextControls.Basketball);
	HGfunctionObj.VerifyEventDetailPageNavigation(TextControls.Basketball);
	
	common.NavigateToSportsPage("Cricket");
	HGfunctionObj.VerifyEventDetailPageNavigation("Cricket");
   
	}
catch( Exception | AssertionError  e){
	String screenShotPath = getScreenshot(testCase); 
    Assert.fail(e.getMessage() , e);
	}
}

@Test
public void VerifyEventTypePageDetails() throws Exception{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	 try
     {
         launchweb();
         common.Removeall_Function();
         common.NavigateToTypes(TextControls.Football);
         common.NavigateToTypes(TextControls.Handball);  
         common.NavigateToTypes(TextControls.Basketball); 

     }
	 catch( Exception | AssertionError  e){
			String screenShotPath = getScreenshot(testCase); 
		    Assert.fail(e.getMessage() , e);
			}
}

@Test
public void VerifyInplayEventTitleOnFootballPageRHS() throws Exception
{
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();

//    int[] rowNumber = new int[2] { 0, 11 };
    String[] sheetName = new String[1];
    sheetName[0] = "PreProdEvents";
    int j = 1;

    TestdataFunction[] testDataLst = new TestdataFunction[2];
    testDataLst[0] = new TestdataFunction();
    testDataLst[1] = new TestdataFunction();
    try
    {
    	launchweb();
        common.Removeall_Function();
        
            for (int x = 0; x <= 1; x++)
            {
                if (x == 1)
                    break;
//                common.NavigateToSportsPage(testDataLst[x].ClassName);

                if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
                    click(HomeGlobalElements.inPlayTab);
                List <WebElement> inPlaySportList = getDriver().findElements(By.xpath("//div[@data-bind='with: featuredInPlay']//event-group-simple//h2"));
                for (int i = 0; i < inPlaySportList.size(); i++)
                {
                    if (i == 2)
                        break;
                    inPlaySportList = getDriver().findElements(By.xpath("//div[@data-bind='with: featuredInPlay']//event-group-simple//h2"));
                    String tabName = inPlaySportList.get(i).getText();
                    if (tabName.contains("Tennis"))
                    {
                        j++;
                    }
                    else
                    {
                        int inplayEventCnt = (int) Convert.ToDouble(common.ExtractNumberFromString(tabName, "(", ")"));
                        if (isElementPresent(By.xpath("//div[@data-bind='with: featuredInPlay']//event-group-simple//h2[@class='expand-list expanded']")))
                        {
                            List <WebElement> inplayEvents = getDriver().findElements(By.xpath("//div[@data-bind='with: featuredInPlay']/event-group-simple[" + j + "]/div/event-list"));
                           Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");

                            click(By.xpath("/html/body/div[2]/div/aside[3]/div[3]/div/div/event-group-simple[1]/div/event-list[1]/div/div[1]/div[1]"));

                            back();
                            j++;
                        }
                        else
                        {
                            inPlaySportList.get(i).click();
                            List<WebElement> inplayEvents = getDriver().findElements(By.xpath("//div[@data-bind='with: featuredInPlay']/event-group-simple[" + j + "]/div/event-list"));
                           Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
                            click(By.xpath("//div[@data-bind='with: featuredInPlay']/event-group-simple[" + j + "]/div/event-list[1]//div[@class='name']"));
                            back();
                            j++;
                        }
                    }
                   System.out.println("Inplay Event Title On RHS of Football Page is displayed Successfully");
                }
            }
    }
    catch( Exception | AssertionError  e){
		String screenShotPath = getScreenshot(testCase); 
	    Assert.fail(e.getMessage() , e);
		}
}

@Test
public void VerifyNavigationToDiffEventSubTypes() throws Exception {
	String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
	String temp, pageTitle, breadcrums, subtpexpath = null;
    try
    {
    	launchweb();
        common.NavigateToEventSubType(TextControls.Football,false,true);                

        List <WebElement> subTypeNameList = getDriver().findElements(By.xpath("//div[@class='links']//a"));
        for (int i = 0; i < subTypeNameList.size(); i++)
        {
            subTypeNameList = getDriver().findElements(By.xpath("//div[@class='links']//a"));
            temp = subTypeNameList.get(i).getText();
            //pageTitle = removeCharAt(temp,temp.length() - 2);
            pageTitle = temp.substring(0, temp.length() - 2);
            subTypeNameList.get(i).click();
            if (isElementPresent(By.xpath("//div[@class='basic-scoreboard']")))
            {
                breadcrums = getDriver().findElement(By.xpath("//ul[@class='breadcrumbs-list']")).getText();
                if (breadcrums.contains("In-Play"))
                subtpexpath = "(//a[@data-bind='html: title, href: href'])[4]";
                else
                subtpexpath = "(//a[@data-bind='html: title, href: href'])[3]";
                
                click(By.xpath(subtpexpath));
            }
            HGfunctionObj.VerifyBreadCrums(TextControls.Home, TextControls.Football, "English", pageTitle);                    
           
        }
    }
    catch( Exception | AssertionError  e){
		String screenShotPath = getScreenshot(testCase); 
	    Assert.fail(e.getMessage() , e);
		}
}


}


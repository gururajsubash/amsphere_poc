package com.ladbrokes.testsuite;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.EnglishOR.SegmentedBannerElements;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;

public class SegmentedBannersTests extends DriverCommon{
	HomeAndGlobalTests HGtests = new HomeAndGlobalTests();
	
	
	@Test
	public void VerifyPromotionsForNewUser() throws Exception{
     String testCase = "VerifyPromotionsForNewUser";
     try{
			HGtests.VerifyRegistration_UKCustomer();
			if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
		   if(isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp , "Odds Boost Pop Up On Home Page is Not Clickable");
			Assert.assertTrue(isElementDisplayed(SegmentedBannerElements.promotionsTab), "Promotions Tab is not found");
			click(SegmentedBannerElements.promotionsTab);
			Assert.assertTrue(isElementDisplayed(SegmentedBannerElements.NewCustomerOffer), "New Customer Offer ");
			}
			catch ( Exception | AssertionError  e)
			{
				String screenShotPath = getScreenshot(testCase); 
			    Assert.fail(e.getMessage() , e);
			}
		
	}
	
	@Test
	public void VerifyPromotionsForExistingUser() throws Exception{
     String testCase = "VerifyPromotionsForExistingUser";
     try{
			launchweb();
			Common.Login();
			Assert.assertTrue(isElementDisplayed(SegmentedBannerElements.promotionsTab), "Promotions Tab is not found");
			click(SegmentedBannerElements.promotionsTab);
			Assert.assertTrue(isElementDisplayed(SegmentedBannerElements.NewCustomerOffer), "New Customer Offer ");
			}
			catch ( Exception | AssertionError  e)
			{
				String screenShotPath = getScreenshot(testCase); 
			    Assert.fail(e.getMessage() , e);
			}
		
	}

}

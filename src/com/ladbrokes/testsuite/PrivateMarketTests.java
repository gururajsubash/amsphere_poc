package com.ladbrokes.testsuite;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;

public class PrivateMarketTests extends DriverCommon{
	Common common= new Common();
	BetslipFunctions betSlipFuns = new BetslipFunctions();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	LoginLogoutFunctions loginFun = new LoginLogoutFunctions();
	
	//MOB-9195_01 , MOB-9195_02 , 
		@Test
		public void VerifyBetPlacementFromPrivateMarketOffersOn_HomePage() throws Exception{
			//caseID =818553;
			String testCase = "VerifyBetPlacementFromPrivateMarketOffersOn_HomePage";
			   try
		        {
		        	launchweb();
		        	Common.Login();
		        	Common.OddSwitcher(TextControls.decimal);
		        	if(!isElementPresent(HomeGlobalElements.privateMarketHeaderHomePage)){
		        		common.NavigateToSportsPage(TextControls.Football);
		        		click(HomeGlobalElements.SportLandingPageOdd);
		        		Entervalue(BetslipElements.stakeBox, "1.00");
		        		click(BetslipElements.placeBet);
		        		Common.logOut();
		        		Common.Login();
		        		click(HomeGlobalElements.ladbrokesLogo);
		        		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user after placing a Bet");
		        	}
		        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user");
		        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.PMmoreTab), "Private Market More tab is not found");
		        	
		        	List<WebElement> offersListOnHomepage = getDriver().findElements(By.xpath("//div[@class='startpage-private-markets list']//selection-button//div[@class='odds-button']"));
		        int offersBefore = offersListOnHomepage.size();
		        String[] oddValue = new String[1];
		        oddValue[0] = offersListOnHomepage.get(0).getText();
		        
		        //adding offer to betSlip
		        int betslipCount = common.GetBetSlipCount();
		        offersListOnHomepage.get(0).click();
		        int laterbetslipCount = common.GetBetSlipCount();
		        Assert.assertTrue( laterbetslipCount == betslipCount + 1 , "Offer is not added to betslip");
		        int multiBetCount = betSlipFuns.GetMultiplesBetCount();
		        
		        //Enter Stake , validating BetSlip and BetReceipt 
		        betSlipFuns.EnterStake("", "", "", TextControls.stakeValue, "single", false);
		        betSlipFuns.VerifyBetDetails("", "", "", oddValue, TextControls.stakeValue,"", "single", "","");
		        String eventSel = GetElementText(By.xpath("//div[@class='market-information-selection']//span"));
		        betSlipFuns.ValidateBetReceipt("", "", "", eventSel, oddValue, TextControls.stakeValue, "", false, multiBetCount, "single");
		        getDriver().navigate().refresh();
		        Thread.sleep(2000);
		        List<WebElement> offersListLater = getDriver().findElements(By.xpath("//div[@class='startpage-private-markets list']//selection-button//div[@class='odds-button']"));
		        int offersLater = offersListLater.size();
		        Assert.assertTrue( offersLater == offersBefore - 1, "offer is not removed from Private Markets List after placing a bet");
		        
		        }
				catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			    Assert.fail(e.getMessage(), e);
			}
		}
		
		//MOB-9208_01 , MOB-9208_02
		@Test
		public void VerifyPrivateMarketPromotionalPage_QualifiedUsers() throws Exception{
			//caseID =818556;
			String testCase = "VerifyPrivateMarketPromotionalPage_QualifiedUsers";
			   try
		        {
		        	launchweb();
		        	Common.Login();
		        	if(!isElementPresent(HomeGlobalElements.privateMarketHeaderHomePage)){
		        		common.NavigateToSportsPage(TextControls.Football);
		        		click(HomeGlobalElements.SportLandingPageOdd);
		        		Entervalue(BetslipElements.stakeBox, "1.00");
		        		click(BetslipElements.placeBet);
		        		Common.logOut();
		        		Common.Login();
		        		click(HomeGlobalElements.ladbrokesLogo);
		        		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user after placing a Bet");
		        	}
		        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user");
		        	click(HomeGlobalElements.promotions);
		        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are not found on Promotional landing Page");
		        	Common.logOut();
		        	Assert.assertFalse(isElementPresent(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are found on Promotional landing Page after logOut");
		        }
			     catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			    Assert.fail(e.getMessage(), e);
			}
		}
		
		//MOB-9208_03 , MOB-9208_04
			@Test
			public void VerifyPrivateMarketPromotionalPage_NonQualifiedUsers() throws Exception{
				//caseID =818555;
				String testCase = "VerifyPrivateMarketPromotionalPage_NonQualifiedUsers";
				   try
			        {
			        	launchweb();
			        	loginFun.login("TestAuto6", "Password123");
			        	Assert.assertFalse(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are found for non qualified user");
			        	click(HomeGlobalElements.promotions);
			        	Assert.assertFalse(isElementDisplayed(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are found on Promotional landing Page for non qualified user");
			        	Common.logOut();
			        	Assert.assertFalse(isElementPresent(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are found on Promotional landing Page for logged out users");
			        }
				     catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
				    Assert.fail(e.getMessage(), e);
				}
			}
		
			//MOB-9198_01 , MOB-9198_04 , MOB-9198_05
			@Test
			public void VerifyBetPlacementFromPrivateMarketOffersOn_PromotionalPage() throws Exception{
				//caseID =818554;
				String testCase = "VerifyBetPlacementFromPrivateMarketOffersOn_PromotionalPage";
				   try
			        {
			        	launchweb();
			        	Common.Login();
			        	Common.OddSwitcher(TextControls.decimal);
			        	if(!isElementPresent(HomeGlobalElements.privateMarketHeaderHomePage)){
			        		common.NavigateToSportsPage(TextControls.Football);
			        		click(HomeGlobalElements.SportLandingPageOdd);
			        		Entervalue(BetslipElements.stakeBox, "1.00");
			        		click(BetslipElements.placeBet);
			        		Common.logOut();
			        		Common.Login();
			        		click(HomeGlobalElements.ladbrokesLogo);
			        		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user after placing a Bet");
			        	}
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user");
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.PMmoreTab), "Private Market More tab is not found");
			        	click(HomeGlobalElements.PMmoreTab,"Private Market More tab is not clickable");
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are not found on Promotional landing Page");
			        
			        	List<WebElement> offersList = getDriver().findElements(By.xpath("//div[@class='promotions-private-markets list']//selection-button//div[@class='odds-button']"));
			        int offersBefore = offersList.size();
			        Assert.assertFalse(offersBefore > 3, "More than 3 offers are displayed in the Private Market list");
			        String[] oddValue = new String[1];
			        oddValue[0] = offersList.get(0).getText();
			        
			        //adding offer to betSlip
			        int betslipCount = common.GetBetSlipCount();
			        offersList.get(0).click();
			        int laterbetslipCount = common.GetBetSlipCount();
			        Assert.assertTrue( laterbetslipCount == betslipCount + 1 , "Offer is not added to betslip");
			        int multiBetCount = betSlipFuns.GetMultiplesBetCount();
			        
			        //Enter Stake , validating BetSlip and BetReceipt 
			        betSlipFuns.EnterStake("", "", "", TextControls.stakeValue, "single", false);
			        betSlipFuns.VerifyBetDetails("", "", "", oddValue, TextControls.stakeValue,"", "single", "","");
			        String eventSel = GetElementText(By.xpath("//div[@class='market-information-selection']//span"));
			        betSlipFuns.ValidateBetReceipt("", "", "", eventSel, oddValue, TextControls.stakeValue, "", false, multiBetCount, "single");
			        getDriver().navigate().refresh();
			        Thread.sleep(2000);
			        
			        List<WebElement> offersListLater = getDriver().findElements(By.xpath("//div[@class='promotions-private-markets list']//selection-button//div[@class='odds-button']"));
			        int offersLater = offersListLater.size();
			        Assert.assertTrue( offersLater == offersBefore - 1, "offer is not removed from Private Markets List after placing a bet");
			        
			        }
					catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
				    Assert.fail(e.getMessage(), e);
				}
			}
			
			@Test
			public void VerifyPrivateMarketsAfter_BetPlacement() throws Exception{
				//caseID =818557;
				String testCase = "VerifyPrivateMarketsAfter_BetPlacement";
				   try
			        {
			        	launchweb();
			        	Common.Login();
			        	Common.OddSwitcher(TextControls.decimal);
			        	if(!isElementPresent(HomeGlobalElements.privateMarketHeaderHomePage)){
			        		common.NavigateToSportsPage(TextControls.Football);
			        		click(HomeGlobalElements.SportLandingPageOdd);
			        		Entervalue(BetslipElements.stakeBox, "1.00");
			        		click(BetslipElements.placeBet);
			        		Common.logOut();
			        		Common.Login();
			        		click(HomeGlobalElements.ladbrokesLogo);
			        		Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user after placing a Bet");
			        	}
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderHomePage), "Private Market offers are not found for qualified user");
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.PMmoreTab), "Private Market More tab is not found");
			        	click(HomeGlobalElements.PMmoreTab,"Private Market More tab is not clickable");
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.promotionsTabActive), "Private Market More tab is not naviagted to Promotions landing page ");
			        	HGfunctionObj.VerifyBreadCrums(TextControls.Home,"Promotions" , "", "");
			        	Assert.assertTrue(isElementDisplayed(HomeGlobalElements.privateMarketHeaderPromoPage), "Private Market offers are not found on Promotional landing Page");
			        }
					catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
				    Assert.fail(e.getMessage(), e);
				}
			}
	//****************************** If this test case has to be run on Pre-prod tokens have to be set by the functional team
			@Test
			public void VerifyPrivateMarketsQualifiedUser() throws Exception{
				
				String testCase = "VerifyPrivateMarketsQualifiedUser";
				boolean isStatus;
		     try{
		    	 
		    	 launchweb();
		    	 Common.Login();
		    		 
		    	 WebElement MyOffersElement =  getDriver().findElement((LoginElements.myOffersForPromotions));
		    	 
		    	 String myOffersText = MyOffersElement.getText();
		    	 if (myOffersText.contains("MY OFFERS")){
		    		isStatus = true;
		    		Assert.assertTrue(isElementPresent(LoginElements.myOffersForPromotions), "MY OFFERS is present for a Qualified user"); 	  
		    	 }
		    	 
		    	 Common.logOut();
		    	 Assert.assertFalse(isElementPresent(LoginElements.myOffersForPromotions), "MY OFFERS is present for a qualified logged out user");
		   	   	 
		     }     
		     catch(AssertionError e){
					String screenShotPath = getScreenshot(testCase); 
					Assert.fail(e.getMessage() ,e);
					}
		     }
			
			@Test
			public void VerifyPrivateMarketsNonQualifiedUser() throws Exception{
				
				String testCase = "VerifyPrivateMarketsNonQualifiedUser";
				//boolean isStatus = false;
		     try{
		    	 
		    	 launchweb();
		    	 Common.Login();
		    	   
		    	 Assert.assertFalse(isElementPresent(LoginElements.myOffersForPromotions), "MY OFFERS is present for a Non-Qualified user");
		   	   	 
		    	 Common.logOut();
		    	 Assert.assertFalse(isElementPresent(LoginElements.myOffersForPromotions), "MY OFFERS is present for a non-qualified logged out user");
		   	   	 
		     }     
		     catch(AssertionError e){
					String screenShotPath = getScreenshot(testCase); 
					Assert.fail(e.getMessage() ,e);
					}
		     
			
			//******************************
		}
	}




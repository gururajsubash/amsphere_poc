package com.ladbrokes.testsuite;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.EnglishOR.*;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.Convert;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;

public class EventDetailsPageTests extends DriverCommon {
	Common common = new Common();
	HomeGlobal_Functions HGObjFunctions = new HomeGlobal_Functions();
	BetslipElements BetslipElements = new BetslipElements();

	/**
	 * TC-01 This covered 2 Test Cases 1) Navigation through Contextual menu on
	 * inplay sports event detail pages 2) All and video toggle on contextual
	 * menu on inplay and upcoming sports event detail pages All and video
	 * toggle on inplay and upcoming module
	 * 
	 * @return none
	 * @throws Exception
	 *             element not found
	 */
	@Test
	public void VerifyContextualMenuInInplayForEventDetailofDifferentSports() throws Exception {
		String testCase = "VerifyContextualMenuInInplayForEventDetailofDifferentSports";
		int count;
		try {
			launchweb();
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			if (inPlayFiltersList.size() > 4)
				count = 4;
			else
				count = inPlayFiltersList.size();
			for (int i = 1; i < count; i++) {
				inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
				String tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.moreText)) {
					// Click on more
					inPlayFiltersList.get(i).click();
					// Get sports listed under MORE
					List<WebElement> sportsNameInMore = getDriver().findElements(HomeGlobalElements.sportsNameInMore);
					if (sportsNameInMore.size() > 1)
						count = 1;
					else
						count = sportsNameInMore.size();
					// Verify Events count for each sports under MORE
					for (int j = 0; j < count; j++) {
						String moreTabName = sportsNameInMore.get(j).getText();
						sportsNameInMore.get(j).click();
						HGObjFunctions.VerifyInplayEventExpandingAndEventCount();
					}
				} else {
					inPlayFiltersList.get(i).click();
					HGObjFunctions.VerifyInplayEventExpandingAndEventCount();
					getDriver().findElement(By.linkText("Ladbrokes")).click();
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-02 Check navigation to preplay/inplay event details page
	 * 
	 * @return none
	 * @throws Exception
	 *             element not found
	 */
	@Test
	public void VerifyPopularMarketOnInplayEventDetailPage() throws Exception {
		String testCase = "VerifyPopularMarketOnInplayEventDetailPage";
		try {
			//// logger.log(LogStatus.INFO, "Test case has Started");
			launchweb();
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++) {
				inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
				String tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football)) {
					inPlayFiltersList.get(i).click();
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(HomeGlobalElements.sportsNameInMore);
					for (int j = 1; j <= sportsNameInMore.size(); j++)
						;
					{
						sportsNameInMore = getDriver().findElements(HomeGlobalElements.InPlayFilters);
						tabName = inPlayFiltersList.get(i).getText();
						if (tabName.contains(TextControls.Football)) {
							inPlayFiltersList.get(i).click();
							break;
						}
					}
				}
			}
			click(HomeGlobalElements.inPlayFBEvent);
			if (isElementDisplayed(HomeGlobalElements.MainMarket))
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.MainMarket),
						"Main Market is not selected by default on event detail page");
			else
				Assert.assertTrue(isElementDisplayed(HomeGlobalElements.popularMarket),
						"Popular Market is not selected by default on event detail page");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 03 Check the landing, sub type and event details pages
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyNavigationToCricketDiffEventSubTypes() throws Exception {
		String testCase = "VerifyNavigationToCricketDiffEventSubTypes";
		String eventTypeName, subtypeXpath = null;
		List<WebElement> TypeNameLists=null;
		try {
			launchweb();
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			common.NavigateToSportsPage(TextControls.cricket);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.cricket, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size() - 1; i++) {
				TypeNameLists = getDriver().findElements(HomeGlobalElements.navigationTabs);
				eventTypeName = TypeNameLists.get(i).getText().toLowerCase();
				if (eventTypeName.contains(TextControls.moreText.toLowerCase()))
					break;
				if (eventTypeName.contains("("))
					eventTypeName = eventTypeName.substring(0, eventTypeName.length() - 4);
				eventTypeName = toTitleCase(eventTypeName);
				TypeNameLists.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				if (!isElementPresent(BetslipElements.EDPHeader)) {
					if (GetElementText(HomeGlobalElements.breadcrumbs).contains(TextControls.inPlay)) {
						subtypeXpath = "(//a[@data-bind='html: title, href: href'])[4]";
					} else {
						HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.cricket, eventTypeName.replaceAll("\n", ""), "");
						subtypeXpath = "(//a[@class='breadcrumbs-link'])[2]";
					}
					click(By.xpath(subtypeXpath));
				} else
					click(FootballElements.typeXpath);
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 04 Check the landing, sub type and event details pages
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test(groups = { "P1", "Regression" })
	public void VerifyNavigationToRugbyLeagueDiffEventSubTypes() throws Exception {
		String testCase = "VerifyNavigationToRugbyLeagueDiffEventSubTypes";
		String eventTypeName, subtypeXpath;
		List<WebElement> TypeNameLists=null;
		try {
			launchweb();
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			common.NavigateToSportsPage(TextControls.rugbyLeague);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.rugbyLeague, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
				TypeNameLists = getDriver().findElements(HomeGlobalElements.navigationTabs);
				eventTypeName = TypeNameLists.get(i).getText().toLowerCase();
				if (eventTypeName.contains("("))
					eventTypeName = eventTypeName.substring(0, eventTypeName.length() - 4);
				if (eventTypeName.contains(TextControls.moreText.toLowerCase()))
					break;
				eventTypeName = toTitleCase(eventTypeName);
				TypeNameLists.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				if (!isElementPresent(BetslipElements.EDPHeader)) {
					if (GetElementText(HomeGlobalElements.breadcrumbs).contains(TextControls.inPlay)) {
						subtypeXpath = "(//a[@data-bind='html: title, href: href'])[4]";
					} else {
						HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.rugbyLeague, eventTypeName.replaceAll("\n", ""), "");
						// String typeHeader = "//h1[contains(text(),'"+
						// eventTypeName +"')]" ;
						String typeHeader = "//h1[translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"
								+ eventTypeName.replaceAll("\n", "").toLowerCase() + "']";
						Assert.assertTrue(isElementDisplayed(By.xpath(typeHeader)),
								"Navigation to " + eventTypeName + " has failed ");
						subtypeXpath = "(//a[@class='breadcrumbs-link'])[2]";
					}
					click(By.xpath(subtypeXpath));
				} else
					click(FootballElements.typeXpath);
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 05 Check the landing, sub type and event details pages
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyNavigationToGolfDiffEventSubTypes() throws Exception {
		String testCase = "VerifyNavigationToGolfDiffEventSubTypes";
		String eventTypeName, subtypeXpath;
		List<WebElement> TypeNameLists=null;
		try {
			launchweb();
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			common.NavigateToSportsPage(TextControls.golf);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.golf, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size() - 1; i++) {
				TypeNameLists = getDriver().findElements(HomeGlobalElements.navigationTabs);
				eventTypeName = TypeNameLists.get(i).getText().toLowerCase();
				if (eventTypeName.contains("("))
					eventTypeName = eventTypeName.substring(0, eventTypeName.length() - 4);
				if (eventTypeName.contains(TextControls.moreText.toLowerCase()))
					break;
				eventTypeName = toTitleCase(eventTypeName);
				TypeNameLists.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				if (!isElementPresent(BetslipElements.EDPHeader)) {
					if (GetElementText(HomeGlobalElements.breadcrumbs).contains(TextControls.inPlay)) {
						subtypeXpath = "(//a[@data-bind='html: title, href: href'])[4]";
					} else {
						HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.golf, eventTypeName.replaceAll("\n", ""), "");
						// String typeHeader = "//h1[contains(text(),'"+
						// eventTypeName +"')]" ;
						String typeHeader = "//h1[translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='"
								+ eventTypeName.toLowerCase() + "']";
						Assert.assertTrue(isElementDisplayed(By.xpath(typeHeader)),
								"Navigation to " + eventTypeName + " has failed ");
						subtypeXpath = "(//a[@class='breadcrumbs-link'])[2]";
					}
					click(By.xpath(subtypeXpath));
				}
				else 
					if (GetElementText(HomeGlobalElements.breadcrumbs).contains(TextControls.inPlay)) {
						subtypeXpath = "(//a[@data-bind='html: title, href: href'])[3]";
						click(By.xpath(subtypeXpath));
						}
					else
					click(FootballElements.typeXpath);
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 06 Check the landing, sub type and event details pages
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyNavigationToBasketballDiffEventSubTypes() throws Exception {
		String testCase = "VerifyNavigationToBasketballDiffEventSubTypes";
		String typeName, subTypeName;
		List<WebElement> TypeNameLists=null;
		try {
			launchweb();
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			common.NavigateToSportsPage(TextControls.Basketball);
			HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.Basketball, "", "");
			List<WebElement> TypeNameList = getDriver().findElements(HomeGlobalElements.navigationTabs);
			for (int i = 0; i < TypeNameList.size(); i++) {
				TypeNameLists = getDriver().findElements(HomeGlobalElements.navigationTabs);
				typeName = TypeNameLists.get(i).getText().toLowerCase();
				if (typeName.equalsIgnoreCase(TextControls.moreText))
					break;
				if (typeName.contains("("))
				typeName = typeName.substring(0, typeName.length() - 4);
				typeName = toTitleCase(typeName);
				TypeNameLists.get(i).click();
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.Basketball, typeName.replaceAll("\n", ""), "");
				// Verifying SubTypes under Type
				List<WebElement> subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
				for (int j = 0; j < subTypeNameList.size(); j++) {
					subTypeNameList = getDriver().findElements(HomeGlobalElements.subTypeList);
					subTypeName = subTypeNameList.get(j).getText();
					subTypeName = subTypeName.substring(0, subTypeName.length() - 2);
					subTypeName = toTitleCase(subTypeName);
					subTypeNameList.get(j).click();
					getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					if (!isElementPresent(BetslipElements.EDPHeader))
						HGObjFunctions.VerifyBreadCrums(TextControls.Home, TextControls.Basketball, typeName.replaceAll("\n", ""),
								subTypeName.replaceAll("\n", ""));
					else
						click(By.xpath("(//a[@class='breadcrumbs-link'])[3]"));
				}
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyBetslipOnDetailPage() throws Exception {
		String testCase = "VerifyBetslipOnDetailPage";
		String tabName;
		Boolean isStatus = false;
		try {
			launchweb();
			String tab = getDriver().findElement(HomeGlobalElements.inPlayTab).getAttribute("class");
			if (!tab.contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			getDriver().findElements(HomeGlobalElements.InPlayFilters);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++) {
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football.toUpperCase())) {
					isStatus = true;
					inPlayFiltersList.get(i).click();
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++) {
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains(TextControls.Football.toUpperCase())) {
							isStatus = true;
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			WebElement fbInplayEvent = getDriver().findElement(HomeGlobalElements.inPlayFBEvent);
			fbInplayEvent.click();
			// wAction.GetAttribute(driver,
			// By.XPath(BetslipControls.betslipArrow), "class", "Betslip is not
			// present in inplay event detail page");
			click(BetslipElements.betslipArrow);
			Assert.assertFalse(isElementDisplayed(BetslipElements.betslipArea),
					"Betslip is not collpased clicking on collapse");
			click(BetslipElements.betslipToCollapseExpand);
			Assert.assertTrue(isElementDisplayed(BetslipElements.betslipArea), "Betslip is not Expanded");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyHierarchyOfTypeAndSubTypeEventsOnContextualMenu() throws Exception {
		String testCase = new Object() {
		}.getClass().getEnclosingMethod().getName();
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			click(By.xpath("/descendant::li[@class='tab'][position()=1]"));
			String Maintype = getDriver()
					.findElement(By.xpath("//li[@class='breadcrumbs-item'][3]/span[@data-bind='html: title']"))
					.getText();
			String Type = (getDriver().findElement(By.xpath("//div[@class='links']//a[1]")).getAttribute("innerHTML"));
			click(By.xpath("//div[@class='links']//a[1]"));
			if (isElementPresent(By.xpath("//header[@class='event-header Football']"))) {
				scrollToView(By.xpath("(//header[@class='event-header Football'])[1]"));
				click(By.xpath("(//header[@class='event-header Football'])[1]"));
			}
			String[] breaksubtype = Type.split("»");
			String joinesubtype = String.join("", breaksubtype);
			String totalsubtype = joinesubtype.trim();
			String xpath = "//span[@class='title'][text()='" + Maintype + "']";
			String xpath1 = "//span[@class='title'][text()='" + totalsubtype + "']";
			Assert.assertTrue(getDriver().findElement(By.xpath(xpath)).isDisplayed(),
					"Type on contextual menu is not displayed");
			Assert.assertTrue(getDriver().findElement(By.xpath(xpath1)).isDisplayed(),
					"SubType on contextual menu is not displayed");
			List<WebElement> allevents = getDriver().findElements(By.xpath("//a[@class='event-link active']"));
			String[] allText = new String[allevents.size()];
			int i = 0;
			for (WebElement element : allevents) {
				allText[i++] = element.getText();
				String[] allanmes = allText;
				String names = String.join("", allanmes);
			}
			String timer = getDriver().findElement(By.xpath("/descendant::span[@class='start-time'][position()=1]"))
					.getText();
			String format = "HHmmss";
			Boolean ok = false;
			// try
			// {
			// System.Globalization.CultureInfo provider =
			// System.Globalization.CultureInfo.InvariantCulture;
			// DateTime dt = DateTime.ParseExact(timer, format, provider);
			// if (dt.Hour != null)
			// {
			// ok = true;
			// }
			// }
			// catch (Exception e)
			// {
			// //// ok = false; // already setup in initializer above.
			// }
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyInplayEventTitleOnEventDetailPage() throws Exception {
		String testCase = new Object() {
		}.getClass().getEnclosingMethod().getName();
		String tabName;
		boolean isStatus = false;
		try {
			launchweb();
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 0; i < inPlayFiltersList.size(); i++) {
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football.toUpperCase())) {
					isStatus = true;
					inPlayFiltersList.get(i).click();
					;
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++) {
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains(TextControls.Football.toUpperCase())) {
							isStatus = true;
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			WebElement fbInplayEvent = getDriver().findElement(HomeGlobalElements.inPlayFBEvent);
			String eventNameTxt = getDriver().findElement(HomeGlobalElements.inPlayFBEvent).getText();
			fbInplayEvent.click();
			Assert.assertTrue(isElementPresent(HomeGlobalElements.inPlayTabOnDetailPage),
					"LHN Inplay module is not present on event detail page");
			if (!getAttribute(HomeGlobalElements.inPlayOnDetialPage, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayOnDetialPage);
			List<WebElement> inPlaySportList = getDriver().findElements(HomeGlobalElements.inPlaySportsOnDetailPage);
			for (int i = 0; i < inPlaySportList.size(); i++) {
				String tabName1 = inPlaySportList.get(i).getText();
				int inplayEventCnt = (int) Convert.ToDouble(common.ExtractNumberFromString(tabName1, "(", ")"));
				if (getAttribute(HomeGlobalElements.inPlaySportsOnDetailPage, "class").contains("expanded")) {
					List<WebElement> inplayEvents = getDriver()
							.findElements(HomeGlobalElements.inPlayEventsOnDetailPage);
					Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
				} else {
					inPlaySportList.get(i).click();
					List<WebElement> inplayEvents = getDriver()
							.findElements(HomeGlobalElements.inPlayEventsOnDetailPage);
					Assert.assertTrue(inplayEventCnt == inplayEvents.size(), "Mismatch in inplay events count");
				}
				inPlaySportList.get(i).click();
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyTypeAndSubTypeInEventDetailPage() throws Exception {
		String testCase = new Object() {
		}.getClass().getEnclosingMethod().getName();
		try {
			launchweb();
			common.Removeall_Function();
			click(HomeGlobalElements.AZMenu);
			click(By.xpath("//span[@class='name' and text()='" + TextControls.Football + "']"));
			String Maintype = getDriver()
					.findElement(By.xpath("/descendant::span[@class='title-container'][position()=1]"))
					.getAttribute("innerHTML");
			click(By.xpath("/descendant::span[@class='title-container'][position()=1]"));
			String Type = (getDriver().findElement(By.xpath("//div[@class='links']//a[1]")).getAttribute("innerHTML"));
			click(By.xpath("//div[@class='links']//a[1]"));
			if (isElementPresent(By.xpath("//header[@class='event-header Football']"))) {
				scrollToView(By.xpath("(//header[@class='event-header Football'])[1]"));
				click(By.xpath("(//header[@class='event-header Football'])[1]"));
			}
			String[] breaksubtype = Type.split("»");
			String joinesubtype = String.join("", breaksubtype);
			String totalsubtype = joinesubtype.trim();
			String xpath = "//a[@class='breadcrumbs-link'][text()='" + Maintype + "']";
			Assert.assertTrue(isElementDisplayed(By.xpath(xpath)), "Type is not present in Event Detail page");
			String xpath1 = "//a[@class='breadcrumbs-link'][text()='" + totalsubtype + "']";
			Assert.assertTrue(isElementDisplayed(By.xpath(xpath1)), "Type is not present in Event Detail page");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyUpcomingEventTitleOnEventDetailPage() throws Exception {
		String testCase = new Object() {
		}.getClass().getEnclosingMethod().getName();
		String tabName;
		boolean isStatus = false;
		int n = 1;
		try {
			launchweb();
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++) {
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains("TENNIS")) {
					isStatus = true;
					inPlayFiltersList.get(i).click();
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, 'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j <= sportsNameInMore.size(); j++) {
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains("TENNIS")) {
							isStatus = true;
							sportsNameInMore.get(j).click();
							break;
						}
					}
				}
			}
			WebElement fbInplayEvent = getDriver().findElement(HomeGlobalElements.inPlayFBEvent);
			String eventNameTxt = getDriver().findElement(HomeGlobalElements.inPlayFBEvent).getText();
			fbInplayEvent.click();
			Assert.assertTrue(isElementPresent(HomeGlobalElements.inPlayTabOnDetailPage),
					"LHN Inplay module is not present on event detail page");
			click(HomeGlobalElements.inPlayTabOnDetailPage);
			if (!(getAttribute(HomeGlobalElements.upcomingOnDetialPage, "class").contains("expanded")))
				click(HomeGlobalElements.upcomingOnDetialPage);
			List<WebElement> upcomingSportList = getDriver()
					.findElements(HomeGlobalElements.upcomingSportsOnDetailPage);
			for (int i = 1; i < upcomingSportList.size(); i++) {
				if (i == 4)
					break;
				Thread.sleep(4000);
				String tabName1 = upcomingSportList.get(i).getText();
				int upcomingEventCnt = (int) Convert.ToDouble(common.ExtractNumberFromString(tabName1, "(", ")"));
				if (isElementPresent(By.xpath("//div[@class='upcoming-wrapper']"))) {
					upcomingSportList.get(i).click();
					List<WebElement> upcomingEvents = getDriver()
							.findElements(By.xpath("//div[@class='upcoming-wrapper']//ul[@class='events']//li"));
					Assert.assertTrue(upcomingEventCnt == upcomingEvents.size(), "Mismatch in Upcoming events count");
				}
				upcomingSportList.get(i).click();
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyBreadcrumbAfterCollapsingAndExpandingContextualMenu() throws Exception {
		String testCase = "VerifyBreadcrumbAfterCollapsingAndExpandingContextualMenu";
		String expBreadcrumbs, actBreadcrumbs, actualURL, expectedURL, tabName;
		boolean isStatus = false;
		try {
			launchweb();
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 1; i < inPlayFiltersList.size(); i++) {
				tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains(TextControls.Football.toUpperCase())) {
					inPlayFiltersList.get(i).click();
					isStatus = true;
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					click(HomeGlobalElements.moreTab);
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params,'inplay')]//div[@class='more']//span[@class='title']"));
					for (int j = 0; j < sportsNameInMore.size(); j++) {
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.contains(TextControls.Football.toUpperCase())) {
							sportsNameInMore.get(j).click();
							isStatus = true;
							break;
						}
					}
				}
			}
			if (isStatus == true)
				System.out.println("Football sports is found");
			// Logger.Pass("Football sports is found");
			else
				Assert.fail("Football sports is not found");
			// Click on tennis inplay event
			click(HomeGlobalElements.inPlayFBEvent);
			if (isElementPresent(By.xpath("//li[1]//a[contains(@class, 'event-link')]//span[@class='start-time']"))) {
				actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs).replaceAll("\n", "");
				expBreadcrumbs = TextControls.Home + ">" + TextControls.inPlay + ">" + TextControls.Football + ">" + "";
				Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs),
						"Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '"
								+ expBreadcrumbs + "'");
			} else if (isElementPresent(
					By.xpath("//li[1]//a[contains(@class, 'event-link')]//span[@class='scores']"))) {
				expectedURL = getDriver().getCurrentUrl();
				Assert.assertTrue(isElementPresent(HomeGlobalElements.collapseButton), "Collapse button is not found");
				// Verify breadcrumb and Url after colapsing contextual menu
				click(HomeGlobalElements.collapseButton, "Collapse button is not found");
				actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs).replaceAll("\n", "");
				expBreadcrumbs = TextControls.Home + ">" + TextControls.inPlay + ">" + TextControls.Football + ">";
				Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs),
						"Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '"
								+ expBreadcrumbs + "'");
				actualURL = getDriver().getCurrentUrl();
				Assert.assertEquals(expectedURL, actualURL);
				// Verify breadcrumb and Url after expanding contextual menu
				click(HomeGlobalElements.collapseButton, "Collapse button is not found");
				actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs).replaceAll("\n", "");
				expBreadcrumbs = TextControls.Home + ">" + TextControls.inPlay + ">" + TextControls.Football + ">";
				Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs),
						"Mismatch in Event Hierarchy (breadcrumbs). Actual: '" + actBreadcrumbs + "', Expected: '"
								+ expBreadcrumbs + "'");
				actualURL = getDriver().getCurrentUrl();
				Assert.assertEquals(expectedURL, actualURL);
			} else {
				Assert.fail("Time/Score is not displayed on contexual menus");
			}
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	@Test
	public void VerifyDefaultSelectedSportExpanded() throws Exception {
		String testCase = "VerifyDefaultSelectedSportExpanded";
		try {
			launchweb();
			// Verifying by default particular sport title is expanded on detail
			// page which the user has selected from the previous page.",
			// "Default particular sport title should be expanded on detail page
			// which the user has selected from the previous page");
			if (!getAttribute(HomeGlobalElements.inPlayTab, "class").contains("expanded"))
				click(HomeGlobalElements.inPlayTab);
			List<WebElement> inPlayFiltersList = getDriver().findElements(HomeGlobalElements.InPlayFilters);
			for (int i = 0; i < inPlayFiltersList.size(); i++) {
				String tabName = inPlayFiltersList.get(i).getText();
				if (tabName.contains("TENNIS")) {
					inPlayFiltersList.get(i).click();
					break;
				} else if (tabName.contains(TextControls.moreText)) {
					inPlayFiltersList.get(i).click();
					click(By.xpath(
							"//nav[@class='tabs']//div[@class='more']//div[@class='tab']//span[@title='Tennis']"),
							"Tennis sport is not present in Inplay module");
					break;
				}
			}
			// Click on Tennis inplay event
			WebElement fbInplayEvent = getDriver().findElement(HomeGlobalElements.inPlayFBEvent);
			fbInplayEvent.click();
			Assert.assertTrue(
					isElementPresent(By
							.xpath("//div[@class='inplay-wrapper']/h2[@class='sport expanded']//span[contains(text(), 'Tennis')]")),
					"Tennis Sport  title is not expanded on detail page by default");
			click(By.xpath(
					"//div[@class='inplay-wrapper']/h2[@class='sport expanded']//span[contains(text(), 'Tennis')]"),
					"Failed to click on Tennis to expand the sport");
			Assert.assertTrue(
					isElementPresent(By
							.xpath("//div[@class='inplay-wrapper']/h2[@class='sport']//span[contains(text(), 'Tennis')]")),
					"Tennis is not collapsed clicking on it on detail page");
		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
}

package com.ladbrokes.testsuite;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.EnglishOR.ResultsElements;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.ResultsFunctions;

public class ResultsTests extends DriverCommon {
	Common common= new Common();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	ResultsFunctions ResultsFunctions = new ResultsFunctions();
	/** 
	 * TC- 01 Verify the Results tab on all landing pages of sports 
	 * EX: Football, Golf, Tennis etc and clicking upon it > it should take the user to that results page
	 * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyResultsTabOnSportsLandingPage() throws Exception{
		String testCase = "VerifyResultsTabOnSportsLandingPage";
		try 
		{
	         launchweb();
	         Common.Login();
	         ResultsFunctions.VerifyResultsTabOnSportsLandingPage_Function(TextControls.Football);
	         ResultsFunctions.VerifyResultsTabOnSportsLandingPage_Function(TextControls.Tennis);
	         ResultsFunctions.VerifyResultsTabOnSportsLandingPage_Function(TextControls.golf);
	         ResultsFunctions.VerifyResultsTabOnSportsLandingPage_Function(TextControls.Basketball);
	         ResultsFunctions.VerifyResultsTabOnSportsLandingPage_Function(TextControls.cricket);
}
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}

	/** 
	 * TC- 02 Verify that on the results page user is shown 'Go to Sports - football, golf, tennis etc tab 
	 * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyGoToSportsLinkOnResultspage() throws Exception{
		String testCase = "VerifyGoToSportsLinkOnResultspage";
		try 
		{
	         launchweb();
	         ResultsFunctions.VerifyGoToSportsLinkOnResultspage_Function(TextControls.Football);
	         ResultsFunctions.VerifyGoToSportsLinkOnResultspage_Function(TextControls.Tennis);
	         ResultsFunctions.VerifyGoToSportsLinkOnResultspage_Function(TextControls.golf);
	         ResultsFunctions.VerifyGoToSportsLinkOnResultspage_Function(TextControls.Basketball);
	         ResultsFunctions.VerifyGoToSportsLinkOnResultspage_Function(TextControls.cricket);
	         
		}
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage() , e);
		}
	}
	
	/** 
	 * TC- 03 Verify the drop down of More option and click on other sports to see the results of that particular sport 
	 * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyMoreOptionOnResultspage() throws Exception{
		String testCase = "VerifyMoreOptionOnResultspage";
		try 
		{
	         launchweb();
	         common.NavigateToSportsPage(TextControls.Football);
	 		 click(ResultsElements.resultsTab);
	 		 Assert.assertTrue(isElementDisplayed(ResultsElements.moreTab), "More tab is not found on Results page");
	 		 click(ResultsElements.moreTab);
	 		 List <WebElement> sportsListInmore = getDriver().findElements(By.xpath("//div[@class='more']//div//span[@class='title']"));
	 		a: for(int i = 0 ; i < sportsListInmore.size(); i++)
	 		 {
	 			sportsListInmore = getDriver().findElements(By.xpath("//div[@class='more']//div//span[@class='title']"));
	 			 String sportname = sportsListInmore.get(i).getText().toLowerCase();
	 			 if(sportname.equalsIgnoreCase("UFC/MMA"))
	 				 continue a;
	 			clickByJS(sportsListInmore.get(i));
	 			String tabnameInMore = GetElementText(By.xpath("//li[@class='tab tabs-dropdown dropdown active']//div[@class='title-container']"));
	 			System.out.println(tabnameInMore);
	 			
	 			Assert.assertTrue(tabnameInMore.toLowerCase().contains(sportname), "Failed to navigate '"+ sportname +"' results page in More tabs");
	 			click(By.xpath("//div[@class='title-container']"));
	 		 }
	         }
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}
	
	/** 
	 * TC- 04 "Verify results on Football /Golf/ Tennis/ Basketball page are in correct format.
        Check Type -> SubType -> Team Names -> Score comes with time for Today's event
        Check that for Yesterdays section date and time is available for all events" 
	 * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyResultsFormatOnSports() throws Exception{
		String testCase = "VerifyResultsFormatOnSports";
		try 
		{
	         launchweb();
	         ResultsFunctions.VerifyTodaysResultsFormatOnSport_Function(TextControls.Football);
	         ResultsFunctions.VerifyYesterdaysResultsFormatOnSport_Function(TextControls.Football);
	         
	         ResultsFunctions.VerifyTodaysResultsFormatOnSport_Function(TextControls.Tennis);
	         ResultsFunctions.VerifyYesterdaysResultsFormatOnSport_Function(TextControls.Tennis);
	         
	         ResultsFunctions.VerifyTodaysResultsFormatOnSport_Function(TextControls.golf);
	         ResultsFunctions.VerifyYesterdaysResultsFormatOnSport_Function(TextControls.golf);
	         
	         ResultsFunctions.VerifyTodaysResultsFormatOnSport_Function(TextControls.Basketball);
	         ResultsFunctions.VerifyYesterdaysResultsFormatOnSport_Function(TextControls.Basketball);
		}
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}
	
	/** 
	 * TC- 05 Verify  the results of HR and GH - Format
        - Race winner - win, place 
          - Non runner
          - Fav
      * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyResultsFormatOnGH_HR() throws Exception{
		String testCase = "VerifyResultsFormatOnGH_HR";
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		try 
		{
			launchweb();
			if(germanLanguage.equals("Desktop_German"))
				 System.out.println("GreyHounds/HorseRacing is not available for German");
			 else{
	         ResultsFunctions.VerifyResultsFormatHR_GH(TextControls.Greyhounds);
	         ResultsFunctions.VerifyResultsFormatHR_GH(TextControls.HorseRacing);
			 }
	   }
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}
	
	/** 
	 * TC- 06 "Verify the results in the tabs - Today, Yesterday and Last 3 days
         Verify for Racing and any other sport"
      * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyResultsTab_Today_Yesterday_Last3Days() throws Exception{
		String testCase = "VerifyResultsTab_Today_Yesterday_Last3Days";
		try 
		{
	         launchweb();
	         ResultsFunctions.VerifyResultsTab_Today_Yesterday_Last3Days_function();
	     }
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}
	
	/** 
	 * TC- 07 "Verify the functionality of 'Pick a date' - Show tab should be highlighted after picking a date
Verify last month is available on left side and current month is available on right side with Today, Last 7 days and Last 30 days option on top"
      * @return none
	 * @throws Exception 
	 * @throws Exception | AssertionError  element not found
	 */
	@Test
	public void VerifyResultsTab_DatePicker() throws Exception{
		String testCase = "VerifyResultsTab_DatePicker";
		try 
		{
	         launchweb();
	         common.NavigateToSportsPage(TextControls.Football);
			 click(ResultsElements.resultsTab);
			 Assert.assertTrue(isElementDisplayed(ResultsElements.calenderIcon), "Calender icon is not found on Results Page");
			 Assert.assertTrue(isElementDisplayed(ResultsElements.PickaDate), "Pick a Date is not found on Results Page");
			 click(ResultsElements.calenderIcon);
			 //Verifying months
			 List<WebElement> monthList = getDriver().findElements(By.xpath("//th[@class='month']"));
			 String Actuallastmonth = monthList.get(0).getText();
			 String ActualCurrentMonth = monthList.get(1).getText();
			 Assert.assertTrue(Actuallastmonth.equals(ResultsFunctions.GetLastMonth()), "Actual : ' "+ Actuallastmonth +" ' and Expected : '"+ ResultsFunctions.GetLastMonth() +"'Last months are not matching");
			 Assert.assertTrue(ActualCurrentMonth.equals(ResultsFunctions.GetCurrentMonth()), "Actual : ' "+ ActualCurrentMonth +" ' and Expected : '"+ ResultsFunctions.GetCurrentMonth() +"'Current months are not matching");
			 
	        
	     }
		catch(Exception | AssertionError  e)
		{
		  String screenShotPath = getScreenshot(testCase); 
		  Assert.fail(e.getMessage(), e);
		}
	}
}

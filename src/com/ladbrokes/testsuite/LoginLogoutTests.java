package com.ladbrokes.testsuite;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;
import com.ladbrokes.Utils.TestdataFunction;

import com.ladbrokes.Utils.ReadTestSettingConfig;

public class LoginLogoutTests extends DriverCommon{
	TestdataFunction testdata = new TestdataFunction();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions(); 
	LoginLogoutFunctions LoginFunctionObj = new LoginLogoutFunctions();
	
	/** 
	 * TC-1 Forgotten login details?
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyForgotLoginDetailsLink() throws Exception {
		String testCase = "VerifyForgotLoginDetailsLink";
		String username = testdata.Readtestdata("Users", 13, 1);
		String email =  testdata.Readtestdata("Users", 13, 3);
		String successMessage, lostUserMessage;
		String projectName=ReadTestSettingConfig.getTestsetting("ProjectName");
		
		try{
			//logger.log(LogStatus.INFO, "Test case has Started"); 
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.forgotLoginDetails), "Forgot Login Details not found in Login Page");
			click(LoginElements.forgotLoginDetails);
			String parentWindow = getDriver().getWindowHandle();
			switchwindow();
			Entervalue(LoginElements.forgotUsernameField, username);
			Entervalue(LoginElements.forgotEmail, email);
			selectValueInDropdown(LoginElements.dateDropDown, "05");
			selectValueInDropdown(LoginElements.monthDropDown, "02");
			selectValueInDropdown(LoginElements.yearDropDown, "1950");
			click(LoginElements.forgotPwdSubmitBtn,"Submit button is not found");
			successMessage = GetElementText(LoginElements.successMessage);
			System.out.println("-->>  "+ successMessage);
			/*String projectName = ReadTestSettingConfig.getTestsetting("ProjectName");
			if(projectName.equals("Desktop_German"))
			Assert.assertTrue(successMessage.contains("Das Passwort wurde erfolgreich an Ihre E-Mail-Adresse gesendet"), "German Password is not sent");
			else*/
			if(projectName.equals("Desktop_Swedish"))
			{
				Assert.assertTrue(successMessage.contains("Password has been sent to your email successfully"), "Password is not sent")	;
				click(LoginElements.lostusrNameTab);
				Entervalue(LoginElements.email, email);
				click(LoginElements.forgotUsrSubmitBtn,"Submit button is not found");
				 lostUserMessage = GetElementText(LoginElements.lostUNameMessage);
				 Assert.assertTrue(lostUserMessage.contains("Multiple accounts were found with that email address. Please contact Customer Support"));
			}
			else
			{
			Assert.assertTrue(successMessage.contains("A temporary password is sent to:"), "Password is not sent");
			click(By.xpath("//div[@class='fn-back-button ladbrokes-header__back-button']"));
			click(LoginElements.forgottenUsernameTab);
			Entervalue(By.xpath("(//input[@name='email'])[2]"), email);
			click(LoginElements.forgotUsrSubmitBtn,"Submit button is not found");
			 lostUserMessage = GetElementText(LoginElements.lostUNameMessage);
			 Assert.assertTrue(lostUserMessage.contains("Multiple accounts were found with that email address. Please contact Customer Support"));
			}
		     getDriver().close();
		     getDriver().switchTo().window(parentWindow);
		     
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			
			Assert.fail(e.getMessage() , e);
		}
	}
	/** 
	 * TC-2 Verify Remember Me Link
	 * @return none
	 * @throws Exception element not found
	 */
	@Test
	public void VerifyRememberMe() throws Exception {
		String testCase = "VerifyRememberMe";
		
		 boolean rememberChkStatus;
		 String retainedUserName;
		 try
         {
			 //logger.log(LogStatus.INFO, "Test case has Started");  
			 launchweb();
			 getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
             rememberChkStatus = isChecked(LoginElements.rememberMe);
             Assert.assertTrue(rememberChkStatus==false, "Rememeber UserName checkbox is checked at startup");
             
             // **Verify Checking Remember Me UserName retains the UserName
         click(LoginElements.rememberMe);
         String userName = ReadTestSettingConfig.getTestsetting("UserName1");
         Common.Login();
         Common.logOut();
         rememberChkStatus = isChecked(LoginElements.rememberMe);
         Assert.assertTrue(rememberChkStatus == true, "Rememeber UserName checkbox failed to remain checked on Login");
         retainedUserName = getAttribute(LoginElements.Username, "value");
         Assert.assertTrue(retainedUserName.equals(userName), "User name was not retained");
         
       //**Verify UnChecking Remember Me UserName does not retain the UserName
         
         click(LoginElements.rememberMe);
         getDriver().findElement(LoginElements.Username).clear();
         Common.Login();
         Common.logOut();
         rememberChkStatus = isChecked(LoginElements.rememberMe);
        Assert.assertTrue(rememberChkStatus == false, "Rememeber UserName checkbox is failed to uncheck");
         retainedUserName = getAttribute(LoginElements.Username, "value");
        Assert.assertFalse(retainedUserName.equals(userName), "User name was retained");
        
         }
		 catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			 
			 Assert.fail( e .getMessage() ,e);
			 }
		 }
	/** 
	 * TC-3 Login using the correct credentials
	 * @return none
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLogin() throws Exception
	{
		
		String testCase = "VerifyLogin";
		
	try{
		
		launchweb();
	    Common.Login();
		 
	}
	catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
		
		Assert.fail(e.getMessage(),e);
	}
	}
	
	/** 
	 * TC-04 Check error message is displayed when Logging in with invalid user name/password 
	 * @return none
	 * @throws Exception element not found
	 */
	@Test	
	
	public void VerifyLoginErrorMessages()throws Exception{
		
		String testCase = "VerifyLoginErrorMessages";
		
		 String username = "test_m";
		 String password = "123456" ;
		 String actErrorMessage, expErrorMessage;
		
		try
		{
			launchweb();
			
			// Login with InCorrrect UserName and Password, appropriate error message should be displayed
			
			actErrorMessage = HGfunctionObj.CaptureLoginErrorMessage("IncorrectUsername","IncorrectPassword");
			expErrorMessage = TextControls.incorrectUNPWText;
			Assert.assertTrue(actErrorMessage.toLowerCase().trim().equals(expErrorMessage.toLowerCase().trim()) , "Mismatch in Actual and Expected error messages");
			System.out.println("Login error message(Attempt to Login with incorrect Username and Password) validated successfully");
			
			// Login with InCorrrect UserName , appropriate error message should be displayed
			
			actErrorMessage = HGfunctionObj.CaptureLoginErrorMessage("IncorrectUsername",password);
			expErrorMessage = TextControls.incorrectUNPWText;
			Assert.assertTrue(actErrorMessage.toLowerCase().trim().equals(expErrorMessage.toLowerCase().trim()), "Mismatch in Actual and Expected error messages");
			System.out.println("Login error message(Attempt to Login with incorrect Username) validated successfully");
			
             // Login with Password and empty UserName", "appropriate error message should be displayed
			
			actErrorMessage = HGfunctionObj.CaptureLoginErrorMessage("",password);
			expErrorMessage = TextControls.emptyUNText;
			Assert.assertTrue(actErrorMessage.toLowerCase().trim().equals(expErrorMessage.toLowerCase().trim()), "Mismatch in Actual and Expected error messages");
			System.out.println("Login error message(Attempt to login leaving the Username blank) validated successfully");
			
           // Login with  UserName and Empty Password", "appropriate error message should be displayed
			
			actErrorMessage = HGfunctionObj.CaptureLoginErrorMessage(username,"");
			expErrorMessage = TextControls.emptyPWText;
			Assert.assertTrue(actErrorMessage.toLowerCase().trim().equals(expErrorMessage.toLowerCase().trim()), "Mismatch in Actual and Expected error messages");
			System.out.println("Login error message(Attempt to login leaving the password blank) validated successfully");
			
                // Login with Empty  UserName and Password", "appropriate error message should be displayed
			
			actErrorMessage = HGfunctionObj.CaptureLoginErrorMessage("","");
			expErrorMessage = TextControls.emptyUNPWText;
			Assert.assertTrue(actErrorMessage.toLowerCase().trim().equals(expErrorMessage.toLowerCase().trim()), "Mismatch in Actual and Expected error messages");
			System.out.println("Login error message(Attempt to Login leaving username and Password fields blank) validated successfull");
			
			
			
		}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
	           
			Assert.fail(e.getMessage(), e);
			
		}
	}
	
	/** 
	 * TC-05  Verifies the details on login logout
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyDetailsOnLoginLogout() throws Exception{
		String testCase = "VerifyDetailsOnLoginLogout";
		String actUsername;
		try
		{
			launchweb();
			Common.Login();
			//"Verify details on Login", "User's account details should be displayed on Login");
            Assert.assertTrue(isElementPresent(HomeGlobalElements.footer), "Footer is not displayed on Login");
            Assert.assertTrue(isElementPresent(LoginElements.ladbrokesLogo), "Header(Ladbrokes Logo) is not displayed on Home page");

             //Check Hide/Unhide account details
          click(LoginElements.userNameAfterLogin, "Username is not displayed on Login");

             //Check elements on Login
            Assert.assertTrue(isElementPresent(LoginElements.userBalance), "Balance is not displayed on Login");
            Assert.assertTrue(isElementPresent(LoginElements.logOutBtn), "Logout link is not present (User is NOT Logged i)n");
            Assert.assertTrue(isElementPresent(LoginElements.loggedInTime), "User loggedin time is not displyed on Login");
            Assert.assertTrue(isElementPresent(LoginElements.depositBtn), "Deposit link is not displyed on Login");
            Assert.assertTrue(isElementPresent(HomeGlobalElements.myAccount), "My Account link is not displyed on Login");
            Assert.assertTrue(isElementPresent(HomeGlobalElements.HelpIcon), "Help link is not displyed on Login");

           Assert.assertFalse(isElementPresent(LoginElements.Username), "User name field is present in the header");
           Assert.assertFalse(isElementPresent(LoginElements.loginPassword), "Password field is present in the header");
           Assert.assertFalse(isElementPresent(LoginElements.Login), "Login button is present in the header");
           Assert.assertFalse(isElementPresent(LoginElements.JoinNow), "Join Now link is present in the header");
           Assert.assertFalse(isElementPresent(LoginElements.forgotLoginDetails), "Forgot Login details link is present in the header");
           Assert.assertFalse(isElementPresent(LoginElements.rememberMe), "Remember Me link is present in the header");
           

             //check for username and show/hide username
             
             actUsername = GetElementText(LoginElements.userNameAfterLogin);
             Assert.assertTrue(isElementPresent(LoginElements.userNameAfterLogin), "Username is displayed after Login");
             
            click(LoginElements.hideUserName, "Hide Username is not displyed on Login");
            Assert.assertTrue(getAttribute(LoginElements.userNameAfterLogin, "class").contains("hidden"));

            click(LoginElements.showUserName, "Show Username element is not displyed on clicking Hide Username");
            Assert.assertTrue(isElementPresent(LoginElements.hideUserName), "Hide Username failed to appear on clicking Show Username");
           
             //Check Hide/Unhide account details
            click(LoginElements.userNameAfterLogin, "Username is not displayed on Login");
           
            
           Common.logOut();
           Thread.sleep(4000);
           //Verify details on Logout", "User's account details should not be displayed on Logout");
           Assert.assertTrue(isElementPresent(HomeGlobalElements.footer), "Footer is not displayed on Logout");
           Assert.assertTrue(isElementPresent(LoginElements.ladbrokesLogo), "Header(Ladbrokes Logo) is not displayed on Home page");

            //Check elements on Logout
           
           Assert.assertFalse(isElementPresent(LoginElements.userBalance), "Balance is displayed on Logout");
           Assert.assertFalse(isElementPresent(LoginElements.logOutBtn), "Logout link is present (User is Logged in");
           Assert.assertFalse(isElementPresent(LoginElements.loggedInTime), "User loggedin time is displyed");
           Assert.assertFalse(isElementPresent(LoginElements.userNameAfterLogin), "Username is displyed on Logout");
           Assert.assertFalse(isElementPresent(LoginElements.depositBtn), "Deposit link is displyed on Logout");
           Assert.assertFalse(isElementPresent(HomeGlobalElements.myAccount), "My Account link is displyed on Logout");

           Assert.assertTrue(isElementPresent(LoginElements.Username), "User name field is not present in the header");
           Assert.assertTrue(isElementPresent(LoginElements.loginPassword), "Password field is not present in the header");
           Assert.assertTrue(isElementPresent(LoginElements.Login), "Login button is not present in the header");
           Assert.assertTrue(isElementPresent(LoginElements.JoinNow), "Join Now link is not present in the header");
           Assert.assertTrue(isElementPresent(LoginElements.forgotLoginDetails), "Forgot Login details link is not present in the header");
           Assert.assertTrue(isElementPresent(LoginElements.rememberMe), "Remember Me link is not present in the header");
           Assert.assertTrue(isElementPresent(HomeGlobalElements.HelpIcon), "Help link is not displyed after logout");
			
	}
catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 

Assert.fail(e.getMessage(), e);
}
}
	
	/** 
	 * TC-06  Verifies the details on login logout
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLinksOnHelp() throws Exception{
		String testCase = "VerifyLinksOnHelp";
		
		try
		{
			launchweb();
			Common.Login();
			
			// Verify 'Links on Help option on Login
            LoginFunctionObj.VerifyHelpLinks();
            
            //check for show/hide balance
           click(LoginElements.userBalance, "Balance is not displayed on Login");
           Assert.assertTrue(isElementPresent(By.xpath("//div[@class='balance']")), "Failed to hide the balance clicking on Hide balance");
           click(By.xpath("//div[@class='balance']"), "Show balance is not displayed to click");
           Assert.assertTrue(isElementPresent(LoginElements.userBalance), "Failed to unhide the balance clicking on Show balance");
          
           Common.logOut();
        // Verify 'Links on Help option on Logout
           LoginFunctionObj.VerifyHelpLinks();
            
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
	
	/** 
	 * TC-06  Verifies the login_ credit customer.
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLogin_CreditCustomer() throws Exception{
		String testCase = "VerifyLogin_CreditCustomer";
		String username = testdata.Readtestdata("Users", 14, 1);
		String password = testdata.Readtestdata("Users", 14, 2);
		try
		{
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
		    Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Credit User is not able to login");
			
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
	
	/** 
	 * TC-07  Verifies the Login_Elite Customer
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLogin_EliteCustomer() throws Exception{
		String testCase = "VerifyLogin_EliteCustomer";
		String username = testdata.Readtestdata("Users", 7, 1);
		String password = testdata.Readtestdata("Users", 7, 2);
		try
		{
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
		    Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "Elite User is not able to login");
			double bal = LoginFunctionObj.GetBalance();
			Assert.assertTrue(bal <= 0, "Elite Customer balance is not Zero");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
	/** 
	 * TC-08  Verifies the HVC Customer
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLogin_HVCCustomer() throws Exception{
		String testCase = "VerifyLogin_HVCCustomer";
		String username = testdata.Readtestdata("Users", 5, 1);
		String password = testdata.Readtestdata("Users", 5, 2);
		try
		{
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
		    Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			Assert.assertTrue(isElementDisplayed(LoginElements.depositBtn), "HVC User is not able to login ");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
	/** 
	 * TC-09  Verifies the login error message_ self exclusion.
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLoginErrorMessage_SelfExclusion() throws Exception{
		String testCase = "VerifyLoginErrorMessage_SelfExclusion";
		String username = testdata.Readtestdata("Users", 2, 1);
		String password = testdata.Readtestdata("Users", 2, 2);
		try
		{
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
		    Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			Assert.assertTrue(isElementDisplayed(LoginElements.selfExclErrorMsg), "HVC User is not able to login ");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
	
	/** 
	 * TC-10  Verifies the login error message_ self exclusion.
	 * @return none
	 * @throws Exception 
	 * @throws Exception element not found
	 */
	@Test	
	public void VerifyLoginErrorMessage_SuspendedUser() throws Exception{
		String testCase = "VerifyLoginErrorMessage_SuspendedUser";
		String username = testdata.Readtestdata("Users", 4, 1);
		String password = testdata.Readtestdata("Users", 4, 2);
		try
		{
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
		    Entervalue(LoginElements.Username , username);
			Entervalue(LoginElements.Password , password);
			click(LoginElements.Login);
			String errorMsg= "Sorry - We've had to freeze your account for security reasons. Please contact Customer Support for assistance and we'll help you get back up and running.";
			String value = "//p[text()="+"\"" + errorMsg + "\"" + "]";
			Assert.assertTrue(isElementDisplayed(By.xpath(value)), "HVC User is not able to login ");
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage(), e);
		}
}
}

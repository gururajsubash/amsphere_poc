package com.ladbrokes.testsuite;

import java.sql.Time;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.EnglishOR.*;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.*;

public class MyBetsTests extends DriverCommon{
	Common common= new Common();
	MyBetsFunctions MyBetsFunctions = new MyBetsFunctions();
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	TestdataFunction testdata = new TestdataFunction();
	HorseRacing_Functions HRfunctionObj = new HorseRacing_Functions();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	
	
	
	/** 
	 * TC-01 "Check Live and past accas from My Accas tab
       And Cash out from ACCAS"
	 * @return none
	 * @throws Exception 
	 * @throws NoSuchElementException 
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyAccaNotLoggedInUser() throws  Exception{
		
		String testCase = "VerifyAccaNotLoggedInUser";
	
    
		String couponTabActive;
        //List<String> events = new List()<String>;
        String[] eventNames = new String[2];
        String pr;
    
		try{
			launchweb();
			Common.OddSwitcher(TextControls.decimal);
			MyBetsFunctions.NavigateToMyBets("my accas");
			
			 Assert.assertTrue(isElementDisplayed(MyBetsControls.accaDescText), "Acca description text is not present");
             Assert.assertTrue(isElementDisplayed(MyBetsControls.accaAddBtn), "Add button is not present");
             Assert.assertTrue(isElementDisplayed(MyBetsControls.createAccaText), "Create new acca text is not present");
             click(MyBetsControls.accaAddBtn, "Acca add button is not clickable");
             
             couponTabActive = getAttribute(MyBetsControls.couponActiveTab, "class");
             if (!(couponTabActive.contains("active")))
              Assert.fail("Clicking on Acca add button is not taking the user to coupons page");
			
           //add selections from coupons page
             eventNames = MyBetsFunctions.NavigateAddSelectionFromCoupons(2);
           //Login and place bet
             Common.Login();
            common.BetPlacement_WithAcceptNContinue("double", "0.1", 2);
              pr = GetElementText(MyBetsControls.prReceiptValue);
              MyBetsFunctions.VerifyAccasPage(eventNames, true, pr, TextControls.stakeValue);
             
		}
		catch ( Exception | AssertionError  e){
			String screenShotPath = getScreenshot(testCase);
            
			//logger.log(LogStatus.FAIL, "VerifyAccaNotLoggedInUser");
			Assert.fail(e.getMessage() , e);
		}
	}
	
	/** 
	 * TC-02  Verifies the Acca switchers.
     *<TestCaseID>SSP-1170_003, SSP-1170_004, SSP-1170_007, SSP-1170_010, SSP-1170_020</TestCaseID>
	 * @return none
	 * @throws Exception 
	 * @throws NoSuchElementException 
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyAccaSwitchers() throws Exception
	{
		String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
		String accaText, couponTabActive, pr;
		String[] eventNames = new String[2];
      List<Date> accaDates = new ArrayList<>();
      List<Date> sortedDates = new ArrayList<>();
        String xpath = "//div[@class='acca-status']";

        try
        {
        	launchweb();
            Common.Login();
            MyBetsFunctions.NavigateToMyBets("my accas");

            click(MyBetsControls.accaDropDown);

            List <WebElement> accasCollDD = getDriver().findElements(MyBetsControls.settledAccasInDD);
           Assert.assertTrue(accasCollDD.size() >= 2, "There are no settled accas to verify the switches");
            accasCollDD.get(0).click();
           Assert.assertTrue(isElementPresent(MyBetsControls.hidePricesAcca), "Hide prices button is not displayed");

           Assert.assertTrue(isElementPresent(MyBetsControls.accaTitle), "Acca title is not present");
           Assert.assertTrue(isElementPresent(MyBetsControls.gamesValueAcca), "Games value is not present in table");
           Assert.assertTrue(isElementPresent(MyBetsControls.stakeValueAcca), "Stake value is not present in table");
           Assert.assertTrue(isElementPresent(MyBetsControls.prValueAcca), "PR is not present in table");

            click(MyBetsControls.hidePricesAcca);
            
            click(MyBetsControls.accaDropDown);
            
            accasCollDD.get(1).click();
            
          Assert.assertTrue(isElementPresent(MyBetsControls.showPricesAcca), "Show prices button is not displayed");
          click(MyBetsControls.accaDropDown);

            accasCollDD = getDriver().findElements(MyBetsControls.openAccasInDD);
            Assert.assertFalse(accasCollDD.size() > 20, "There are more than 20 accas displayed in dropdown");
           Assert.assertTrue(accasCollDD.size() >= 2, "There are no open accas to verify the switches");
            accasCollDD.get(0).click();
            Assert.assertTrue(isElementPresent(By.xpath(xpath)), "Acca is not loaded");
            click(MyBetsControls.accaDropDown);
            accasCollDD.get(1).click();
            Assert.assertTrue(isElementPresent(By.xpath(xpath)), "Acca is not loaded");

            click(MyBetsControls.accaDropDown);
            Assert.assertTrue(isElementPresent(MyBetsControls.newAccaInDD), "Create new acca button is not present in acca dropdown");
           click(MyBetsControls.newAccaInDD);

            couponTabActive = getAttribute(MyBetsControls.couponActiveTab, "class");
            if (!(couponTabActive.contains("active")))
              System.out.println("Clicking on Acca add button is not taking the user to coupons page");
            eventNames = MyBetsFunctions.NavigateAddSelectionFromCoupons(2);
            common.BetPlacement_WithAcceptNContinue("Double", TextControls.stakeValue, 2);
            pr = GetElementText(MyBetsControls.prReceiptValue);
            MyBetsFunctions.VerifyAccasPage(eventNames, false, pr, "0.10");

            MyBetsFunctions.NavigateToMyBets("my accas");
            click(MyBetsControls.accaDropDown);
            accasCollDD = getDriver().findElements(By.xpath("//span[@class='my-accas-dropdown-option']//span[@class='my-accas-dropdown-option-date']"));
            for (int i = 0; i < accasCollDD.size(); i++)
            {
            	String date = accasCollDD.get(i).getText();
                accaDates.add(Convert.toDate(date));
                sortedDates.add(Convert.toDate(date));
            }
            
            boolean ascendingOrder =  common.isSorted(sortedDates);
            Assert.assertTrue(ascendingOrder == false, "Dates are not sorted in descending order");
        }
        catch( Exception | AssertionError  e){
    		String screenShotPath = getScreenshot(testCase); 
    	    Assert.fail(e.getMessage() , e);
    		}
	}
	
	/** 
	 * TC-02 Check Open Bets & Settled bets Stake /Returns and history
	 * @return none
	 * @throws Exception 
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyHistoryCashoutLinksInMyBets() throws Exception{
		
		String testCase = "VerifyHistoryCashoutLinksInMyBets";
	
   
		try{
			launchweb();
			Common.Login();
			MyBetsFunctions.VerifyHistoryCashoutLinksInMyBets_function("open bets");
			MyBetsFunctions.VerifyHistoryCashoutLinksInMyBets_function("my accas");
			MyBetsFunctions.VerifyHistoryCashoutLinksInMyBets_function("settled bets");
			
		}
		catch ( Exception | AssertionError  e){String screenShotPath = getScreenshot(testCase); 
			
			Assert.fail(e.getMessage() ,e);
		}
    }
	
	
	/** 
	 * TC-05 Cash out from Open Bets tab
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOpenBets_SingleBet() throws Exception{
		
		String testCase = "VerifyOpenBets_SingleBet";
		
		String[] arayOdds = new String[1] ;
		String[] eventName = new String[1];
        String[] marketName = new String[1];
        String[] selection = new String[1];
        String[] typeName = new String[1];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 1, 2);
		 double totalPR = 0.0;
		try{
			
			launchweb();
			String mainwindow = getDriver().getWindowHandle();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			arayOdds[0] = testdata.NavigateUsingTestData(1);
			 Assert.assertTrue(isElementDisplayed(HomeGlobalElements.cashoutIcon), "Cashout Icon is not found on EDP");
			 click(HomeGlobalElements.cashoutIcon ,"Cashout icon on EDP is not clickable");
			 switchwindow();
		     String url = getDriver().getCurrentUrl();
		     Assert.assertTrue(url.contains("cashout"), "Failed to Switch to Cashout window");
		     getDriver().close();
			 getDriver().switchTo().window(mainwindow);
			
			 BTbetslipObj.VerifyBetSlip(eventName[0], selection[0], marketName[0], "", "", "single", 1, arayOdds);
			 BTbetslipObj.EnterStake(eventName[0], selection[0], marketName[0],TextControls.stakeValue, "single", false);
			 BTbetslipObj.VerifyBetDetails(eventName[0], selection[0], marketName[0], arayOdds, TextControls.stakeValue,"", "single", "","");
			 totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
			 BTbetslipObj.ValidateBetReceipt(typeName[0], eventName[0], marketName[0], selection[0], arayOdds, TextControls.stakeValue, "", false, 1,"single");
			 String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
             
             String BetTime = time.split(" ")[1];
             MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "single", BetTime);
             MyBetsFunctions.VerifyBetUnderOpenSettledBets("open bets", eventName, marketName, selection, arayOdds, TextControls.stakeValue, totalPR, TextControls.openStatusText, "Single", true);
             MyBetsFunctions.VerifyBetUnderOpenSettledBets( "settled bets", eventName, marketName, selection, arayOdds, TextControls.stakeValue, totalPR,TextControls.cashedOutText, "Single" , false);
             MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("open bets" ,eventName, selection, TextControls.stakeValue, totalPR, "single", BetTime);
            
             
}
		catch(AssertionError e){String screenShotPath = getScreenshot(testCase); 
			
			Assert.fail(e.getMessage() ,e);
			}
		}
	
	@Test
	public void VerifyAccasWithBetPlacement() throws Exception{
		String testCase = "VerifyAccasWithBetPlacement";
		String pr = null, prevPr = null, homeTeamName;
         int multiBetCount;
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			//Place two bets and add selections from coupons page
			 
			MyBetsFunctions.NavigateAddSelectionFromCoupons(4);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
            common.BetPlacement_WithAcceptNContinue("accumulator (4)", "0.10", multiBetCount);
            prevPr = GetElementText(MyBetsControls.receiptpotential);
            
           MyBetsFunctions.NavigateAddSelectionFromCoupons(4);
           multiBetCount = BTbetslipObj.GetMultiplesBetCount();
           common.BetPlacement_WithAcceptNContinue("accumulator (4)", "0.10", multiBetCount);
            pr = GetElementText(MyBetsControls.receiptpotential);
                
          //Verify show/hide markets
            MyBetsFunctions.NavigateToMyBets("My Accas");
           click(MyBetsControls.hidePricesAcca, "Hide prices button is not present");
            Thread.sleep(5000);
           Assert.assertFalse(isElementDisplayed(MyBetsControls.oddsInAcca), "Odds are not hidden when clicked on Hide prices");
           click(MyBetsControls.showPricesAcca, "Show prices button is not present");
           Assert.assertTrue(isElementDisplayed(MyBetsControls.oddsInAcca), "Odds are not shown when clicked on Show prices");

           click(MyBetsControls.accaDropDown, "Acca dropdown is not present in accas");
           Assert.assertTrue(isElementDisplayed(MyBetsControls.betTypeAccaDD), "Bet type column is not present in Acca dropdown area");
           Assert.assertTrue(isElementDisplayed(MyBetsControls.prAccaDD), "PR column is not present in Acca dropdown area");
           Assert.assertTrue(isElementDisplayed(MyBetsControls.dateAccaDD), "Bet date column is not present in Acca dropdown area");

            List <WebElement> accasCollDD = getDriver().findElements(MyBetsControls.accasInDD);
            accasCollDD.get(0).click();
            Thread.sleep(1000);
            Assert.assertTrue(isTextPresent(pr), "Second acca placed is not present in Acca Dropdown");
            click(MyBetsControls.accaDropDown, "Acca dropdown is not present in accas");
            accasCollDD.get(1).click();
            Thread.sleep(1000);
            Assert.assertTrue(isTextPresent(prevPr), "First acca placed is not present in Acca Dropdown");

            //Place bet from my accas
            click(By.xpath("//div[contains(text(),'Edit My Acca')]/following::div[@class='close-popup']"));
            Thread.sleep(2000);
            List <WebElement> oddsCollInAcca = getDriver().findElements(By.xpath("//div[@class='acca-event-selections']//span[@class='odds-convert']"));
            oddsCollInAcca.get(0).click();
            
            Assert.assertTrue(common.GetBetSlipCount() == 1, "Bet is not added from My accas page");
            Entervalue(BetslipElements.stakeBox,"0.10");
            Thread.sleep(2000);
            click(BetslipElements.placeBet, "Place bet button is not present");
            Assert.assertTrue(isElementDisplayed(BetslipElements.tickMarkOnBetslip), "Bet is not placed from my accas page");

            //Verify more links in accas
            MyBetsFunctions.NavigateToMyBets("my accas");
            homeTeamName = GetElementText(MyBetsControls.firstEventNameInAccas);
            List <WebElement> moreMarketsInAccas = getDriver().findElements(MyBetsControls.moreMarketsInAccas);
            moreMarketsInAccas.get(0).click();
            Assert.assertTrue(isElementDisplayed(By.xpath("//div[@class='basic-scoreboard']//span[@class='name' and contains(text(),'" + homeTeamName + "')]")), "More link sis not navigating to EDP");
		}
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	
	
	@Test
	public void VerifyBetNotOnAccas() throws Exception{
		String testCase = "VerifyBetNotOnAccas";
		String[] arryOdds = new String[2];
		int multiBetCount;
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			arryOdds[0] = testdata.NavigateUsingTestData(1);
			arryOdds[1] =  HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents" , 17, 1), testdata.Readtestdata("PreProdEvents" , 17, 2), testdata.Readtestdata("PreProdEvents" , 17, 4),testdata.Readtestdata("PreProdEvents" , 17, 6),testdata.Readtestdata("PreProdEvents" , 17, 5));
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			
			BTbetslipObj.VerifyBetSlip("","","","","","double", multiBetCount, arryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			BTbetslipObj.VerifyBetDetails("", "", "", arryOdds, TextControls.stakeValue,"", "double", "","");
			String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-" + testdata.Readtestdata("PreProdEvents", 17, 5);
			String eventSel =  testdata.Readtestdata("PreProdEvents", 1, 4)+ "-" + testdata.Readtestdata("PreProdEvents", 17, 4) ;
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, arryOdds, TextControls.stakeValue, "", false, multiBetCount, "Double");
		  
			MyBetsFunctions.NavigateToMyBets("my accas");
			List<WebElement> mybetHistroyItems = getDriver().findElements(By.xpath("//div[@class='acca-events-wrapper']"));
			 Assert.assertFalse(mybetHistroyItems.get(0).getText().contains(testdata.Readtestdata("PreProdEvents", 1, 4)) , "Non acca bet is displayed under Accas");
		}
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	
	@Test
	public void VerifyCashout_DoubleBet() throws Exception{
		String testCase = "VerifyCashout_DoubleBet";
		String[] arryOdds = new String[2];
		int multiBetCount;
		double totalPR = 0.0;
		String[] eventName = new String[2];
        String[] marketName = new String[2];
        String[] selection = new String[2];
        String[] typeName = new String[2];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   eventName[1] = testdata.Readtestdata("PreProdEvents", 2, 4);
	   selection[1] = testdata.Readtestdata("PreProdEvents", 2, 5);
	   marketName[1] = testdata.Readtestdata("PreProdEvents", 2, 6);
	   typeName[1] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   
		try{
			
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			arryOdds[0] = testdata.NavigateUsingTestData(1);
			arryOdds[1] = testdata.NavigateUsingTestData(2);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("","","","","","double", multiBetCount, arryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			BTbetslipObj.VerifyBetDetails("", "", "", arryOdds, TextControls.stakeValue,"", "double", "","");
			totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
			String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-" + testdata.Readtestdata("PreProdEvents", 2, 5);
			String eventSel =  testdata.Readtestdata("PreProdEvents", 1, 4)+ "-" + testdata.Readtestdata("PreProdEvents", 2, 4) ;
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, arryOdds, TextControls.stakeValue, "", false, multiBetCount, "Double");
			String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
            String BetTime = time.split(" ")[1];
            
            MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "Double", BetTime);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets("open bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR, TextControls.openStatusText, "Double", true);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets( "settled bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR,"Cashed Out", "Double" , false);
            
       }
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	@Test
	public void VerifyCashout_SingleBet() throws Exception{
		String testCase = "VerifyCashout_SingleBet";
		String[] arryOdds = new String[1];
		double totalPR = 0.0;
		String[] eventName = new String[1];
        String[] marketName = new String[1];
        String[] selection = new String[1];
        String[] typeName = new String[1];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   
		try{
			
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			arryOdds[0] = testdata.NavigateUsingTestData(1);	
			BTbetslipObj.VerifyBetSlip(eventName[0], selection[0], marketName[0], "", "", "single", 1, arryOdds);
			BTbetslipObj.EnterStake(eventName[0], selection[0], marketName[0],TextControls.stakeValue, "single", false);
			BTbetslipObj.VerifyBetDetails(eventName[0], selection[0], marketName[0], arryOdds, TextControls.stakeValue,"", "single", "","");
			totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
		    BTbetslipObj.ValidateBetReceipt(typeName[0], eventName[0], marketName[0], selection[0], arryOdds, TextControls.stakeValue, "", false, 1,"single");
			 String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
            
            String BetTime = time.split(" ")[1];
            MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "single", BetTime);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets("open bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR, TextControls.openStatusText, "Single", true);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets( "settled bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR,"Cashed Out", "Single" , false);
	       }
			catch(AssertionError e){
				String screenShotPath = getScreenshot(testCase); 
				Assert.fail(e.getMessage() ,e);
				}
		}
	
	
	@Test
	public void VerifyCashoutInPlayUpcomingEvents() throws Exception{
		String testCase = "VerifyCashoutInPlayUpcomingEvents";
		String[] eventNames = new String[2];
		String pr;
           try{
			
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			eventNames = MyBetsFunctions.NavigateAddSelectionFromInplayModule(2, "Football");
			/*if(isElementPresent(BetslipElements.oddsBoostTooltipInfo))
	        	   click(BetslipElements.oddsBoostTooltipIcon);*/
			int multiBetCount = BTbetslipObj.GetMultiplesBetCount();
            common.BetPlacement_WithAcceptNContinue("Double", "0.1", multiBetCount);
            Thread.sleep(2000);
            pr = GetElementText(By.xpath("//div[@class='slip-total-potentials']//div[@class='amount']"));
            
          //Navigate to accas and verify bets
            MyBetsFunctions.NavigateToMyBets("My Accas");
           Assert.assertTrue(isElementPresent(By.xpath("//div[@class='acca-event-status-text']")), "In-play status text is not present");
           Assert.assertTrue(isElementPresent(By.xpath("//div[contains(@class,'acca-event-status-icon')]")), "In-play status icon is not present");
           
           MyBetsFunctions.VerifyAccasPage(eventNames, true, pr, "0.10");
 }
         catch(AssertionError e){
	           String screenShotPath = getScreenshot(testCase); 
	           Assert.fail(e.getMessage() ,e);
	             }
               }
	
	@Test
	public void VerifyLogoutJourney() throws Exception{
		String testCase = "VerifyLogoutJourney";
     try{
			launchweb();
			Common.Login();
			MyBetsFunctions.NavigateToMyBets("open bets");
			Assert.assertFalse(isElementPresent(MyBetsControls.openBetsInfoMsg), "Login Info message is displayed in Open Bets when users is logged in");
			Common.logOut();
			MyBetsFunctions.NavigateToMyBets("open bets");
			Assert.assertTrue(isElementPresent(MyBetsControls.openBetsInfoMsg), "Login Info message is NOT displayed in Open Bets when users is not logged in");
          }
        catch(AssertionError e){
        String screenShotPath = getScreenshot(testCase); 
        Assert.fail(e.getMessage() ,e);
        }
      }

	
	@Test
	public void VerifyOpenBets_DoubleBet() throws Exception{
		String testCase = "VerifyOpenBets_DoubleBet";
		String[] arryOdds = new String[2];
		int multiBetCount;
		double totalPR = 0.0;
		String[] eventName = new String[2];
        String[] marketName = new String[2];
        String[] selection = new String[2];
        String[] typeName = new String[2];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   eventName[1] = testdata.Readtestdata("PreProdEvents", 2, 4);
	   selection[1] = testdata.Readtestdata("PreProdEvents", 2, 5);
	   marketName[1] = testdata.Readtestdata("PreProdEvents", 2, 6);
	   typeName[1] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   
		try{
			
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			arryOdds[0] = testdata.NavigateUsingTestData(1);
			arryOdds[1] = testdata.NavigateUsingTestData(2);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("","","","","","double", multiBetCount, arryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			BTbetslipObj.VerifyBetDetails("", "", "", arryOdds, TextControls.stakeValue,"", "double", "","");
			totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
			String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-" + testdata.Readtestdata("PreProdEvents", 2, 5);
			String eventSel =  testdata.Readtestdata("PreProdEvents", 1, 4)+ "-" + testdata.Readtestdata("PreProdEvents", 2, 4) ;
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, arryOdds, TextControls.stakeValue, "", false, multiBetCount, "Double");
			String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
            String BetTime = time.split(" ")[1];
            
            MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "Double", BetTime);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets("open bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR, TextControls.openStatusText, "Double", true);
            MyBetsFunctions.VerifyBetUnderOpenSettledBets( "settled bets", eventName, marketName, selection, arryOdds, TextControls.stakeValue, totalPR,"Cashed Out", "Double" , false);
            MyBetsFunctions.VerifyBetDroppedFromOpenSettledBets("open bets" ,eventName, selection, TextControls.stakeValue, totalPR, "Double", BetTime);
       }
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	
	@Test
	public void VerifyShowMoreInOpenSettledBets() throws Exception{
		String testCase = "VerifyShowMoreInOpenSettledBets";
     try{
			launchweb();
			Common.Login();
			MyBetsFunctions.VerifyShowMoreInOpenSettledBets_function("settled bets");
			MyBetsFunctions.VerifyShowMoreInOpenSettledBets_function("open bets");
		}
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	
	@Test
	public void VerifyTabsInMyBets() throws Exception{
		String testCase = "VerifyTabsInMyBets";
     try{
			launchweb();
			MyBetsFunctions.NavigateToMyBets("My Accas");
            Assert.assertTrue(isElementPresent(MyBetsControls.myAccasInfoMsg), "Acca Info message is NOT displayed in My Accas when users is not logged in");
            Assert.assertTrue(isElementPresent(MyBetsControls.createNewAcca), "Create New Accas is NOT displayed in My Accas when users is not logged in");

            MyBetsFunctions.NavigateToMyBets("open bets");
            Assert.assertTrue(isElementPresent(MyBetsControls.openBetsInfoMsg), "Login Info message is NOT displayed in Open Bets when users is not logged in");
             Assert.assertFalse(isElementPresent(MyBetsControls.accountHistoryText), "Account History Info message is displayed in Settled Bets when user is NOT logged in");
             Assert.assertFalse(isElementPresent(MyBetsControls.cashoutTermsAndCoText), "Cashout terms & conditions message is displayed in Settled Bets when user is NOT logged in");

             MyBetsFunctions.NavigateToMyBets("settled bets");
             Assert.assertTrue(isElementPresent(MyBetsControls.settledBetsInfoMsg), "Login Info message is NOT displayed in Settled Bets when users is not logged in");
             Assert.assertFalse(isElementPresent(MyBetsControls.accountHistoryText), "Account History Info message is displayed in Settled Bets when user is NOT logged in");
             Assert.assertFalse(isElementPresent(MyBetsControls.cashoutTermsAndCoText), "Cashout terms & conditions message is displayed in Settled Bets when user is NOT logged in");

            // Verify the Info message is not displayed for all the tabs in My Bets when user is logged in
             Common.Login();
         
            MyBetsFunctions.NavigateToMyBets("My Accas");
            Assert.assertFalse(isElementPresent(MyBetsControls.myAccasInfoMsg), "Acca Info message is displayed in My Accas when users is logged in");
            Assert.assertFalse(isElementPresent(MyBetsControls.createNewAcca), "Create New Accas is displayed in My Accas when users is logged in");

            MyBetsFunctions.NavigateToMyBets("open bets");
            Assert.assertFalse(isElementPresent(MyBetsControls.openBetsInfoMsg), "Login Info message is displayed in Open Bets when users is logged in");
            Assert.assertTrue(isElementPresent(MyBetsControls.accountHistoryText), "Account History Info message is NOT displayed in Settled Bets when user is logged in");
            Assert.assertTrue(isElementPresent(MyBetsControls.cashoutTermsAndCoText), "Cashout terms & conditions message is NOT displayed in openBets when user is logged in");

             MyBetsFunctions.NavigateToMyBets("settled bets");
             Assert.assertFalse(isElementPresent(MyBetsControls.settledBetsInfoMsg), "Login Info message is displayed in Settled Bets when users is logged in");
             Assert.assertTrue(isElementPresent(MyBetsControls.accountHistoryText), "Account History Info message is NOT displayed in Settled Bets when user is logged in");
      
     }
		catch(AssertionError e){
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage() ,e);
			}
	}
	
	
}

package com.ladbrokes.testsuite;

import java.io.IOException;
import java.util.Arrays;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.Utils.ExcelReportGenerator;
import com.ladbrokes.Utils.TestRail;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.SendMail;

public class SBP1Desktop extends DriverCommon {

	Common common = new Common();
	BetSlipTests betSlipTests = new BetSlipTests();
	EventDetailsPageTests EDPTests = new EventDetailsPageTests();
	FootballTests FBTests = new FootballTests();
	GreyHoundsTests GHTests = new GreyHoundsTests();
	HomeAndGlobalTests HGTests = new HomeAndGlobalTests();
	HorseRacingTests HRTests = new HorseRacingTests();
	LoginLogoutTests LogInOutTests = new LoginLogoutTests();
	MyBetsTests MyBetsTests = new MyBetsTests();
	ExcelReportGenerator ERG = new ExcelReportGenerator();
	OddsBoostTests OddsBoostTests = new OddsBoostTests();
	ResultsTests ResultsTests = new ResultsTests();
	SegmentedBannersTests SBTests = new SegmentedBannersTests();
	PrivateMarketTests PMtests = new PrivateMarketTests();
	TestRail TestRail = new TestRail();
	SendMail SendMail = new SendMail();
	CashOutTests CashOutTests = new CashOutTests();

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyBalanceTopHeaderChanges() throws Exception {
		caseID = Arrays.asList(811020);
		betSlipTests.VerifyBalanceTopHeaderChanges();

	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyCouponLandingPage_SingleBet() throws Exception {
		caseID = Arrays.asList(811214);
		betSlipTests.VerifyCouponLandingPage_SingleBet();

	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyCouponLandingPage_MultipleBet() throws Exception {
		caseID = Arrays.asList(811214);
		betSlipTests.VerifyCouponLandingPage_MultipleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyFreeBetForAnySport_1() throws Exception {
		caseID = Arrays.asList(811027);
		betSlipTests.VerifyFreeBetForAnySport();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyFreeBetForAnySport_2() throws Exception {
		caseID = Arrays.asList(811031);
		betSlipTests.VerifyFreeBetForAnySport();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyFreeBetForSpecificSport_1() throws Exception {
		caseID = Arrays.asList(811028);
		betSlipTests.VerifyFreeBetForSpecificSport();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyFreeBetForSpecificSport_2() throws Exception {
		caseID = Arrays.asList(811032);
		betSlipTests.VerifyFreeBetForSpecificSport();
	}

	@Test(priority = 74, groups = { "P1", "MyBetsTests" })
	public void VerifyHistoryCashoutLinksInMyBets() throws Exception {
		caseID = Arrays.asList(811223);
		MyBetsTests.VerifyHistoryCashoutLinksInMyBets();
	}

	@Test(priority = 74, groups = { "P1", "MyBetsTests", "P0" })
	public void VerifyOpenBets_SingleBet() throws Exception {
		caseID = Arrays.asList(811222);
		MyBetsTests.VerifyOpenBets_SingleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifySpecificCouponPage_MultipleBet() throws Exception {
		caseID = Arrays.asList(811169);
		betSlipTests.VerifySpecificCouponPage_MultipleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifySpecificCouponPage_SingleBet() throws Exception {
		caseID = Arrays.asList(811169);
		betSlipTests.VerifySpecificCouponPage_SingleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifySpecificCouponPage_SwitcherDropdown_MultipleBet() throws Exception {
		caseID = Arrays.asList(811215);
		betSlipTests.VerifySpecificCouponPage_SwitcherDropdown_MultipleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyTotalStakeTotalPR_ByChangingStake_1() throws Exception {
		caseID = Arrays.asList(811018);
		betSlipTests.VerifyTotalStakeTotalPR_ByChangingStake();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyTotalStakeTotalPR_ByChangingStake_2() throws Exception {
		caseID = Arrays.asList(811019);
		betSlipTests.VerifyTotalStakeTotalPR_ByChangingStake();
	}

	@Test(priority = 74, groups = { "Missing", "BetSlip" })
	public void VerifyTrebleBetPlacement() throws Exception {
		caseID = Arrays.asList(811088);
		betSlipTests.VerifyTrebleBetPlacement();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyTennisTypePagePlaceBet() throws Exception {
		caseID = Arrays.asList(811002);
		betSlipTests.VerifyTennisTypePagePlaceBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyBasketballsubTypePagePlaceBet() throws Exception {
		caseID = Arrays.asList(811003);
		betSlipTests.VerifyBasketballsubTypePagePlaceBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyAnySportsEDPPlaceBet() throws Exception {
		caseID = Arrays.asList(811004);
		betSlipTests.VerifyAnySportsEDPPlaceBet();

	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyBetPlacementWithInsufficientBalance() throws Exception {
		caseID = Arrays.asList(811008);
		betSlipTests.VerifyBetPlacementWithInsufficientBalance();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyBetplacementHomePageSingleBet() throws Exception {
		caseID = Arrays.asList(811013);
		betSlipTests.VerifyBetplacementHomePageSingleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyDoubleOnLiveEvent() throws Exception {
		caseID = Arrays.asList(811025);
		betSlipTests.VerifyDoubleOnLiveEvent();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyDoubleBetPlacement_1() throws Exception {
		caseID = Arrays.asList(811014);
		betSlipTests.VerifyDoubleBetPlacement();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyDoubleBetPlacement_2() throws Exception {
		caseID = Arrays.asList(811085);
		betSlipTests.VerifyDoubleBetPlacement();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyDoubleBetPlacement_3() throws Exception {
		caseID = Arrays.asList(811088);
		betSlipTests.VerifyDoubleBetPlacement();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyReUseMultiSels() throws Exception {
		caseID = Arrays.asList(811015);
		betSlipTests.VerifyReUseMultiSels();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyEWBetPlacement_SingleBet() throws Exception {
		caseID = Arrays.asList(811024);
		betSlipTests.VerifyEWBetPlacement_SingleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyEWBetPlacement_DoubleBet() throws Exception {
		caseID = Arrays.asList(811026);
		betSlipTests.VerifyEWBetPlacement_DoubleBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacement_SP() throws Exception {

		caseID = Arrays.asList(1992296);

		betSlipTests.VerifyBetPlacement_SP();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyFractionalToDecimalOnAllModulesOnBetSlip() throws Exception {
		caseID = Arrays.asList(811110);
		betSlipTests.VerifyFractionalToDecimalOnAllModulesOnBetSlip();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyNavigationToInPlayModuleBelowBetslip() throws Exception {
		caseID = Arrays.asList(811201);
		betSlipTests.VerifyNavigationToInPlayModuleBelowBetslip();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyMinMaxStake_StakefieldLength_functionality() throws Exception {
		caseID = Arrays.asList(811021);
		betSlipTests.VerifyMinMaxStake_StakefieldLength_functionality();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyInplaySingleBet_1() throws Exception {
		caseID = Arrays.asList(811023);
		betSlipTests.VerifyInplaySingleBet();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyInplaySingleBet_2() throws Exception {
		caseID = Arrays.asList(811086);
		betSlipTests.VerifyInplaySingleBet();
	}
	
	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyInplaySingleBet_3() throws Exception {
		caseID = Arrays.asList(811107);
		betSlipTests.VerifyInplaySingleBet();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip", "P0" })
	public void VerifyRaceCardPageBetPlacement() throws Exception {
		caseID = Arrays.asList(811005);
		betSlipTests.VerifyRaceCardPageBetPlacement();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyRemoveSelections() throws Exception {
		caseID = Arrays.asList(811016);
		betSlipTests.VerifyRemoveSelections();
	}

	@Test(priority = 74, groups = { "P1", "BetSlip" })
	public void VerifyRemoveAllButtonInBetSLip() throws Exception {
		caseID = Arrays.asList(811017);
		betSlipTests.VerifyRemoveAllButtonInBetSLip();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage" })
	public void VerifyContextualMenuInInplayForEventDetailofDifferentSports_1() throws Exception {
		caseID = Arrays.asList(811109);
		EDPTests.VerifyContextualMenuInInplayForEventDetailofDifferentSports();
	}
	
	@Test(priority = 74, groups = { "P1", "EventDetailsPage" })
	public void VerifyContextualMenuInInplayForEventDetailofDifferentSports_2() throws Exception {
		caseID = Arrays.asList(811210);
		EDPTests.VerifyContextualMenuInInplayForEventDetailofDifferentSports();
	}
	
	@Test(priority = 74, groups = { "P1", "EventDetailsPage" })
	public void VerifyContextualMenuInInplayForEventDetailofDifferentSports_3() throws Exception {
		caseID = Arrays.asList(811211);
		EDPTests.VerifyContextualMenuInInplayForEventDetailofDifferentSports();
	}
	
	@Test(priority = 74, groups = { "P1", "EventDetailsPage" })
	public void VerifyContextualMenuInInplayForEventDetailofDifferentSports_4() throws Exception {
		caseID = Arrays.asList(811213);
		EDPTests.VerifyContextualMenuInInplayForEventDetailofDifferentSports();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage", "P0" })
	public void VerifyNavigationToCricketDiffEventSubTypes() throws Exception {
		caseID = Arrays.asList(811175);
		EDPTests.VerifyNavigationToCricketDiffEventSubTypes();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage", "P0" })
	public void VerifyNavigationToRugbyLeagueDiffEventSubTypes() throws Exception {
		caseID = Arrays.asList(811177);
		EDPTests.VerifyNavigationToRugbyLeagueDiffEventSubTypes();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage", "P0" })
	public void VerifyNavigationToGolfDiffEventSubTypes() throws Exception {
		caseID = Arrays.asList(811182);
		EDPTests.VerifyNavigationToGolfDiffEventSubTypes();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage", "P0" })
	public void VerifyNavigationToBasketballDiffEventSubTypes() throws Exception {
		caseID = Arrays.asList(811184);
		EDPTests.VerifyNavigationToBasketballDiffEventSubTypes();
	}

	@Test(priority = 74, groups = { "P1", "EventDetailsPage" })
	public void VerifyPopularMarketOnInplayEventDetailPage() throws Exception {
		caseID = Arrays.asList(811082);
		EDPTests.VerifyPopularMarketOnInplayEventDetailPage();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyBetslipOnDetailPage() throws Exception {
		caseID = Arrays.asList(1992311);
		EDPTests.VerifyBetslipOnDetailPage();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyHierarchyOfTypeAndSubTypeEventsOnContextualMenu() throws Exception {
		caseID = Arrays.asList(1992327);
		EDPTests.VerifyHierarchyOfTypeAndSubTypeEventsOnContextualMenu();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyUpcomingEventTitleOnEventDetailPage() throws Exception {
		caseID = Arrays.asList(1992371);
		EDPTests.VerifyUpcomingEventTitleOnEventDetailPage();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyTypeAndSubTypeInEventDetailPage() throws Exception {
		caseID = Arrays.asList(1992370);
		EDPTests.VerifyTypeAndSubTypeInEventDetailPage();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyDefaultSelectedSportExpanded() throws Exception {
		caseID = Arrays.asList(1992317);
		EDPTests.VerifyDefaultSelectedSportExpanded();
	}

	@Test(priority = 74, groups = { "P3P4", "EventDetailsPage" })
	public void VerifyBreadcrumbAfterCollapsingAndExpandingContextualMenu() throws Exception {
		caseID = Arrays.asList(1992313);
		EDPTests.VerifyBreadcrumbAfterCollapsingAndExpandingContextualMenu();
	}

	@Test(priority = 43, groups = { "P1", "GreyHounds" })
	public void VerifyEventNavContextualMenuForTodayEvent_GH() throws Exception {
		caseID = Arrays.asList(811212);
		GHTests.VerifyEventNavContextualMenuForTodayEvent_GH();
	}

	@Test(priority = 44, groups = { "P1", "GreyHounds", "P0" })
	public void VerifyGHEventBuildRaceCard() throws Exception {
		caseID = Arrays.asList(811157);
		GHTests.VerifyGHEventBuildRaceCard();
	}

	@Test(priority = 45, groups = { "P1", "GreyHounds" })
	public void VerifyGreyhoundsCombinationTricastPlaceBet() throws Exception {
		caseID = Arrays.asList(811159);
		GHTests.VerifyGreyhoundsCombinationTricastPlaceBet();
	}

	@Test(priority = 46, groups = { "P1", "GreyHounds", "P0" })
	public void VerifyGreyhoundsForecastPlaceBet() throws Exception {
		caseID = Arrays.asList(811159);
		GHTests.VerifyGreyhoundsForecastPlaceBet();
	}

	@Test(priority = 47, groups = { "P1", "GreyHounds" })
	public void VerifyGreyHoundsRacingLandingPage() throws Exception {
		caseID = Arrays.asList(811156);
		GHTests.VerifyGreyHoundsRacingLandingPage();
	}

	@Test(priority = 48, groups = { "Missing", "GreyHounds" })
	public void VerifyGreyhoundsReverseForecastPlaceBet() throws Exception {
		caseID = Arrays.asList(811159);
		GHTests.VerifyGreyhoundsReverseForecastPlaceBet();
	}

	@Test(priority = 49, groups = { "Missing", "GreyHounds" })
	public void VerifyGreyhoundsTrapWinnerPlaceBet() throws Exception {
		caseID = Arrays.asList(811159);
		GHTests.VerifyGreyhoundsTrapWinnerPlaceBet();
	}

	@Test(priority = 50, groups = { "Missing", "GreyHounds", "P0" })
	public void VerifyGreyhoundsTricastPlaceBet() throws Exception {
		caseID = Arrays.asList(811159);
		GHTests.VerifyGreyhoundsTricastPlaceBet();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyAZSportsLink() throws Exception {
		caseID = Arrays.asList(811199);
		HGTests.VerifyAZSportsLink();
	}

	@Test(alwaysRun = true, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyChooseMyStartPage() throws Exception {
		caseID = Arrays.asList(811111);
		HGTests.VerifyChooseMyStartPage();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyCountdownMessageAndTimerForNonRacingInplayEvents() throws Exception {
		caseID = Arrays.asList(811078);
		HGTests.VerifyCountdownMessageAndTimerForNonRacingInplayEvents();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyCustomerEditAndViewDisplaySettings() throws Exception {

		caseID = Arrays.asList(811083);

		HGTests.VerifyCustomerEditAndViewDisplaySettings();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyFooterOnHomePage() throws Exception {

		caseID = Arrays.asList(811079);

		HGTests.VerifyFooterOnHomePage();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyHeaderLinks() throws Exception {

		caseID = Arrays.asList(811143);

		HGTests.VerifyHeaderLinks();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyLiveChat() throws Exception {
		caseID = Arrays.asList(811010);
		HGTests.VerifyLiveChat();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal", "P0" })
	public void VerifyShowmoreAndShowLessOnInplayLP() throws Exception {
		caseID = Arrays.asList(811108);
		HGTests.VerifyShowmoreAndShowLessOnInplayLP();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyUpComingMatchesOnSportsPage() throws Exception {
		caseID = Arrays.asList(811200);
		HGTests.VerifyUpComingMatchesOnSportsPage();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyNavigation_Login_NonSportsSite_1() throws Exception {
		caseID = Arrays.asList(811006);
		HGTests.VerifyNavigation_Login_NonSportsSite();
	}
	
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyNavigation_Login_NonSportsSite_2() throws Exception {
		caseID = Arrays.asList(811081);
		HGTests.VerifyNavigation_Login_NonSportsSite();
	}

	@Test(priority = 1, groups = { "P1", "HorseRacing" })
	public void VerifyHorseRacingCombinationTricastPlaceBet() throws Exception {
		caseID = Arrays.asList(811150);
		HRTests.VerifyHorseRacingCombinationTricastPlaceBet();
	}

	@Test(priority = 2, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingForecastPlaceBet() throws Exception {
		caseID = Arrays.asList(811150);
		HRTests.VerifyHorseRacingForecastPlaceBet();
	}

	@Test(priority = 3, groups = { "P1", "HorseRacing", "P0" })
	public void VerifyHorseRacingLandingPage() throws Exception {
		caseID = Arrays.asList(811145);
		HRTests.VerifyHorseRacingLandingPage();
	}

	@Test(priority = 4, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingReverseForecastPlaceBet() throws Exception {
		caseID = Arrays.asList(811150);
		HRTests.VerifyHorseRacingReverseForecastPlaceBet();
	}

	@Test(priority = 5, groups = { "Missing", "HorseRacing", "P0" })
	public void VerifyHorseRacingTricastPlaceBet() throws Exception {
		caseID = Arrays.asList(811150);
		HRTests.VerifyHorseRacingTricastPlaceBet();
	}

	@Test(priority = 6, groups = { "Missing", "HorseRacing", "P0" })
	public void VerifyHRBetPlacement_SP() throws Exception {
		caseID = Arrays.asList(811150);
		HRTests.VerifyHRBetPlacement_SP();
	}

	@Test(priority = 7, groups = { "P1", "HorseRacing", "P0" })
	public void VerifyHREventBuildRaceCard() throws Exception {
		caseID = Arrays.asList(811146);
		HRTests.VerifyHREventBuildRaceCard();
	}

	@Test(priority = 8, groups = { "P1", "HorseRacing" })
	public void VerifyHRSelctionInfoBetSlip() throws Exception {
		caseID = Arrays.asList(811022);
		HRTests.VerifyHRSelctionInfoBetSlip();
	}

	@Test(priority = 74, groups = { "P1", "LoginLogout" })
	public void VerifyForgotLoginDetailsLink() throws Exception {
		caseID = Arrays.asList(811142);
		LogInOutTests.VerifyForgotLoginDetailsLink();
	}

	@Test(priority = 74, groups = { "P1", "LoginLogout" })
	public void VerifyLogin() throws Exception {
		caseID = Arrays.asList(811000);
		LogInOutTests.VerifyLogin();
	}

	@Test(priority = 74, groups = { "P1", "LoginLogout" })
	public void VerifyLoginErrorMessages() throws Exception {
		caseID = Arrays.asList(811007);
		LogInOutTests.VerifyLoginErrorMessages();
	}

	@Test(priority = 74, groups = { "Missing", "LoginLogout" })
	public void VerifyRememberMe() throws Exception {
		caseID = Arrays.asList(811142);
		LogInOutTests.VerifyRememberMe();
	}

	@Test(priority = 74, groups = { "P1", "Football" })
	public void VerifyFootballLandingPage() throws Exception {
		caseID = Arrays.asList(811165);
		FBTests.VerifyFootballLandingPage();
	}

	@Test(priority = 74, groups = { "P1", "Football", "P0" })
	public void VerifyFootballLandingPagePlaceBet() throws Exception {
		caseID = Arrays.asList(811001);
		FBTests.VerifyFootballLandingPagePlaceBet();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyMarketSwitcherInCoupons() throws Exception {
		caseID = Arrays.asList(1992342);
		FBTests.VerifyMarketSwitcherInCoupons();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyMoreOptionOnEventType() throws Exception {
		caseID = Arrays.asList(1992343);
		FBTests.VerifyMoreOptionOnEventType();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyMoreTabOnFootbalPage() throws Exception {
		caseID = Arrays.asList(1992345);
		FBTests.VerifyMoreTabOnFootbalPage();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyShowMoreEvents() throws Exception {
		caseID = Arrays.asList(1992366);
		FBTests.VerifyShowMoreEvents();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyOutrightMarketsOnFBSubtypePage() throws Exception {
		caseID = Arrays.asList(1992357);
		FBTests.VerifyOutrightMarketsOnFBSubtypePage();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyNavigationToDiffEventSubTypes() throws Exception {
		caseID = Arrays.asList(1992350);
		FBTests.VerifyNavigationToDiffEventSubTypes();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyInplayEventTitleOnFootballPageRHS() throws Exception {
		caseID = Arrays.asList(1992330);
		FBTests.VerifyInplayEventTitleOnFootballPageRHS();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyEventTypePageDetails() throws Exception {
		caseID = Arrays.asList(1992324);
		FBTests.VerifyEventTypePageDetails();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyEventDetailPageNavigationForClassType() throws Exception {
		caseID = Arrays.asList(1992323);
		FBTests.VerifyEventDetailPageNavigationForClassType();
	}

	@Test(priority = 74, groups = { "P3P4", "Football" })
	public void VerifyETHighlighted() throws Exception {
		caseID = Arrays.asList(1992319);
		FBTests.VerifyETHighlighted();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_TrixieBet() throws Exception {
		caseID = Arrays.asList(1992309);
		betSlipTests.VerifyBetplacement_TrixieBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_PatentBet() throws Exception {
		caseID = Arrays.asList(1992307);
		betSlipTests.VerifyBetplacement_PatentBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_YankeeBet() throws Exception {
		caseID = Arrays.asList(1992310);
		betSlipTests.VerifyBetplacement_YankeeBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_FourfoldAccumulatorBet() throws Exception {
		caseID = Arrays.asList(1992303);
		betSlipTests.VerifyBetplacement_FourfoldAccumulatorBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_FivefoldAccumulatorBet() throws Exception {
		caseID = Arrays.asList(1992302);
		betSlipTests.VerifyBetplacement_FivefoldAccumulatorBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip", })
	public void VerifyBetplacement_HeinzBet() throws Exception {
		caseID = Arrays.asList(1992304);
		betSlipTests.VerifyBetplacement_HeinzBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_Lucky15Bet() throws Exception {
		caseID = Arrays.asList(1992305);
		betSlipTests.VerifyBetplacement_Lucky15Bet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_Lucky63Bet() throws Exception {
		caseID = Arrays.asList(1992306);
		betSlipTests.VerifyBetplacement_Lucky63Bet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacement_SixfoldAccumulatorBet() throws Exception {
		caseID = Arrays.asList(1992308);
		betSlipTests.VerifyBetplacement_SixfoldAccumulatorBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacement_GreyHoundMultiBet() throws Exception {
		caseID = Arrays.asList(1992294);
		betSlipTests.VerifyBetPlacement_GreyHoundMultiBet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacement_SP_Double() throws Exception {
		caseID = Arrays.asList(1992297);
		betSlipTests.VerifyBetPlacement_SP_Double();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip", "P0" })
	public void VerifyBetPlacement_Treble_OnLiveEvent() throws Exception {
		caseID = Arrays.asList(1992298);
		betSlipTests.VerifyBetPlacement_Treble_OnLiveEvent();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip", "P0", })
	public void VerifyBetPlacement_Trixie_OnLiveEvent() throws Exception {
		caseID = Arrays.asList(1992299);
		betSlipTests.VerifyBetPlacement_Trixie_OnLiveEvent();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacement_Patent_OnLiveEvent() throws Exception {
		caseID = Arrays.asList(1992295);
		betSlipTests.VerifyBetPlacement_Patent_OnLiveEvent();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacement_WinOrEachWay_EWnonEW() throws Exception {
		caseID = Arrays.asList(1992300);
		betSlipTests.VerifyBetPlacement_WinOrEachWay_EWnonEW();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetPlacementWithDiffMarketsOfSubtypePage() throws Exception {
		caseID = Arrays.asList(1992293);
		betSlipTests.VerifyBetPlacementWithDiffMarketsOfSubtypePage();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyBetplacementWoLoginErrorMsg() throws Exception {
		caseID = Arrays.asList(1992301);
		betSlipTests.VerifyBetplacementWoLoginErrorMsg();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyErrorMsg_20Sels() throws Exception {
		caseID = Arrays.asList(1992321);
		betSlipTests.VerifyErrorMsg_20Sels();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyErrorMsg_PlaceBetWithoutStake() throws Exception {
		caseID = Arrays.asList(1992322);
		betSlipTests.VerifyErrorMsg_PlaceBetWithoutStake();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyMultiBetByAdding2SelectionsFromSameEvent() throws Exception {
		caseID = Arrays.asList(1992346);
		betSlipTests.VerifyMultiBetByAdding2SelectionsFromSameEvent();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyMultipleSelectionsWithEW() throws Exception {
		caseID = Arrays.asList(1992348);
		betSlipTests.VerifyMultipleSelectionsWithEW();
	}

	@Test(priority = 74, groups = { "Missing", "BetSlip" })
	public void VerifyEWDisplayForMultipleBets_TrebleBetPlacement() throws Exception {
		caseID = Arrays.asList(1992320);
		betSlipTests.VerifyEWDisplayForMultipleBets_TrebleBetPlacement();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyMultiBetUpdtdOnAddingSelectionsFromSameEvnt() throws Exception {
		caseID = Arrays.asList(1992347);
		betSlipTests.VerifyMultiBetUpdtdOnAddingSelectionsFromSameEvnt();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyPotentialReturns_CheckUncheckIncludeInMultiples_Multibet() throws Exception {
		caseID = Arrays.asList(1992359);
		betSlipTests.VerifyPotentialReturns_CheckUncheckIncludeInMultiples_Multibet();
	}

	@Test(priority = 74, groups = { "P3P4", "BetSlip" })
	public void VerifyStakes_WithAndWithoutEW() throws Exception {
		caseID = Arrays.asList(1992368);
		betSlipTests.VerifyStakes_WithAndWithoutEW();
	}

	@Test(priority = 51, groups = { "Missing", "GreyHounds" })
	public void VerifyEventNavContextualMenuForNextRacesEvent_GH() throws Exception {

		GHTests.VerifyEventNavContextualMenuForNextRacesEvent_GH();
	}

	@Test(priority = 52, groups = { "Missing", "GreyHounds", "P0" })
	public void VerifyFutureGreyhoundsEventDetailsPage() throws Exception {

		GHTests.VerifyFutureGreyhoundsEventDetailsPage();
	}

	@Test(priority = 53, groups = { "Missing", "GreyHounds" })
	public void VerifyGH_SelectDeselectTodayTomorrowFutureRaceEvents() throws Exception {

		GHTests.VerifyGH_SelectDeselectTodayTomorrowFutureRaceEvents();
	}

	@Test(priority = 54, groups = { "Missing", "GreyHounds" })
	public void VerifyGH_TodayTomorrowFutureRaceCard() throws Exception {

		GHTests.VerifyGH_TodayTomorrowFutureRaceCard();
	}

	@Test(priority = 55, groups = { "Missing", "GreyHounds" })
	public void VerifyGHMarketTabsForSelectedRacesOnRaceCardPage_GH() throws Exception {

		GHTests.VerifyGHMarketTabsForSelectedRacesOnRaceCardPage_GH();
	}

	@Test(priority = 56, groups = { "Missing", "GreyHounds" })
	public void VerifyGHSelectedRacesListedInTimeOrderOnRaceCardPage() throws Exception {

		GHTests.VerifyGHSelectedRacesListedInTimeOrderOnRaceCardPage();
	}

	@Test(priority = 57, groups = { "Missing", "GreyHounds" })
	public void VerifyGreyhoundsNextRacesMoreLink() throws Exception {

		GHTests.VerifyGreyhoundsNextRacesMoreLink();
	}

	@Test(priority = 58, groups = { "Missing", "GreyHounds" })
	public void VerifyGreyhoundsTodaysModuleRaceCardPage() throws Exception {

		GHTests.VerifyGreyhoundsTodaysModuleRaceCardPage();
	}

	@Test(priority = 59, groups = { "Missing", "GreyHounds" })
	public void VerifyGreyhoundsTodaysModuleRaces() throws Exception {

		GHTests.VerifyGreyhoundsTodaysModuleRaces();
	}

	@Test(priority = 60, groups = { "Missing", "GreyHounds", "P0" })
	public void VerifyGreyhoundsTodaysModuleResultsForm() throws Exception {

		GHTests.VerifyGreyhoundsTodaysModuleResultsForm();
	}

	@Test(priority = 61, groups = { "Missing", "GreyHounds" })
	public void VerifyMarketTabsOnGreyhoundsPage() throws Exception {

		GHTests.VerifyMarketTabsOnGreyhoundsPage();
	}

	@Test(priority = 62, groups = { "Missing", "GreyHounds" })
	public void VerifyMeetingVenueTabAndTimeInContextualMenuInGreyhounds() throws Exception {

		GHTests.VerifyMeetingVenueTabAndTimeInContextualMenuInGreyhounds();
	}

	@Test(priority = 63, groups = { "Missing", "GreyHounds" })
	public void VerifyMeetingVenueTabInContextualMenuInGreyhounds() throws Exception {

		GHTests.VerifyMeetingVenueTabInContextualMenuInGreyhounds();
	}

	@Test(priority = 64, groups = { "Missing", "GreyHounds" })
	public void VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_GH() throws Exception {

		GHTests.VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_GH();
	}

	@Test(priority = 65, groups = { "Missing", "GreyHounds" })
	public void VerifyResultsLinkOnGreyhoundsEventDetailsPage() throws Exception {

		GHTests.VerifyResultsLinkOnGreyhoundsEventDetailsPage();
	}

	@Test(priority = 66, groups = { "Missing", "GreyHounds" })
	public void VerifySelectGHSingleEventAndRefreshRaceCard() throws Exception {

		GHTests.VerifySelectGHSingleEventAndRefreshRaceCard();
	}

	@Test(priority = 67, groups = { "Missing", "GreyHounds" })
	public void VerifySpecialGreyhoundsEventDetailsPage() throws Exception {

		GHTests.VerifySpecialGreyhoundsEventDetailsPage();
	}

	@Test(priority = 68, groups = { "Missing", "GreyHounds" })
	public void VerifyTrapWinnerMarketOnGreyhoundsPage() throws Exception {

		GHTests.VerifyTrapWinnerMarketOnGreyhoundsPage();
	}

	@Test(priority = 69, groups = { "Missing", "GreyHounds" })
	public void VerifyTypeOFBetSelectedtInForecastTricastInGH() throws Exception {

		GHTests.VerifyTypeOFBetSelectedtInForecastTricastInGH();
	}

	@Test(priority = 70, groups = { "Missing", "GreyHounds" })
	public void VerifyUrlForGHSingleAndMultipleMeetings() throws Exception {

		GHTests.VerifyUrlForGHSingleAndMultipleMeetings();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyBetslipPromoOnHomePage() throws Exception {
		caseID = Arrays.asList(1992312);
		HGTests.VerifyBetslipPromoOnHomePage();
	}

	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void VerifyRegistration_UKCustomer() throws Exception {
		caseID = Arrays.asList(810999);
		HGTests.VerifyRegistration_UKCustomer();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyRegistration_NonUKCustomer() throws Exception {
		caseID = Arrays.asList(1992362);
		HGTests.VerifyRegistration_NonUKCustomer();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyFractionalToDecimalAppearOnAllModuleCouponPage() throws Exception {
		caseID = Arrays.asList(1992325);
		HGTests.VerifyFractionalToDecimalAppearOnAllModuleCouponPage();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyFractionalToDecimalAppearOnAllModulesOnHomePage() throws Exception {
		caseID = Arrays.asList(1992326);
		HGTests.VerifyFractionalToDecimalAppearOnAllModulesOnHomePage();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyHighlightsOnHomePage() throws Exception {
		caseID = Arrays.asList(1992328);
		HGTests.VerifyHighlightsOnHomePage();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyInplayEventTitleOnEventDetailPage() throws Exception {
		caseID = Arrays.asList(1992329);
		HGTests.VerifyInplayEventTitleOnEventDetailPage();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyInplayOnHomePage() throws Exception {
		caseID = Arrays.asList(1992331);
		HGTests.VerifyInplayOnHomePage();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyInplaySectionExpandCollapse() throws Exception {
		caseID = Arrays.asList(1992332);
		HGTests.VerifyInplaySectionExpandCollapse();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyLHSClassSortOrder() throws Exception {
		caseID = Arrays.asList(1992333);
		HGTests.VerifyLHSClassSortOrder();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyLHSInAllPages() throws Exception {
		caseID = Arrays.asList(1992334);
		HGTests.VerifyLHSInAllPages();
	}

	@Test(priority = 74, groups = { "P3P4", "HomeAndGlobal" })
	public void VerifyPaymentMethodLinksOnFooter() throws Exception {
		caseID = Arrays.asList(1992358);
		HGTests.VerifyPaymentMethodLinksOnFooter();
	}

	@Test(priority = 9, groups = { "Missing", "HorseRacing" })
	public void VerifyEventNavContextualMenuForNextRacesEvent_HR() throws Exception {

		HRTests.VerifyEventNavContextualMenuForNextRacesEvent_HR();
	}

	@Test(priority = 10, groups = { "Missing", "HorseRacing" })
	public void VerifyEventNavContextualMenuForTodayEvent_HR() throws Exception {

		HRTests.VerifyEventNavContextualMenuForTodayEvent_HR();
	}

	@Test(priority = 11, groups = { "Missing", "HorseRacing" })
	public void VerifyEventNavContextualMenuForTomorrowEvent() throws Exception {

		HRTests.VerifyEventNavContextualMenuForTomorrowEvent();
	}

	@Test(priority = 12, groups = { "Missing", "HorseRacing" })
	public void VerifyFavouriteIndexAndAggregateDistanceDisplayedBelowRacecard() throws Exception {

		HRTests.VerifyFavouriteIndexAndAggregateDistanceDisplayedBelowRacecard();
	}

	@Test(priority = 13, groups = { "Missing", "HorseRacing" })
	public void VerifyForecastClearFunctionality() throws Exception {

		HRTests.VerifyForecastClearFunctionality();
	}

	@Test(priority = 14, groups = { "Missing", "HorseRacing" })
	public void VerifyFormResultAndCommentaryOnHorseRacingPage() throws Exception {

		HRTests.VerifyFormResultAndCommentaryOnHorseRacingPage();
	}

	@Test(priority = 15, groups = { "Missing", "HorseRacing" })
	public void VerifyFractionalToDecimalAppearOnAllModulesOnHorseRacingPage() throws Exception {

		HRTests.VerifyFractionalToDecimalAppearOnAllModulesOnHorseRacingPage();
	}

	@Test(priority = 16, groups = { "Missing", "HorseRacing", "P0" })
	public void VerifyFutureHorseRacingEventDetailsPage() throws Exception {

		HRTests.VerifyFutureHorseRacingEventDetailsPage();
	}

	@Test(priority = 17, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingAllRacingUkIrish() throws Exception {

		HRTests.VerifyHorseRacingAllRacingUkIrish();
	}

	@Test(priority = 18, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseracingFCTC_SelectionInfo() throws Exception {

		HRTests.VerifyHorseracingFCTC_SelectionInfo();
	}

	@Test(priority = 19, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingHierarchyDisplayOnContextualMenu() throws Exception {

		HRTests.VerifyHorseRacingHierarchyDisplayOnContextualMenu();
	}

	@Test(priority = 20, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces() throws Exception {

		HRTests.VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoFutureRaces();
	}

	@Test(priority = 21, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoTmrwRaces() throws Exception {

		HRTests.VerifyHorseRacingHierarchyDisplayOnContextualMenu_NoTmrwRaces();
	}

	@Test(priority = 22, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseracingMeetingVenueTabAndTimeInContextualMenu() throws Exception {

		HRTests.VerifyHorseracingMeetingVenueTabAndTimeInContextualMenu();
	}

	@Test(priority = 23, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseracingMeetingVenueTabInContextualMenu() throws Exception {

		HRTests.VerifyHorseracingMeetingVenueTabInContextualMenu();
	}

	@Test(priority = 24, groups = { "Missing", "HorseRacing", "P0" })
	public void VerifyHorseRacingTodaysModuleFormResult() throws Exception {

		HRTests.VerifyHorseRacingTodaysModuleFormResult();
	}

	@Test(priority = 25, groups = { "Missing", "HorseRacing" })
	public void VerifyHorseracingWOMarketsPlaceBet() throws Exception {

		HRTests.VerifyHorseracingWOMarketsPlaceBet();
	}

	@Test(priority = 26, groups = { "Missing", "HorseRacing" })
	public void VerifyHR_SelectDeselectTodayTomorrowFutureRaceEvents() throws Exception {

		HRTests.VerifyHR_SelectDeselectTodayTomorrowFutureRaceEvents();
	}

	@Test(priority = 27, groups = { "Missing", "HorseRacing" })
	public void VerifyMarketTabsOnHorseRacing() throws Exception {

		HRTests.VerifyMarketTabsOnHorseRacing();
	}

	@Test(priority = 28, groups = { "Missing", "HorseRacing" })
	public void VerifyHRNextRacesMoreLink() throws Exception {

		HRTests.VerifyHRNextRacesMoreLink();
	}

	@Test(priority = 29, groups = { "Missing", "HorseRacing" })
	public void VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_HR() throws Exception {

		HRTests.VerifyOptionToCollapseExpandTodaysRacingOnContextualMenu_HR();
	}

	@Test(priority = 30, groups = { "Missing", "HorseRacing" })
	public void VerifyRacecardBySortingSelectionName() throws Exception {

		HRTests.VerifyRacecardBySortingSelectionName();
	}

	@Test(priority = 31, groups = { "Missing", "HorseRacing" })
	public void VerifyRacecardForEachHorse() throws Exception {

		HRTests.VerifyRacecardForEachHorse();
	}

	@Test(priority = 32, groups = { "Missing", "HorseRacing" })
	public void VerifyRacingPostResultFromHorseRacingEventDetailsPage() throws Exception {

		HRTests.VerifyRacingPostResultFromHorseRacingEventDetailsPage();
	}

	@Test(priority = 33, groups = { "Missing", "HorseRacing" })
	public void VerifySelectHRSingleEventAndRefreshRaceCard() throws Exception {

		HRTests.VerifySelectHRSingleEventAndRefreshRaceCard();
	}

	@Test(priority = 34, groups = { "Missing", "HorseRacing" })
	public void VerifySelectingBetInForecastTricastOnRacecardPage() throws Exception {

		HRTests.VerifySelectingBetInForecastTricastOnRacecardPage();
	}

	@Test(priority = 35, groups = { "Missing", "HorseRacing" })
	public void VerifyTodayRacesModuleFavIndex() throws Exception {

		HRTests.VerifyTodayRacesModuleFavIndex();
	}

	@Test(priority = 36, groups = { "Missing", "HorseRacing" })
	public void VerifyTodaysModuleHorseRaces() throws Exception {

		HRTests.VerifyTodaysModuleHorseRaces();
	}

	@Test(priority = 37, groups = { "Missing", "HorseRacing" })
	public void VerifyTodaysModuleHorseRacesSorted() throws Exception {

		HRTests.VerifyTodaysModuleHorseRacesSorted();
	}

	@Test(priority = 38, groups = { "Missing", "HorseRacing" })
	public void VerifyTodaysModuleRaceCardPage() throws Exception {

		HRTests.VerifyTodaysModuleRaceCardPage();
	}

	@Test(priority = 39, groups = { "Missing", "HorseRacing" })
	public void VerifyTomorrowsModuleHorseRaces() throws Exception {

		HRTests.VerifyTomorrowsModuleHorseRaces();
	}

	@Test(priority = 40, groups = { "Missing", "HorseRacing" })
	public void VerifyTricastClearFunctionality() throws Exception {

		HRTests.VerifyTricastClearFunctionality();
	}

	@Test(priority = 41, groups = { "Missing", "HorseRacing" })
	public void VerifyUnNamedFavouriteDisplayedAfterNamedSelections() throws Exception {

		HRTests.VerifyUnNamedFavouriteDisplayedAfterNamedSelections();
	}

	@Test(priority = 42, groups = { "Missing", "HorseRacing" })
	public void VerifyUrlForSingleAndMultipleMeetings() throws Exception {

		HRTests.VerifyUrlForSingleAndMultipleMeetings();
	}

	@Test(priority = 71, groups = { "P3P4", "HorseRacing" })
	public void VerifyHRSelectedRacesListedInTimeOrderOnRaceCardPage() throws Exception {
		caseID = Arrays.asList(1992287);

		HRTests.VerifyHRSelectedRacesListedInTimeOrderOnRaceCardPage();
	}

	@Test(priority = 72, groups = { "P3P4", "HorseRacing"})
	public void VerifyHRMarketTabsForSelectedRacesOnRaceCardPage() throws Exception {
		caseID = Arrays.asList(1992288);
		HRTests.VerifyHRMarketTabsForSelectedRacesOnRaceCardPage();
	}

	@Test(priority = 73, groups = { "P3P4", "HorseRacing" })
	public void VerifyHR_TodayTomorrowFutureRaceCard() throws Exception {
		caseID = Arrays.asList(1992289);
		HRTests.VerifyHR_TodayTomorrowFutureRaceCard();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyDetailsOnLoginLogout() throws Exception {
		caseID = Arrays.asList(1992318);
		LogInOutTests.VerifyDetailsOnLoginLogout();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout", "P0" })
	public void VerifyLinksOnHelp() throws Exception {
		caseID = Arrays.asList(1992335);
		LogInOutTests.VerifyLinksOnHelp();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyLogin_CreditCustomer() throws Exception {
		caseID = Arrays.asList(1992338);
		LogInOutTests.VerifyLogin_CreditCustomer();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyLogin_EliteCustomer() throws Exception {
		caseID = Arrays.asList(1992339);
		LogInOutTests.VerifyLogin_EliteCustomer();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyLogin_HVCCustomer() throws Exception {
		caseID = Arrays.asList(1992340);
		LogInOutTests.VerifyLogin_HVCCustomer();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyLoginErrorMessage_SelfExclusion() throws Exception {
		caseID = Arrays.asList(1992336);
		LogInOutTests.VerifyLoginErrorMessage_SelfExclusion();
	}

	@Test(priority = 74, groups = { "P3P4", "LoginLogout" })
	public void VerifyLoginErrorMessage_SuspendedUser() throws Exception {
		caseID = Arrays.asList(1992337);
		LogInOutTests.VerifyLoginErrorMessage_SuspendedUser();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyOddsBoostBetplacement_Singles() throws Exception {
		caseID = Arrays.asList(1992352);
		OddsBoostTests.VerifyOddsBoostBetplacement_Singles();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyOddsBoostBetplacement_Doubles() throws Exception {
		caseID = Arrays.asList(1992351);
		OddsBoostTests.VerifyOddsBoostBetplacement_Doubles();
	}

	@Test(priority = 74, groups = { "P1", "OddsBoostTests" })
	public void VerifyOddsBoostWithLogIn_1() throws Exception {
		caseID = Arrays.asList(811034);
		OddsBoostTests.VerifyOddsBoostWithLogIn();
	}
	
	@Test(priority = 74, groups = { "P1", "OddsBoostTests" })
	public void VerifyOddsBoostWithLogIn_2() throws Exception {
		caseID = Arrays.asList(811035);
		OddsBoostTests.VerifyOddsBoostWithLogIn();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyOddsBoostWithLoggedOut() throws Exception {
		caseID = Arrays.asList(1992355);
		OddsBoostTests.VerifyOddsBoostWithLoggedOut();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyOddsBoostButtonCheckUnchek() throws Exception {
		caseID = Arrays.asList(1992354);
		OddsBoostTests.VerifyOddsBoostButtonCheckUnchek();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyOddsBoostBetslipNavigationAndRefreshPage() throws Exception {
		caseID = Arrays.asList(1992353);
		OddsBoostTests.VerifyOddsBoostBetslipNavigationAndRefreshPage();
	}

	@Test(priority = 74, groups = { "P1", "OddsBoostTests" })
	public void VerifyMyBets_OddsBoostSingleBet() throws Exception {
		caseID = Arrays.asList(811037);
		OddsBoostTests.VerifyMyBets_OddsBoostSingleBet();
	}

	@Test(priority = 74, groups = { "P3P4", "OddsBoostTests" })
	public void VerifyMyBets_OddsBoostDoubleBet() throws Exception {
		caseID = Arrays.asList(1992349);
		OddsBoostTests.VerifyMyBets_OddsBoostDoubleBet();
	}

	@Test(priority = 74, groups = { "P1", "OddsBoostTests" })
	public void VerifyOddsBoostWithEWCheck() throws Exception {
		caseID = Arrays.asList(811266);
		OddsBoostTests.VerifyOddsBoostWithEWCheck();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests", "P0" })
	public void VerifyAccaSwitchers() throws Exception {
		caseID = Arrays.asList(1992290);
		MyBetsTests.VerifyAccaSwitchers();
	}

	@Test(priority = 74, groups = { "P1", "MyBetsTests" })
	public void VerifyAccaNotLoggedInUser() throws Exception {
		// caseID =811221;
		// caseID =811049;
		caseID = Arrays.asList(811221);
		MyBetsTests.VerifyAccaNotLoggedInUser();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyAccasWithBetPlacement() throws Exception {
		caseID = Arrays.asList(1992291);
		MyBetsTests.VerifyAccasWithBetPlacement();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyBetNotOnAccas() throws Exception {
		caseID = Arrays.asList(1992292);
		MyBetsTests.VerifyBetNotOnAccas();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyCashout_DoubleBet() throws Exception {
		caseID = Arrays.asList(1992315);
		MyBetsTests.VerifyCashout_DoubleBet();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyCashout_SingleBet() throws Exception {
		caseID = Arrays.asList(1992316);
		MyBetsTests.VerifyCashout_SingleBet();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyCashoutInPlayUpcomingEvents() throws Exception {
		caseID = Arrays.asList(1992314);
		MyBetsTests.VerifyCashoutInPlayUpcomingEvents();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyLogoutJourney() throws Exception {
		caseID = Arrays.asList(1992341);
		MyBetsTests.VerifyLogoutJourney();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyOpenBets_DoubleBet() throws Exception {
		caseID = Arrays.asList(1992356);
		MyBetsTests.VerifyOpenBets_DoubleBet();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyShowMoreInOpenSettledBets() throws Exception {
		caseID = Arrays.asList(1992367);
		MyBetsTests.VerifyShowMoreInOpenSettledBets();
	}

	@Test(priority = 74, groups = { "P3P4", "MyBetsTests" })
	public void VerifyTabsInMyBets() throws Exception {
		caseID = Arrays.asList(1992369);
		MyBetsTests.VerifyTabsInMyBets();
	}

	// If this test case has to be run on Pre-prod tokens have to be set by the
	// functional team

	@Test(priority = 25, groups = { "Missing", "PrivateMarket" })
	public void VerifyPrivateMarketsQualifiedUser() throws Exception {
		caseID = Arrays.asList(811244);
		PMtests.VerifyPrivateMarketsQualifiedUser();
	}

	// If this test case has to be run on Pre-prod tokens have to be set by the
	// functional team

	@Test(priority = 25, groups = { "Missing", "PrivateMarket" })

	public void VerifyPrivateMarketsNonQualifiedUser() throws Exception {
		caseID = Arrays.asList(811244);
		PMtests.VerifyPrivateMarketsNonQualifiedUser();

	}
	// ********************************

	@Test(priority = 74, groups = { "P1", "ResultsTests" })
	public void VerifyGoToSportsLinkOnResultspage() throws Exception {
		caseID = Arrays.asList(811057);
		ResultsTests.VerifyGoToSportsLinkOnResultspage();
	}

	@Test(priority = 74, groups = { "P3P4", "ResultsTests" })
	public void VerifyMoreOptionOnResultspage() throws Exception {
		caseID = Arrays.asList(1992344);
		ResultsTests.VerifyMoreOptionOnResultspage();
	}

	@Test(priority = 74, groups = { "P3P4", "ResultsTests" })
	public void VerifyResultsFormatOnGH_HR() throws Exception {
		caseID = Arrays.asList(1992363);
		ResultsTests.VerifyResultsFormatOnGH_HR();
	}

	@Test(priority = 74, groups = { "P3P4", "ResultsTests" })
	public void VerifyResultsFormatOnSports() throws Exception {
		caseID = Arrays.asList(1992364);
		ResultsTests.VerifyResultsFormatOnSports();
	}

	@Test(priority = 74, groups = { "P1", "ResultsTests" })
	public void VerifyResultsTab_DatePicker() throws Exception {
		caseID = Arrays.asList(811059);
		ResultsTests.VerifyResultsTab_DatePicker();
	}

	@Test(priority = 74, groups = { "P3P4", "ResultsTests" })
	public void VerifyResultsTab_Today_Yesterday_Last3Days() throws Exception {
		caseID = Arrays.asList(1992365);
		ResultsTests.VerifyResultsTab_Today_Yesterday_Last3Days();
	}

	@Test(priority = 74, groups = { "P1", "ResultsTests" })
	public void VerifyResultsTabOnSportsLandingPage() throws Exception {
		caseID = Arrays.asList(811056);
		ResultsTests.VerifyResultsTabOnSportsLandingPage();
	}

	// @Test(priority=74, groups={"P3P4","SBTests"})
	public void VerifyPromotionsForExistingUser() throws Exception {
		caseID = Arrays.asList(1992360);
		SBTests.VerifyPromotionsForExistingUser();
	}

	@Test(priority = 74, groups = { "P3P4", "SBTests" })
	public void VerifyPromotionsForNewUser() throws Exception {
		caseID = Arrays.asList(1992361);
		SBTests.VerifyPromotionsForNewUser();
	}

	@Test(priority = 74, groups = { "Missing", "HomeAndGlobal" })
	public void AutoHighlights_SportsTabEvents() throws Exception {
		// Stories MOB-9577_3 and MOB-9577_4
		// caseID= Arrays.asList(822685);

		HGTests.AutoHighlights_SportsTabEvents();
	}

	@Test(priority = 74, groups = { "Missing", "HomeAndGlobal" })
	public void AutoHighlights_Grouping() throws Exception {
		// Stories MOB-9577_1 and MOB-9577_2
		// caseID= Arrays.asList(822685);
		HGTests.AutoHighlights_Grouping();
	}
	
	@Test
	public void CashOut_Racing_WhenSelectionChangedToLOST() throws Exception {
		// Stories MOB-10113_1
		// caseID= Arrays.asList(??);
		CashOutTests.CashOut_Racing_WhenSelectionChangedToLOST();
	}
	
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_The_Promotions_Page_Via_Quick_Links() throws Exception {
		caseID = Arrays.asList(811219);
		HGTests.Verify_The_Promotions_Page_Via_Quick_Links();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_Claim_Now_For_Promotion() throws Exception {
		caseID = Arrays.asList(811220);
		HGTests.Verify_Claim_Now_For_Promotion();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_The_Tennis_Virtual_Tab() throws Exception {
		caseID = Arrays.asList(811066);
		HGTests.Verify_The_Tennis_Virtual_Tab();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_Virtual_Bet_placement() throws Exception {
		caseID = Arrays.asList(811066);
		HGTests.Verify_Virtual_Bet_placement();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_Results_Of_Perticular_Sport() throws Exception {
		caseID = Arrays.asList(811058);
		HGTests.Verify_Results_Of_Perticular_Sport();
	}
	
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Verify_Functionality_Of_Different_Tabs_On_Deposit_Screen() throws Exception {
		caseID = Arrays.asList(811058);
		HGTests.Verify_Functionality_Of_Different_Tabs_On_Deposit_Screen();
	}
	
	@AfterClass(alwaysRun = true)
	public void SendReport() throws IOException {
		String email = ReadTestSettingConfig.getTestsetting("SendEmail");
		if (email.equalsIgnoreCase("yes")) {
			TestRail.getTestRailReport();
			SendMail.SendmailwithReport();
		}
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Check_AEM_banners_on_homepage() throws Exception {
		caseID = Arrays.asList(1200459);
		HGTests.Check_AEM_banners_on_homepage();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Check_AEM_banners_on_FootballPage() throws Exception {
		caseID = Arrays.asList(1200460);
		HGTests.Check_AEM_banners_on_FootballPage();
	}
	@Test(priority = 74, groups = { "P1", "HomeAndGlobal" })
	public void Check_AEM_banners_on_HRandGHPage() throws Exception {
		caseID = Arrays.asList(1200461);
		HGTests.Check_AEM_banners_on_HRandGHPage();
	}
}
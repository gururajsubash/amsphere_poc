package com.ladbrokes.testsuite;

import java.util.Arrays;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import com.ladbrokes.EnglishOR.VirtualSportsElements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.Convert;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.HomeGlobal_Functions;
import com.ladbrokes.commonclasses.HorseRacing_Functions;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;
import com.ladbrokes.commonclasses.MyBetsFunctions;

public class BetSlipTests extends DriverCommon {
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	TestdataFunction testdata = new TestdataFunction();
	Common common = new Common();
	MyBetsFunctions myBetsfun = new MyBetsFunctions();
	LoginLogoutFunctions LoginfunctionObj = new LoginLogoutFunctions();
	HomeGlobal_Functions HGfunctionObj = new HomeGlobal_Functions();
	HorseRacing_Functions HRfunctionObj = new HorseRacing_Functions();

	/**
	 * TC-1 Place single/ multiple accumulator bet from different types of
	 * coupon
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyCouponLandingPage_SingleBet() throws Exception {
		String testCase = "VerifyCouponLandingPage_SingleBet";
		String eventNameTxt;
		String marketNameTxt;
		String selNameTxt;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponLink),
					"Football Coupons Links not found on HomePage");
			click(FootballElements.couponLink);
			Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
					"Events are not present on coupon page");
			click(FootballElements.coupon1stEvent1stSel); // adding Selection
			eventNameTxt = GetElementText(FootballElements.coupon1stEventName);
			marketNameTxt = GetElementText(FootballElements.marketOnCoupon);
			selNameTxt = GetElementText(FootballElements.couponHomeSelName);
			// odds =
			// GetElementText(By.xpath("(//div[@class='left-column']//coupon-group[contains(@params,
			// 'events')]//a[1]//span[@class='selection'][1]//span[@class='price'])[1]"));
			String couponMrktName = TextControls.couponMarketText;
			String couponSelName = TextControls.couponSelText;
			if (marketNameTxt.equals(couponMrktName) && selNameTxt.equals(couponSelName)) {
				BTbetslipObj.GetSelectionName(eventNameTxt);
			} else {
			}
			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			Entervalue(BetslipElements.stakeBox, TextControls.stakeValue);
			BTbetslipObj.BetPlacement(false);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-2 Place single/ multiple accumulator bet from different types of
	 * coupon
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyCouponLandingPage_MultipleBet() throws Exception {
		String testCase = "VerifyCouponLandingPage_MultipleBet";
		String selXpath, eventXpath, marketNameTxt, selNameTxt, marketName, selBetSlipPath;
		String[] eventNameTxt = new String[3];
		String[] odds = new String[3];
		String[] selName = new String[3];
		int j = 1;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponLink),
					"Football Coupons Links not found on HomePage");
			click(FootballElements.couponLink);
			Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
					"Events are not present on coupon page");
			List<WebElement> couponEvents = getDriver().findElements(FootballElements.eventsOnCoupon);
			if (couponEvents.size() > 4) {
				for (int i = 1; i < 3; i++) {
					selXpath = "((//coupon-group//a//span[@class='selections'])[" + i
							+ "]//descendant::div[@class='odds-button'])[1]";
					// **Get event, market & selection name and odds from Coupon
					// landing page
					eventXpath = "(//div[@class='left-column']//coupon-group[contains(@params, 'events')]//a[1]//span[@class='name'])["
							+ i + "]";
					eventNameTxt[j] = getDriver().findElement(By.xpath(eventXpath)).getText();
					marketNameTxt = getDriver()
							.findElement(By
									.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h2//span"))
							.getText();
					marketNameTxt = toTitleCase(marketNameTxt);
					selNameTxt = getDriver()
							.findElement(By
									.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h3//span//span[1]"))
							.getText();
					odds[j] = getDriver().findElement(By.xpath("((//coupon-group//a//span[@class='selections'])[" + i
							+ "]//descendant::div[@class='odds-button']//span[@class='price'])[1]")).getText();
					click(By.xpath(selXpath), "Selection not present for event " + i + " on coupon landing page");
					String couponMrktName = TextControls.couponMarketText;
					String couponSelName = TextControls.couponSelText;
					if (marketNameTxt.equals(couponMrktName) && selNameTxt.equals(couponSelName)) {
						marketName = TextControls.couponMarketNameOnBetSlip;
						selName[j] = BTbetslipObj.GetSelectionName(eventNameTxt[j]);
					} else {
						marketName = marketNameTxt;
						selName[j] = selNameTxt;
					}
					// Verify the correct selections are added to BetSlip
					selBetSlipPath = "//div[@class='market-information-selection']//span[contains(text(), '"
							+ selName[j] + "')]/../../following-sibling::div//input";
					Assert.assertTrue(isElementDisplayed(By.xpath(selBetSlipPath)), "Selection-Event-Market '"
							+ selName[j] + "-" + eventNameTxt[j] + "-" + marketName + "' was not found in the Betslip");
					j++;
				}
				int multiBetCount = BTbetslipObj.GetMultiplesBetCount();
				// Enter stake, Place Bet and Validate
				common.BetPlacement_WithAcceptNContinue("Double", TextControls.stakeValue, multiBetCount);
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-3 Place bet from different markets using the switcher drop down from
	 * coupon page
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifySpecificCouponPage_SwitcherDropdown_MultipleBet() throws Exception {
		String testCase = "VerifySpecificCouponPage_SwitcherDropdown_MultipleBet";
		int count = 0;
		int j = 1;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponLink),
					"Football Coupons Links not found on HomePage");
			click(FootballElements.couponLink);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponOnList),
					"Coupons list is not present in LHS of coupon landing page");
			Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
					"Events are not present on coupon page");
			Assert.assertTrue(isElementDisplayed(FootballElements.marketDropDown),
					"Market dropdown switcher is not present in coupons page");
			String switcherXpath = "//*[@class='right-column']//select[contains(@data-bind,'marketToShow')]//option";
			List<WebElement> switcherMarketList = getDriver().findElements(By.xpath(switcherXpath));
			if (switcherMarketList.size() > 4)
				count = 3;
			for (int i = 0; i < count; i++) {
				switcherMarketList = getDriver().findElements(By.xpath(switcherXpath));
				switcherMarketList.get(i).click();
				scrollPage("top");
				Thread.sleep(2000);
				click(By.xpath(
						"(//div[@class='right-column']//span[@class='selection']//selection-button//*[@class='odds-button'])["
								+ j + "]"),
						"Selection not present in Swticher module in coupon page");
				j = j + 6;
			}
			int multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			common.BetPlacement_WithAcceptNContinue("Treble", TextControls.stakeValue, multiBetCount);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-04 Treble bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyTrebleBetPlacement() throws Exception {
		String testCase = "VerifyTrebleBetPlacement";
		int multiBetCount;
		String[] aryOdds = new String[3];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "treble", multiBetCount, aryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "treble", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "treble", "", "");
			String trebleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", trebleSelName, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "Treble");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-08 Stake amount - Enter stake to couple of selections made and change
	 * the stake amount and check for the updated potential returns Stake amount
	 * - Enter stake to couple of selections made and check the stake amount and
	 * potential return
	 * 
	 * @return none
	 * @throws Exception
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyTotalStakeTotalPR_ByChangingStake() throws Exception {
		String testCase = "VerifyTotalStakeTotalPR_ByChangingStake";
		double totalStake, totalPR, PRsel1, PRsel2, PRsel3;
		String odds1, odds2, odds3;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			odds1 = testdata.NavigateUsingTestData(1);
			odds2 = testdata.NavigateUsingTestData(5);
			odds3 = testdata.NavigateUsingTestData(13);
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 1, 4),
					testdata.Readtestdata("PreProdEvents", 1, 5), testdata.Readtestdata("PreProdEvents", 1, 6),
					TextControls.stakeValue, "single", false);
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 5, 4),
					testdata.Readtestdata("PreProdEvents", 5, 5), testdata.Readtestdata("PreProdEvents", 5, 6),
					TextControls.stakeValue, "single", false);
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 13, 4),
					testdata.Readtestdata("PreProdEvents", 13, 5), testdata.Readtestdata("PreProdEvents", 13, 6),
					TextControls.stakeValue, "single", false);
			Double TotalStake = Convert.ToDouble(TextControls.stakeValue) * 3;
			TotalStake = Convert.roundOff(TotalStake);
			// Get the total stake and PR
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(TotalStake.equals(totalStake), "total stake is not matching");
			totalPR = BTbetslipObj.GetTotalsFromBetslip("potential");
			PRsel1 = BTbetslipObj.GetPotentialReturnFromBetSlip(testdata.Readtestdata("PreProdEvents", 1, 4),
					testdata.Readtestdata("PreProdEvents", 1, 5), testdata.Readtestdata("PreProdEvents", 1, 6), odds1,
					"single");
			PRsel2 = BTbetslipObj.GetPotentialReturnFromBetSlip(testdata.Readtestdata("PreProdEvents", 5, 4),
					testdata.Readtestdata("PreProdEvents", 5, 5), testdata.Readtestdata("PreProdEvents", 5, 6), odds1,
					"single");
			PRsel3 = BTbetslipObj.GetPotentialReturnFromBetSlip(testdata.Readtestdata("PreProdEvents", 13, 4),
					testdata.Readtestdata("PreProdEvents", 13, 5), testdata.Readtestdata("PreProdEvents", 13, 6), odds1,
					"single");
			double calTotalPR = (PRsel1 + PRsel2 + PRsel3);
			calTotalPR = Convert.roundOff(calTotalPR);
			Assert.assertTrue(totalPR == calTotalPR, "Total potential return is not displayed correctly in BetSlip");
			double expTotalPR = (Convert.ToDouble(odds1) * Convert.ToDouble(TextControls.stakeValue))
					+ (Convert.ToDouble(odds2) * Convert.ToDouble(TextControls.stakeValue))
					+ (Convert.ToDouble(odds3) * Convert.ToDouble(TextControls.stakeValue));
			expTotalPR = Convert.roundOff(calTotalPR);
			Assert.assertTrue(totalPR == expTotalPR, "Total potential return not calculated correctly in BetSlip");
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 5, 4),
					testdata.Readtestdata("PreProdEvents", 5, 5), testdata.Readtestdata("PreProdEvents", 5, 6), "0.20",
					"", false);
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 13, 4),
					testdata.Readtestdata("PreProdEvents", 13, 5), testdata.Readtestdata("PreProdEvents", 13, 6),
					"0.30", "", false);
			// Get the total stake and PR
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Double Tstake = Convert.roundOff(Convert.ToDouble(TextControls.stakeValue) + 0.20 + 0.30);
			Assert.assertTrue(totalStake == Tstake,
					"Total stakes not displayed/calculated correctly on changing the stakes");
			totalPR = BTbetslipObj.GetTotalsFromBetslip("potential");
			PRsel2 = BTbetslipObj.GetPotentialReturnFromBetSlip(testdata.Readtestdata("PreProdEvents", 5, 4),
					testdata.Readtestdata("PreProdEvents", 5, 5), testdata.Readtestdata("PreProdEvents", 5, 6), odds1,
					"single");
			PRsel3 = BTbetslipObj.GetPotentialReturnFromBetSlip(testdata.Readtestdata("PreProdEvents", 13, 4),
					testdata.Readtestdata("PreProdEvents", 13, 5), testdata.Readtestdata("PreProdEvents", 13, 6), odds1,
					"single");
			calTotalPR = PRsel1 + PRsel2 + PRsel3;
			calTotalPR = Convert.roundOff(calTotalPR);
			Assert.assertTrue(totalPR == calTotalPR, "Total potential return not calculated correctly in BetSlip");
			expTotalPR = Convert.roundOff((Convert.ToDouble(odds1) * Convert.ToDouble(TextControls.stakeValue))
					+ (Convert.ToDouble(odds2) * 0.2) + (Convert.ToDouble(odds3) * 0.3));
			Assert.assertTrue(totalPR == expTotalPR, "Total potential return not calculated correctly in BetSlip");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-09 Place single/ multiple accumulator bet from different Type/Sub Type
	 * page And Cash out from ACCAS"
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifySpecificCouponPage_SingleBet() throws Exception {
		String testCase = "VerifySpecificCouponPage_SingleBet";
		String eventNameTxt, marketNameTxt, selNameTxt;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponLink),
					"Football Coupons Links not found on HomePage");
			click(FootballElements.couponLink);
			List<WebElement> couponList = getDriver().findElements(FootballElements.couponOnList);
			if (couponList.size() > 1) {
				click(By.xpath("//div[@class='container coupon-sidebar']/ul/li[2]/a"),
						"Coupons is not present on home page");
				getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
				Thread.sleep(3000);
			} else
				Assert.fail("Cannot navigate to specific coupon page as only one Coupon exists");
			Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
					"Events are not present on coupon page");
			click(FootballElements.coupon1stEvent1stSel); // adding Selection
			eventNameTxt = GetElementText(FootballElements.coupon1stEventName);
			marketNameTxt = GetElementText(FootballElements.marketOnCoupon);
			selNameTxt = GetElementText(FootballElements.couponHomeSelName);
			GetElementText(By.xpath(
					"(//div[@class='left-column']//coupon-group[contains(@params, 'events')]//a[1]//span[@class='selection'][1]//span[@class='price'])[1]"));
			String couponMrktName = TextControls.couponMarketText;
			String couponSelName = TextControls.couponSelText;
			if (marketNameTxt.equals(couponMrktName) && selNameTxt.equals(couponSelName)) {
				BTbetslipObj.GetSelectionName(eventNameTxt);
			} else {
			}
			// BTbetslipObj.EnterStake(eventNameTxt,selName, marketName,
			// TextControls.stakeValue, TextControls.single, false);
			common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 10 Place single/ multiple accumulator bet from different Type/Sub
	 * Type page And Cash out from ACCAS"
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifySpecificCouponPage_MultipleBet() throws Exception {
		String selXpath, eventXpath, marketNameTxt, selNameTxt, marketName, selBetSlipPath;
		String[] eventNameTxt = new String[3];
		String[] odds = new String[3];
		String[] selName = new String[3];
		int j = 1;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(FootballElements.couponLink),
					"Football Coupons Links not found on HomePage");
			click(FootballElements.couponLink);
			List<WebElement> couponList = getDriver().findElements(FootballElements.couponOnList);
			if (couponList.size() > 1) {
				click(By.xpath("//div[@class='container coupon-sidebar']/ul/li[2]/a"),
						"Coupons is not present on home page");
				getDriver().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
				Thread.sleep(3000);
			} else
				Assert.fail("Cannot navigate to specific coupon page as only one Coupon exists");
			Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
					"Events are not present on coupon page");
			List<WebElement> couponEvents = getDriver().findElements(FootballElements.eventsOnCoupon);
			if (couponEvents.size() > 4) {
				for (int i = 1; i < 3; i++) {
					selXpath = "((//coupon-group//a//span[@class='selections'])[" + i
							+ "]//descendant::div[@class='odds-button'])[1]";
					// **Get event, market & selection name and odds from Coupon
					// landing page
					eventXpath = "(//div[@class='left-column']//coupon-group[contains(@params, 'events')]//a[1]//span[@class='name'])["
							+ i + "]";
					eventNameTxt[j] = getDriver().findElement(By.xpath(eventXpath)).getText();
					marketNameTxt = getDriver()
							.findElement(By
									.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h2//span"))
							.getText();
					marketNameTxt = toTitleCase(marketNameTxt);
					selNameTxt = getDriver()
							.findElement(By
									.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h3//span//span[1]"))
							.getText();
					odds[j] = getDriver().findElement(By.xpath("((//coupon-group//a//span[@class='selections'])[" + i
							+ "]//descendant::div[@class='odds-button']//span[@class='price'])[1]")).getText();
					click(By.xpath(selXpath), "Selection not present for event " + i + " on coupon landing page");
					String couponMrktName = TextControls.couponMarketText;
					String couponSelName = TextControls.couponSelText;
					if (marketNameTxt.equals(couponMrktName) && selNameTxt.equals(couponSelName)) {
						marketName = TextControls.couponMarketNameOnBetSlip;
						selName[j] = BTbetslipObj.GetSelectionName(eventNameTxt[j]);
					} else {
						marketName = marketNameTxt;
						selName[j] = selNameTxt;
					}
					// Verify the correct selections are added to BetSlip
					selBetSlipPath = "//div[@class='market-information-selection']//span[contains(text(), '"
							+ selName[j] + "')]/../../following-sibling::div//input";
					Assert.assertTrue(isElementDisplayed(By.xpath(selBetSlipPath)), "Selection-Event-Market '"
							+ selName[j] + "-" + eventNameTxt[j] + "-" + marketName + "' was not found in the Betslip");
					j++;
				}
				int multiBetCount = BTbetslipObj.GetMultiplesBetCount();
				// Enter stake, Place Bet and Validate
				common.BetPlacement_WithAcceptNContinue("Double", TextControls.stakeValue, multiBetCount);
			}
		} catch (AssertionError e) {
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 11 Place bet using a free bet valid for any sport and Add free bet
	 * when no stake has been added to the selection and confirm the error
	 * message
	 * 
	 * @return none
	 * @throws Exception
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyFreeBetForAnySport() throws Exception {
		String testCase = "VerifyFreeBetForAnySport";
		String username = testdata.Readtestdata("Users", 15, 1);
		String password = testdata.Readtestdata("Users", 15, 2);
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String freeBetType, odds;
		String newPR = null;
		double actualFreebets, actPotReturns;
		String[] totalFreeBets;
		try {
			launchweb();
			if (ProjectName.contains("Desktop_English")) {
				Entervalue(LoginElements.Username, username);
				Entervalue(LoginElements.Password, password);
				click(LoginElements.Login);
			} else {
				Common.Login();
			}
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Common.OddSwitcher(TextControls.decimal);
			odds = testdata.NavigateUsingTestData(1);
			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			if (getDriver().findElement(By.xpath("//div[@class='freebets']")).isDisplayed()) {
				Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetsTitle), "Free Bets title is not Present");
				List<WebElement> FreeBetsList = getDriver().findElements(By.xpath("//div[@class='information']"));
				for (int i = 0; i < FreeBetsList.size(); i++) {
					FreeBetsList = getDriver().findElements(By.xpath("//div[@class='information']"));
					freeBetType = FreeBetsList.get(i).getText().toLowerCase();
					if (freeBetType.contains("any") || freeBetType.contains("free bet of your choice")) {
						totalFreeBets = freeBetType.split(" ");
						String TotalFB = totalFreeBets[0];
						String stake = TotalFB.replace(TextControls.currencySymbol, "");
						if (ProjectName.contains("Desktop_Swedish")) {
							stake = stake.replace(",", ".");
						}
						if (freeBetType.contains("any")) {
							click(BetslipElements.anySportFreeBetRadio);
							Assert.assertTrue(isElementDisplayed(BetslipElements.freebetErrorInfo),
									"Free bet invalid stake Error message is not displayed");
							Common.Enterstake(stake);
							click(BetslipElements.anySportFreeBetRadio);
						} else {
							click(BetslipElements.freeBetOfYourChoiceRadio);
							Assert.assertTrue(isElementDisplayed(BetslipElements.freebetErrorInfo),
									"Free bet invalid stake Error message is not displayed");
							Common.Enterstake(stake);
							click(BetslipElements.freeBetOfYourChoiceRadio);
						}
						Assert.assertFalse(isElementPresent(BetslipElements.freebetErrorInfo),
								"Free bet invalid stake Error message is displayed , after entering the stake");
						actualFreebets = BTbetslipObj.GetTotalsFromBetslip("free");
						Assert.assertTrue(stake.contains(Convert.ToString(actualFreebets)),
								"Total free bets " + TotalFB + "  is not found");
						Assert.assertTrue(isElementDisplayed(BetslipElements.freebetDeductInfo),
								" 'Freebet stake will be deducted from potential returns ' information is not Present on BetSlip");
						double expTotalPR = Convert.roundOff((Convert.ToDouble(odds) * Convert.ToDouble(stake)));
						newPR = String.valueOf(expTotalPR);
						String expPR = newPR.substring(0, newPR.indexOf('.'));
						actPotReturns = BTbetslipObj.GetTotalsFromBetslip("potential");
						if (actPotReturns == expTotalPR)
							Assert.assertTrue(expTotalPR == actPotReturns,
									"Total potential return not calculated correctly in BetSlip");
						else
							Assert.assertTrue(Convert.ToString(actPotReturns).contains(expPR),
									"Total potential return not calculated correctly in BetSlip");
						break;
					}
				}
			} else
				Assert.fail("Free Bets are not available");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-12 Place a bet using a free bet which is restricted to the particular
	 * event class. and Add free bet when no stake has been added to the
	 * selection and confirm the error message
	 * 
	 * @return none
	 * @throws Exception
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyFreeBetForSpecificSport() throws Exception {
		String testCase = "VerifyFreeBetForSpecificSport";
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String username = testdata.Readtestdata("Users", 15, 1);
		String password = testdata.Readtestdata("Users", 15, 2);
		String freeBetType, odds;
		double actualFreebets, actPotReturns;
		String[] totalFreeBets;
		try {
			launchweb();
			if (ProjectName.contains("Desktop_English")) {
				Entervalue(LoginElements.Username, username);
				Entervalue(LoginElements.Password, password);
				click(LoginElements.Login);
			} else {
				Common.Login();
			}
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Common.OddSwitcher(TextControls.decimal);
			odds = testdata.NavigateUsingTestData(1);
			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			if (getDriver().findElement(By.xpath("//div[@class='freebets']")).isDisplayed()) {
				Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetsTitle), "Free Bets title is not Present");
				List<WebElement> FreeBetsList = getDriver().findElements(By.xpath("//div[@class='information']"));
				for (int i = 0; i < FreeBetsList.size(); i++) {
					FreeBetsList = getDriver().findElements(By.xpath("//div[@class='information']"));
					freeBetType = FreeBetsList.get(i).getText();
					if (freeBetType.contains("Football")) {
						totalFreeBets = freeBetType.split(" ");
						String TotalFB = totalFreeBets[0];
						String stake = TotalFB.replace(TextControls.currencySymbol, "");
						click(BetslipElements.footballFreeBetRadio);
						Assert.assertTrue(isElementDisplayed(BetslipElements.freebetErrorInfo),
								"Free bet invalid stake Error message is not displayed");
						Common.Enterstake(stake);
						click(BetslipElements.footballFreeBetRadio);
						Assert.assertFalse(isElementPresent(BetslipElements.freebetErrorInfo),
								"Free bet invalid stake Error message is displayed , after entering the stake");
						actualFreebets = BTbetslipObj.GetTotalsFromBetslip("free");
						Assert.assertTrue(stake.contains(Convert.ToString(actualFreebets)),
								"Total free bets " + TotalFB + "  is not found");
						Assert.assertTrue(isElementDisplayed(BetslipElements.freebetDeductInfo),
								" 'Freebet stake will be deducted from potential returns ' information is not Present on BetSlip");
						double expTotalPR = Convert.roundOff((Convert.ToDouble(odds) * Convert.ToDouble(stake)));
						actPotReturns = BTbetslipObj.GetTotalsFromBetslip("potential");
						Assert.assertTrue(expTotalPR == actPotReturns,
								"Total potential return not calculated correctly in BetSlip");
						break;
					}
				}
			} else
				Assert.fail("Free Bets are not available");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-13 TennisTypePage PlaceBet - Login from Header,add selection to Bet
	 * slip and Place bet
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyTennisTypePagePlaceBet() throws Exception {
		String testCase = "VerifyTennisTypePagePlaceBet";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.NavigateToEventSubType(TextControls.Tennis, true, false);
			int initBetslipCount = common.GetBetSlipCount();
			if (isElementPresent(BetslipElements.EDPHeader)) {
				// logger.log(LogStatus.INFO,"Type have only one event so there
				// is no type page");
				System.out.println("Type have only one event so there is no type page");
				getDriver().findElement(By.xpath("(//div[@class='odds-button'])[1]")).click();
			} else {
				click(HomeGlobalElements.SportLandingPageOdd);
			}
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 14 Basketball sub type page - Login from Header,add selection to
	 * betslip and placebet
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBasketballsubTypePagePlaceBet() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			BTbetslipObj.VerifyAnySportsEDPPlaceBet_function(TextControls.Basketball);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 15 Any sports event detail page - Login from Header,add selection to
	 * bet slip and place bet
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyAnySportsEDPPlaceBet() throws Exception {
		String testCase = "VerifyAnySportsEDPPlaceBet";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			BTbetslipObj.VerifyAnySportsEDPPlaceBet_function(TextControls.Football);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-16 Place bet more than account balance
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacementWithInsufficientBalance() throws Exception {
		String testCase = "VerifyBetPlacementWithInsufficientBalance";
		double intiBalance, addStake;
		try {
			launchweb();
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			intiBalance = LoginfunctionObj.GetBalance();
			addStake = intiBalance + 1.00;
			String stake1 = String.valueOf(addStake);
			common.NavigateToSportsPage(TextControls.Football);
			click(HomeGlobalElements.SportLandingPageOdd);
			Common.Enterstake(stake1);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			click(BetslipElements.placeBet);
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertTrue(isElementDisplayed(LoginElements.insuffFundsText),
					"Insufficient fund error message is not displayed");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 17 Add a selection, enter stake and place a bet
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetplacementHomePageSingleBet() throws Exception {
		String testCase = "VerifyBetplacementHomePageSingleBet";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			int initBetslipCount = common.GetBetSlipCount();
			// Add selection to BetSlip
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			if(isElementPresent(BetslipElements.HomePage_RightColumnRaces))
			click(BetslipElements.HomePage_RightColumnRaces);
			else
				click(BetslipElements.HomePage_RightColumnEvents);
			// Validate the BetSlip counter on adding selection to BetSlip
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			// Enter stake and place bet
			common.BetPlacement_WithAcceptNContinue("Single", TextControls.stakeValue, 1);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-18 Double bets - inPlay & Preplay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyDoubleOnLiveEvent() throws Exception {
		String testCase = "VerifyDoubleOnLiveEvent";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			HGfunctionObj.NavigateAddInplaySelection(2);
			common.BetPlacement_WithAcceptNContinue("double", TextControls.stakeValue, 2);
			// logger.log(LogStatus.PASS, "VerifyDoubleOnLiveEvent");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-19 Double bets - PrePlay -BetSlip & Receipt Place a bet when odds are
	 * not changing
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyDoubleBetPlacement() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		// logger = extent.startTest("VerifyDoubleBetPlacement");
		int multiBetCount;
		String[] aryOdds = new String[2];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(8);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "double", multiBetCount, aryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "double", "", "");
			String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 8, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 8, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "Double");
			// logger.log(LogStatus.PASS, "VerifyDoubleBetPlacement");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-20 Reuse Selections - Make 2-3 selections and place the bet. Click on
	 * 'Reuse Selections' and again place the bet
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyReUseMultiSels() throws Exception {
		String testCase = "VerifyReUseMultiSels";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			testdata.NavigateUsingTestData(8);
			testdata.NavigateUsingTestData(1);
			common.BetPlacement_WithAcceptNContinue("double", TextControls.stakeValue, 2);
			click(BetslipElements.reUseOnBetslip);
			common.BetPlacement_WithAcceptNContinue("double", TextControls.stakeValue, 2);
			;
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-21 Place Each Way Single Bet
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyEWBetPlacement_SingleBet() throws Exception {
		String testCase = "VerifyEWBetPlacement_SingleBet";
		String[] aryOdd = new String[1];
		String TypeName = testdata.Readtestdata("PreProdEvents", 1, 2);
		String Market = testdata.Readtestdata("PreProdEvents", 1, 6);
		String eventName = testdata.Readtestdata("PreProdEvents", 1, 4);
		String Selection = testdata.Readtestdata("PreProdEvents", 1, 5);
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdd[0] = testdata.NavigateUsingTestData(1);
			String eachwayTerms = GetElementText(By.xpath("//div[@class='each-way-container']"));
			BTbetslipObj.EnterStake(eventName, Selection, Market, TextControls.stakeValue, "Single", true);
			BTbetslipObj.ValidateBetReceipt(TypeName, eventName, Market, Selection, aryOdd, TextControls.stakeValue,
					eachwayTerms, false, 1, "single");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-22 Multiple bets - Each Way - Betslip & Receipt Validate Bet
	 * Information
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyEWBetPlacement_DoubleBet() throws Exception {
		String testCase = "VerifyEWBetPlacement_DoubleBet";
		String[] aryOdds = new String[2];
		int multiBetCount;
		String doubleSel, eventSel, eachwayTerms1, eachwayTerms2, eachwayTerms;
		String eventName = testdata.Readtestdata("PreProdEvents", 1, 4);
		String Selection = testdata.Readtestdata("PreProdEvents", 1, 5);
		String eventName1 = testdata.Readtestdata("PreProdEvents", 14, 4);
		String Selection1 = testdata.Readtestdata("PreProdEvents", 14, 5);
		doubleSel = Selection + "-" + Selection1;
		eventSel = eventName + "-" + eventName1;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			eachwayTerms1 = GetElementText(HorseRacingElements.EWbox);
			aryOdds[1] = testdata.NavigateUsingTestData(14);
			eachwayTerms2 = GetElementText(HorseRacingElements.EWbox);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			eachwayTerms = eachwayTerms1 + "|" + eachwayTerms2;
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", true);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSel, aryOdds, TextControls.stakeValue, eachwayTerms,
					false, multiBetCount, "Double");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-23 SP Bet Placement
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_SP() throws Exception {
		String testCase = "VerifyBetPlacement_SP";
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String selection = testdata.Readtestdata("PreProdEvents", 16, 5);
		int initBetSlipCount;
		int laterBetslipCnt;
		String oddBtn;
		try {
			if (!ProjectName.contains("Desktop_German")) {
				launchweb();
				Common.Login();
				Common.OddSwitcher(TextControls.decimal);
				initBetSlipCount = common.GetBetSlipCount();
				common.NavigateToSportsPage(className);
				String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName
						+ "')]/../..//div//*[contains(text(), '" + eventName + "')]";
				WebElement Element = getDriver().findElement(By.xpath(hrEvent));
				scrollToView(Element);
				click(By.xpath(hrEvent));
				oddBtn = "//*[@class='horse']//*[@class='name' and contains(text(), '" + selection
						+ "')]/../../../../../..//selection-button/div/div/span[@class='price' and contains(text(), 'SP')]";
				click(By.xpath(oddBtn));
				laterBetslipCnt = common.GetBetSlipCount();
				Assert.assertTrue((initBetSlipCount + 1 == laterBetslipCnt),
						"Mismatch in Betslip count on adding a selction, Expected:" + (initBetSlipCount + 1)
								+ ", Actual:" + laterBetslipCnt + ".");
				Common.Enterstake(TextControls.stakeValue);
				BTbetslipObj.BetPlacement(true);
			} else {
				System.out.println("GH/HR is not valid for German Site");
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-24 Change 'View odds as' from fractional to decimal and confirm then
	 * the the odds change on inPlay section they are still in decimal
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyFractionalToDecimalOnAllModulesOnBetSlip() throws Exception {
		String testCase = "VerifyFractionalToDecimalOnAllModulesOnBetSlip";
		String fractionalValue, decimalValue, betslipText, OddsText, tabName, selXpath;
		double highlightnum1, highlightnum2, decimalResult;
		int selCount = 0;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher("fractional");
			if (!getAttribute(HomeGlobalElements.highLightsTab, "class").contains("expanded"))
				click(HomeGlobalElements.highLightsTab);
			List<WebElement> highlightsFiltersList = getDriver().findElements(By.xpath(
					"//h1[contains(@class, 'expand-list')]//span[contains(text(), '" + TextControls.hightlightText
							+ "')]/following::tabs[contains(@params, 'featured')]//nav[@class='tabs']/ul/li"));
			for (int i = 1; i < highlightsFiltersList.size(); i++) {
				scrollToView(highlightsFiltersList.get(i));
				highlightsFiltersList.get(i).click();
				break;
				/*tabName = highlightsFiltersList.get(i).getText();
				if (tabName.toLowerCase().contains(TextControls.Tennis.toLowerCase())) {
					scrollToView(highlightsFiltersList.get(i));
					highlightsFiltersList.get(i).click();
					break;
					} else
				 if (tabName.toLowerCase().contains(TextControls.moreText.toLowerCase())) {
					// click(HomeGlobalElements.moreTab);
					scrollToView(highlightsFiltersList.get(i));
					highlightsFiltersList.get(i).click();
					List<WebElement> sportsNameInMore = getDriver().findElements(
							By.xpath("//tabs[contains(@params, '" + TextControls.hightlightText.toLowerCase()
									+ "')]//div[@class='more']//span[@class='title']"));
					// tabs[contains(@params,
					// '"+TextControls.hightlightText+"')]//div[@class='more']//span[@class='title']
					for (int j = 0; j <= sportsNameInMore.size(); j++) {
						tabName = sportsNameInMore.get(j).getText();
						if (tabName.toLowerCase().contains(TextControls.Tennis.toLowerCase())) {
							sportsNameInMore.get(j).click();
							break;
						}
						if (j == sportsNameInMore.size()) {
							if (!tabName.toLowerCase().contains(TextControls.Tennis))
								Assert.fail("Tennis event not found in Highlight module");
						}
					}
				}*/
			}
			List<WebElement> highlightsEventList = getDriver().findElements(By.xpath(
					"//h1[contains(@class, 'expand-list')]//span[contains(text(), '" + TextControls.hightlightText
							+ "')]/following::event-group-simple//div[@class='event-group']//event-list//div[@class='event-list pre']"));
			for (int i = 1; i < highlightsEventList.size(); i++) {
				if (selCount == 1)
					break;
				selXpath = "(//h1[contains(@class, 'expand-list')]//span[contains(text(), '"
						+ TextControls.hightlightText
						+ "')]/following::event-group-simple//div[@class='event-group']//event-list//div[@class='event-list pre'])["
						+ i + "]//div[@class='selection'][1]//selection-button";
				if (!getDriver().findElement(By.xpath(selXpath)).isDisplayed()) {
					// logger.log(LogStatus.INFO,"selection not found for the
					// event");
				} else {
					fractionalValue = GetElementText(By.xpath(selXpath));
					String[] eventNumber = fractionalValue.split("/");
					highlightnum1 = Double.parseDouble(eventNumber[0]);
					highlightnum2 = Double.parseDouble(eventNumber[1]);
					decimalResult = (highlightnum1 / highlightnum2) + 1;
					decimalResult = Math.floor(decimalResult * 100) / 100;
					// change fractional to decimal
					common.scrollPage("top");
					Thread.sleep(3000);
					Common.OddSwitcher(TextControls.decimal);
					decimalValue = GetElementText(By.xpath(selXpath));
					double value = Convert.ToDouble(decimalValue);
					// compare
					Assert.assertTrue(decimalResult <= value,
							"Fraction value is not changed to Decimal value: Fractional value = " + fractionalValue
									+ ", Decimal value: " + decimalValue);
					// Add to BetSlip
					WebElement element = getDriver().findElement(By.xpath(selXpath));
					scrollToView(element);
					click(By.xpath(selXpath));
					Thread.sleep(2000);
					// Verify decimal odd value displayed in betSlip
					// String oddxPath = "//div[@class='betslip-text
					// odds-convert' and text()='" + decimalValue + "']";
					String oddxPath = "//div[@class='odds-container']//div[text()='" + decimalValue + "']";
					if (!isElementDisplayed(By.xpath(oddxPath)))
						oddxPath = "//div[@class='odds-container']//select/option[text()='" + decimalValue + "']";
					betslipText = GetElementText(By.xpath(oddxPath));
					Assert.assertTrue(betslipText.equals(decimalValue),
							"Same decimal value is not reflected in Betslip");
					common.scrollPage("Top");
					Common.Enterstake(TextControls.stakeValue);
					Common.placebet();
					// Verifying decimal text in receipt
					OddsText = GetElementText(BetslipElements.receiptOdds);
					Assert.assertTrue(decimalValue.equals(OddsText),
							"Fraction value is not changed to Decimal value in receipt");
					// logger.log(LogStatus.INFO,"changing from fractional to
					// decimal on Betslip was successfull");
					selCount++;
				}
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-25 Event Navigation from inPlay module below betSlip
	 * 
	 * @return none
	 * @throws Exception
	 * @throws InvalidPropertiesFormatException
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyNavigationToInPlayModuleBelowBetslip() throws Exception {
		String testCase = "VerifyNavigationToInPlayModuleBelowBetslip";
		String actBreadcrumbs, expBreadcrumbs;
		String BrowserType = ReadTestSettingConfig.getTestsetting("BrowserType");
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			WebElement inplayModule = getDriver().findElement(BetslipElements.betslipInPlay);
			scrollToView(inplayModule);
			List<WebElement> inplayEvents = getDriver()
					.findElements(By.xpath("//div[@class='event-list event-list-narrow live']"));
			inplayEvents.get(1).click();
			actBreadcrumbs = GetElementText(HomeGlobalElements.breadcrumbs);
			actBreadcrumbs = actBreadcrumbs.replaceAll("\n", "");
			if (BrowserType.equals("InternetExplorer"))
				expBreadcrumbs = TextControls.Home + ">" + " " + TextControls.inPlay + ">" + " "
						+ TextControls.Football;
			else
				expBreadcrumbs = TextControls.Home + ">" + TextControls.inPlay + ">" + TextControls.Football;
			Assert.assertTrue(actBreadcrumbs.contains(expBreadcrumbs));
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.genricScoreBoard),
					"Score Board is not found , Hence navigation to inplay event failed");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 26 Check the error messages -Enter stake value with Min/Max stake for
	 * the selection and No stake for selection
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyMinMaxStake_StakefieldLength_functionality() throws Exception {
		String testCase = "VerifyMinMaxStake_StakefieldLength_functionality";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			BTbetslipObj.VerifyMinMax_StakeField_Function();
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 27 Single bets - inplay -Betslip & Receipt Place a bet when odds are
	 * changing Data updates and bet placement - In Play
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyInplaySingleBet() throws Exception {
		String testCase = "VerifyInplaySingleBet";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			click(By.xpath("(//div[@class='odds-button live'])[2]"));
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			common.BetPlacement_WithAcceptNContinue("single", TextControls.stakeValue, 1);
			// logger.log(LogStatus.PASS, "VerifyInplaySingleBet");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 28 Race card page - Login from Header,add selection to betSlip and
	 * placeBet
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyRaceCardPageBetPlacement() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		String ProjectName = ReadTestSettingConfig.getTestsetting("ProjectName");
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String selection = testdata.Readtestdata("PreProdEvents", 16, 5);
		try {
			if (!ProjectName.contains("Desktop_German")) {
				launchweb();
				Common.Login();
				Common.OddSwitcher(TextControls.decimal);
				common.NavigateToSportsPage(className);
				HGfunctionObj.VerifyBreadCrums(TextControls.Home, className, "", "");
				getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName
						+ "')]/../..//div//*[contains(text(), '" + eventName + "')]";
				WebElement Element = getDriver().findElement(By.xpath(hrEvent));
				scrollToView(Element);
				click(By.xpath(hrEvent));
				String oddBtn = "//*[@class='horse']//*[@class='name' and contains(text(), '" + selection
						+ "')]/../../../../../..//selection-button/div/div/span[@class='price' and contains(text(), 'SP')]";
				click(By.xpath(oddBtn));
				Common.Enterstake(TextControls.stakeValue);
				Common.placebet();
			} else {
				System.out.println("GH/HR is not valid for German Site");
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-29 Remove selections - Add one selection and click on 'X' icon
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyRemoveSelections() throws Exception {
		String testCase = "VerifyRemoveSelections";
		int initBetslipCount, laterBetslipCnt;
		try {
			launchweb();
			common.NavigateToSportsPage(TextControls.Football);
			initBetslipCount = common.GetBetSlipCount();
			click(HomeGlobalElements.SportLandingPageOdd);
			laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			click(BetslipElements.removeSel);
			laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount == laterBetslipCnt),
					" Betslip count is not zero after removing a selction");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-30 Remove selections - Add multiple selections and click on Remove all
	 * button
	 * 
	 * @return none
	 * @throws Exception
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyRemoveAllButtonInBetSLip() throws Exception {
		String testCase = "VerifyRemoveAllButtonInBetSLip";
		int initBetslipCount, laterBetslipCnt;
		try {
			launchweb();
			common.OddSwitcher("decimal");
			initBetslipCount = common.GetBetSlipCount();
			testdata.NavigateUsingTestData(1);
			testdata.NavigateUsingTestData(8);
			laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 2 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 2) + ", Actual:"
							+ laterBetslipCnt + ".");
			Assert.assertTrue(isElementDisplayed(BetslipElements.removeAllBtn), "Remove all Button is not found");
			click(BetslipElements.removeAllBtn);
			laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount == laterBetslipCnt),
					"Betslip count is not zero after removing a selctions");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-31 Trixie bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_TrixieBet() throws Exception {
		String testCase = "VerifyBetplacement_TrixieBet";
		int multiBetCount;
		String[] aryOdds = new String[3];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "Trixie", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "Trixie", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "Trixie", "", "");
			String trixieSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", trixieSelName, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "Trixie");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-32 Patent bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_PatentBet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_PatentBet";
		int multiBetCount;
		String[] aryOdds = new String[3];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "Patent", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "Patent", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "Patent", "", "");
			String patentSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", patentSelName, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "Patent");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-33 Yankee bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_YankeeBet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_YankeeBet";
		int multiBetCount;
		String[] aryOdds = new String[4];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "yankee", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "yankee", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "yankee", "", "");
			String yankeeSel = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4);
			;
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", yankeeSel, aryOdds, TextControls.stakeValue, "", false,
					multiBetCount, "yankee");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-34 FourfoldAccumulator bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_FourfoldAccumulatorBet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_FourfoldAccumulatorBet";
		int multiBetCount;
		String[] aryOdds = new String[4];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "accumulator (4)", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "accumulator (4)", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "accumulator (4)", "", "");
			String fourfoldAccumulatorSel = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", fourfoldAccumulatorSel, aryOdds, TextControls.stakeValue,
					"", false, multiBetCount, "accumulator (4)");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-35 FivefoldAccumulator bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_FivefoldAccumulatorBet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_FivefoldAccumulatorBet";
		int multiBetCount;
		String[] aryOdds = new String[5];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			aryOdds[4] = testdata.NavigateUsingTestData(7);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "accumulator (5)", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "accumulator (5)", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "accumulator (5)", "", "");
			String AccumulatorSel = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", AccumulatorSel, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "accumulator (5)");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-36 Heinz bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_HeinzBet() throws Exception {
		String testCase = "VerifyBetplacement_HeinzBet";
		int multiBetCount;
		String[] aryOdds = new String[6];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			aryOdds[4] = testdata.NavigateUsingTestData(7);
			aryOdds[5] = testdata.NavigateUsingTestData(14);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "heinz", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "heinz", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "heinz", "", "");
			String heinzSel = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 14, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 14, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", heinzSel, aryOdds, TextControls.stakeValue, "", false,multiBetCount, "heinz");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-37 Lucky15 bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_Lucky15Bet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_Lucky15Bet";
		int multiBetCount;
		String[] aryOdds = new String[4];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "lucky 15", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "lucky 15", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "lucky 15", "", "");
			String lucky15Sel = testdata.Readtestdata("PreProdEvents", 1, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4);
			;
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", lucky15Sel, aryOdds, TextControls.stakeValue, "", false,
					multiBetCount, "lucky 15");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-38 Lucky63 bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_Lucky63Bet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_Lucky63Bet";
		int multiBetCount;
		String[] aryOdds = new String[6];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(14);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			aryOdds[4] = testdata.NavigateUsingTestData(7);
			aryOdds[5] = testdata.NavigateUsingTestData(1);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "lucky 63", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "lucky 63", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "lucky 63", "", "");
			String lucky63Sel = testdata.Readtestdata("PreProdEvents", 14, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 5) + "-" + testdata.Readtestdata("PreProdEvents", 1, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 14, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 4) + "-" + testdata.Readtestdata("PreProdEvents", 1, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", lucky63Sel, aryOdds, TextControls.stakeValue, "", false,
					multiBetCount, "lucky 63");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-38 SixfoldAccumulator bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyBetplacement_SixfoldAccumulatorBet() throws Exception {
		// caseID =814242;
		String testCase = "VerifyBetplacement_Lucky63Bet";
		int multiBetCount;
		String[] aryOdds = new String[6];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(14);
			aryOdds[1] = testdata.NavigateUsingTestData(5);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(13);
			aryOdds[4] = testdata.NavigateUsingTestData(7);
			aryOdds[5] = testdata.NavigateUsingTestData(1);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "accumulator (6)", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "accumulator (6)", false);
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "accumulator (6)", "", "");
			String accumulatorSel = testdata.Readtestdata("PreProdEvents", 14, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 5) + "-" + testdata.Readtestdata("PreProdEvents", 8, 5)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 5) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 5) + "-" + testdata.Readtestdata("PreProdEvents", 1, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 14, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 5, 4) + "-" + testdata.Readtestdata("PreProdEvents", 8, 4)
					+ "-" + testdata.Readtestdata("PreProdEvents", 13, 4) + "-"
					+ testdata.Readtestdata("PreProdEvents", 7, 4) + "-" + testdata.Readtestdata("PreProdEvents", 1, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", accumulatorSel, aryOdds, TextControls.stakeValue, "",
					false, multiBetCount, "accumulator (6)");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-39 GreyHoundMulti bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_GreyHoundMultiBet() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		String germanLanguage = ReadTestSettingConfig.getTestsetting("ProjectName");
		int multiBetCount;
		String[] aryOdds = new String[2];
		String className = testdata.Readtestdata("PreProdEvents", 18, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 18, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 18, 4);
		String marketName = testdata.Readtestdata("PreProdEvents", 18, 6);
		String selName = testdata.Readtestdata("PreProdEvents", 18, 5);
		String className1 = testdata.Readtestdata("PreProdEvents", 17, 1);
		String typeName1 = testdata.Readtestdata("PreProdEvents", 17, 2);
		String eventName1 = testdata.Readtestdata("PreProdEvents", 17, 4);
		String marketName1 = testdata.Readtestdata("PreProdEvents", 17, 6);
		String selName1 = testdata.Readtestdata("PreProdEvents", 17, 5);
		try {
			launchweb();
			if (germanLanguage.equals("Desktop_German"))
				System.out.println("GreyHounds is not available for German");
			else {
				Common.Login();
				Common.OddSwitcher(TextControls.decimal);
				aryOdds[0] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(className, typeName, eventName,
						marketName, selName);
				aryOdds[1] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(className1, typeName1, eventName1,
						marketName1, selName1);
				multiBetCount = BTbetslipObj.GetMultiplesBetCount();
				BTbetslipObj.VerifyBetSlip("", "", "", "", "", "double", multiBetCount, aryOdds);
				BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
				BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "double", "", "");
				String doubleSelName = testdata.Readtestdata("PreProdEvents", 18, 5) + "-"
						+ testdata.Readtestdata("PreProdEvents", 17, 5);
				String eventSel = testdata.Readtestdata("PreProdEvents", 18, 4) + "-"
						+ testdata.Readtestdata("PreProdEvents", 17, 4);
				BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, aryOdds, TextControls.stakeValue, "",
						false, multiBetCount, "Double");
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-40 SP Double Bet Placement
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_SP_Double() throws Exception {
		String testCase = "VerifyBetPlacement_SP_Double";
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String selection = testdata.Readtestdata("PreProdEvents", 16, 5);
		String className1 = testdata.Readtestdata("PreProdEvents", 10, 1);
		String typeName1 = testdata.Readtestdata("PreProdEvents", 10, 2);
		String eventName1 = testdata.Readtestdata("PreProdEvents", 10, 4);
		String selection1 = testdata.Readtestdata("PreProdEvents", 10, 5);
		String[] aryOdds = new String[2];
		int multiBetCount;
		String GHselBtn, HRselBtn;
		String hrEvent;
		String odd = "SP";
		try {
			{
				launchweb();
				Common.Login();
				Common.OddSwitcher(TextControls.decimal);
				common.NavigateToSportsPage(className);
				hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName
						+ "')]/../..//div//*[contains(text(), '" + eventName + "')]";
				WebElement Element = getDriver().findElement(By.xpath(hrEvent));
				scrollToView(Element);
				click(By.xpath(hrEvent));
				aryOdds[0] = odd;
				GHselBtn = "//*[@class='horse']//*[@class='name' and contains(text(), '" + selection
						+ "')]/../../../../../..//selection-button/div/div/span[@class='price' and contains(text(), 'SP')]";
				click(By.xpath(GHselBtn));
				common.NavigateToSportsPage(className1);
				hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName1
						+ "')]/../..//div//*[contains(text(), '" + eventName1 + "')]";
				Element = getDriver().findElement(By.xpath(hrEvent));
				scrollToView(Element);
				click(By.xpath(hrEvent));
				aryOdds[1] = odd;
				HRselBtn = "//*[@class='horse']//*[@class='name' and contains(text(), '" + selection1
						+ "')]/../../../../../../..//div[@class='odds-button']/span[text()='SP']";
				click(By.xpath(HRselBtn));
				multiBetCount = BTbetslipObj.GetMultiplesBetCount();
				// BTbetslipObj.VerifyBetSlip("","","","","","double",
				// multiBetCount, aryOdds);
				BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
				BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "double", "", "");
				String doubleSelName = testdata.Readtestdata("PreProdEvents", 16, 5) + "-"
						+ testdata.Readtestdata("PreProdEvents", 10, 5);
				String eventSel = testdata.Readtestdata("PreProdEvents", 16, 4) + "-"
						+ testdata.Readtestdata("PreProdEvents", 10, 4);
				BTbetslipObj.ValidateBetReceipt("", eventSel, "", doubleSelName, aryOdds, TextControls.stakeValue, "",
						true, multiBetCount, "Double");
			}
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-41 Treble bets - inPlay BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_Treble_OnLiveEvent() throws Exception {
		String testCase = "VerifyBetPlacement_Treble_OnLiveEvent";
		int multiBetCount;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			HGfunctionObj.NavigateAddInplaySelection(3);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			common.BetPlacement_WithAcceptNContinue("treble", TextControls.stakeValue, multiBetCount);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-42 Trixie bets - inPlay BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_Trixie_OnLiveEvent() throws Exception {
		String testCase = "VerifyBetPlacement_Trixie_OnLiveEvent";
		int multiBetCount;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			HGfunctionObj.NavigateAddInplaySelection(3);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			common.BetPlacement_WithAcceptNContinue("Trixie", TextControls.stakeValue, multiBetCount);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-43 Patent bets - inPlay BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_Patent_OnLiveEvent() throws Exception {
		String testCase = "VerifyBetPlacement_Patent_OnLiveEvent";
		int multiBetCount;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			Assert.assertTrue(isElementDisplayed(BetslipElements.inPlay), "Inplay module not found on home page");
			getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			HGfunctionObj.NavigateAddInplaySelection(3);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			common.BetPlacement_WithAcceptNContinue("patent", TextControls.stakeValue, multiBetCount);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-44 GreyHoundMulti bets - PrePlay -BetSlip & Receipt
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacement_WinOrEachWay_EWnonEW() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		int multiBetCount;
		String[] aryOdds = new String[1];
		String className = testdata.Readtestdata("PreProdEvents", 16, 1);
		String typeName = testdata.Readtestdata("PreProdEvents", 16, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 16, 4);
		String selection = testdata.Readtestdata("PreProdEvents", 16, 5);
		String oddBtn, odd = "SP";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.GetBetSlipCount();
			common.NavigateToSportsPage(className);
			String hrEvent = "//div[@class='racing-scheduled-meeting']//div//*[contains(text(), '" + typeName
					+ "')]/../..//div//*[contains(text(), '" + eventName + "')]";
			WebElement Element = getDriver().findElement(By.xpath(hrEvent));
			scrollToView(Element);
			click(By.xpath(hrEvent));
			oddBtn = "//*[@class='horse']//*[@class='name' and contains(text(), '" + selection
					+ "')]/../../../../../..//selection-button/div/div/span[@class='price' and contains(text(), 'SP')]";
			click(By.xpath(oddBtn));
			aryOdds[0] = odd;
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", odd, "", "single", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "single", true);
			BTbetslipObj.BetPlacement(true);
			Assert.assertTrue(isElementDisplayed(BetslipElements.reUseOnBetslip), "");
			click(BetslipElements.reUseOnBetslip);
			Thread.sleep(2000);
			click(HorseRacingElements.EWbox);
			// Enter stake and Place bet NON EW
			BTbetslipObj.VerifyBetSlip("", "", "", odd, "", "single", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "single", false);
			BTbetslipObj.BetPlacement(true);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-44 Verifying Different Markets of a sub type appear in tabs on subType
	 * page
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetPlacementWithDiffMarketsOfSubtypePage() throws Exception {
		String testCase = SBP1Desktop.class.getName();
		String showMoreOnTabComp;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.NavigateToSportsPage(TextControls.Football);
			String Type = "//li[@class='tab']//span[text()=" + "\"" + testdata.Readtestdata("PreProdEvents", 1, 2)
					+ "\"" + "]";
			String subType = "//div[@class='links']//a[contains(text(),'" + testdata.Readtestdata("PreProdEvents", 1, 3)
					+ "')]";
			click(By.xpath(Type));
			click(By.xpath(subType));
			String tabListXpath = "//header[@class='event-header Football']/..//nav[@class='tabs']";
			if (isElementPresent(By.xpath(tabListXpath))) {
				Assert.assertTrue(isElementDisplayed(By.xpath(tabListXpath)), "Tabbed component is not present");
				List<WebElement> tabList = getDriver().findElements(By.xpath(tabListXpath + "//ul//li"));
				for (int i = 0; i < tabList.size(); i++) {
					String tabName = tabList.get(i).getText();
					if (tabName.toLowerCase().contains(TextControls.moreText.toLowerCase())) {
						break;
					} else {
						tabList.get(i).click();
						showMoreOnTabComp = getDriver().findElement(By.xpath("//div[@class='event'][1]")).getText();
						if (showMoreOnTabComp.toLowerCase().contains(TextControls.moreText.toLowerCase())) {
							List<WebElement> intEventList = getDriver()
									.findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
							click(HomeGlobalElements.showMoreEvents);
							List<WebElement> eventList = getDriver()
									.findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
							Assert.assertTrue((eventList.size() > intEventList.size()),
									"Remaining events not displayed on click of Show More tab");
							Assert.assertTrue(isElementPresent(HomeGlobalElements.showLessEvents),
									"Show Less link/tab not found");
							click(HomeGlobalElements.showLessEvents);
							List<WebElement> showLessList = getDriver()
									.findElements(By.xpath("//div[@class='outright']//div[@class='selection']"));
							Assert.assertTrue(showLessList.size() == intEventList.size(),
									"Few not displayed on click of Show Less tab");
							common.scrollPage("top");
							Thread.sleep(1000);
						}
					}
				}
				click(By.xpath("//div[@class='event'][1]//tabs//div[@class='selection'][1]//span[@class='price']"));
				Entervalue(BetslipElements.stakeBox, TextControls.stakeValue);
				BTbetslipObj.BetPlacement(false);
			} else
				System.out.println("Outright bucket - Tabbed compontent is not Present ");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-45 Verifies the bet placement with out login error MSG.
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBetplacementWoLoginErrorMsg() throws Exception {
		String testCase = "VerifyBetplacementWoLoginErrorMsg";
		int multiBetCount;
		String[] aryOdds = new String[1];
		try {
			launchweb();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "single", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "single", false);
			click(BetslipElements.placeBet);
			Assert.assertTrue(isElementDisplayed(BetslipElements.placebetLoginErrMsg),
					"'To place a bet you must log in' message was not found in betslip");
			Common.Login();
			BTbetslipObj.VerifyBetDetails("", "", "", aryOdds, TextControls.stakeValue, "", "single", "", "");
			String SelName = testdata.Readtestdata("PreProdEvents", 1, 5);
			String eventSel = testdata.Readtestdata("PreProdEvents", 1, 4);
			BTbetslipObj.ValidateBetReceipt("", eventSel, "", SelName, aryOdds, TextControls.stakeValue, "", false,
					multiBetCount, "single");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-46 Verifies the error MSG_20 selections.
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyErrorMsg_20Sels() throws Exception {
		String testCase = "VerifyErrorMsg_25Sels";
		boolean isStatus = false;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			click(FootballElements.couponLink);
			List<WebElement> couponList = getDriver().findElements(FootballElements.couponOnList);
			for (int z = 1; z <= couponList.size(); z++) {
				if (isStatus == true)
					break;
				click(By.xpath("//div[@class='container coupon-sidebar']/ul/li[" + z + "]/a"),
						"Coupons is not present on home page");
				Assert.assertTrue(isElementDisplayed(FootballElements.eventsOnCoupon),
						"Events are not present on coupon page");
				List<WebElement> eventList = getDriver()
						.findElements(By.xpath("//*[@class='coupon-list']//*[@class='name']"));
				if (eventList.size() > 10) {
					// Add 21 selections
					for (int i = 1; i < 22; i++) {
						if (i == 21)
							Thread.sleep(6000);
						getDriver().findElement(By.xpath("(//span[@class='odds-convert'])[" + i + "]")).click();
					}
					isStatus = true;
				} else
					System.out.println("Required number of events not present in Coupon page");
			}
			Thread.sleep(3000);
			// check for error message
			Assert.assertTrue(isElementDisplayed(BetslipElements.betSlipFullMsg),
					"Error message was not displayed when betSlip is full");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-47 Verifies the error MSG_ place bet without stake.
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyErrorMsg_PlaceBetWithoutStake() throws Exception {
		String testCase = "VerifyErrorMsg_PlaceBetWithoutStake";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.NavigateToSportsPage(TextControls.Football);
			click(HomeGlobalElements.SportLandingPageOdd);
			Thread.sleep(2000);
			click(BetslipElements.placeBet);
			Assert.assertTrue(isElementDisplayed(BetslipElements.emptyStakeErrorMsg),
					"Error message was not displayed on attempting to Place Bet without entering any Stake");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-47 Verifies the error MSG_ place bet without stake.
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyMultiBetByAdding2SelectionsFromSameEvent() throws Exception {
		String testCase = "VerifyMultiBetByAdding2SelectionsFromSameEvent";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			testdata.NavigateUsingTestData(1);
			/*
			 * common.NavigateToEventSubType(TextControls.Football, false,
			 * true); getDriver().manage().timeouts().implicitlyWait(60,
			 * TimeUnit.SECONDS);
			 * if(!isElementPresent(BetslipElements.EDPHeader))
			 * scrollAndClick(BetslipElements.eventClick);
			 */
			List<WebElement> oddsList = getDriver().findElements(By.xpath("//div[@class='odds-button']"));
			oddsList.get(0).click();
			Assert.assertFalse(isElementPresent(By.xpath("//span[@class='market-information-selection']")),
					"Multiple bet should not be displayed on adding two selections from same Event");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-48 Verifies the multiple selections with EachWay
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyMultipleSelectionsWithEW() throws Exception {
		String testCase = "VerifyMultipleSelectionsWithEW";
		int multiBetCount;
		String[] aryOdds = new String[4];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(
					testdata.Readtestdata("PreProdEvents", 18, 1), testdata.Readtestdata("PreProdEvents", 18, 2),
					testdata.Readtestdata("PreProdEvents", 18, 4), testdata.Readtestdata("PreProdEvents", 18, 6),
					testdata.Readtestdata("PreProdEvents", 18, 5));
			aryOdds[1] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(
					testdata.Readtestdata("PreProdEvents", 17, 1), testdata.Readtestdata("PreProdEvents", 17, 2),
					testdata.Readtestdata("PreProdEvents", 17, 4), testdata.Readtestdata("PreProdEvents", 17, 6),
					testdata.Readtestdata("PreProdEvents", 17, 5));
			aryOdds[2] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(
					testdata.Readtestdata("PreProdEvents", 3, 1), testdata.Readtestdata("PreProdEvents", 3, 2),
					testdata.Readtestdata("PreProdEvents", 3, 4), testdata.Readtestdata("PreProdEvents", 3, 6),
					testdata.Readtestdata("PreProdEvents", 3, 5));
			aryOdds[3] = HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(
					testdata.Readtestdata("PreProdEvents", 4, 1), testdata.Readtestdata("PreProdEvents", 4, 2),
					testdata.Readtestdata("PreProdEvents", 4, 4), testdata.Readtestdata("PreProdEvents", 4, 6),
					testdata.Readtestdata("PreProdEvents", 4, 5));
			Assert.assertTrue(
					isElementDisplayed(
							By.xpath("//div[@class='slip-item multiple']//div[@class='each-way-container']")),
					"Ew checkbox not present in Multiple bet container");
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "accumulator (4)", multiBetCount, aryOdds);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "accumulator (4)", true);
			BTbetslipObj.BetPlacement(false);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC-49 Check the balance - Place the bet and check the balance in the top
	 * header changes accordingly.
	 * 
	 * @return none
	 * @throws Exception
	 * @throws NoSuchElementException
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyBalanceTopHeaderChanges() throws NoSuchElementException, Exception {
		String testCase = "VerifyBalanceTopHeaderChanges";
		double initbalance, totalStake, actualBal, expectedBal;
		try {
			launchweb();
			// BTbetslipObj.ChecktheBalance_topHeaderChanges_function();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			int initBetslipCount = common.GetBetSlipCount();
			initbalance = LoginfunctionObj.GetBalance();
			// Add selection to BetSlip
			testdata.NavigateUsingTestData(1);
			// Validating the BetSlip counter on adding selection to BetSlip
			int laterBetslipCnt = common.GetBetSlipCount();
			Assert.assertTrue((initBetslipCount + 1 == laterBetslipCnt),
					"Mismatch in Betslip count on adding a selction, Expected:" + (initBetslipCount + 1) + ", Actual:"
							+ laterBetslipCnt + ".");
			Common.Enterstake(TextControls.stakeValue);
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			click(BetslipElements.placeBet);
			// common.BetPlacement_WithAcceptNContinue("single",
			// TextControls.stakeValue, 1);
			actualBal = LoginfunctionObj.GetBalance();
			expectedBal = initbalance - totalStake;
			expectedBal=Math.round(expectedBal*100.0)/100.0;
			System.out.println("actual bal : " + actualBal + "== expected bal :" + expectedBal);
			Assert.assertTrue(actualBal == expectedBal, "Header Balance is not updated accordingly after Placing Bet");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 50 Verifies the multiple selections with EachWay
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyEWDisplayForMultipleBets_TrebleBetPlacement() throws Exception {
		String testCase = "VerifyEWDisplayForMultipleBets_TrebleBetPlacement";
		int multiBetCount;
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents", 18, 1),
					testdata.Readtestdata("PreProdEvents", 18, 2), testdata.Readtestdata("PreProdEvents", 18, 4),
					testdata.Readtestdata("PreProdEvents", 18, 6), testdata.Readtestdata("PreProdEvents", 18, 5));
			HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents", 17, 1),
					testdata.Readtestdata("PreProdEvents", 17, 2), testdata.Readtestdata("PreProdEvents", 17, 4),
					testdata.Readtestdata("PreProdEvents", 17, 6), testdata.Readtestdata("PreProdEvents", 17, 5));
			Assert.assertTrue(isElementDisplayed(BetslipElements.multiplesEWbox),
					"Ew checkbox not present in Multiple bet container");
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "double", multiBetCount, null);
			HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents", 3, 1),
					testdata.Readtestdata("PreProdEvents", 3, 2), testdata.Readtestdata("PreProdEvents", 3, 4),
					testdata.Readtestdata("PreProdEvents", 3, 6), testdata.Readtestdata("PreProdEvents", 3, 5));
			Assert.assertTrue(isElementDisplayed(BetslipElements.multiplesEWbox),
					"Ew checkbox not present in Multiple bet container");
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "treble", multiBetCount, null);
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "Trixie", multiBetCount, null);
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "patent", multiBetCount, null);
			BTbetslipObj.VerifyBetSlip("", "", "", "", "", "treble", multiBetCount, null);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "treble", true);
			BTbetslipObj.BetPlacement(false);
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 51 Verifies the multiple bet updates on adding two selections from
	 * same event
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyMultiBetUpdtdOnAddingSelectionsFromSameEvnt() throws Exception {
		String testCase = "VerifyMultiBetUpdtdOnAddingSelectionsFromSameEvnt";
		String[] aryOdds = new String[3];
		String[] selectionName = new String[3];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			common.NavigateToEventSubType(TextControls.Football, false, true);
			List<WebElement> events = getDriver().findElements(BetslipElements.eventClick);
			scrollToView(events.get(0));
			events.get(0).click();
			String eventname = GetElementText(
					By.xpath("//span[@class='gt']/following::span[@data-bind='html: title']"));
			String marketName = GetElementText(By.xpath("(//span[@data-bind='text: nameTranslations.get'])[1]"));
			for (int i = 1; i < 3; i++) {
				String selectionxpath = "//span[text()='" + marketName + "']/following::div[@class='selection'][" + i
						+ "]";
				aryOdds[i] = GetElementText(
						By.xpath(selectionxpath + "//div[@class='odds-button'] | //div[@class='odds-button live']"));
				click(By.xpath(selectionxpath + "//div[@class='odds-button'] | //div[@class='odds-button live']"));
				selectionName[i] = GetElementText(By.xpath(selectionxpath + "//div[@class='selection-name']"));
			}
			Assert.assertFalse(isElementPresent(By.xpath("//div[@class='slip-item multiple']")),
					"Multiples section is displayed on BetSlip");
			BTbetslipObj.EnterStake(eventname, selectionName[1], marketName, TextControls.stakeValue, "Single", false);
			BTbetslipObj.EnterStake(eventname, selectionName[2], marketName, TextControls.stakeValue, "Single", false);
			click(BetslipElements.placeBet);
			Assert.assertTrue(
					isElementDisplayed(By.xpath("//span[@class='your-bets' and contains(text(),'Your bets (2)')]")),
					"Your Bets for two singles is not displayed on Bet Receipt");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 52 Verifies the multiple selections with EachWay
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyPotentialReturns_CheckUncheckIncludeInMultiples_Multibet() throws Exception {
		String testCase = "VerifyPotentialReturns_CheckUncheckIncludeInMultiples_Multibet";
		String[] aryOdds = new String[4];
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] = testdata.NavigateUsingTestData(1);
			aryOdds[1] = testdata.NavigateUsingTestData(14);
			aryOdds[2] = testdata.NavigateUsingTestData(8);
			aryOdds[3] = testdata.NavigateUsingTestData(7);
			String multiBetTypes = "//div[@class='slip-item multiple']//span[@class='market-information-selection']";
			List<String> betType = Arrays.asList(TextControls.doublesText + " (x6)", TextControls.treblesText + " (x4)",
					TextControls.acc4Text + " (x1)", TextControls.yankeeText + " (x11)",
					TextControls.Lucky15Text + " (x15)");
			BTbetslipObj.VerifyMultipleBetContainer(betType, multiBetTypes);
			// UnCheck Include Multiples for one selection
			String selID = BTbetslipObj.GetSelectionID(testdata.Readtestdata("PreProdEvents", 1, 1),
					testdata.Readtestdata("PreProdEvents", 1, 6), testdata.Readtestdata("PreProdEvents", 1, 5));
			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			String includeMultiplesChkbx1 = "//input[@id='slip-odds-stake-" + selID
					+ "']/../../..//div[@class='stake-information']//label[contains(text(), '"
					+ TextControls.includeText + "')]//preceding-sibling::input";
			common.TickCheckBox(includeMultiplesChkbx1, "Uncheck");
			List<String> betType1 = Arrays.asList(TextControls.doublesText + " (x3)",
					TextControls.treblesText + " (x1)", TextControls.trixieText + " (x4)", "Patent" + " (x7)");
			String multiBetTypes1 = "//div[@class='slip-item multiple']//span[@class='market-information-selection']";
			BTbetslipObj.VerifyMultipleBetContainer(betType1, multiBetTypes1);
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "treble", false);
			double actTotalPotentialReturns = BTbetslipObj.GetTotalsFromBetslip("potential");
			double treblesPR = (0.1 * Convert.ToDouble(aryOdds[1]) * Convert.ToDouble(aryOdds[2])
					* Convert.ToDouble(aryOdds[3]));
			System.out.println(treblesPR);
			
			String treblePRXpath = "//div[@class='multiples-container']//span[contains(text(), '"
					+ TextControls.treblesText + "')]/../../..//*[@class='potential-container']//*[@class='amount']";
			System.out.println(treblePRXpath);
			String actTreblesPotRet = GetElementText(By.xpath(treblePRXpath));
			System.out.println(actTreblesPotRet);
			System.out.println(treblesPR);
			if (actTreblesPotRet.contains("£"))
				actTreblesPotRet = actTreblesPotRet.substring(1);
			Assert.assertTrue(Convert.ToDouble(actTreblesPotRet) <= treblesPR,
					"Mismatch in Actual and Expected Treble Potential Return in Betslip . Expected-'" + treblesPR
							+ "', Actual-'" + actTotalPotentialReturns + "'");
			Assert.assertTrue(actTotalPotentialReturns <= treblesPR,
					"Mismatch in Actual and Expected Total Potential Return in Betslip . Expected-'" + treblesPR
							+ "', Actual-'" + actTotalPotentialReturns + "'");
			// Check each way for treble bet
			BTbetslipObj.EnterStake("", "", "", "0.10", "Treble", true);
			// Uncheck Include Multiples for 2nd selection
			selID = BTbetslipObj.GetSelectionID(testdata.Readtestdata("PreProdEvents", 14, 1),
					testdata.Readtestdata("PreProdEvents", 14, 6), testdata.Readtestdata("PreProdEvents", 14, 5));
			String includeMultiplesChkbx2 = "//input[@id='slip-odds-stake-" + selID
					+ "']/../../..//div[@class='stake-information']//label[contains(text(), '"
					+ TextControls.includeText + "')]//preceding-sibling::input";
			// Assert.IsTrue(browser.IsChecked(includeMultiplesChkbx2 +
			// "//preceding-sibling::input"), "Include Multiples is NOT checked
			// by default for " + testDataLst[1].SelectionName + " selection");
			common.TickCheckBox(includeMultiplesChkbx2, "Uncheck");
			Thread.sleep(1000);
			// Enter stake and verify potential returns for double bet
			BTbetslipObj.EnterStake("", "", "", "0.10", "Double", false);
			actTotalPotentialReturns = BTbetslipObj.GetTotalsFromBetslip("potential");
			double doublePR = ((0.1 * Convert.ToDouble(aryOdds[2]) * Convert.ToDouble(aryOdds[3])));
			String doublePRXpath = "//div[@class='multiples-container']//span[contains(text(), '"
					+ TextControls.doublesText + "')]/../../..//*[@class='potential-container']//*[@class='amount']";
			String actDoublePotRet = GetElementText(By.xpath(doublePRXpath));
			if (actDoublePotRet.contains("£"))
				actDoublePotRet = actDoublePotRet.substring(1);
			Assert.assertTrue(Convert.ToDouble(actDoublePotRet) <= doublePR,
					"Mismatch in Actual and Expected Double Potential Return in Betslip . Expected-'" + treblesPR
							+ "', Actual-'" + actTotalPotentialReturns + "'");
			Assert.assertTrue(actTotalPotentialReturns <= doublePR,
					"Mismatch in Actual and Expected Total Potential Return in Betslip . Expected-'" + treblesPR
							+ "', Actual-'" + actTotalPotentialReturns + "'");
			// Check each way for double bet
			BTbetslipObj.EnterStake("", "", "", "0.10", "Double", true);
			common.placebet();
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * TC- 53 Verifies the multiple selections with EachWay
	 * 
	 * @return none
	 * @throws Exception
	 *             | AssertionError element not found
	 */
	@Test
	public void VerifyStakes_WithAndWithoutEW() throws Exception {
		String testCase = "VerifyStakes_WithAndWithoutEW";
		try {
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents", 18, 1),
					testdata.Readtestdata("PreProdEvents", 18, 2), testdata.Readtestdata("PreProdEvents", 18, 4),
					testdata.Readtestdata("PreProdEvents", 18, 6), testdata.Readtestdata("PreProdEvents", 18, 5));
			// String odd = GetElementText(By.xpath("//span[text()='"+
			// testdata.Readtestdata("PreProdEvents" , 18, 6)
			// +"']//../..//div[text()='"+ testdata.Readtestdata("PreProdEvents"
			// , 18, 5) +"']/..//span[@class='odds-convert']"));
			HRfunctionObj.AddAndVerifyFutureRacingSelectionInBetslip(testdata.Readtestdata("PreProdEvents", 3, 1),
					testdata.Readtestdata("PreProdEvents", 3, 2), testdata.Readtestdata("PreProdEvents", 3, 4),
					testdata.Readtestdata("PreProdEvents", 3, 6), testdata.Readtestdata("PreProdEvents", 3, 5));
			// String odd1 = GetElementText(By.xpath("//span[text()='"+
			// testdata.Readtestdata("PreProdEvents" , 3, 6)
			// +"']//../..//div[text()='"+ testdata.Readtestdata("PreProdEvents"
			// , 3, 5) +"']/..//span[@class='odds-convert']"));
			// Verify Each way checkbox is present for HR events and not checked
			String EW1 = "//div[@class='selection-information']//div[@class='market-information-event' and contains(text(), '"
					+ testdata.Readtestdata("PreProdEvents", 18, 4) + " "
					+ testdata.Readtestdata("PreProdEvents", 18, 2)
					+ "')]/../../..//div[@class='stake-information']//div[@class='each-way-container']/input";
			String EW2 = "//div[@class='selection-information']//div[@class='market-information-event' and contains(text(), '"
					+ testdata.Readtestdata("PreProdEvents", 3, 4) + " " + testdata.Readtestdata("PreProdEvents", 3, 2)
					+ "')]/../../..//div[@class='stake-information']//div[@class='each-way-container']/input";
			Assert.assertTrue(isElementDisplayed(By.xpath(EW1)), "EW checkbox for the event "
					+ testdata.Readtestdata("PreProdEvents", 18, 4) + " is not present in BetSlip");
			Assert.assertTrue(isElementDisplayed(By.xpath(EW2)), "EW checkbox for the event "
					+ testdata.Readtestdata("PreProdEvents", 3, 4) + " is not present in BetSlip");
			// Enter stake without EW checkbox checked and verify total stake
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 18, 4),
					testdata.Readtestdata("PreProdEvents", 18, 5), testdata.Readtestdata("PreProdEvents", 18, 6), "0.1",
					"single", false);
			BTbetslipObj.EnterStake(testdata.Readtestdata("PreProdEvents", 3, 4),
					testdata.Readtestdata("PreProdEvents", 3, 5), testdata.Readtestdata("PreProdEvents", 3, 6), "0.1",
					"single", false);
			BTbetslipObj.EnterStake("", "", "", "0.1", "Double", false);
			double totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(totalStake <= Convert.ToDouble("0.3"),
					"Total stakes not displayed correctly on entering stake without EW checked");
			Thread.sleep(1000);
			// check EW checkbox and verify total stake
			common.TickCheckBox(EW1, "check");
			common.TickCheckBox(EW2, "check");
			String doubleEW = "//div[@class='multiples-container']//span[contains(text(), '" + TextControls.doublesText
					+ "')]/../..//following-sibling::div//input[@type='checkbox']";
			common.TickCheckBox(doubleEW, "Check");
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(totalStake <= Convert.ToDouble("0.6"), "Total stakes not doubled on ticking EW checkbox");
			// Uncheck EW checkbox for single bets and verify total stake
			common.TickCheckBox(EW1, "Uncheck");
			common.TickCheckBox(EW2, "Uncheck");
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(totalStake <= Convert.ToDouble("0.4"),
					"Total stakes not displayed correctly on unchecking EW checkbox");
			// Uncheck EW checkbox for double bet and verify total stake
			common.TickCheckBox(doubleEW, "Uncheck");
			totalStake = BTbetslipObj.GetTotalsFromBetslip(TextControls.stakeText);
			Assert.assertTrue(totalStake <= Convert.ToDouble("0.3"),
					"Total stakes not displayed correctly on unchecking EW checkbox for all bets");
		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * C811029
	 * Make two selections - one with higher stake and the other with lower stake.
	 * Then use the free bet while placing the bet.
	 * Check the free bet is assigned to the first selection on betslip
	 * confirm this in bet receipt.
	 *
	 * @return none
	 * @throws Exception
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyFreeBetWhenTwoSelectionsHigherAndLowerStakeAndConfirmReceipt() throws Exception {
		String testCase = "VerifyFreeBetWhenTwoSelectionsHigherAndLowerStakeAndConfirmReceipt";
		String username = testdata.Readtestdata("Users", 15, 1);
		String password = testdata.Readtestdata("Users", 15, 2);
		String odds, odds2;
		try {
			launchweb();
			Entervalue(LoginElements.Username, username);
			Entervalue(LoginElements.Password, password);
			click(LoginElements.Login);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Common.OddSwitcher(TextControls.decimal);
			odds = testdata.NavigateUsingTestData(1);
			odds2 = testdata.NavigateUsingTestData(2);

			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);
			if (getDriver().findElement(By.xpath("//div[@class='freebets']")).isDisplayed()) {
				Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetsTitle), "Free Bets title is not Present");

			} else
				Assert.fail("Free Bets are not available");

			//Enter stake higher on first bet and lower on second
			List<WebElement> stakeBoxes = getDriver().findElements(BetslipElements.stakeBox);
			stakeBoxes.get(0).sendKeys(TextControls.stakeValueHigher);
			stakeBoxes.get(1).sendKeys(TextControls.stakeValue);

			//Click on first freebet and place bet
			click(BetslipElements.freeBetFirstRadioButton);
			click(BetslipElements.placeBet);

			//Validate that the free bet is assigned to the first bet on the bet receipt
			String receiptTextFirstBet = getDriver().findElement(BetslipElements.betReceiptFirstContainer).getText();
			Assert.assertTrue(receiptTextFirstBet.contains(odds)&receiptTextFirstBet.contains("Freebet Used:"), "The Freebet wasn't assigned to the first bet on the bet receipt");

		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}

	/**
	 * C811030
	 * Make two selections - one from a event class for which the free bet is assigned to and the other selection of another event class.
	 * Use the free bet assigned to the event class of  the first selection.
	 * Then Place the bet.
	 *
	 * @return none
	 * @throws Exception
	 * @throws AssertionError
	 *             element not found
	 */
	@Test
	public void VerifyFreeBetWhenTwoSelectionsOneAssignedToFreebet() throws Exception {
		String testCase = "VerifyFreeBetWhenTwoSelectionsOneAssignedToFreebet";
		String username = testdata.Readtestdata("Users", 15, 1);
		String password = testdata.Readtestdata("Users", 15, 2);
		String odds, odds2, freeBetType;
		try {
			launchweb();
			Entervalue(LoginElements.Username, username);
			Entervalue(LoginElements.Password, password);
			click(LoginElements.Login);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Common.OddSwitcher(TextControls.decimal);
			//Assigned Football freebet
			odds = testdata.NavigateUsingTestData(1);
			//Selection not assigned to Football freebet
			odds2 = testdata.NavigateUsingTestData(5);

			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);

			//Verify that user has freebets
			if (getDriver().findElement(By.xpath("//div[@class='freebets']")).isDisplayed()) {
				Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetsTitle), "Free Bets title is not Present");

			} else
				Assert.fail("Free Bets are not available");

			//Enter equal stakes on selections
			List<WebElement> stakeBoxes = getDriver().findElements(BetslipElements.stakeBox);
			stakeBoxes.get(0).sendKeys(TextControls.stakeValueHigher);
			stakeBoxes.get(1).sendKeys(TextControls.stakeValueHigher);

			//Click on Football freebet
			click(BetslipElements.freebetFootballTokenRadioButtons);

			//Verify that Free bet amount and with message is displaying in the Betslip after checking the radio button of Freebet
			Assert.assertTrue(isElementDisplayed(BetslipElements.freebetDeductInfo), "freebetDeductInfo not displayed in Betslip");
			Assert.assertTrue(isElementDisplayed(BetslipElements.totalFreeBets), "Free bet amount not visible below the Total Stake");

			click(BetslipElements.placeBet);

			//Validate that the free bet is assigned to the first bet on the bet receipt
			String receiptTextFirstBet = getDriver().findElement(BetslipElements.betReceiptFirstContainer).getText();
			Assert.assertTrue(receiptTextFirstBet.contains(odds)&receiptTextFirstBet.contains("Freebet Used:"), "The Freebet wasn't assigned to the first bet on the bet receipt");


		} catch (Exception | AssertionError e) {
			getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
}
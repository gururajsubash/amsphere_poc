package com.ladbrokes.testsuite;

import com.ladbrokes.Utils.ReadTestSettingConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.Convert;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.MyBetsFunctions;
import com.ladbrokes.commonclasses.OddsBoostFunctions;

import javax.xml.soap.Text;
import java.util.List;

public class OddsBoostTests extends DriverCommon {
	BetslipFunctions BTbetslipObj = new BetslipFunctions();
	TestdataFunction testdata = new TestdataFunction();
	MyBetsFunctions myBetsfun = new  MyBetsFunctions();
	Common common= new Common();
	OddsBoostFunctions OBFunctions = new OddsBoostFunctions();
	public static String username = null;
	public static String password = null;
	
	/** 
	 * TC- 01, Story No :  
	 * MOB-7372 - F1: Desktop - Odds Boost Betslip - Boosted bet placement (Happy flow) - Singles only
	 * MOB-7352 -  F6: Desktop - Odds Boost Bet receipt
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostBetplacement_Singles() throws Exception{
		String testCase = "VerifyOddsBoostBetplacement_Singles";
		String[] boostedOdds = new String[1]; 
	    String ActualOdds ;
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			ActualOdds = testdata.NavigateUsingTestData(1);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.boostedValue) , "Boosted value is not displayed");
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "single", false);
			click(BetslipElements.placeBet);
			boostedOdds[0] = GetElementText(BetslipElements.ReceiptOdds);
			Assert.assertTrue(Convert.ToDouble(boostedOdds[0]) > Convert.ToDouble(ActualOdds), "odds are not boosted");
			OBFunctions.ValidateBetReceiptOddsBoost(testdata.Readtestdata("PreProdEvents", 1, 2), testdata.Readtestdata("PreProdEvents", 1, 4), testdata.Readtestdata("PreProdEvents", 1, 6), testdata.Readtestdata("PreProdEvents", 1, 5), boostedOdds, TextControls.stakeValue, "", false, 1, "single" , 1);
		}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage(), e);
		}
	}


	/** 
	 * TC- 02, Story No : MOB-7361
	 * F1: Desktop - F3: Desktop - Odds Boost Betslip - Boosted button & price display - Multiples (Single-line)
	 * MOB-7352 -  F6: Desktop - Odds Boost Bet receipt
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostBetplacement_Doubles() throws Exception{
		String testCase = "VerifyOddsBoostBetplacement_Doubles";
		int multiBetCount ;
	    String[] aryOdds = new String[2];
	    String [] boostedValue = new String[2];
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] =  testdata.NavigateUsingTestData(1);
			aryOdds[1] =  testdata.NavigateUsingTestData(2);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.MultiplesBoostedOdd), "Odds Boost is not displayed for Multiples");
			Assert.assertTrue(isElementDisplayed(BetslipElements.boostedValue) , "Boosted value is not displayed for First Selection");
			Assert.assertTrue(isElementDisplayed(BetslipElements.seceondSelBoost), "Boosted value is not displayed for Second Selection");
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			click(BetslipElements.placeBet);
			myBetsfun.NavigateToMyBets("open bets");
			 boostedValue[0] = GetElementText(By.xpath("(//div[@class='bet-detail']//div[@class='mb-content'])[1]//span[@class='mb-content-odds']"));
			 boostedValue[1] = GetElementText(By.xpath("(//div[@class='bet-detail']//div[@class='mb-content'])[2]//span[@class='mb-content-odds']"));
			click(BetslipElements.betSlipTab);
			 Assert.assertTrue(Convert.ToDouble(boostedValue[0]) > Convert.ToDouble(aryOdds[0]), "odds are not boosted");
		    Assert.assertTrue(Convert.ToDouble(boostedValue[1]) > Convert.ToDouble(aryOdds[1]), "odds are not boosted");
		    String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-" + testdata.Readtestdata("PreProdEvents", 2, 5);
			String eventSel =  testdata.Readtestdata("PreProdEvents", 1, 4)+ "-" + testdata.Readtestdata("PreProdEvents", 2, 4) ;
			OBFunctions.ValidateBetReceiptOddsBoost("", eventSel, "", doubleSelName, boostedValue, TextControls.stakeValue, "", false, multiBetCount, "Double" , 2);
	}
	catch(Exception | AssertionError  e)
	{
		String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
	}
	}

	/** 
	 * TC- 03, Story No :  MOB_7361_02
	 * F1: Desktop - F3: Desktop - Odds Boost Betslip - Boosted button & price display - Multiples (Single-line)
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostWithLogIn() throws Exception{
		String testCase = "VerifyOddsBoostWithLogIn";
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			testdata.NavigateUsingTestData(1);
			Assert.assertTrue(isElementPresent(BetslipElements.oddsBoostTooltipIcon), "Odds Boosts are not available");
			click(BetslipElements.oddsBoostTooltipIcon);
			click(BetslipElements.oddsBoostButton , "Odds Boost button is not tappable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.boostedValue) , "Boosted value is not displayed");
		}
			catch(Exception | AssertionError  e)
			{
				String screenShotPath = getScreenshot(testCase); 
				Assert.fail(e.getMessage(), e);
			}

	}
	/** 
	 * TC- 04, Story No : MOB_7361_01 
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostWithLoggedOut() throws Exception{
		String testCase = "VerifyOddsBoostWithoutLoggedOut";
		try{
			launchweb();
			testdata.NavigateUsingTestData(1);
			testdata.NavigateUsingTestData(2);
			Assert.assertFalse(isElementPresent(BetslipElements.oddsBoostTittle), "Odds Boost Titte is displayed on Betslip for loggedout user");
			Assert.assertFalse(isElementPresent(BetslipElements.oddsBoostTooltip), "Odds Boost Tooltip is displayed on Betslip for loggedout user");
			Assert.assertFalse(isElementPresent(BetslipElements.oddsBoostTooltipIcon), "Odds Boost Tooltip icon 'i' is displayed on Betslip for loggedout user");
			Assert.assertFalse(isElementPresent(BetslipElements.oddsBoostButton), "Odds Boost button is displayed on Betslip for loggedout user");
			Assert.assertFalse(isElementPresent(BetslipElements.oddsBoostButtonIcon), "Odds Boost button  icon  is displayed on Betslip for loggedout user");
		}
			catch(Exception | AssertionError  e)
			{
				String screenShotPath = getScreenshot(testCase); 
				Assert.fail(e.getMessage(), e);
			}
}

	/** 
	 * TC- 05, Story No : MOB-7361_4
	 * F1: Desktop - F3: Desktop - Odds Boost Betslip - Boosted button & price display - Multiples (Single-line)
	 * MOB-7352 -  F6: Desktop - Odds Boost Bet receipt
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostButtonCheckUnchek() throws Exception{
		String testCase = "VerifyOddsBoostButtonCheckUnchek";
		String selection1 = testdata.Readtestdata("PreProdEvents", 1, 5);
		String selection2 = testdata.Readtestdata("PreProdEvents", 2, 5);
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			testdata.NavigateUsingTestData(1);
			testdata.NavigateUsingTestData(2);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			String OBSel1 = "//span[contains(text(), '"+ selection1 +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel1)) , "Boosted value is not displayed for First Selection");
			String OBSel2 = "//span[contains(text(), '"+ selection2 +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel2)) , "Boosted value is not displayed for First Selection");
			Assert.assertTrue(isElementDisplayed(By.xpath("//span[contains(text(), '"+ TextControls.doublesText +" (x1)')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']")), "Boosted value for doubles is not displayed ");
			
			//UnChecking the OddsBoost Button
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertFalse(isElementPresent(By.xpath(OBSel1)) , "Boosted value is  displayed for First Selection after unchecking Odds Boost");
			Assert.assertFalse(isElementPresent(By.xpath(OBSel2)), "Boosted value is displayed for Second Selection after unchecking Odds Boost");
			Assert.assertFalse(isElementPresent(By.xpath("//span[contains(text(), '"+ TextControls.doublesText +" (x1)')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']")), "Boosted value for doubles is displayed after unchecking odds boost ");
			
		}
			catch(Exception | AssertionError  e)
			{
				String screenShotPath = getScreenshot(testCase); 
				Assert.fail(e.getMessage(), e);
			}
}

	/** 
	 * TC- 06, Story No : MOB-7361_5
	 * F1: Desktop - F3: Desktop - Odds Boost Betslip - Boosted button & price display - Multiples (Single-line)
	 * MOB-7352 -  F6: Desktop - Odds Boost Bet receipt
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostBetslipNavigationAndRefreshPage() throws Exception{
		String testCase = "VerifyOddsBoostBetslipNavigationAndRefreshPage";
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			testdata.NavigateUsingTestData(1);
			testdata.NavigateUsingTestData(2);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			String MultiplesBoostedOdd = "//span[contains(text(), '"+ TextControls.doublesText +" (x1)')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			String OBSel1 = "//span[contains(text(), '"+ testdata.Readtestdata("PreProdEvents", 1, 5) +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			String OBSel2 = "//span[contains(text(), '"+ testdata.Readtestdata("PreProdEvents", 2, 5) +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			click(BetslipElements.oddsBoostButtonIcon);
			
			//Refresh the Page
			getDriver().navigate().refresh();
			Thread.sleep(2000);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(By.xpath(MultiplesBoostedOdd)), "Odds Boost is not displayed for Multiples");
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel1)) , "Boosted value is not displayed for First Selection");
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel2)), "Boosted value is not displayed for Second Selection");
		   
			//navigating Betslip 
			 click(MyBetsControls.myBets, "My Bets was not found in Betslip");
			 click(BetslipElements.betSlipTab);
			 Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			 Assert.assertTrue(isElementDisplayed(By.xpath(MultiplesBoostedOdd)), "Odds Boost is not displayed for Multiples");
			 Assert.assertTrue(isElementDisplayed(By.xpath(OBSel1)) , "Boosted value is not displayed for First Selection");
			 Assert.assertTrue(isElementDisplayed(By.xpath(OBSel2)), "Boosted value is not displayed for Second Selection");
			 
		}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage());
		}
}	
	/** 
	 * TC- 07, Story No : MOB-7432-01 , MOB-7432-02
	 * Desktop - Display of open bet that has been boosted 
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyMyBets_OddsBoostSingleBet() throws Exception{
		String testCase = "VerifyMyBets_OddsBoostSingleBet";
		String[] boostedOdds = new String[1]; 
	    String ActualOdds;
	    
	    String[] eventName = new String[1];
        String[] marketName = new String[1];
        String[] selection = new String[1];
        String[] typeName = new String[1];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 1, 2);
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			ActualOdds = testdata.NavigateUsingTestData(1);
			System.out.println(ActualOdds);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.boostedValue) , "Boosted value is not displayed");
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "single", false);
			//double totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
			
			click(BetslipElements.placeBet);
			boostedOdds[0] = GetElementText(BetslipElements.ReceiptOdds);
			Assert.assertTrue(Convert.ToDouble(boostedOdds[0]) > Convert.ToDouble(ActualOdds), "odds are not boosted");
			String totalPR = OBFunctions.ValidateBetReceiptOddsBoost(testdata.Readtestdata("PreProdEvents", 1, 2), testdata.Readtestdata("PreProdEvents", 1, 4), testdata.Readtestdata("PreProdEvents", 1, 6), testdata.Readtestdata("PreProdEvents", 1, 5), boostedOdds, TextControls.stakeValue, "", false, 1, "single" , 1);
            String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
             
             String BetTime = time.split(" ")[1];
             
             OBFunctions.VerifyOddsBoostBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "single", BetTime);
             OBFunctions.VerifyOddsBoostBetUnderOpenSettledBets("open bets", eventName, marketName, selection, boostedOdds, TextControls.stakeValue, totalPR, TextControls.openStatusText, "Single", BetTime);
       }
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage(), e);
		}
}
	/** 
	 * TC- 08, Story No : MOB-7432-01 , MOB-7432-02 for multiple single line(Doubles)
	 * Desktop - Display of open bet that has been boosted 
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyMyBets_OddsBoostDoubleBet() throws Exception{
		String testCase = "VerifyMyBets_OddsBoostDoubleBet";
		int multiBetCount ;
	    String[] aryOdds = new String[2];
	    String [] boostedValue = new String[2];
	    
	    String[] eventName = new String[2];
        String[] marketName = new String[2];
        String[] selection = new String[2];
        String[] typeName = new String[2];
		
       eventName[0] = testdata.Readtestdata("PreProdEvents", 1, 4);
	   selection[0] = testdata.Readtestdata("PreProdEvents", 1, 5);
	   marketName[0] = testdata.Readtestdata("PreProdEvents", 1, 6);
	   typeName[0] = testdata.Readtestdata("PreProdEvents", 1, 2);
	   
	   eventName[1] = testdata.Readtestdata("PreProdEvents", 2, 4);
	   selection[1] = testdata.Readtestdata("PreProdEvents",2, 5);
	   marketName[1] = testdata.Readtestdata("PreProdEvents", 2, 6);
	   typeName[1] = testdata.Readtestdata("PreProdEvents", 2, 2);
	   
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			aryOdds[0] =  testdata.NavigateUsingTestData(1);
			aryOdds[1] =  testdata.NavigateUsingTestData(2);
			BTbetslipObj.VerifyOddsBoostOnBetslip();
			click(BetslipElements.oddsBoostButtonIcon);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton) , "odds boost button is not clickable");
			Assert.assertTrue(isElementDisplayed(BetslipElements.MultiplesBoostedOdd), "Odds Boost is not displayed for Multiples");
			String OBSel1 = "//span[contains(text(), '"+ selection[0] +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel1)) , "Boosted value is not displayed for First Selection");
			String OBSel2 = "//span[contains(text(), '"+ selection[1] +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			Assert.assertTrue(isElementDisplayed(By.xpath(OBSel2)), "Boosted value is not displayed for Second Selection");
			multiBetCount = BTbetslipObj.GetMultiplesBetCount();
			BTbetslipObj.EnterStake("", "", "", TextControls.stakeValue, "double", false);
			//double totalPR = BTbetslipObj.GetTotalsFromBetslip(TextControls.PRetText);
			click(BetslipElements.placeBet);
			myBetsfun.NavigateToMyBets("open bets");
			 boostedValue[0] = GetElementText(By.xpath("(//div[@class='bet-detail']//div[@class='mb-content'])[1]//span[@class='mb-content-odds']"));
			 boostedValue[1] = GetElementText(By.xpath("(//div[@class='bet-detail']//div[@class='mb-content'])[2]//span[@class='mb-content-odds']"));
			click(BetslipElements.betSlipTab);
			 Assert.assertTrue(Convert.ToDouble(boostedValue[0]) > Convert.ToDouble(aryOdds[0]), "odds are not boosted");
		    Assert.assertTrue(Convert.ToDouble(boostedValue[1]) > Convert.ToDouble(aryOdds[1]), "odds are not boosted");
		    
		    String doubleSelName = testdata.Readtestdata("PreProdEvents", 1, 5) + "-" + testdata.Readtestdata("PreProdEvents", 2, 5);
			String eventSel =  testdata.Readtestdata("PreProdEvents", 1, 4)+ "-" + testdata.Readtestdata("PreProdEvents", 2, 4) ;
			
			String totalPR = OBFunctions.ValidateBetReceiptOddsBoost("", eventSel, "", doubleSelName, boostedValue, TextControls.stakeValue, "", false, multiBetCount, "Double" , 2);
            String time = getDriver().findElement(By.xpath("//span[@class='receipt-time-value']")).getText();
             
             String BetTime = time.split(" ")[1];
             OBFunctions.VerifyOddsBoostBetDroppedFromOpenSettledBets("settled bets", eventName, selection, TextControls.stakeValue, totalPR, "double", BetTime);
             OBFunctions.VerifyOddsBoostBetUnderOpenSettledBets("open bets", eventName, marketName, selection, boostedValue, TextControls.stakeValue, totalPR, TextControls.openStatusText, "double", BetTime);
	}
	catch(Exception | AssertionError  e)
	{
		String screenShotPath = getScreenshot(testCase); 
		Assert.fail(e.getMessage(), e);
	}
	}
	
	/** 
	 * TC- 09
	 * Verifying Odds Boost With EW
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostWithEWCheck() throws Exception{
		String testCase = "VerifyOddsBoostWithEWCheck";
		String[] boostedOdds = new String[1];
		String TypeName =  testdata.Readtestdata("PreProdEvents", 1, 2);
		String Market =  testdata.Readtestdata("PreProdEvents", 1,6);
		String eventName = testdata.Readtestdata("PreProdEvents", 1, 4);
		String Selection = testdata.Readtestdata("PreProdEvents", 1, 5);
		String selection = testdata.Readtestdata("PreProdEvents", 1, 5);
		try{
			launchweb();
			Common.Login();
			Common.OddSwitcher(TextControls.decimal);
			String ActualOdds = testdata.NavigateUsingTestData(1);
			 BTbetslipObj.VerifyOddsBoostOnBetslip();
			 click(BetslipElements.oddsBoostButton);
			// Verifying Error Message box for EW
			/*Assert.assertTrue(isElementDisplayed(BetslipElements.EWErrorMsgOB), "Each Way popup Error Message 'Odds Boost is unavailable for EW selections!' is not displayed");
			click(BetslipElements.EWErrorMsgOB_OkBtn);*/
			String oddsBoostXpath = "//span[contains(text(), '"+ selection +"')]/../following::div[@class='odds-container']//div//div[@class='boosted-value']";
			Assert.assertTrue(isElementPresent(By.xpath(oddsBoostXpath)), "Boosted value is displayed for EW selection");
			String eachwayTerms = GetElementText(By.xpath("//div[@class='each-way-container']"));
			BTbetslipObj.EnterStake(eventName, Selection, Market,TextControls.stakeValue, "Single", true);
			click(BetslipElements.placeBet);
			boostedOdds[0] = GetElementText(BetslipElements.ReceiptOdds);
			Assert.assertTrue(Convert.ToDouble(boostedOdds[0]) > Convert.ToDouble(ActualOdds), "odds are not boosted");
			OBFunctions.ValidateBetReceiptOddsBoost(TypeName, eventName, Market, Selection, boostedOdds, TextControls.stakeValue, eachwayTerms, false, 1, "single" , 1);
			
		}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase); 
			Assert.fail(e.getMessage());
		}
	}

	/**
	 * TC - 811036
	 * Odds boost - Info Page Under Promotions
	 * @return none
	 * @throws AssertionError element not found
	 */
	@Test
	public void VerifyOddsBoostInfoPageUnderPromotions() throws Exception {
		String testCase = "VerifyOddsBoostInfoPageUnderPromotions";


		try {

			launchweb();

			username = ReadTestSettingConfig.getTestsetting("UserName1");
			password = ReadTestSettingConfig.getTestsetting("Password1");
			Assert.assertTrue(isElementDisplayed(LoginElements.Login), "User is already LoggedIn");

			Entervalue(LoginElements.Username, username);
			Entervalue(LoginElements.Password, password);
			click(LoginElements.Login);

			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.oddsBoostAlertBox));
			click(HomeGlobalElements.oddsBoostThanksButton);

			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.oddsBoostAlertToken));
			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.oddsBoostShowMoreButton));
			click(HomeGlobalElements.oddsBoostShowMoreButton);

			Assert.assertTrue(isElementDisplayed(HomeGlobalElements.oddsBoostInfo));

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage());
		}
	}


	/********************************************************************************************************************************************
	 * TC- c811166
	 * Desktop - Odds boost - Info page under Promotions
	 * @return none
	 * @throws AssertionError element not found
	 ********************************************************************************************************************************************/
	@Test
	public void verify_InfoPage_UnderPromotions() throws Exception {
		String testCase = "verify_InfoPage_UnderPromotions";

		try {
			launchweb();
			common.Login();
			click(HomeGlobalElements.footBallTab);
			Assert.assertTrue(isElementDisplayed(FootballElements.FootballPage));

			Assert.assertTrue(isElementDisplayed(FootballElements.timeTabActive));
			click(By.cssSelector(".odds-convert"));
			Assert.assertTrue(isElementDisplayed(By.cssSelector(".odds-button.selected")));
			click(FootballElements.competitionsTab);
			Assert.assertTrue(isElementDisplayed(FootballElements.competitionsTabActive));
			click(FootballElements.timeTab);
			Assert.assertTrue(isElementDisplayed(By.cssSelector(".odds-button.selected")));

		} catch (Exception | AssertionError e) {
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage(), e);
		}
	}
	/**
	 * C811039 - Odds boost - Unable to place boosted bet via free bets
	 */
	@Test
	public void VerifyUnableToPlaceBoostedBetViaFreeBets() throws Exception{
		String testCase = "VerifyUnableToPlaceBoostedBetViaFreeBets";

		//User that has Freebets
		String username = testdata.Readtestdata("Users", 15, 1);
		String password = testdata.Readtestdata("Users", 15, 2);

		try {
			launchweb();
			Entervalue(LoginElements.Username, username);
			Entervalue(LoginElements.Password, password);
			click(LoginElements.Login);
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			if (isElementPresent(HomeGlobalElements.oddsBoostPopUp))
				click(HomeGlobalElements.oddsBoostPopUp, "Odds Boost Pop Up On Home Page is Not Clickable");
			Common.OddSwitcher(TextControls.decimal);

			//Add preplay selection to Betlsip
			testdata.NavigateUsingTestData(1);

			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);

			//Verify that user has freebets
			if (getDriver().findElement(By.xpath("//div[@class='freebets']")).isDisplayed()) {
				Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetsTitle), "Free Bets title is not Present");

			} else
				Assert.fail("Free Bets are not available");

			//Enter stake
			Common.Enterstake(TextControls.stakeValueHigher);

			//Click on boost button and verify boosted
			click(BetslipElements.oddsBoostButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton)&isElementDisplayed(BetslipElements.boostedValue), "Bet wasn't boosted");

			//Select a freebet and verify that it's not possible to boost with freebet
			click(BetslipElements.freeBetFirstRadioButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetPopupTitle)&isElementDisplayed(BetslipElements.freeBetPopupMessage), "Continue with freebet popup not displayed");

			//Click accept and verify that bet isn't boosted
			click(BetslipElements.freeBetPopupEnableButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostButton), "Bet shouldn't be boosted ");

			//Remove all from betslip
			click(BetslipElements.removeAllBtn);

			//Add Inplay selection to Betlsip
			click(FootballElements.inplayAndStreamingMenuButton);
			click(FootballElements.firstInplaySelection);

			if (isElementPresent(BetslipElements.oddsBoostTooltipInfo))
				click(BetslipElements.oddsBoostTooltipIcon);

			//Enter stake
			Common.Enterstake(TextControls.stakeValueHigher);

			//Click on boost button and verify boosted
			click(BetslipElements.oddsBoostButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.BoostedButton)&isElementDisplayed(BetslipElements.boostedValue), "Bet wasn't boosted");

			//Select a freebet and verify that it's not possible to boost with freebet
			click(BetslipElements.freeBetFirstRadioButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.freeBetPopupTitle)&isElementDisplayed(BetslipElements.freeBetPopupMessage), "Continue with freebet popup not displayed");

			//Click accept and verify that bet isn't boosted
			click(BetslipElements.freeBetPopupEnableButton);
			Assert.assertTrue(isElementDisplayed(BetslipElements.oddsBoostButton), "Bet shouldn't be boosted ");

		}
		catch(Exception | AssertionError  e)
		{
			String screenShotPath = getScreenshot(testCase);
			Assert.fail(e.getMessage());
		}
	}
}


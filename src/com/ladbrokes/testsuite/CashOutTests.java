package com.ladbrokes.testsuite;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;

public class CashOutTests extends DriverCommon {
	TestdataFunction testdata = new TestdataFunction();
	Common common= new Common();

	@Test
	public void CashOut_Racing_WhenSelectionChangedToLOST() throws Exception{
		
		String testCase = "VerifyHorseRacingForecastPlaceBet";
		String className = testdata.Readtestdata("PreProdEvents" , 10, 1);
		String typeName =  testdata.Readtestdata("PreProdEvents",10, 2);
		String eventName = testdata.Readtestdata("PreProdEvents", 10, 4);
		
		 try{
			 launchweb();
			 common.Login();
			 common.OddSwitcher("decimal");

         }
	 catch ( Exception | AssertionError  e){
		 String screenShotPath = getScreenshot(testCase); 
		 Assert.fail(e.getMessage() , e);
		 }
	 }

}

package com.ladbrokes.testsuite;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.ladbrokes.EnglishOR.OBTIElements;
import com.ladbrokes.Utils.ReadTestSettingConfig;
import com.ladbrokes.Utils.TestdataFunction;
import com.ladbrokes.commonclasses.BetslipFunctions;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.Convert;
import com.ladbrokes.commonclasses.DriverCommon;
import com.ladbrokes.commonclasses.LoginLogoutFunctions;
import com.ladbrokes.commonclasses.OddsBoostFunctions;
import com.ladbrokes.commonclasses.PotentialReturnFunctions;

public class OverAskTests extends DriverCommon{
	LoginLogoutFunctions LoginFunctions = new LoginLogoutFunctions();
	TestdataFunction testData = new TestdataFunction();
	BetslipFunctions BTObjFun = new BetslipFunctions();
	PotentialReturnFunctions PRfun = new PotentialReturnFunctions();
	OddsBoostFunctions OBfunctions = new OddsBoostFunctions();
	
	@Test(groups="StageOA")
	public void VerifyTraderAcceptOA() throws Exception {
    String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
		String selXpath , maxValue; 
		String[] odds = new String[1];
		
     try {
            launchweb();
            Common.Login();
            Common.OddSwitcher("decimal");
           odds[0] = testData.NavigateUsingTestData(6);
            if(isElementPresent(BetslipElements.oddsBoostButtonIcon))
				  click(BetslipElements.oddsBoostTooltipIcon);
			
            int multiBetCount = BTObjFun.GetMultiplesBetCount();
            maxValue = BTObjFun.GetMinMaxStakeValue("max");
		    Double maxStake= Double.parseDouble(maxValue) + (1.00);
		    Entervalue(BetslipElements.stakeBox, String.valueOf(maxStake));
		    BTObjFun.VerifyBetDetails("", "", "", odds, Convert.ToString(maxStake),"", "single", "","");
		    click(BetslipElements.placeBet);
			Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingSpinner), "'Your bet is processing' is not displayed on Bet Processing Page");
		    Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingMessage), "'We are considering your bet. Please bear with us, this may take a few moments' is not displayed on Bet Processing Page");
            
		    ((JavascriptExecutor)getDriver()).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
            getDriver().switchTo().window(tabs.get(1));
            LoginFunctions.LoginOB_TI();
			click(OBTIElements.TiBetsTab);
			Thread.sleep(1000);
			click(OBTIElements.BiSearchtab);
			Thread.sleep(1000);
			click(OBTIElements.BiResultsTab);
			Thread.sleep(1000);
			click(OBTIElements.BiRefreshBtn);
			Thread.sleep(4000);
			String username = "//div[@class='col acct_no normal_purple' and contains(text(),'"+ ReadTestSettingConfig.getTestsetting("UserName1")+"')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(username)),"User is not found in OB Trading Interface Request page");
			click(By.xpath(username) , "user is not clickable in OB");
			click(OBTIElements.BiRequestDropdown);
			Thread.sleep(1000);
			Select select = new Select(getDriver().findElement(OBTIElements.BiRequestDropdown));
			select.selectByValue("A");
			Common.scrollAndClick(getDriver(),OBTIElements.BiSubmitBtn);
			getDriver().switchTo().window(tabs.get(0));
			OBfunctions.ValidateBetReceipt_OA("", testData.Readtestdata("PreProdEvents", 6, 4), "", testData.Readtestdata("PreProdEvents", 6, 5), odds, Convert.ToString(maxStake), "", false, multiBetCount, "single" , 1);
		} 
		catch ( Exception | AssertionError  e) { String screenShotPath = getScreenshot(testCase); 
     Assert.fail(e.getMessage());
		}

}
	@Test(groups="StageOA")
	public void VerifyTraderDeclineOA() throws Exception {
    String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
		String selXpath , maxValue; 
		
     try {
            launchweb();
			Common.Login();
			Common.OddSwitcher("decimal");
			testData.NavigateUsingTestData(6);
            if(isElementPresent(BetslipElements.oddsBoostButtonIcon))
				  click(BetslipElements.oddsBoostTooltipIcon);
			
			maxValue = BTObjFun.GetMinMaxStakeValue("max");
		    Double maxStake= Double.parseDouble(maxValue) + (1.00);
		    Entervalue(BetslipElements.stakeBox, String.valueOf(maxStake));
			click(BetslipElements.placeBet);
			Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingSpinner), "'Your bet is processing' is not displayed on Bet Processing Page");
		    Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingMessage), "'We are considering your bet. Please bear with us, this may take a few moments' is not displayed on Bet Processing Page");
            
		    ((JavascriptExecutor)getDriver()).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
            getDriver().switchTo().window(tabs.get(1));
            LoginFunctions.LoginOB_TI();
			click(OBTIElements.TiBetsTab);
			click(OBTIElements.BiSearchtab);
			Thread.sleep(1000);
			click(OBTIElements.BiResultsTab);
			Thread.sleep(1000);
			click(OBTIElements.BiRefreshBtn);
			Thread.sleep(4000);
			String username = "//div[@class='col acct_no normal_purple' and contains(text(),'"+ ReadTestSettingConfig.getTestsetting("UserName1")+"')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(username)),"User is not found in OB Trading Interface Request page");
			click(By.xpath(username) , "user is not clickable in OB");
			click(OBTIElements.BiRequestDropdown);
			Thread.sleep(1000);
			Select select = new Select(getDriver().findElement(OBTIElements.BiRequestDropdown));
			select.selectByValue("D");
			click(OBTIElements.BiDeclinePopup);
			Select declineReason = new Select(getDriver().findElement(OBTIElements.BiDeclinePopup));
			declineReason.selectByValue("81876");
			click(OBTIElements.BiDeclineApplyBtn);
			Thread.sleep(2000);
			Common.scrollAndClick(getDriver(), OBTIElements.BiSubmitBtn);
			getDriver().switchTo().window(tabs.get(0));
			Assert.assertTrue(isElementDisplayed(OBTIElements.DeclineMsg),"Decline Message is not displayed");
			String stakeCheck = GetElementText(BetslipElements.totalStake);
			Assert.assertTrue(Convert.ToDouble(stakeCheck) <= Convert.ToDouble(maxValue) ,"Max stake is not seen in stake box after trader declines the offer");
			System.out.println("stakeCheck : " + stakeCheck);
			System.out.println("maxValue : " + maxValue);
		} 
		catch ( Exception | AssertionError  e) { String screenShotPath = getScreenshot(testCase); 
     Assert.fail(testCase ,e);
		}

}
	

	@Test(groups="StageOA")
	public void VerifyCounterOfferPage_SingleBet_OA() throws Exception {
    String testCase = new Object(){}.getClass().getEnclosingMethod().getName();
    String selXpath , maxValue , OAStake;
    String stakePerLine = "1.00" ;
    String[] odds = new String[1];
     try {
            launchweb();
			Common.Login();
			Common.OddSwitcher("decimal");
			odds[0] = testData.NavigateUsingTestData(6);
            if(isElementPresent(BetslipElements.oddsBoostButtonIcon))
				  click(BetslipElements.oddsBoostTooltipIcon);
			
            int multiBetCount = BTObjFun.GetMultiplesBetCount();
			maxValue = BTObjFun.GetMinMaxStakeValue("max");
		    Double maxStake= Double.parseDouble(maxValue) + (1.00);
		    Entervalue(BetslipElements.stakeBox, String.valueOf(maxStake));
		    click(BetslipElements.placeBet);
			Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingSpinner), "'Your bet is processing' is not displayed on Bet Processing Page");
		    Assert.assertTrue(isElementDisplayed(OBTIElements.BetProccessingMessage), "'We are considering your bet. Please bear with us, this may take a few moments' is not displayed on Bet Processing Page");
			
		    ((JavascriptExecutor)getDriver()).executeScript("window.open()");
            ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());
            getDriver().switchTo().window(tabs.get(1));
            LoginFunctions.LoginOB_TI();
			click(OBTIElements.TiBetsTab);
			click(OBTIElements.BiSearchtab);
			Thread.sleep(1000);
			click(OBTIElements.BiResultsTab);
			Thread.sleep(1000);
			click(OBTIElements.BiRefreshBtn);
			Thread.sleep(4000);
			String username = "//div[@class='col acct_no normal_purple' and contains(text(),'"+ ReadTestSettingConfig.getTestsetting("UserName1")+"')]";
			Assert.assertTrue(isElementDisplayed(By.xpath(username)),"User is not found in OB Trading Interface Request page");
			click(By.xpath(username) , "user is not clickable in OB");
			Thread.sleep(1000);
			click(OBTIElements.BiRequestDropdown);
			Thread.sleep(1000);
			Select select = new Select(getDriver().findElement(OBTIElements.BiRequestDropdown));
			select.selectByValue("O");
			getDriver().findElement(OBTIElements.stakePerLine).clear();
			Entervalue(OBTIElements.stakePerLine, stakePerLine);
			Common.scrollAndClick(getDriver(), OBTIElements.BiSubmitBtn);
			
			getDriver().switchTo().window(tabs.get(0));
			Assert.assertTrue(isElementDisplayed(OBTIElements.OAheaderMsg), "'Sorry, your bet has not been processed as requested. Please consider the following offer(s) instead:' Message is not displayed");
			Assert.assertTrue(isElementDisplayed(OBTIElements.OAheaderTimer), "''Offer expires:'' Timer is not displayed");
			Assert.assertTrue(isElementDisplayed(OBTIElements.overaskHighlight), "Trader offered stake is not highlighted and displayed on betslip");
			OAStake = GetElementText(OBTIElements.overaskHighlight).replace(TextControls.currencySymbol, "");
			Assert.assertTrue(OAStake.equals(stakePerLine), "Trader offered stake is not equal to stake highlighted on betslip");
			String totalStake = GetElementText(OBTIElements.OAtotalStake).replace(TextControls.currencySymbol, "");
			Assert.assertTrue(totalStake.equals(OAStake), "Mismatch in Expected : "+ OAStake + " and Actual : "+ totalStake +" Total Stake");
			String actualPotentialReturns = GetElementText(OBTIElements.OAtotalPotentialReturns).replace(TextControls.currencySymbol, "");
			double expectedPotentialReturns = PRfun.GetPotentialReturnForMultiples(odds, Convert.ToDouble(OAStake) , "single");
			Assert.assertTrue(Convert.ToDouble(actualPotentialReturns) <= expectedPotentialReturns, "Mismatch in Expected : "+ expectedPotentialReturns + " and Actual : "+ actualPotentialReturns +" potential return");
			 click(OBTIElements.OAplacebet);
			OBfunctions.ValidateBetReceipt_OA("", testData.Readtestdata("PreProdEvents", 6, 4), "", testData.Readtestdata("PreProdEvents", 6, 5), odds, OAStake, "", false, multiBetCount, "single" , 1);
		} 
		catch ( Exception | AssertionError  e) { String screenShotPath = getScreenshot(testCase); 
        Assert.fail(testCase ,e);
		}

}
	}

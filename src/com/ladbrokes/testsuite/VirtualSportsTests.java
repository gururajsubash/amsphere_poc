package com.ladbrokes.testsuite;

import com.ladbrokes.EnglishOR.VirtualSportsElements;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import java.util.List;
import com.ladbrokes.commonclasses.Common;
import com.ladbrokes.commonclasses.DriverCommon;

public class VirtualSportsTests extends DriverCommon{

    /**
     * TC - 811067 - Verify Place A Bet From Virtual
     * @return none
     * @throws Exception element not found
     */
    @Test
    public void VerifyPlaceABetFromVirtual() throws Exception{
        String testCase = "VerifyPlaceABetFromVirtual";

        try{

            launchweb();
            Common.Login();

            click(HomeGlobalElements.AZMenu);
            click(HomeGlobalElements.Virtual);

            List<WebElement> eventTime = getDriver().findElements(VirtualSportsElements.eventTime);
            Assert.assertTrue(eventTime.size() > 1, "To few selections to be able to verify testcase");
            eventTime.get(1).click();

            click(VirtualSportsElements.oddsButton);

            Entervalue(BetslipElements.stakeBox, TextControls.stakeValue);
            click(BetslipElements.placeBet);
            Assert.assertTrue(isElementDisplayed(BetslipElements.betSuccessfulMsg));

        }
        catch ( Exception | AssertionError  e)
        {
            String screenShotPath = getScreenshot(testCase);
            Assert.fail(e.getMessage() , e);
        }

    }

}

package com.ladbrokes.IrishOR;
import org.openqa.selenium.By;

public class LoginElements extends com.ladbrokes.EnglishOR.LoginElements{

	public static final By Login = By.xpath("//*[@class='button' and contains(text(), 'Log In')]");
	public static final By Username = By.xpath("//input[@placeholder='Username']");
	public static final By Password = By.xpath("//input[@placeholder='Password']");
	public static final By NonSportsSiteUsername = By.xpath("//input[@name='username']");
	public static final By NonSportsSitePassword = By.xpath("//input[@name='password']");
	public static final By casinoLogin = By.xpath("//div//button[@title='Login']");
	public static final By BingoLogin = By.xpath("//button[@title='Login']");
	public static final By ladbrokesLogo = By.xpath("//h1[@class='logo']//a[contains(text(),'Ladbrokes')]");
	public static final By depositBtn = By.xpath("//div[contains(text(), 'Deposit')]");
	public static final By depositIcon = By.xpath("//div[@class='icon-deposit']");
	public static final By settingsBtn = By.xpath("//div[@class='icon-settings']");
	public static final By settingsContent= By.xpath("//*[@id='header']/div/nav[1]/div[2]/div[5]");
	public static final By settingsStaticText = By.xpath("//div[@class='tooltip-header' and text()='Settings']/following-sibling::h2[text()='Set odds to']");
	public static final By fractionalOdd = By.xpath("//span[@class='title' and text()='Fractional']/following-sibling::input[@type='radio' and @value='fractional']");
	public static final By decimalOdd = By.xpath("//span[@class='title' and text()='Decimal']/following-sibling::input[@type='radio' and @value='decimal']");
	public static final By ContactUsBtn = By.xpath("//div[contains(@class,'icon-contact')]");
	public static final By contactUsContent = By.xpath("//contact-us[1]");
	public static final By contactUsStaticText = By.xpath("//div[@class='tooltip-header' and text()='Contact Us']");
	public static final By liveChatLinkHeader = By.xpath("//div[@class='tooltip-section-link-content']//div[text()='Go to Live Chat']");
	public static final By liveChatFooterLink = By.xpath("//div[@class='footer-links-list']//a[contains(text(),'Launch Live Chat')]");
	public static final By loginErrorpanel = By.xpath("//div[@class='login-error']/p");
	public static final By userBalance =By.xpath("//div[contains(@class, 'balance')]/following-sibling::div[@data-bind='currencyFormat: user.balance']");
	public static final By insuffFundsText = By.xpath("//li[@class='bet-error' and contains(text(), 'Please deposit additional funds to place this bet.')]");
	public static final By userNameAfterLogin = By.xpath("//div[contains(@class, 'user')]//following-sibling::div[starts-with(@class, 'name')]");
	public static final By logOutBtn = By.xpath("//div[@id='logoutSubmit']");
	public static final By forgotLoginDetails = By.xpath("//span[@class='forgot-login']//span");
	public static final By loginPassword = By.xpath("//input[@id='password']");
    public static final By forgotUsernameField = By.xpath("//input[@name='username']");
    public static final By forgotEmail = By.xpath("//input[@name='email']");
    public static final By successMessage = By.xpath("//div[@class='initial-success-message']");
    public static final By forgotPwdSubmitBtn = By.xpath("//button[@type='submit' and text()='Reset password']");
    public static final By forgotUsrSubmitBtn = By.xpath("//button[@type='submit' and text()='Retrieve username']");
    public static final By lostUNameMessage = By.xpath("//p[@class='message error']");
    public static final By dateDropDown = By.xpath("//select[@name='day']");
    public static final By monthDropDown = By.xpath("//select[@name='month']");
    public static final By yearDropDown = By.xpath("//select[@name='year']"); 
    public static final By forgottenUsernameTab = By.xpath("//div[@class='tab fn-tab-activate tab-username']");
    public static final By rememberMe = By.xpath("//span[text()= 'Remember me']/../input[@id='remember-me']");
    public static final By loggedInTime = By.xpath("//span[@class='icon-hourglass']/following-sibling::span[@data-bind='showLoggedInTime']");
    public static final By JoinNow = By.xpath("//*[text()= 'Join Now']");
    public static final By hideUserName = By.xpath("//div[@class='account-button hide-username' and contains(text(), 'Hide username')]");
    public static final By showUserName = By.xpath("//div[@class='account-button hide-username' and contains(text(), 'Show username')]");
    public static final By selfExclErrorMsg = By.xpath("//p[contains(text(),'Sorry - you are unable to log in as you have opted to self exclude from the Ladbrokes website and apps. You can call customer services on 0800 731 6191 to re-enable your account after your self exclusion period has expired.')]");
}
package com.ladbrokes.IrishOR;

import org.openqa.selenium.By;

public class HorseRacingElements extends com.ladbrokes.EnglishOR.HorseRacingElements{

	public static final By nextRaces = By.xpath("//span[contains(text(), 'Next Races')]");
	public static final By todayRaces = By.xpath("//span[contains(text(), 'Today')]");
	public static final By tomorrowsHorseraces = By.xpath("//div[@class='module']//span[contains(text(), 'Horse Racing Tomorrow')]");
	public static final By futureHorseRaces = By.xpath("//div[@class='module']//span[contains(text(), 'Future Racing')]");
	public static final By nextRacesModules = By.xpath("//div[@class='next-races']//div[@class='next-race']");
	public static final By EWbox = By.xpath("//div[@class='each-way-container']");
	public static final By FcTc_Tab = By.xpath("//div[@class='market-selector__item']/div[contains(text(),'Forecast / Tricast')]");
    public static final By checkbox = By.xpath("(//input[@type='checkbox' and @data-row ='0' and @data-position='1' and @data-col='0'])[1]");
    public static final By checkbox1 = By.xpath("(//input[@type='checkbox' and @data-row ='1' and @data-position='2' and @data-col='1'])[1]");
    public static final By checkbox2 = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='0' and @data-position='1' and  @data-row='0']");
    public static final By checkbox3 = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='1' and @data-position='2' and  @data-row='1']");
    public static final By checkbox4 = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='2' and @data-position='3' and  @data-row='2']");
    public static final By clearButton = By.xpath("//div[@class='buttons']//span[@class='forecastTricastClearButton button']");
    public static final By addToBetslipButton = By.xpath("//span[text()='Add to betslip']");
    public static final By bettingValue = By.xpath("//div[@class='container']//div[@class='info']//span[2]");
    public static final By greyhoundsHorse1 = By.xpath("//div[@class='item'][1]//span[@class='horse']//span[@class='name']");
    public static final By greyhoundsHorse2 = By.xpath("//div[@class='item'][2]//span[@class='horse']//span[@class='name']");
    public static final By greyhoundsHorse3 = By.xpath("//div[@class='item'][3]//span[@class='horse']//span[@class='name']");
    public static final By eventName = By.xpath("//div[contains(@class, 'wrap-time-name')]//span[@class='time']");
    public static final By raceName = By.xpath("//div[contains(@class, 'wrap-time-name')]//span[@class='name']");
    public static final By raceName1 = By.xpath("(//div[@class='next-races']//header[@class='next-race-header']//span[@class='next-race-header-title'])[1]");
    public static final By raceName2 = By.xpath("(//div[@class='next-races']//header[@class='next-race-header']//span[@class='next-race-header-title'])[2]");
    public static final By raceName3 = By.xpath("(//div[@class='next-races']//header[@class='next-race-header']//span[@class='next-race-header-title'])[3]");
    public static final By checkboxAny = By.xpath("(//input[@type='checkbox' and @data-row ='0' and @data-position='any' and @data-col='2'])[1]");
    public static final By checkboxAny1 = By.xpath("(//input[@type='checkbox' and @data-row ='1' and @data-position='any' and @data-col='2'])[1]");
    public static final By checkboxAny2 = By.xpath("(//input[@type='checkbox' and @data-row ='2' and @data-position='any' and @data-col='2'])[1]");
    public static final By checkboxTricastAny = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='3' and @data-position='any' and  @data-row='0']");
    public static final By checkboxTricastAny1 = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='3' and @data-position='any' and  @data-row='1']");
    public static final By checkboxTricastAny2 = By.xpath("//span[@class='tricast']//input[@data-type='tricast' and @type='checkbox' and @data-col='3' and @data-position='any' and  @data-row='2']");
    public static final By straightForecast = By.xpath("//div[@class='selection-names']//div[contains(@data-bind, 'text')]");
    public static final By otherMarketTab = By.xpath("//div[@class='title-container' and contains(text(),'Other Markets')]");
    public static final By trapWinner = By.xpath("//div[@class='text' and contains(text(),'Trap Winner')]");
    public static final By trap1 = By.xpath("//div[@class='item'][1]//span[@class='runner']//span[@class='horse']");
    public static final By SP = By.xpath("//div[@class='item'][1]//span[@class='selections']//div[@class='odds-button']//span[contains(text(), 'SP')]");
    public static final By forecastHorseOne = By.xpath("//div[@class='race-card FC_TC']//div[@class='runners']/div[1]//span//div//span[@class='name' and contains(@data-bind, 'text')]");
    public static final By forecastHorseTwo = By.xpath("//div[@class='race-card FC_TC']//div[@class='runners']/div[2]//span//div//span[@class='name' and contains(@data-bind, 'text')]");
    public static final By tricastHorseThree = By.xpath("//div[@class='race-card FC_TC']//div[@class='runners']/div[3]//span//div//span[@class='name' and contains(@data-bind, 'text')]");
    public static final By allCheckBox1 = By.xpath("//span[contains(text(), 'Today')]/../..//div[@class='racing-scheduled-meeting'][1]//div[@class='racing-scheduled-meeting-race-times-all']//input");
    public static final By buildNxt3RacesBtn = By.xpath("//a[@class='button action' and contains(text(),'BUILD NEXT 3 RACES')]");
    
}
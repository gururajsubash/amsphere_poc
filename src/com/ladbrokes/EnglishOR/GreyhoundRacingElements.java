package com.ladbrokes.EnglishOR;

import org.openqa.selenium.By;

public class GreyhoundRacingElements {

    public static final By GHresultedRaceFlagOnLadningPage = By.xpath("//span[@class='racing-scheduled-meeting-race-time finished']");
    public static final By GHresultedRaceFlagOnLeftMenu = By.xpath("//a[@class='meetings-link finished selected']");
}

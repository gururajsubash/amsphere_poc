package com.ladbrokes.EnglishOR;

import org.openqa.selenium.By;

public class VirtualSportsElements {

    public static final By eventTime = By.xpath("//li[@class='virtuals-event-selector__event']");
    public static final By oddsButton = By.xpath("//div[@class='odds-button']");
}

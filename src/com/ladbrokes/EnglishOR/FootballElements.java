package com.ladbrokes.EnglishOR;

import org.openqa.selenium.By;

public class FootballElements {
	public static final By couponLink = By.xpath("//div[contains(text(), 'Coupons')]");
    public static final By firstCouponLink = By.xpath("//div[@class='container coupon-sidebar']/ul/li[1]/a");
    public static final By couponOnLHN = By.xpath("//div[@class='container coupon-sidebar']");
    public static final By couponOnList = By.xpath("//div[@class='container coupon-sidebar']/ul/li/a");
    public static final By marketOnCoupon = By.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h2//span");
    public static final By eventsOnCoupon = By.xpath("//coupon-group//a[@class='coupon-list']");
    public static final By couponEvent = By.xpath("//coupon-group//a[@class='coupon-list']//span[@class='name']");
    public static final By coupon1stEvent1stSel = By.xpath("(//coupon-group//a[1]//descendant::div[@class='odds-button'])[1]");
    public static final By coupon1stEventName = By.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//a[1]//span[@class='name']");
    public static final By marketDropDown = By.xpath("(//select[contains(@data-bind,'marketToShow')])[2]");
    public static final By wdwRightColumn = By.xpath("//div[@class='right-column']//h2[@class='market-header']/following::span[@class='h-d-a']");
    public static final By typeName = By.xpath("//nav/ul//li[@class='breadcrumbs-item'][3]//a");
    public static final By SubTypeName = By.xpath("//nav/ul//li[@class='breadcrumbs-item'][4]//a");
    public static final By couponHomeSelName = By.xpath("//div[@class='left-column']//coupon-group[contains(@params, 'events')]//h3//span//span[1]");
    public static final By eventList = By.xpath("//div[@class='event-list pre']");
    public static final By showMoreEvents = By.xpath("//div[@class='show-more-link']//span[text()='Show more']");
    public static final By showLessEvents = By.xpath("//span[contains(text(), 'Show less')]");
    public static final By TimeFilterEvents = By.xpath("//div[@class='filter-button' and contains(text(), 'Time')]");
    public static final By todayFilter = By.xpath("//div[@class='module']//nav[@class='tabs']//li[contains(@class, 'tab')][1]");
    public static final By tomorrowFilter = By.xpath("//div[@class='module']//nav[@class='tabs']//li[contains(@class, 'tab')][2]");
    public static final By futureFootball = By.xpath(".//*[@id='content']/div/div/div/div[1]/div/div/tabs/nav/ul/li/div/span[contains(text(), 'Future ')]");
    public static final By toggleList = By.xpath("//div[@style='position: relative']//h2[contains(@class, 'expand-list expande')]");
    public static final By compEventList = By.xpath("//div[@style='position: relative']//div[starts-with(@class, 'event-group')]");
    public static final By upcomingEventName = By.xpath("//event-list[1]//div[@class='event-list pre'][1]//div[@class='event-list-details']//div[@class='name']");
    public static final By typeXpath =  By.xpath("(//a[@class='breadcrumbs-link'])[2]");
    public static final By more = By.xpath("//nav[@class='tabs']//li[@class='tab tabs-dropdown dropdown']");
    public static final By MoreTypes = By.xpath("//li[contains(@class,'tab tabs-dropdown dropdown')]//div[@class='more']//div[@class='tab']");
    public static final By moreSubTypes = By.xpath("//tabs//div[@class='links']//a");
    public static final By moreOption = By.xpath("//nav[@class='tabs']//div[@class='title-container' and contains(text(), 'More')]");
    public static final By moreOnOutrightwithType = By.xpath("//nav[@class='tabs']//div[@class='more']//span");
    public static final By EdpTitle = By.xpath("//div[@class='basic-scoreboard']//span[@class='name']");
    public static final By compEventName = By.xpath("(//div[@class='load-handler']//event-group-simple//h2[@class = 'expand-list expanded'])[1]/following::event-list[1]//div[@class='event-list pre'][1]//div[@class='event-list-details']//div[@class='name']");
    public static final By FootballPage = By.cssSelector("header.filter-header.Football");
    public static final By timeTab =  By.xpath("//div[@class='filter-button' and contains(text(), 'Time')]");
    public static final By competitionsTab =  By.xpath("//div[@class='filter-button' and contains(text(), 'Competitions')]");
    public static final By competitionsTabActive = By.xpath("//div[@class='filter-button active' and contains (text(), 'Competitions')]");
    public static final By timeTabActive =By.xpath("//div[@class='filter-button active' and contains(text(), 'Time')]");
    public static final By firstInplaySelection = By.xpath("(//div[@class='odds-button live'])[1]");
    public static final By inplayAndStreamingMenuButton = By.xpath("//div[@class='link' and text()='In-Play & Streaming']");
}

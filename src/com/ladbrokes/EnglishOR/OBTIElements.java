package com.ladbrokes.EnglishOR;

import org.openqa.selenium.By;

public class OBTIElements {

	public static final By username_TI = By.xpath("//input[@name='username']");
	public static final By password_TI = By.xpath("//input[@name='password']");
	public static final By login_TI = By.xpath("//input[@name='login']");
	public static final By BetProccessingSpinner = By.xpath("//span[@class='overask__spinner-header' and contains(text(),'You bet is being processed')]");
	public static final By BetProccessingText = By.xpath("//p[text()='Your bet is processing']");
	public static final By BetProccessingMessage = By.xpath("//span[text()='We are considering your bet. Please bear with us, this may take a few moments']");
	public static final By TiBetsTab = By.xpath("//li[@class='bet']");
	public static final By BiSearchtab = By.xpath("//div[@id='menu_bi_search']");
	public static final By BiResultsTab = By.xpath("//li[@id='bi_search_tabs_results']");
	public static final By BiRefreshBtn = By.xpath("//div[@class='content' and text()='Refresh']");
	public static final By BiRequestDropdown = By.xpath("//select[@class='adco_select']");
	public static final By BiDeclinePopup = By.xpath("//select[@id='bi_message_reasons']");
	public static final By BiDeclineApplyBtn = By.xpath("//div[@class='content' and text()='Apply']");
	public static final By BiSubmitBtn = By.xpath("//*[@id='bi_request_submit_btn']/div[2]");
	public static final By DeclineMsg = By.xpath("//li[@class='bet-error' and contains(text(),'Sorry, your bet has not been accepted by our traders')]");
	public static final By stakePerLine = By.xpath("//div[@class='stake_per_line']/input");
	public static final By close = By.xpath("//div[@class='content' and contains(text(),'Close')]");
    public static final By overaskHighlight = By.xpath("//span[@class='overask__highlight']");
    public static final By OAtotalPotentialReturns = By.xpath("//div[contains(text(), 'Total Potential Returns:')]/following-sibling::div[@class='overask__footer-pot-returns-value']");
    public static final By OAtotalStake = By.xpath("//div[contains(text(), 'Total Stake:')]/following-sibling::div[@class='overask__footer-stake-value']");
    public static final By OAplacebet = By.xpath("//a[@id='overAskPlaceButton']");
    public static final By OAheaderMsg = By.xpath("//div[@class='overask__header-message' and contains(text(),'Sorry, your bet has not been processed as requested. Please consider the following offer(s) instead:')]");
    public static final By OAheaderTimer = By.xpath("//div[@class='overask__header-timer-text' and contains(text(),'Offer expires:')]");
    public static final By OAtickMarkOnBetslip = By.xpath("//div[@id='betslip-container']/overask-offer/div/span[2]");
    public static final By betSuccessfulMsg = By.xpath( "//div[@id='betslip-container']/overask-offer/div[1]/span[1]");
    
 	public static final By office_TopBarFrame = By.name("officeMenu");
 	public static final By content_Pane = By.name("officePane");
 	public static final By LHS_frame = By.xpath("/html/frameset/frame[1]");
 	public static final By RHS_frame = By.xpath("/html/frameset/frame[2]");
 	
 	//'Campaign Manager' menu Elements
 	public static By CampMgr_Menu = By.xpath("//span[contains(text(),'Campaign Manager')]");
 	public static By offers_expand_option = By.xpath("//span[contains(text(),'Offers')]");
 	public static By adhoctokens = By.xpath("//a[text()='Adhoc Tokens']");
 	public static By username = By.xpath("//input[contains(@name,'Username')]");
 	public static By CampMgr_Search_btn = By.xpath("//input[@type='Submit']");
 	public static By GBP_value = By.xpath("//input[@name='Value']");
 	public static By redemption_value = By.xpath("//select[@name='RValID']");
 	public static By absoluteexp_value = By.xpath("//input[@name='AbsoluteEx']");
 	public static By rewardtoken_value = By.xpath("//input[@value='Reward token']");
 	public static By tokengranted_msg = By.xpath("//p[contains(@class,'1 / 1 tokens were succesfully granted')]");
 	
 	//'Admin' menu elements
 	public static By Admin_Menu = By.xpath("//span[contains(text(),'Admin')]");
 	public static final By Admin_RHS_frame = By.xpath("/html/frameset/frameset/frame[1]");
 	public static By BettingSetUp_expand_option = By.xpath("//span[contains(text(),'Betting Setup')]");
 	public static By events = By.xpath("//a[text()='Events']");
 	public static By searchByID_txtbox = By.xpath("//input[@id='searchform_id']");
 	public static By Admin_Search_btn = By.xpath("//input[@id='searchform_search']");
 	public static By Status_drpdwn = By.xpath("//select[@name='MktStatus']");
 	public static By Result_drpdwn = By.xpath("//select[@name='OcResult']");
 	public static By setSelectionResult_btn = By.xpath("//input[@value='Set Selection Result']");
 	public static By modifyMarket_btn = By.xpath("//input[@name='mod_mkt']");
 	public static By updateEvent_btn = By.xpath("//input[@value='Update Event']");
}

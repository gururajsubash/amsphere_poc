package com.ladbrokes.EnglishOR;

import org.openqa.selenium.By;

public class SegmentedBannerElements {
	
	public static final By promotionsTab = By.xpath("//div[@class='link' and text() = 'Promotions']");
	public static final By NewCustomerOffer = By.xpath("//h2[text()='AHB New Customer Offer – McGregor to win 33/1']");

}
